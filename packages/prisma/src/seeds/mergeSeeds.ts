import * as mergeFiles from 'merge-files';

const pathPrefix = __dirname;

const seedFiles = [
	'/users.graphql',
];
const outputPath = `${pathPrefix}/merged-seeds.graphql`;

const merge = async ()=> {
	const paths = seedFiles.map(path => pathPrefix + path);
	return mergeFiles(paths, outputPath)
};

merge().then((status)=>{
	if (status){
		console.log('Files merged:');
		seedFiles.forEach((path)=>{
			console.log(`- ${path}`);
		});
		console.log(`to: ${outputPath}`);
	}
}).catch(err => console.error(err));


