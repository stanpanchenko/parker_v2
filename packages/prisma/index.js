import * as prismaTypes from './prisma.interfaces';
import { importSchema } from 'graphql-import';

const prismaSchemas = importSchema(
	__dirname + '/prisma-schemas.graphql'
);

export {prismaSchemas, prismaTypes}
