import { GraphQLResolveInfo, GraphQLSchema } from 'graphql'
import { IResolvers } from 'graphql-tools/dist/Interfaces'
import { Options } from 'graphql-binding'
import { makePrismaBindingClass, BasePrismaOptions } from 'prisma-binding'

export interface Query {
    features: <T = Array<Feature | null>>(args: { where?: FeatureWhereInput | null, orderBy?: FeatureOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    pricePeriods: <T = Array<PricePeriod | null>>(args: { where?: PricePeriodWhereInput | null, orderBy?: PricePeriodOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    properties: <T = Array<Property | null>>(args: { where?: PropertyWhereInput | null, orderBy?: PropertyOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    geometries: <T = Array<Geometry | null>>(args: { where?: GeometryWhereInput | null, orderBy?: GeometryOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    addresses: <T = Array<Address | null>>(args: { where?: AddressWhereInput | null, orderBy?: AddressOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    points: <T = Array<Point | null>>(args: { where?: PointWhereInput | null, orderBy?: PointOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygonLineCoordinates: <T = Array<PolygonLineCoordinate | null>>(args: { where?: PolygonLineCoordinateWhereInput | null, orderBy?: PolygonLineCoordinateOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tagKeys: <T = Array<TagKey | null>>(args: { where?: TagKeyWhereInput | null, orderBy?: TagKeyOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    lineStrings: <T = Array<LineString | null>>(args: { where?: LineStringWhereInput | null, orderBy?: LineStringOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tagValues: <T = Array<TagValue | null>>(args: { where?: TagValueWhereInput | null, orderBy?: TagValueOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygonLines: <T = Array<PolygonLine | null>>(args: { where?: PolygonLineWhereInput | null, orderBy?: PolygonLineOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    lineStringCoordinates: <T = Array<LineStringCoordinate | null>>(args: { where?: LineStringCoordinateWhereInput | null, orderBy?: LineStringCoordinateOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    users: <T = Array<User | null>>(args: { where?: UserWhereInput | null, orderBy?: UserOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    propertyChangeSets: <T = Array<PropertyChangeSet | null>>(args: { where?: PropertyChangeSetWhereInput | null, orderBy?: PropertyChangeSetOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tags: <T = Array<Tag | null>>(args: { where?: TagWhereInput | null, orderBy?: TagOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygons: <T = Array<Polygon | null>>(args: { where?: PolygonWhereInput | null, orderBy?: PolygonOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    feature: <T = Feature | null>(args: { where: FeatureWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    pricePeriod: <T = PricePeriod | null>(args: { where: PricePeriodWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    property: <T = Property | null>(args: { where: PropertyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    geometry: <T = Geometry | null>(args: { where: GeometryWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    address: <T = Address | null>(args: { where: AddressWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    point: <T = Point | null>(args: { where: PointWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    polygonLineCoordinate: <T = PolygonLineCoordinate | null>(args: { where: PolygonLineCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    tagKey: <T = TagKey | null>(args: { where: TagKeyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    lineString: <T = LineString | null>(args: { where: LineStringWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    tagValue: <T = TagValue | null>(args: { where: TagValueWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    polygonLine: <T = PolygonLine | null>(args: { where: PolygonLineWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    lineStringCoordinate: <T = LineStringCoordinate | null>(args: { where: LineStringCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    user: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    propertyChangeSet: <T = PropertyChangeSet | null>(args: { where: PropertyChangeSetWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    tag: <T = Tag | null>(args: { where: TagWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    polygon: <T = Polygon | null>(args: { where: PolygonWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    featuresConnection: <T = FeatureConnection>(args: { where?: FeatureWhereInput | null, orderBy?: FeatureOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    pricePeriodsConnection: <T = PricePeriodConnection>(args: { where?: PricePeriodWhereInput | null, orderBy?: PricePeriodOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    propertiesConnection: <T = PropertyConnection>(args: { where?: PropertyWhereInput | null, orderBy?: PropertyOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    geometriesConnection: <T = GeometryConnection>(args: { where?: GeometryWhereInput | null, orderBy?: GeometryOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    addressesConnection: <T = AddressConnection>(args: { where?: AddressWhereInput | null, orderBy?: AddressOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    pointsConnection: <T = PointConnection>(args: { where?: PointWhereInput | null, orderBy?: PointOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygonLineCoordinatesConnection: <T = PolygonLineCoordinateConnection>(args: { where?: PolygonLineCoordinateWhereInput | null, orderBy?: PolygonLineCoordinateOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tagKeysConnection: <T = TagKeyConnection>(args: { where?: TagKeyWhereInput | null, orderBy?: TagKeyOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    lineStringsConnection: <T = LineStringConnection>(args: { where?: LineStringWhereInput | null, orderBy?: LineStringOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tagValuesConnection: <T = TagValueConnection>(args: { where?: TagValueWhereInput | null, orderBy?: TagValueOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygonLinesConnection: <T = PolygonLineConnection>(args: { where?: PolygonLineWhereInput | null, orderBy?: PolygonLineOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    lineStringCoordinatesConnection: <T = LineStringCoordinateConnection>(args: { where?: LineStringCoordinateWhereInput | null, orderBy?: LineStringCoordinateOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    usersConnection: <T = UserConnection>(args: { where?: UserWhereInput | null, orderBy?: UserOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    propertyChangeSetsConnection: <T = PropertyChangeSetConnection>(args: { where?: PropertyChangeSetWhereInput | null, orderBy?: PropertyChangeSetOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    tagsConnection: <T = TagConnection>(args: { where?: TagWhereInput | null, orderBy?: TagOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    polygonsConnection: <T = PolygonConnection>(args: { where?: PolygonWhereInput | null, orderBy?: PolygonOrderByInput | null, skip?: Int | null, after?: String | null, before?: String | null, first?: Int | null, last?: Int | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    node: <T = Node | null>(args: { id: ID_Output }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> 
  }

export interface Mutation {
    createFeature: <T = Feature>(args: { data: FeatureCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPricePeriod: <T = PricePeriod>(args: { data: PricePeriodCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createProperty: <T = Property>(args: { data: PropertyCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createGeometry: <T = Geometry>(args: { data: GeometryCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createAddress: <T = Address>(args: { data: AddressCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPoint: <T = Point>(args: { data: PointCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPolygonLineCoordinate: <T = PolygonLineCoordinate>(args: { data: PolygonLineCoordinateCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createTagKey: <T = TagKey>(args: { data: TagKeyCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createLineString: <T = LineString>(args: { data: LineStringCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createTagValue: <T = TagValue>(args: { data: TagValueCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPolygonLine: <T = PolygonLine>(args: { data: PolygonLineCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createLineStringCoordinate: <T = LineStringCoordinate>(args: { data: LineStringCoordinateCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createUser: <T = User>(args: { data: UserCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPropertyChangeSet: <T = PropertyChangeSet>(args: { data: PropertyChangeSetCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createTag: <T = Tag>(args: { data: TagCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    createPolygon: <T = Polygon>(args: { data: PolygonCreateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateFeature: <T = Feature | null>(args: { data: FeatureUpdateInput, where: FeatureWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePricePeriod: <T = PricePeriod | null>(args: { data: PricePeriodUpdateInput, where: PricePeriodWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateProperty: <T = Property | null>(args: { data: PropertyUpdateInput, where: PropertyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateGeometry: <T = Geometry | null>(args: { data: GeometryUpdateInput, where: GeometryWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateAddress: <T = Address | null>(args: { data: AddressUpdateInput, where: AddressWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePoint: <T = Point | null>(args: { data: PointUpdateInput, where: PointWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePolygonLineCoordinate: <T = PolygonLineCoordinate | null>(args: { data: PolygonLineCoordinateUpdateInput, where: PolygonLineCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateTagKey: <T = TagKey | null>(args: { data: TagKeyUpdateInput, where: TagKeyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateLineString: <T = LineString | null>(args: { data: LineStringUpdateInput, where: LineStringWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateTagValue: <T = TagValue | null>(args: { data: TagValueUpdateInput, where: TagValueWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePolygonLine: <T = PolygonLine | null>(args: { data: PolygonLineUpdateInput, where: PolygonLineWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateLineStringCoordinate: <T = LineStringCoordinate | null>(args: { data: LineStringCoordinateUpdateInput, where: LineStringCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateUser: <T = User | null>(args: { data: UserUpdateInput, where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePropertyChangeSet: <T = PropertyChangeSet | null>(args: { data: PropertyChangeSetUpdateInput, where: PropertyChangeSetWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updateTag: <T = Tag | null>(args: { data: TagUpdateInput, where: TagWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    updatePolygon: <T = Polygon | null>(args: { data: PolygonUpdateInput, where: PolygonWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteFeature: <T = Feature | null>(args: { where: FeatureWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePricePeriod: <T = PricePeriod | null>(args: { where: PricePeriodWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteProperty: <T = Property | null>(args: { where: PropertyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteGeometry: <T = Geometry | null>(args: { where: GeometryWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteAddress: <T = Address | null>(args: { where: AddressWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePoint: <T = Point | null>(args: { where: PointWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePolygonLineCoordinate: <T = PolygonLineCoordinate | null>(args: { where: PolygonLineCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteTagKey: <T = TagKey | null>(args: { where: TagKeyWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteLineString: <T = LineString | null>(args: { where: LineStringWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteTagValue: <T = TagValue | null>(args: { where: TagValueWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePolygonLine: <T = PolygonLine | null>(args: { where: PolygonLineWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteLineStringCoordinate: <T = LineStringCoordinate | null>(args: { where: LineStringCoordinateWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteUser: <T = User | null>(args: { where: UserWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePropertyChangeSet: <T = PropertyChangeSet | null>(args: { where: PropertyChangeSetWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deleteTag: <T = Tag | null>(args: { where: TagWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    deletePolygon: <T = Polygon | null>(args: { where: PolygonWhereUniqueInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T | null> ,
    upsertFeature: <T = Feature>(args: { where: FeatureWhereUniqueInput, create: FeatureCreateInput, update: FeatureUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPricePeriod: <T = PricePeriod>(args: { where: PricePeriodWhereUniqueInput, create: PricePeriodCreateInput, update: PricePeriodUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertProperty: <T = Property>(args: { where: PropertyWhereUniqueInput, create: PropertyCreateInput, update: PropertyUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertGeometry: <T = Geometry>(args: { where: GeometryWhereUniqueInput, create: GeometryCreateInput, update: GeometryUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertAddress: <T = Address>(args: { where: AddressWhereUniqueInput, create: AddressCreateInput, update: AddressUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPoint: <T = Point>(args: { where: PointWhereUniqueInput, create: PointCreateInput, update: PointUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPolygonLineCoordinate: <T = PolygonLineCoordinate>(args: { where: PolygonLineCoordinateWhereUniqueInput, create: PolygonLineCoordinateCreateInput, update: PolygonLineCoordinateUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertTagKey: <T = TagKey>(args: { where: TagKeyWhereUniqueInput, create: TagKeyCreateInput, update: TagKeyUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertLineString: <T = LineString>(args: { where: LineStringWhereUniqueInput, create: LineStringCreateInput, update: LineStringUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertTagValue: <T = TagValue>(args: { where: TagValueWhereUniqueInput, create: TagValueCreateInput, update: TagValueUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPolygonLine: <T = PolygonLine>(args: { where: PolygonLineWhereUniqueInput, create: PolygonLineCreateInput, update: PolygonLineUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertLineStringCoordinate: <T = LineStringCoordinate>(args: { where: LineStringCoordinateWhereUniqueInput, create: LineStringCoordinateCreateInput, update: LineStringCoordinateUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertUser: <T = User>(args: { where: UserWhereUniqueInput, create: UserCreateInput, update: UserUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPropertyChangeSet: <T = PropertyChangeSet>(args: { where: PropertyChangeSetWhereUniqueInput, create: PropertyChangeSetCreateInput, update: PropertyChangeSetUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertTag: <T = Tag>(args: { where: TagWhereUniqueInput, create: TagCreateInput, update: TagUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    upsertPolygon: <T = Polygon>(args: { where: PolygonWhereUniqueInput, create: PolygonCreateInput, update: PolygonUpdateInput }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyFeatures: <T = BatchPayload>(args: { data: FeatureUpdateManyMutationInput, where?: FeatureWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyPricePeriods: <T = BatchPayload>(args: { data: PricePeriodUpdateManyMutationInput, where?: PricePeriodWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyGeometries: <T = BatchPayload>(args: { data: GeometryUpdateManyMutationInput, where?: GeometryWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyAddresses: <T = BatchPayload>(args: { data: AddressUpdateManyMutationInput, where?: AddressWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyPoints: <T = BatchPayload>(args: { data: PointUpdateManyMutationInput, where?: PointWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyPolygonLineCoordinates: <T = BatchPayload>(args: { data: PolygonLineCoordinateUpdateManyMutationInput, where?: PolygonLineCoordinateWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyTagKeys: <T = BatchPayload>(args: { data: TagKeyUpdateManyMutationInput, where?: TagKeyWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyTagValues: <T = BatchPayload>(args: { data: TagValueUpdateManyMutationInput, where?: TagValueWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyLineStringCoordinates: <T = BatchPayload>(args: { data: LineStringCoordinateUpdateManyMutationInput, where?: LineStringCoordinateWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyUsers: <T = BatchPayload>(args: { data: UserUpdateManyMutationInput, where?: UserWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    updateManyPropertyChangeSets: <T = BatchPayload>(args: { data: PropertyChangeSetUpdateManyMutationInput, where?: PropertyChangeSetWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyFeatures: <T = BatchPayload>(args: { where?: FeatureWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPricePeriods: <T = BatchPayload>(args: { where?: PricePeriodWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyProperties: <T = BatchPayload>(args: { where?: PropertyWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyGeometries: <T = BatchPayload>(args: { where?: GeometryWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyAddresses: <T = BatchPayload>(args: { where?: AddressWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPoints: <T = BatchPayload>(args: { where?: PointWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPolygonLineCoordinates: <T = BatchPayload>(args: { where?: PolygonLineCoordinateWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyTagKeys: <T = BatchPayload>(args: { where?: TagKeyWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyLineStrings: <T = BatchPayload>(args: { where?: LineStringWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyTagValues: <T = BatchPayload>(args: { where?: TagValueWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPolygonLines: <T = BatchPayload>(args: { where?: PolygonLineWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyLineStringCoordinates: <T = BatchPayload>(args: { where?: LineStringCoordinateWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyUsers: <T = BatchPayload>(args: { where?: UserWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPropertyChangeSets: <T = BatchPayload>(args: { where?: PropertyChangeSetWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyTags: <T = BatchPayload>(args: { where?: TagWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> ,
    deleteManyPolygons: <T = BatchPayload>(args: { where?: PolygonWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<T> 
  }

export interface Subscription {
    feature: <T = FeatureSubscriptionPayload | null>(args: { where?: FeatureSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    pricePeriod: <T = PricePeriodSubscriptionPayload | null>(args: { where?: PricePeriodSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    property: <T = PropertySubscriptionPayload | null>(args: { where?: PropertySubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    geometry: <T = GeometrySubscriptionPayload | null>(args: { where?: GeometrySubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    address: <T = AddressSubscriptionPayload | null>(args: { where?: AddressSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    point: <T = PointSubscriptionPayload | null>(args: { where?: PointSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    polygonLineCoordinate: <T = PolygonLineCoordinateSubscriptionPayload | null>(args: { where?: PolygonLineCoordinateSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    tagKey: <T = TagKeySubscriptionPayload | null>(args: { where?: TagKeySubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    lineString: <T = LineStringSubscriptionPayload | null>(args: { where?: LineStringSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    tagValue: <T = TagValueSubscriptionPayload | null>(args: { where?: TagValueSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    polygonLine: <T = PolygonLineSubscriptionPayload | null>(args: { where?: PolygonLineSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    lineStringCoordinate: <T = LineStringCoordinateSubscriptionPayload | null>(args: { where?: LineStringCoordinateSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    user: <T = UserSubscriptionPayload | null>(args: { where?: UserSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    propertyChangeSet: <T = PropertyChangeSetSubscriptionPayload | null>(args: { where?: PropertyChangeSetSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    tag: <T = TagSubscriptionPayload | null>(args: { where?: TagSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> ,
    polygon: <T = PolygonSubscriptionPayload | null>(args: { where?: PolygonSubscriptionWhereInput | null }, info?: GraphQLResolveInfo | string, options?: Options) => Promise<AsyncIterator<T | null>> 
  }

export interface Exists {
  Feature: (where?: FeatureWhereInput) => Promise<boolean>
  PricePeriod: (where?: PricePeriodWhereInput) => Promise<boolean>
  Property: (where?: PropertyWhereInput) => Promise<boolean>
  Geometry: (where?: GeometryWhereInput) => Promise<boolean>
  Address: (where?: AddressWhereInput) => Promise<boolean>
  Point: (where?: PointWhereInput) => Promise<boolean>
  PolygonLineCoordinate: (where?: PolygonLineCoordinateWhereInput) => Promise<boolean>
  TagKey: (where?: TagKeyWhereInput) => Promise<boolean>
  LineString: (where?: LineStringWhereInput) => Promise<boolean>
  TagValue: (where?: TagValueWhereInput) => Promise<boolean>
  PolygonLine: (where?: PolygonLineWhereInput) => Promise<boolean>
  LineStringCoordinate: (where?: LineStringCoordinateWhereInput) => Promise<boolean>
  User: (where?: UserWhereInput) => Promise<boolean>
  PropertyChangeSet: (where?: PropertyChangeSetWhereInput) => Promise<boolean>
  Tag: (where?: TagWhereInput) => Promise<boolean>
  Polygon: (where?: PolygonWhereInput) => Promise<boolean>
}

export interface Prisma {
  query: Query
  mutation: Mutation
  subscription: Subscription
  exists: Exists
  request: <T = any>(query: string, variables?: {[key: string]: any}) => Promise<T>
  delegate(operation: 'query' | 'mutation', fieldName: string, args: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<any>;
delegateSubscription(fieldName: string, args?: {
    [key: string]: any;
}, infoOrQuery?: GraphQLResolveInfo | string, options?: Options): Promise<AsyncIterator<any>>;
getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers;
}

export interface BindingConstructor<T> {
  new(options: BasePrismaOptions): T
}
/**
 * Type Defs
*/

const typeDefs = `type Address implements Node {
  id: ID!
  uuid: String!
  street: String!
  houseNumber: String!
  city: String!
  countryCode: CountryCode!
  lng: Float!
  lat: Float!
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

"""A connection to a list of items."""
type AddressConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [AddressEdge]!
  aggregate: AggregateAddress!
}

input AddressCreateInput {
  id: ID
  uuid: String!
  street: String!
  houseNumber: String!
  city: String!
  countryCode: CountryCode!
  lng: Float!
  lat: Float!
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

input AddressCreateOneInput {
  create: AddressCreateInput
  connect: AddressWhereUniqueInput
}

"""An edge in a connection."""
type AddressEdge {
  """The item at the end of the edge."""
  node: Address!

  """A cursor for use in pagination."""
  cursor: String!
}

enum AddressOrderByInput {
  id_ASC
  id_DESC
  uuid_ASC
  uuid_DESC
  street_ASC
  street_DESC
  houseNumber_ASC
  houseNumber_DESC
  city_ASC
  city_DESC
  countryCode_ASC
  countryCode_DESC
  lng_ASC
  lng_DESC
  lat_ASC
  lat_DESC
  cityDistrict_ASC
  cityDistrict_DESC
  continent_ASC
  continent_DESC
  neighbourhood_ASC
  neighbourhood_DESC
  postcode_ASC
  postcode_DESC
  state_ASC
  state_DESC
  suburb_ASC
  suburb_DESC
  road_ASC
  road_DESC
  importance_ASC
  importance_DESC
}

type AddressPreviousValues {
  id: ID!
  uuid: String!
  street: String!
  houseNumber: String!
  city: String!
  countryCode: CountryCode!
  lng: Float!
  lat: Float!
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

type AddressSubscriptionPayload {
  mutation: MutationType!
  node: Address
  updatedFields: [String!]
  previousValues: AddressPreviousValues
}

input AddressSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [AddressSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [AddressSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AddressSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: AddressWhereInput
}

input AddressUpdateDataInput {
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

input AddressUpdateInput {
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

input AddressUpdateManyMutationInput {
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict: String
  continent: String
  neighbourhood: String
  postcode: Int
  state: String
  suburb: String
  road: String
  importance: Float
}

input AddressUpdateOneInput {
  create: AddressCreateInput
  connect: AddressWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: AddressUpdateDataInput
  upsert: AddressUpsertNestedInput
}

input AddressUpsertNestedInput {
  update: AddressUpdateDataInput!
  create: AddressCreateInput!
}

input AddressWhereInput {
  """Logical AND on all given filters."""
  AND: [AddressWhereInput!]

  """Logical OR on all given filters."""
  OR: [AddressWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AddressWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  uuid: String

  """All values that are not equal to given value."""
  uuid_not: String

  """All values that are contained in given list."""
  uuid_in: [String!]

  """All values that are not contained in given list."""
  uuid_not_in: [String!]

  """All values less than the given value."""
  uuid_lt: String

  """All values less than or equal the given value."""
  uuid_lte: String

  """All values greater than the given value."""
  uuid_gt: String

  """All values greater than or equal the given value."""
  uuid_gte: String

  """All values containing the given string."""
  uuid_contains: String

  """All values not containing the given string."""
  uuid_not_contains: String

  """All values starting with the given string."""
  uuid_starts_with: String

  """All values not starting with the given string."""
  uuid_not_starts_with: String

  """All values ending with the given string."""
  uuid_ends_with: String

  """All values not ending with the given string."""
  uuid_not_ends_with: String
  street: String

  """All values that are not equal to given value."""
  street_not: String

  """All values that are contained in given list."""
  street_in: [String!]

  """All values that are not contained in given list."""
  street_not_in: [String!]

  """All values less than the given value."""
  street_lt: String

  """All values less than or equal the given value."""
  street_lte: String

  """All values greater than the given value."""
  street_gt: String

  """All values greater than or equal the given value."""
  street_gte: String

  """All values containing the given string."""
  street_contains: String

  """All values not containing the given string."""
  street_not_contains: String

  """All values starting with the given string."""
  street_starts_with: String

  """All values not starting with the given string."""
  street_not_starts_with: String

  """All values ending with the given string."""
  street_ends_with: String

  """All values not ending with the given string."""
  street_not_ends_with: String
  houseNumber: String

  """All values that are not equal to given value."""
  houseNumber_not: String

  """All values that are contained in given list."""
  houseNumber_in: [String!]

  """All values that are not contained in given list."""
  houseNumber_not_in: [String!]

  """All values less than the given value."""
  houseNumber_lt: String

  """All values less than or equal the given value."""
  houseNumber_lte: String

  """All values greater than the given value."""
  houseNumber_gt: String

  """All values greater than or equal the given value."""
  houseNumber_gte: String

  """All values containing the given string."""
  houseNumber_contains: String

  """All values not containing the given string."""
  houseNumber_not_contains: String

  """All values starting with the given string."""
  houseNumber_starts_with: String

  """All values not starting with the given string."""
  houseNumber_not_starts_with: String

  """All values ending with the given string."""
  houseNumber_ends_with: String

  """All values not ending with the given string."""
  houseNumber_not_ends_with: String
  city: String

  """All values that are not equal to given value."""
  city_not: String

  """All values that are contained in given list."""
  city_in: [String!]

  """All values that are not contained in given list."""
  city_not_in: [String!]

  """All values less than the given value."""
  city_lt: String

  """All values less than or equal the given value."""
  city_lte: String

  """All values greater than the given value."""
  city_gt: String

  """All values greater than or equal the given value."""
  city_gte: String

  """All values containing the given string."""
  city_contains: String

  """All values not containing the given string."""
  city_not_contains: String

  """All values starting with the given string."""
  city_starts_with: String

  """All values not starting with the given string."""
  city_not_starts_with: String

  """All values ending with the given string."""
  city_ends_with: String

  """All values not ending with the given string."""
  city_not_ends_with: String
  countryCode: CountryCode

  """All values that are not equal to given value."""
  countryCode_not: CountryCode

  """All values that are contained in given list."""
  countryCode_in: [CountryCode!]

  """All values that are not contained in given list."""
  countryCode_not_in: [CountryCode!]
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
  cityDistrict: String

  """All values that are not equal to given value."""
  cityDistrict_not: String

  """All values that are contained in given list."""
  cityDistrict_in: [String!]

  """All values that are not contained in given list."""
  cityDistrict_not_in: [String!]

  """All values less than the given value."""
  cityDistrict_lt: String

  """All values less than or equal the given value."""
  cityDistrict_lte: String

  """All values greater than the given value."""
  cityDistrict_gt: String

  """All values greater than or equal the given value."""
  cityDistrict_gte: String

  """All values containing the given string."""
  cityDistrict_contains: String

  """All values not containing the given string."""
  cityDistrict_not_contains: String

  """All values starting with the given string."""
  cityDistrict_starts_with: String

  """All values not starting with the given string."""
  cityDistrict_not_starts_with: String

  """All values ending with the given string."""
  cityDistrict_ends_with: String

  """All values not ending with the given string."""
  cityDistrict_not_ends_with: String
  continent: String

  """All values that are not equal to given value."""
  continent_not: String

  """All values that are contained in given list."""
  continent_in: [String!]

  """All values that are not contained in given list."""
  continent_not_in: [String!]

  """All values less than the given value."""
  continent_lt: String

  """All values less than or equal the given value."""
  continent_lte: String

  """All values greater than the given value."""
  continent_gt: String

  """All values greater than or equal the given value."""
  continent_gte: String

  """All values containing the given string."""
  continent_contains: String

  """All values not containing the given string."""
  continent_not_contains: String

  """All values starting with the given string."""
  continent_starts_with: String

  """All values not starting with the given string."""
  continent_not_starts_with: String

  """All values ending with the given string."""
  continent_ends_with: String

  """All values not ending with the given string."""
  continent_not_ends_with: String
  neighbourhood: String

  """All values that are not equal to given value."""
  neighbourhood_not: String

  """All values that are contained in given list."""
  neighbourhood_in: [String!]

  """All values that are not contained in given list."""
  neighbourhood_not_in: [String!]

  """All values less than the given value."""
  neighbourhood_lt: String

  """All values less than or equal the given value."""
  neighbourhood_lte: String

  """All values greater than the given value."""
  neighbourhood_gt: String

  """All values greater than or equal the given value."""
  neighbourhood_gte: String

  """All values containing the given string."""
  neighbourhood_contains: String

  """All values not containing the given string."""
  neighbourhood_not_contains: String

  """All values starting with the given string."""
  neighbourhood_starts_with: String

  """All values not starting with the given string."""
  neighbourhood_not_starts_with: String

  """All values ending with the given string."""
  neighbourhood_ends_with: String

  """All values not ending with the given string."""
  neighbourhood_not_ends_with: String
  postcode: Int

  """All values that are not equal to given value."""
  postcode_not: Int

  """All values that are contained in given list."""
  postcode_in: [Int!]

  """All values that are not contained in given list."""
  postcode_not_in: [Int!]

  """All values less than the given value."""
  postcode_lt: Int

  """All values less than or equal the given value."""
  postcode_lte: Int

  """All values greater than the given value."""
  postcode_gt: Int

  """All values greater than or equal the given value."""
  postcode_gte: Int
  state: String

  """All values that are not equal to given value."""
  state_not: String

  """All values that are contained in given list."""
  state_in: [String!]

  """All values that are not contained in given list."""
  state_not_in: [String!]

  """All values less than the given value."""
  state_lt: String

  """All values less than or equal the given value."""
  state_lte: String

  """All values greater than the given value."""
  state_gt: String

  """All values greater than or equal the given value."""
  state_gte: String

  """All values containing the given string."""
  state_contains: String

  """All values not containing the given string."""
  state_not_contains: String

  """All values starting with the given string."""
  state_starts_with: String

  """All values not starting with the given string."""
  state_not_starts_with: String

  """All values ending with the given string."""
  state_ends_with: String

  """All values not ending with the given string."""
  state_not_ends_with: String
  suburb: String

  """All values that are not equal to given value."""
  suburb_not: String

  """All values that are contained in given list."""
  suburb_in: [String!]

  """All values that are not contained in given list."""
  suburb_not_in: [String!]

  """All values less than the given value."""
  suburb_lt: String

  """All values less than or equal the given value."""
  suburb_lte: String

  """All values greater than the given value."""
  suburb_gt: String

  """All values greater than or equal the given value."""
  suburb_gte: String

  """All values containing the given string."""
  suburb_contains: String

  """All values not containing the given string."""
  suburb_not_contains: String

  """All values starting with the given string."""
  suburb_starts_with: String

  """All values not starting with the given string."""
  suburb_not_starts_with: String

  """All values ending with the given string."""
  suburb_ends_with: String

  """All values not ending with the given string."""
  suburb_not_ends_with: String
  road: String

  """All values that are not equal to given value."""
  road_not: String

  """All values that are contained in given list."""
  road_in: [String!]

  """All values that are not contained in given list."""
  road_not_in: [String!]

  """All values less than the given value."""
  road_lt: String

  """All values less than or equal the given value."""
  road_lte: String

  """All values greater than the given value."""
  road_gt: String

  """All values greater than or equal the given value."""
  road_gte: String

  """All values containing the given string."""
  road_contains: String

  """All values not containing the given string."""
  road_not_contains: String

  """All values starting with the given string."""
  road_starts_with: String

  """All values not starting with the given string."""
  road_not_starts_with: String

  """All values ending with the given string."""
  road_ends_with: String

  """All values not ending with the given string."""
  road_not_ends_with: String
  importance: Float

  """All values that are not equal to given value."""
  importance_not: Float

  """All values that are contained in given list."""
  importance_in: [Float!]

  """All values that are not contained in given list."""
  importance_not_in: [Float!]

  """All values less than the given value."""
  importance_lt: Float

  """All values less than or equal the given value."""
  importance_lte: Float

  """All values greater than the given value."""
  importance_gt: Float

  """All values greater than or equal the given value."""
  importance_gte: Float
}

input AddressWhereUniqueInput {
  id: ID
  uuid: String
}

type AggregateAddress {
  count: Int!
}

type AggregateFeature {
  count: Int!
}

type AggregateGeometry {
  count: Int!
}

type AggregateLineString {
  count: Int!
}

type AggregateLineStringCoordinate {
  count: Int!
}

type AggregatePoint {
  count: Int!
}

type AggregatePolygon {
  count: Int!
}

type AggregatePolygonLine {
  count: Int!
}

type AggregatePolygonLineCoordinate {
  count: Int!
}

type AggregatePricePeriod {
  count: Int!
}

type AggregateProperty {
  count: Int!
}

type AggregatePropertyChangeSet {
  count: Int!
}

type AggregateTag {
  count: Int!
}

type AggregateTagKey {
  count: Int!
}

type AggregateTagValue {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

enum CountryCode {
  DE
  US
}

scalar DateTime

type Feature implements Node {
  id: ID!
  type: FeatureType!
  geometry: Geometry!
  properties: Property!
  createdAt: DateTime!
  updatedAt: DateTime!
}

"""A connection to a list of items."""
type FeatureConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [FeatureEdge]!
  aggregate: AggregateFeature!
}

input FeatureCreateInput {
  id: ID
  type: FeatureType!
  geometry: GeometryCreateOneInput!
  properties: PropertyCreateOneInput!
}

"""An edge in a connection."""
type FeatureEdge {
  """The item at the end of the edge."""
  node: Feature!

  """A cursor for use in pagination."""
  cursor: String!
}

enum FeatureOrderByInput {
  id_ASC
  id_DESC
  type_ASC
  type_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type FeaturePreviousValues {
  id: ID!
  type: FeatureType!
  createdAt: DateTime!
  updatedAt: DateTime!
}

type FeatureSubscriptionPayload {
  mutation: MutationType!
  node: Feature
  updatedFields: [String!]
  previousValues: FeaturePreviousValues
}

input FeatureSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [FeatureSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [FeatureSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FeatureSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: FeatureWhereInput
}

enum FeatureType {
  FeatureCollection
  Feature
}

input FeatureUpdateInput {
  type: FeatureType
  geometry: GeometryUpdateOneRequiredInput
  properties: PropertyUpdateOneRequiredInput
}

input FeatureUpdateManyMutationInput {
  type: FeatureType
}

input FeatureWhereInput {
  """Logical AND on all given filters."""
  AND: [FeatureWhereInput!]

  """Logical OR on all given filters."""
  OR: [FeatureWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [FeatureWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  type: FeatureType

  """All values that are not equal to given value."""
  type_not: FeatureType

  """All values that are contained in given list."""
  type_in: [FeatureType!]

  """All values that are not contained in given list."""
  type_not_in: [FeatureType!]
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  geometry: GeometryWhereInput
  properties: PropertyWhereInput
}

input FeatureWhereUniqueInput {
  id: ID
}

type Geometry implements Node {
  id: ID!
  type: GeometryType!
  lineString: LineString
  point: Point
  polygon: Polygon
}

"""A connection to a list of items."""
type GeometryConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [GeometryEdge]!
  aggregate: AggregateGeometry!
}

input GeometryCreateInput {
  id: ID
  type: GeometryType!
  lineString: LineStringCreateOneInput
  point: PointCreateOneInput
  polygon: PolygonCreateOneInput
}

input GeometryCreateOneInput {
  create: GeometryCreateInput
  connect: GeometryWhereUniqueInput
}

"""An edge in a connection."""
type GeometryEdge {
  """The item at the end of the edge."""
  node: Geometry!

  """A cursor for use in pagination."""
  cursor: String!
}

enum GeometryOrderByInput {
  id_ASC
  id_DESC
  type_ASC
  type_DESC
}

type GeometryPreviousValues {
  id: ID!
  type: GeometryType!
}

type GeometrySubscriptionPayload {
  mutation: MutationType!
  node: Geometry
  updatedFields: [String!]
  previousValues: GeometryPreviousValues
}

input GeometrySubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [GeometrySubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [GeometrySubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GeometrySubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: GeometryWhereInput
}

enum GeometryType {
  LineString
  Point
  Polygon
}

input GeometryUpdateDataInput {
  type: GeometryType
  lineString: LineStringUpdateOneInput
  point: PointUpdateOneInput
  polygon: PolygonUpdateOneInput
}

input GeometryUpdateInput {
  type: GeometryType
  lineString: LineStringUpdateOneInput
  point: PointUpdateOneInput
  polygon: PolygonUpdateOneInput
}

input GeometryUpdateManyMutationInput {
  type: GeometryType
}

input GeometryUpdateOneRequiredInput {
  create: GeometryCreateInput
  connect: GeometryWhereUniqueInput
  update: GeometryUpdateDataInput
  upsert: GeometryUpsertNestedInput
}

input GeometryUpsertNestedInput {
  update: GeometryUpdateDataInput!
  create: GeometryCreateInput!
}

input GeometryWhereInput {
  """Logical AND on all given filters."""
  AND: [GeometryWhereInput!]

  """Logical OR on all given filters."""
  OR: [GeometryWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [GeometryWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  type: GeometryType

  """All values that are not equal to given value."""
  type_not: GeometryType

  """All values that are contained in given list."""
  type_in: [GeometryType!]

  """All values that are not contained in given list."""
  type_not_in: [GeometryType!]
  lineString: LineStringWhereInput
  point: PointWhereInput
  polygon: PolygonWhereInput
}

input GeometryWhereUniqueInput {
  id: ID
}

type LineString implements Node {
  id: ID!
  coordinates(where: LineStringCoordinateWhereInput, orderBy: LineStringCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [LineStringCoordinate!]
}

"""A connection to a list of items."""
type LineStringConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [LineStringEdge]!
  aggregate: AggregateLineString!
}

type LineStringCoordinate implements Node {
  id: ID!
  lng: Float!
  lat: Float!
}

"""A connection to a list of items."""
type LineStringCoordinateConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [LineStringCoordinateEdge]!
  aggregate: AggregateLineStringCoordinate!
}

input LineStringCoordinateCreateInput {
  id: ID
  lng: Float!
  lat: Float!
}

input LineStringCoordinateCreateManyInput {
  create: [LineStringCoordinateCreateInput!]
  connect: [LineStringCoordinateWhereUniqueInput!]
}

"""An edge in a connection."""
type LineStringCoordinateEdge {
  """The item at the end of the edge."""
  node: LineStringCoordinate!

  """A cursor for use in pagination."""
  cursor: String!
}

enum LineStringCoordinateOrderByInput {
  id_ASC
  id_DESC
  lng_ASC
  lng_DESC
  lat_ASC
  lat_DESC
}

type LineStringCoordinatePreviousValues {
  id: ID!
  lng: Float!
  lat: Float!
}

input LineStringCoordinateScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [LineStringCoordinateScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [LineStringCoordinateScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LineStringCoordinateScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
}

type LineStringCoordinateSubscriptionPayload {
  mutation: MutationType!
  node: LineStringCoordinate
  updatedFields: [String!]
  previousValues: LineStringCoordinatePreviousValues
}

input LineStringCoordinateSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [LineStringCoordinateSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [LineStringCoordinateSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LineStringCoordinateSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: LineStringCoordinateWhereInput
}

input LineStringCoordinateUpdateDataInput {
  lng: Float
  lat: Float
}

input LineStringCoordinateUpdateInput {
  lng: Float
  lat: Float
}

input LineStringCoordinateUpdateManyDataInput {
  lng: Float
  lat: Float
}

input LineStringCoordinateUpdateManyInput {
  create: [LineStringCoordinateCreateInput!]
  connect: [LineStringCoordinateWhereUniqueInput!]
  set: [LineStringCoordinateWhereUniqueInput!]
  disconnect: [LineStringCoordinateWhereUniqueInput!]
  delete: [LineStringCoordinateWhereUniqueInput!]
  update: [LineStringCoordinateUpdateWithWhereUniqueNestedInput!]
  updateMany: [LineStringCoordinateUpdateManyWithWhereNestedInput!]
  deleteMany: [LineStringCoordinateScalarWhereInput!]
  upsert: [LineStringCoordinateUpsertWithWhereUniqueNestedInput!]
}

input LineStringCoordinateUpdateManyMutationInput {
  lng: Float
  lat: Float
}

input LineStringCoordinateUpdateManyWithWhereNestedInput {
  where: LineStringCoordinateScalarWhereInput!
  data: LineStringCoordinateUpdateManyDataInput!
}

input LineStringCoordinateUpdateWithWhereUniqueNestedInput {
  where: LineStringCoordinateWhereUniqueInput!
  data: LineStringCoordinateUpdateDataInput!
}

input LineStringCoordinateUpsertWithWhereUniqueNestedInput {
  where: LineStringCoordinateWhereUniqueInput!
  update: LineStringCoordinateUpdateDataInput!
  create: LineStringCoordinateCreateInput!
}

input LineStringCoordinateWhereInput {
  """Logical AND on all given filters."""
  AND: [LineStringCoordinateWhereInput!]

  """Logical OR on all given filters."""
  OR: [LineStringCoordinateWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LineStringCoordinateWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
}

input LineStringCoordinateWhereUniqueInput {
  id: ID
}

input LineStringCreateInput {
  id: ID
  coordinates: LineStringCoordinateCreateManyInput
}

input LineStringCreateOneInput {
  create: LineStringCreateInput
  connect: LineStringWhereUniqueInput
}

"""An edge in a connection."""
type LineStringEdge {
  """The item at the end of the edge."""
  node: LineString!

  """A cursor for use in pagination."""
  cursor: String!
}

enum LineStringOrderByInput {
  id_ASC
  id_DESC
}

type LineStringPreviousValues {
  id: ID!
}

type LineStringSubscriptionPayload {
  mutation: MutationType!
  node: LineString
  updatedFields: [String!]
  previousValues: LineStringPreviousValues
}

input LineStringSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [LineStringSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [LineStringSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LineStringSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: LineStringWhereInput
}

input LineStringUpdateDataInput {
  coordinates: LineStringCoordinateUpdateManyInput
}

input LineStringUpdateInput {
  coordinates: LineStringCoordinateUpdateManyInput
}

input LineStringUpdateOneInput {
  create: LineStringCreateInput
  connect: LineStringWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: LineStringUpdateDataInput
  upsert: LineStringUpsertNestedInput
}

input LineStringUpsertNestedInput {
  update: LineStringUpdateDataInput!
  create: LineStringCreateInput!
}

input LineStringWhereInput {
  """Logical AND on all given filters."""
  AND: [LineStringWhereInput!]

  """Logical OR on all given filters."""
  OR: [LineStringWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [LineStringWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  coordinates_every: LineStringCoordinateWhereInput
  coordinates_some: LineStringCoordinateWhereInput
  coordinates_none: LineStringCoordinateWhereInput
}

input LineStringWhereUniqueInput {
  id: ID
}

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Mutation {
  createFeature(data: FeatureCreateInput!): Feature!
  createPricePeriod(data: PricePeriodCreateInput!): PricePeriod!
  createProperty(data: PropertyCreateInput!): Property!
  createGeometry(data: GeometryCreateInput!): Geometry!
  createAddress(data: AddressCreateInput!): Address!
  createPoint(data: PointCreateInput!): Point!
  createPolygonLineCoordinate(data: PolygonLineCoordinateCreateInput!): PolygonLineCoordinate!
  createTagKey(data: TagKeyCreateInput!): TagKey!
  createLineString(data: LineStringCreateInput!): LineString!
  createTagValue(data: TagValueCreateInput!): TagValue!
  createPolygonLine(data: PolygonLineCreateInput!): PolygonLine!
  createLineStringCoordinate(data: LineStringCoordinateCreateInput!): LineStringCoordinate!
  createUser(data: UserCreateInput!): User!
  createPropertyChangeSet(data: PropertyChangeSetCreateInput!): PropertyChangeSet!
  createTag(data: TagCreateInput!): Tag!
  createPolygon(data: PolygonCreateInput!): Polygon!
  updateFeature(data: FeatureUpdateInput!, where: FeatureWhereUniqueInput!): Feature
  updatePricePeriod(data: PricePeriodUpdateInput!, where: PricePeriodWhereUniqueInput!): PricePeriod
  updateProperty(data: PropertyUpdateInput!, where: PropertyWhereUniqueInput!): Property
  updateGeometry(data: GeometryUpdateInput!, where: GeometryWhereUniqueInput!): Geometry
  updateAddress(data: AddressUpdateInput!, where: AddressWhereUniqueInput!): Address
  updatePoint(data: PointUpdateInput!, where: PointWhereUniqueInput!): Point
  updatePolygonLineCoordinate(data: PolygonLineCoordinateUpdateInput!, where: PolygonLineCoordinateWhereUniqueInput!): PolygonLineCoordinate
  updateTagKey(data: TagKeyUpdateInput!, where: TagKeyWhereUniqueInput!): TagKey
  updateLineString(data: LineStringUpdateInput!, where: LineStringWhereUniqueInput!): LineString
  updateTagValue(data: TagValueUpdateInput!, where: TagValueWhereUniqueInput!): TagValue
  updatePolygonLine(data: PolygonLineUpdateInput!, where: PolygonLineWhereUniqueInput!): PolygonLine
  updateLineStringCoordinate(data: LineStringCoordinateUpdateInput!, where: LineStringCoordinateWhereUniqueInput!): LineStringCoordinate
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updatePropertyChangeSet(data: PropertyChangeSetUpdateInput!, where: PropertyChangeSetWhereUniqueInput!): PropertyChangeSet
  updateTag(data: TagUpdateInput!, where: TagWhereUniqueInput!): Tag
  updatePolygon(data: PolygonUpdateInput!, where: PolygonWhereUniqueInput!): Polygon
  deleteFeature(where: FeatureWhereUniqueInput!): Feature
  deletePricePeriod(where: PricePeriodWhereUniqueInput!): PricePeriod
  deleteProperty(where: PropertyWhereUniqueInput!): Property
  deleteGeometry(where: GeometryWhereUniqueInput!): Geometry
  deleteAddress(where: AddressWhereUniqueInput!): Address
  deletePoint(where: PointWhereUniqueInput!): Point
  deletePolygonLineCoordinate(where: PolygonLineCoordinateWhereUniqueInput!): PolygonLineCoordinate
  deleteTagKey(where: TagKeyWhereUniqueInput!): TagKey
  deleteLineString(where: LineStringWhereUniqueInput!): LineString
  deleteTagValue(where: TagValueWhereUniqueInput!): TagValue
  deletePolygonLine(where: PolygonLineWhereUniqueInput!): PolygonLine
  deleteLineStringCoordinate(where: LineStringCoordinateWhereUniqueInput!): LineStringCoordinate
  deleteUser(where: UserWhereUniqueInput!): User
  deletePropertyChangeSet(where: PropertyChangeSetWhereUniqueInput!): PropertyChangeSet
  deleteTag(where: TagWhereUniqueInput!): Tag
  deletePolygon(where: PolygonWhereUniqueInput!): Polygon
  upsertFeature(where: FeatureWhereUniqueInput!, create: FeatureCreateInput!, update: FeatureUpdateInput!): Feature!
  upsertPricePeriod(where: PricePeriodWhereUniqueInput!, create: PricePeriodCreateInput!, update: PricePeriodUpdateInput!): PricePeriod!
  upsertProperty(where: PropertyWhereUniqueInput!, create: PropertyCreateInput!, update: PropertyUpdateInput!): Property!
  upsertGeometry(where: GeometryWhereUniqueInput!, create: GeometryCreateInput!, update: GeometryUpdateInput!): Geometry!
  upsertAddress(where: AddressWhereUniqueInput!, create: AddressCreateInput!, update: AddressUpdateInput!): Address!
  upsertPoint(where: PointWhereUniqueInput!, create: PointCreateInput!, update: PointUpdateInput!): Point!
  upsertPolygonLineCoordinate(where: PolygonLineCoordinateWhereUniqueInput!, create: PolygonLineCoordinateCreateInput!, update: PolygonLineCoordinateUpdateInput!): PolygonLineCoordinate!
  upsertTagKey(where: TagKeyWhereUniqueInput!, create: TagKeyCreateInput!, update: TagKeyUpdateInput!): TagKey!
  upsertLineString(where: LineStringWhereUniqueInput!, create: LineStringCreateInput!, update: LineStringUpdateInput!): LineString!
  upsertTagValue(where: TagValueWhereUniqueInput!, create: TagValueCreateInput!, update: TagValueUpdateInput!): TagValue!
  upsertPolygonLine(where: PolygonLineWhereUniqueInput!, create: PolygonLineCreateInput!, update: PolygonLineUpdateInput!): PolygonLine!
  upsertLineStringCoordinate(where: LineStringCoordinateWhereUniqueInput!, create: LineStringCoordinateCreateInput!, update: LineStringCoordinateUpdateInput!): LineStringCoordinate!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  upsertPropertyChangeSet(where: PropertyChangeSetWhereUniqueInput!, create: PropertyChangeSetCreateInput!, update: PropertyChangeSetUpdateInput!): PropertyChangeSet!
  upsertTag(where: TagWhereUniqueInput!, create: TagCreateInput!, update: TagUpdateInput!): Tag!
  upsertPolygon(where: PolygonWhereUniqueInput!, create: PolygonCreateInput!, update: PolygonUpdateInput!): Polygon!
  updateManyFeatures(data: FeatureUpdateManyMutationInput!, where: FeatureWhereInput): BatchPayload!
  updateManyPricePeriods(data: PricePeriodUpdateManyMutationInput!, where: PricePeriodWhereInput): BatchPayload!
  updateManyGeometries(data: GeometryUpdateManyMutationInput!, where: GeometryWhereInput): BatchPayload!
  updateManyAddresses(data: AddressUpdateManyMutationInput!, where: AddressWhereInput): BatchPayload!
  updateManyPoints(data: PointUpdateManyMutationInput!, where: PointWhereInput): BatchPayload!
  updateManyPolygonLineCoordinates(data: PolygonLineCoordinateUpdateManyMutationInput!, where: PolygonLineCoordinateWhereInput): BatchPayload!
  updateManyTagKeys(data: TagKeyUpdateManyMutationInput!, where: TagKeyWhereInput): BatchPayload!
  updateManyTagValues(data: TagValueUpdateManyMutationInput!, where: TagValueWhereInput): BatchPayload!
  updateManyLineStringCoordinates(data: LineStringCoordinateUpdateManyMutationInput!, where: LineStringCoordinateWhereInput): BatchPayload!
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  updateManyPropertyChangeSets(data: PropertyChangeSetUpdateManyMutationInput!, where: PropertyChangeSetWhereInput): BatchPayload!
  deleteManyFeatures(where: FeatureWhereInput): BatchPayload!
  deleteManyPricePeriods(where: PricePeriodWhereInput): BatchPayload!
  deleteManyProperties(where: PropertyWhereInput): BatchPayload!
  deleteManyGeometries(where: GeometryWhereInput): BatchPayload!
  deleteManyAddresses(where: AddressWhereInput): BatchPayload!
  deleteManyPoints(where: PointWhereInput): BatchPayload!
  deleteManyPolygonLineCoordinates(where: PolygonLineCoordinateWhereInput): BatchPayload!
  deleteManyTagKeys(where: TagKeyWhereInput): BatchPayload!
  deleteManyLineStrings(where: LineStringWhereInput): BatchPayload!
  deleteManyTagValues(where: TagValueWhereInput): BatchPayload!
  deleteManyPolygonLines(where: PolygonLineWhereInput): BatchPayload!
  deleteManyLineStringCoordinates(where: LineStringCoordinateWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
  deleteManyPropertyChangeSets(where: PropertyChangeSetWhereInput): BatchPayload!
  deleteManyTags(where: TagWhereInput): BatchPayload!
  deleteManyPolygons(where: PolygonWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

type Point implements Node {
  id: ID!
  lng: Float!
  lat: Float!
}

"""A connection to a list of items."""
type PointConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PointEdge]!
  aggregate: AggregatePoint!
}

input PointCreateInput {
  id: ID
  lng: Float!
  lat: Float!
}

input PointCreateOneInput {
  create: PointCreateInput
  connect: PointWhereUniqueInput
}

"""An edge in a connection."""
type PointEdge {
  """The item at the end of the edge."""
  node: Point!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PointOrderByInput {
  id_ASC
  id_DESC
  lng_ASC
  lng_DESC
  lat_ASC
  lat_DESC
}

type PointPreviousValues {
  id: ID!
  lng: Float!
  lat: Float!
}

type PointSubscriptionPayload {
  mutation: MutationType!
  node: Point
  updatedFields: [String!]
  previousValues: PointPreviousValues
}

input PointSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PointSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PointSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PointSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PointWhereInput
}

input PointUpdateDataInput {
  lng: Float
  lat: Float
}

input PointUpdateInput {
  lng: Float
  lat: Float
}

input PointUpdateManyMutationInput {
  lng: Float
  lat: Float
}

input PointUpdateOneInput {
  create: PointCreateInput
  connect: PointWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: PointUpdateDataInput
  upsert: PointUpsertNestedInput
}

input PointUpsertNestedInput {
  update: PointUpdateDataInput!
  create: PointCreateInput!
}

input PointWhereInput {
  """Logical AND on all given filters."""
  AND: [PointWhereInput!]

  """Logical OR on all given filters."""
  OR: [PointWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PointWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
}

input PointWhereUniqueInput {
  id: ID
}

type Polygon implements Node {
  id: ID!
  lines(where: PolygonLineWhereInput, orderBy: PolygonLineOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PolygonLine!]
}

"""A connection to a list of items."""
type PolygonConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PolygonEdge]!
  aggregate: AggregatePolygon!
}

input PolygonCreateInput {
  id: ID
  lines: PolygonLineCreateManyInput
}

input PolygonCreateOneInput {
  create: PolygonCreateInput
  connect: PolygonWhereUniqueInput
}

"""An edge in a connection."""
type PolygonEdge {
  """The item at the end of the edge."""
  node: Polygon!

  """A cursor for use in pagination."""
  cursor: String!
}

type PolygonLine implements Node {
  id: ID!
  coordinates(where: PolygonLineCoordinateWhereInput, orderBy: PolygonLineCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PolygonLineCoordinate!]
}

"""A connection to a list of items."""
type PolygonLineConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PolygonLineEdge]!
  aggregate: AggregatePolygonLine!
}

type PolygonLineCoordinate implements Node {
  id: ID!
  lng: Float!
  lat: Float!
}

"""A connection to a list of items."""
type PolygonLineCoordinateConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PolygonLineCoordinateEdge]!
  aggregate: AggregatePolygonLineCoordinate!
}

input PolygonLineCoordinateCreateInput {
  id: ID
  lng: Float!
  lat: Float!
}

input PolygonLineCoordinateCreateManyInput {
  create: [PolygonLineCoordinateCreateInput!]
  connect: [PolygonLineCoordinateWhereUniqueInput!]
}

"""An edge in a connection."""
type PolygonLineCoordinateEdge {
  """The item at the end of the edge."""
  node: PolygonLineCoordinate!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PolygonLineCoordinateOrderByInput {
  id_ASC
  id_DESC
  lng_ASC
  lng_DESC
  lat_ASC
  lat_DESC
}

type PolygonLineCoordinatePreviousValues {
  id: ID!
  lng: Float!
  lat: Float!
}

input PolygonLineCoordinateScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineCoordinateScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineCoordinateScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineCoordinateScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
}

type PolygonLineCoordinateSubscriptionPayload {
  mutation: MutationType!
  node: PolygonLineCoordinate
  updatedFields: [String!]
  previousValues: PolygonLineCoordinatePreviousValues
}

input PolygonLineCoordinateSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineCoordinateSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineCoordinateSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineCoordinateSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PolygonLineCoordinateWhereInput
}

input PolygonLineCoordinateUpdateDataInput {
  lng: Float
  lat: Float
}

input PolygonLineCoordinateUpdateInput {
  lng: Float
  lat: Float
}

input PolygonLineCoordinateUpdateManyDataInput {
  lng: Float
  lat: Float
}

input PolygonLineCoordinateUpdateManyInput {
  create: [PolygonLineCoordinateCreateInput!]
  connect: [PolygonLineCoordinateWhereUniqueInput!]
  set: [PolygonLineCoordinateWhereUniqueInput!]
  disconnect: [PolygonLineCoordinateWhereUniqueInput!]
  delete: [PolygonLineCoordinateWhereUniqueInput!]
  update: [PolygonLineCoordinateUpdateWithWhereUniqueNestedInput!]
  updateMany: [PolygonLineCoordinateUpdateManyWithWhereNestedInput!]
  deleteMany: [PolygonLineCoordinateScalarWhereInput!]
  upsert: [PolygonLineCoordinateUpsertWithWhereUniqueNestedInput!]
}

input PolygonLineCoordinateUpdateManyMutationInput {
  lng: Float
  lat: Float
}

input PolygonLineCoordinateUpdateManyWithWhereNestedInput {
  where: PolygonLineCoordinateScalarWhereInput!
  data: PolygonLineCoordinateUpdateManyDataInput!
}

input PolygonLineCoordinateUpdateWithWhereUniqueNestedInput {
  where: PolygonLineCoordinateWhereUniqueInput!
  data: PolygonLineCoordinateUpdateDataInput!
}

input PolygonLineCoordinateUpsertWithWhereUniqueNestedInput {
  where: PolygonLineCoordinateWhereUniqueInput!
  update: PolygonLineCoordinateUpdateDataInput!
  create: PolygonLineCoordinateCreateInput!
}

input PolygonLineCoordinateWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineCoordinateWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineCoordinateWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineCoordinateWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lng: Float

  """All values that are not equal to given value."""
  lng_not: Float

  """All values that are contained in given list."""
  lng_in: [Float!]

  """All values that are not contained in given list."""
  lng_not_in: [Float!]

  """All values less than the given value."""
  lng_lt: Float

  """All values less than or equal the given value."""
  lng_lte: Float

  """All values greater than the given value."""
  lng_gt: Float

  """All values greater than or equal the given value."""
  lng_gte: Float
  lat: Float

  """All values that are not equal to given value."""
  lat_not: Float

  """All values that are contained in given list."""
  lat_in: [Float!]

  """All values that are not contained in given list."""
  lat_not_in: [Float!]

  """All values less than the given value."""
  lat_lt: Float

  """All values less than or equal the given value."""
  lat_lte: Float

  """All values greater than the given value."""
  lat_gt: Float

  """All values greater than or equal the given value."""
  lat_gte: Float
}

input PolygonLineCoordinateWhereUniqueInput {
  id: ID
}

input PolygonLineCreateInput {
  id: ID
  coordinates: PolygonLineCoordinateCreateManyInput
}

input PolygonLineCreateManyInput {
  create: [PolygonLineCreateInput!]
  connect: [PolygonLineWhereUniqueInput!]
}

"""An edge in a connection."""
type PolygonLineEdge {
  """The item at the end of the edge."""
  node: PolygonLine!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PolygonLineOrderByInput {
  id_ASC
  id_DESC
}

type PolygonLinePreviousValues {
  id: ID!
}

input PolygonLineScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
}

type PolygonLineSubscriptionPayload {
  mutation: MutationType!
  node: PolygonLine
  updatedFields: [String!]
  previousValues: PolygonLinePreviousValues
}

input PolygonLineSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PolygonLineWhereInput
}

input PolygonLineUpdateDataInput {
  coordinates: PolygonLineCoordinateUpdateManyInput
}

input PolygonLineUpdateInput {
  coordinates: PolygonLineCoordinateUpdateManyInput
}

input PolygonLineUpdateManyInput {
  create: [PolygonLineCreateInput!]
  connect: [PolygonLineWhereUniqueInput!]
  set: [PolygonLineWhereUniqueInput!]
  disconnect: [PolygonLineWhereUniqueInput!]
  delete: [PolygonLineWhereUniqueInput!]
  update: [PolygonLineUpdateWithWhereUniqueNestedInput!]
  deleteMany: [PolygonLineScalarWhereInput!]
  upsert: [PolygonLineUpsertWithWhereUniqueNestedInput!]
}

input PolygonLineUpdateWithWhereUniqueNestedInput {
  where: PolygonLineWhereUniqueInput!
  data: PolygonLineUpdateDataInput!
}

input PolygonLineUpsertWithWhereUniqueNestedInput {
  where: PolygonLineWhereUniqueInput!
  update: PolygonLineUpdateDataInput!
  create: PolygonLineCreateInput!
}

input PolygonLineWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonLineWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonLineWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonLineWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  coordinates_every: PolygonLineCoordinateWhereInput
  coordinates_some: PolygonLineCoordinateWhereInput
  coordinates_none: PolygonLineCoordinateWhereInput
}

input PolygonLineWhereUniqueInput {
  id: ID
}

enum PolygonOrderByInput {
  id_ASC
  id_DESC
}

type PolygonPreviousValues {
  id: ID!
}

type PolygonSubscriptionPayload {
  mutation: MutationType!
  node: Polygon
  updatedFields: [String!]
  previousValues: PolygonPreviousValues
}

input PolygonSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PolygonWhereInput
}

input PolygonUpdateDataInput {
  lines: PolygonLineUpdateManyInput
}

input PolygonUpdateInput {
  lines: PolygonLineUpdateManyInput
}

input PolygonUpdateOneInput {
  create: PolygonCreateInput
  connect: PolygonWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: PolygonUpdateDataInput
  upsert: PolygonUpsertNestedInput
}

input PolygonUpsertNestedInput {
  update: PolygonUpdateDataInput!
  create: PolygonCreateInput!
}

input PolygonWhereInput {
  """Logical AND on all given filters."""
  AND: [PolygonWhereInput!]

  """Logical OR on all given filters."""
  OR: [PolygonWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PolygonWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  lines_every: PolygonLineWhereInput
  lines_some: PolygonLineWhereInput
  lines_none: PolygonLineWhereInput
}

input PolygonWhereUniqueInput {
  id: ID
}

type PricePeriod implements Node {
  id: ID!
  startTime: DateTime!
  endTime: DateTime!
  startDate: DateTime!
  endDate: DateTime
  repeat: TimePeriod!
  price: Int!
  priceUnit: TimePeriod!
}

"""A connection to a list of items."""
type PricePeriodConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PricePeriodEdge]!
  aggregate: AggregatePricePeriod!
}

input PricePeriodCreateInput {
  id: ID
  startTime: DateTime!
  endTime: DateTime!
  startDate: DateTime!
  endDate: DateTime
  repeat: TimePeriod!
  price: Int!
  priceUnit: TimePeriod!
}

"""An edge in a connection."""
type PricePeriodEdge {
  """The item at the end of the edge."""
  node: PricePeriod!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PricePeriodOrderByInput {
  id_ASC
  id_DESC
  startTime_ASC
  startTime_DESC
  endTime_ASC
  endTime_DESC
  startDate_ASC
  startDate_DESC
  endDate_ASC
  endDate_DESC
  repeat_ASC
  repeat_DESC
  price_ASC
  price_DESC
  priceUnit_ASC
  priceUnit_DESC
}

type PricePeriodPreviousValues {
  id: ID!
  startTime: DateTime!
  endTime: DateTime!
  startDate: DateTime!
  endDate: DateTime
  repeat: TimePeriod!
  price: Int!
  priceUnit: TimePeriod!
}

type PricePeriodSubscriptionPayload {
  mutation: MutationType!
  node: PricePeriod
  updatedFields: [String!]
  previousValues: PricePeriodPreviousValues
}

input PricePeriodSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PricePeriodSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PricePeriodSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PricePeriodSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PricePeriodWhereInput
}

input PricePeriodUpdateInput {
  startTime: DateTime
  endTime: DateTime
  startDate: DateTime
  endDate: DateTime
  repeat: TimePeriod
  price: Int
  priceUnit: TimePeriod
}

input PricePeriodUpdateManyMutationInput {
  startTime: DateTime
  endTime: DateTime
  startDate: DateTime
  endDate: DateTime
  repeat: TimePeriod
  price: Int
  priceUnit: TimePeriod
}

input PricePeriodWhereInput {
  """Logical AND on all given filters."""
  AND: [PricePeriodWhereInput!]

  """Logical OR on all given filters."""
  OR: [PricePeriodWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PricePeriodWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  startTime: DateTime

  """All values that are not equal to given value."""
  startTime_not: DateTime

  """All values that are contained in given list."""
  startTime_in: [DateTime!]

  """All values that are not contained in given list."""
  startTime_not_in: [DateTime!]

  """All values less than the given value."""
  startTime_lt: DateTime

  """All values less than or equal the given value."""
  startTime_lte: DateTime

  """All values greater than the given value."""
  startTime_gt: DateTime

  """All values greater than or equal the given value."""
  startTime_gte: DateTime
  endTime: DateTime

  """All values that are not equal to given value."""
  endTime_not: DateTime

  """All values that are contained in given list."""
  endTime_in: [DateTime!]

  """All values that are not contained in given list."""
  endTime_not_in: [DateTime!]

  """All values less than the given value."""
  endTime_lt: DateTime

  """All values less than or equal the given value."""
  endTime_lte: DateTime

  """All values greater than the given value."""
  endTime_gt: DateTime

  """All values greater than or equal the given value."""
  endTime_gte: DateTime
  startDate: DateTime

  """All values that are not equal to given value."""
  startDate_not: DateTime

  """All values that are contained in given list."""
  startDate_in: [DateTime!]

  """All values that are not contained in given list."""
  startDate_not_in: [DateTime!]

  """All values less than the given value."""
  startDate_lt: DateTime

  """All values less than or equal the given value."""
  startDate_lte: DateTime

  """All values greater than the given value."""
  startDate_gt: DateTime

  """All values greater than or equal the given value."""
  startDate_gte: DateTime
  endDate: DateTime

  """All values that are not equal to given value."""
  endDate_not: DateTime

  """All values that are contained in given list."""
  endDate_in: [DateTime!]

  """All values that are not contained in given list."""
  endDate_not_in: [DateTime!]

  """All values less than the given value."""
  endDate_lt: DateTime

  """All values less than or equal the given value."""
  endDate_lte: DateTime

  """All values greater than the given value."""
  endDate_gt: DateTime

  """All values greater than or equal the given value."""
  endDate_gte: DateTime
  repeat: TimePeriod

  """All values that are not equal to given value."""
  repeat_not: TimePeriod

  """All values that are contained in given list."""
  repeat_in: [TimePeriod!]

  """All values that are not contained in given list."""
  repeat_not_in: [TimePeriod!]
  price: Int

  """All values that are not equal to given value."""
  price_not: Int

  """All values that are contained in given list."""
  price_in: [Int!]

  """All values that are not contained in given list."""
  price_not_in: [Int!]

  """All values less than the given value."""
  price_lt: Int

  """All values less than or equal the given value."""
  price_lte: Int

  """All values greater than the given value."""
  price_gt: Int

  """All values greater than or equal the given value."""
  price_gte: Int
  priceUnit: TimePeriod

  """All values that are not equal to given value."""
  priceUnit_not: TimePeriod

  """All values that are contained in given list."""
  priceUnit_in: [TimePeriod!]

  """All values that are not contained in given list."""
  priceUnit_not_in: [TimePeriod!]
}

input PricePeriodWhereUniqueInput {
  id: ID
}

type Property implements Node {
  id: ID!
  tags(where: TagWhereInput, orderBy: TagOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Tag!]
  address: Address
  changeSet(where: PropertyChangeSetWhereInput, orderBy: PropertyChangeSetOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PropertyChangeSet!]
  createdAt: DateTime!
  updatedAt: DateTime!
}

type PropertyChangeSet implements Node {
  id: ID!
  createdAt: DateTime!
  updatedAt: DateTime!
  version: Int!
  value: String
  changedBy: User
}

"""A connection to a list of items."""
type PropertyChangeSetConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PropertyChangeSetEdge]!
  aggregate: AggregatePropertyChangeSet!
}

input PropertyChangeSetCreateInput {
  id: ID
  version: Int
  value: String
  changedBy: UserCreateOneInput
}

input PropertyChangeSetCreateManyInput {
  create: [PropertyChangeSetCreateInput!]
  connect: [PropertyChangeSetWhereUniqueInput!]
}

"""An edge in a connection."""
type PropertyChangeSetEdge {
  """The item at the end of the edge."""
  node: PropertyChangeSet!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PropertyChangeSetOrderByInput {
  id_ASC
  id_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
  version_ASC
  version_DESC
  value_ASC
  value_DESC
}

type PropertyChangeSetPreviousValues {
  id: ID!
  createdAt: DateTime!
  updatedAt: DateTime!
  version: Int!
  value: String
}

input PropertyChangeSetScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [PropertyChangeSetScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [PropertyChangeSetScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PropertyChangeSetScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  version: Int

  """All values that are not equal to given value."""
  version_not: Int

  """All values that are contained in given list."""
  version_in: [Int!]

  """All values that are not contained in given list."""
  version_not_in: [Int!]

  """All values less than the given value."""
  version_lt: Int

  """All values less than or equal the given value."""
  version_lte: Int

  """All values greater than the given value."""
  version_gt: Int

  """All values greater than or equal the given value."""
  version_gte: Int
  value: String

  """All values that are not equal to given value."""
  value_not: String

  """All values that are contained in given list."""
  value_in: [String!]

  """All values that are not contained in given list."""
  value_not_in: [String!]

  """All values less than the given value."""
  value_lt: String

  """All values less than or equal the given value."""
  value_lte: String

  """All values greater than the given value."""
  value_gt: String

  """All values greater than or equal the given value."""
  value_gte: String

  """All values containing the given string."""
  value_contains: String

  """All values not containing the given string."""
  value_not_contains: String

  """All values starting with the given string."""
  value_starts_with: String

  """All values not starting with the given string."""
  value_not_starts_with: String

  """All values ending with the given string."""
  value_ends_with: String

  """All values not ending with the given string."""
  value_not_ends_with: String
}

type PropertyChangeSetSubscriptionPayload {
  mutation: MutationType!
  node: PropertyChangeSet
  updatedFields: [String!]
  previousValues: PropertyChangeSetPreviousValues
}

input PropertyChangeSetSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PropertyChangeSetSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PropertyChangeSetSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PropertyChangeSetSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PropertyChangeSetWhereInput
}

input PropertyChangeSetUpdateDataInput {
  version: Int
  value: String
  changedBy: UserUpdateOneInput
}

input PropertyChangeSetUpdateInput {
  version: Int
  value: String
  changedBy: UserUpdateOneInput
}

input PropertyChangeSetUpdateManyDataInput {
  version: Int
  value: String
}

input PropertyChangeSetUpdateManyInput {
  create: [PropertyChangeSetCreateInput!]
  connect: [PropertyChangeSetWhereUniqueInput!]
  set: [PropertyChangeSetWhereUniqueInput!]
  disconnect: [PropertyChangeSetWhereUniqueInput!]
  delete: [PropertyChangeSetWhereUniqueInput!]
  update: [PropertyChangeSetUpdateWithWhereUniqueNestedInput!]
  updateMany: [PropertyChangeSetUpdateManyWithWhereNestedInput!]
  deleteMany: [PropertyChangeSetScalarWhereInput!]
  upsert: [PropertyChangeSetUpsertWithWhereUniqueNestedInput!]
}

input PropertyChangeSetUpdateManyMutationInput {
  version: Int
  value: String
}

input PropertyChangeSetUpdateManyWithWhereNestedInput {
  where: PropertyChangeSetScalarWhereInput!
  data: PropertyChangeSetUpdateManyDataInput!
}

input PropertyChangeSetUpdateWithWhereUniqueNestedInput {
  where: PropertyChangeSetWhereUniqueInput!
  data: PropertyChangeSetUpdateDataInput!
}

input PropertyChangeSetUpsertWithWhereUniqueNestedInput {
  where: PropertyChangeSetWhereUniqueInput!
  update: PropertyChangeSetUpdateDataInput!
  create: PropertyChangeSetCreateInput!
}

input PropertyChangeSetWhereInput {
  """Logical AND on all given filters."""
  AND: [PropertyChangeSetWhereInput!]

  """Logical OR on all given filters."""
  OR: [PropertyChangeSetWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PropertyChangeSetWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  version: Int

  """All values that are not equal to given value."""
  version_not: Int

  """All values that are contained in given list."""
  version_in: [Int!]

  """All values that are not contained in given list."""
  version_not_in: [Int!]

  """All values less than the given value."""
  version_lt: Int

  """All values less than or equal the given value."""
  version_lte: Int

  """All values greater than the given value."""
  version_gt: Int

  """All values greater than or equal the given value."""
  version_gte: Int
  value: String

  """All values that are not equal to given value."""
  value_not: String

  """All values that are contained in given list."""
  value_in: [String!]

  """All values that are not contained in given list."""
  value_not_in: [String!]

  """All values less than the given value."""
  value_lt: String

  """All values less than or equal the given value."""
  value_lte: String

  """All values greater than the given value."""
  value_gt: String

  """All values greater than or equal the given value."""
  value_gte: String

  """All values containing the given string."""
  value_contains: String

  """All values not containing the given string."""
  value_not_contains: String

  """All values starting with the given string."""
  value_starts_with: String

  """All values not starting with the given string."""
  value_not_starts_with: String

  """All values ending with the given string."""
  value_ends_with: String

  """All values not ending with the given string."""
  value_not_ends_with: String
  changedBy: UserWhereInput
}

input PropertyChangeSetWhereUniqueInput {
  id: ID
}

"""A connection to a list of items."""
type PropertyConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [PropertyEdge]!
  aggregate: AggregateProperty!
}

input PropertyCreateInput {
  id: ID
  tags: TagCreateManyInput
  address: AddressCreateOneInput
  changeSet: PropertyChangeSetCreateManyInput
}

input PropertyCreateOneInput {
  create: PropertyCreateInput
  connect: PropertyWhereUniqueInput
}

"""An edge in a connection."""
type PropertyEdge {
  """The item at the end of the edge."""
  node: Property!

  """A cursor for use in pagination."""
  cursor: String!
}

enum PropertyOrderByInput {
  id_ASC
  id_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type PropertyPreviousValues {
  id: ID!
  createdAt: DateTime!
  updatedAt: DateTime!
}

type PropertySubscriptionPayload {
  mutation: MutationType!
  node: Property
  updatedFields: [String!]
  previousValues: PropertyPreviousValues
}

input PropertySubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [PropertySubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [PropertySubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PropertySubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: PropertyWhereInput
}

input PropertyUpdateDataInput {
  tags: TagUpdateManyInput
  address: AddressUpdateOneInput
  changeSet: PropertyChangeSetUpdateManyInput
}

input PropertyUpdateInput {
  tags: TagUpdateManyInput
  address: AddressUpdateOneInput
  changeSet: PropertyChangeSetUpdateManyInput
}

input PropertyUpdateOneRequiredInput {
  create: PropertyCreateInput
  connect: PropertyWhereUniqueInput
  update: PropertyUpdateDataInput
  upsert: PropertyUpsertNestedInput
}

input PropertyUpsertNestedInput {
  update: PropertyUpdateDataInput!
  create: PropertyCreateInput!
}

input PropertyWhereInput {
  """Logical AND on all given filters."""
  AND: [PropertyWhereInput!]

  """Logical OR on all given filters."""
  OR: [PropertyWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [PropertyWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  tags_every: TagWhereInput
  tags_some: TagWhereInput
  tags_none: TagWhereInput
  address: AddressWhereInput
  changeSet_every: PropertyChangeSetWhereInput
  changeSet_some: PropertyChangeSetWhereInput
  changeSet_none: PropertyChangeSetWhereInput
}

input PropertyWhereUniqueInput {
  id: ID
}

type Query {
  features(where: FeatureWhereInput, orderBy: FeatureOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Feature]!
  pricePeriods(where: PricePeriodWhereInput, orderBy: PricePeriodOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PricePeriod]!
  properties(where: PropertyWhereInput, orderBy: PropertyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Property]!
  geometries(where: GeometryWhereInput, orderBy: GeometryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Geometry]!
  addresses(where: AddressWhereInput, orderBy: AddressOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Address]!
  points(where: PointWhereInput, orderBy: PointOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Point]!
  polygonLineCoordinates(where: PolygonLineCoordinateWhereInput, orderBy: PolygonLineCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PolygonLineCoordinate]!
  tagKeys(where: TagKeyWhereInput, orderBy: TagKeyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TagKey]!
  lineStrings(where: LineStringWhereInput, orderBy: LineStringOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [LineString]!
  tagValues(where: TagValueWhereInput, orderBy: TagValueOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [TagValue]!
  polygonLines(where: PolygonLineWhereInput, orderBy: PolygonLineOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PolygonLine]!
  lineStringCoordinates(where: LineStringCoordinateWhereInput, orderBy: LineStringCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [LineStringCoordinate]!
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  propertyChangeSets(where: PropertyChangeSetWhereInput, orderBy: PropertyChangeSetOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [PropertyChangeSet]!
  tags(where: TagWhereInput, orderBy: TagOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Tag]!
  polygons(where: PolygonWhereInput, orderBy: PolygonOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Polygon]!
  feature(where: FeatureWhereUniqueInput!): Feature
  pricePeriod(where: PricePeriodWhereUniqueInput!): PricePeriod
  property(where: PropertyWhereUniqueInput!): Property
  geometry(where: GeometryWhereUniqueInput!): Geometry
  address(where: AddressWhereUniqueInput!): Address
  point(where: PointWhereUniqueInput!): Point
  polygonLineCoordinate(where: PolygonLineCoordinateWhereUniqueInput!): PolygonLineCoordinate
  tagKey(where: TagKeyWhereUniqueInput!): TagKey
  lineString(where: LineStringWhereUniqueInput!): LineString
  tagValue(where: TagValueWhereUniqueInput!): TagValue
  polygonLine(where: PolygonLineWhereUniqueInput!): PolygonLine
  lineStringCoordinate(where: LineStringCoordinateWhereUniqueInput!): LineStringCoordinate
  user(where: UserWhereUniqueInput!): User
  propertyChangeSet(where: PropertyChangeSetWhereUniqueInput!): PropertyChangeSet
  tag(where: TagWhereUniqueInput!): Tag
  polygon(where: PolygonWhereUniqueInput!): Polygon
  featuresConnection(where: FeatureWhereInput, orderBy: FeatureOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): FeatureConnection!
  pricePeriodsConnection(where: PricePeriodWhereInput, orderBy: PricePeriodOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PricePeriodConnection!
  propertiesConnection(where: PropertyWhereInput, orderBy: PropertyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PropertyConnection!
  geometriesConnection(where: GeometryWhereInput, orderBy: GeometryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): GeometryConnection!
  addressesConnection(where: AddressWhereInput, orderBy: AddressOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): AddressConnection!
  pointsConnection(where: PointWhereInput, orderBy: PointOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PointConnection!
  polygonLineCoordinatesConnection(where: PolygonLineCoordinateWhereInput, orderBy: PolygonLineCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PolygonLineCoordinateConnection!
  tagKeysConnection(where: TagKeyWhereInput, orderBy: TagKeyOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TagKeyConnection!
  lineStringsConnection(where: LineStringWhereInput, orderBy: LineStringOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): LineStringConnection!
  tagValuesConnection(where: TagValueWhereInput, orderBy: TagValueOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TagValueConnection!
  polygonLinesConnection(where: PolygonLineWhereInput, orderBy: PolygonLineOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PolygonLineConnection!
  lineStringCoordinatesConnection(where: LineStringCoordinateWhereInput, orderBy: LineStringCoordinateOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): LineStringCoordinateConnection!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  propertyChangeSetsConnection(where: PropertyChangeSetWhereInput, orderBy: PropertyChangeSetOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PropertyChangeSetConnection!
  tagsConnection(where: TagWhereInput, orderBy: TagOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): TagConnection!
  polygonsConnection(where: PolygonWhereInput, orderBy: PolygonOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PolygonConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

enum Roles {
  ADMIN
  SUPER_ADMIN
  USER
}

type Subscription {
  feature(where: FeatureSubscriptionWhereInput): FeatureSubscriptionPayload
  pricePeriod(where: PricePeriodSubscriptionWhereInput): PricePeriodSubscriptionPayload
  property(where: PropertySubscriptionWhereInput): PropertySubscriptionPayload
  geometry(where: GeometrySubscriptionWhereInput): GeometrySubscriptionPayload
  address(where: AddressSubscriptionWhereInput): AddressSubscriptionPayload
  point(where: PointSubscriptionWhereInput): PointSubscriptionPayload
  polygonLineCoordinate(where: PolygonLineCoordinateSubscriptionWhereInput): PolygonLineCoordinateSubscriptionPayload
  tagKey(where: TagKeySubscriptionWhereInput): TagKeySubscriptionPayload
  lineString(where: LineStringSubscriptionWhereInput): LineStringSubscriptionPayload
  tagValue(where: TagValueSubscriptionWhereInput): TagValueSubscriptionPayload
  polygonLine(where: PolygonLineSubscriptionWhereInput): PolygonLineSubscriptionPayload
  lineStringCoordinate(where: LineStringCoordinateSubscriptionWhereInput): LineStringCoordinateSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
  propertyChangeSet(where: PropertyChangeSetSubscriptionWhereInput): PropertyChangeSetSubscriptionPayload
  tag(where: TagSubscriptionWhereInput): TagSubscriptionPayload
  polygon(where: PolygonSubscriptionWhereInput): PolygonSubscriptionPayload
}

type Tag implements Node {
  id: ID!
  key: TagKey!
  value: TagValue!
}

"""A connection to a list of items."""
type TagConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TagEdge]!
  aggregate: AggregateTag!
}

input TagCreateInput {
  id: ID
  key: TagKeyCreateOneInput!
  value: TagValueCreateOneInput!
}

input TagCreateManyInput {
  create: [TagCreateInput!]
  connect: [TagWhereUniqueInput!]
}

"""An edge in a connection."""
type TagEdge {
  """The item at the end of the edge."""
  node: Tag!

  """A cursor for use in pagination."""
  cursor: String!
}

type TagKey implements Node {
  id: ID!
  value: String!
}

"""A connection to a list of items."""
type TagKeyConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TagKeyEdge]!
  aggregate: AggregateTagKey!
}

input TagKeyCreateInput {
  id: ID
  value: String!
}

input TagKeyCreateOneInput {
  create: TagKeyCreateInput
  connect: TagKeyWhereUniqueInput
}

"""An edge in a connection."""
type TagKeyEdge {
  """The item at the end of the edge."""
  node: TagKey!

  """A cursor for use in pagination."""
  cursor: String!
}

enum TagKeyOrderByInput {
  id_ASC
  id_DESC
  value_ASC
  value_DESC
}

type TagKeyPreviousValues {
  id: ID!
  value: String!
}

type TagKeySubscriptionPayload {
  mutation: MutationType!
  node: TagKey
  updatedFields: [String!]
  previousValues: TagKeyPreviousValues
}

input TagKeySubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TagKeySubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagKeySubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagKeySubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TagKeyWhereInput
}

input TagKeyUpdateDataInput {
  value: String
}

input TagKeyUpdateInput {
  value: String
}

input TagKeyUpdateManyMutationInput {
  value: String
}

input TagKeyUpdateOneRequiredInput {
  create: TagKeyCreateInput
  connect: TagKeyWhereUniqueInput
  update: TagKeyUpdateDataInput
  upsert: TagKeyUpsertNestedInput
}

input TagKeyUpsertNestedInput {
  update: TagKeyUpdateDataInput!
  create: TagKeyCreateInput!
}

input TagKeyWhereInput {
  """Logical AND on all given filters."""
  AND: [TagKeyWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagKeyWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagKeyWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  value: String

  """All values that are not equal to given value."""
  value_not: String

  """All values that are contained in given list."""
  value_in: [String!]

  """All values that are not contained in given list."""
  value_not_in: [String!]

  """All values less than the given value."""
  value_lt: String

  """All values less than or equal the given value."""
  value_lte: String

  """All values greater than the given value."""
  value_gt: String

  """All values greater than or equal the given value."""
  value_gte: String

  """All values containing the given string."""
  value_contains: String

  """All values not containing the given string."""
  value_not_contains: String

  """All values starting with the given string."""
  value_starts_with: String

  """All values not starting with the given string."""
  value_not_starts_with: String

  """All values ending with the given string."""
  value_ends_with: String

  """All values not ending with the given string."""
  value_not_ends_with: String
}

input TagKeyWhereUniqueInput {
  id: ID
  value: String
}

enum TagOrderByInput {
  id_ASC
  id_DESC
}

type TagPreviousValues {
  id: ID!
}

input TagScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [TagScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
}

type TagSubscriptionPayload {
  mutation: MutationType!
  node: Tag
  updatedFields: [String!]
  previousValues: TagPreviousValues
}

input TagSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TagSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TagWhereInput
}

input TagUpdateDataInput {
  key: TagKeyUpdateOneRequiredInput
  value: TagValueUpdateOneRequiredInput
}

input TagUpdateInput {
  key: TagKeyUpdateOneRequiredInput
  value: TagValueUpdateOneRequiredInput
}

input TagUpdateManyInput {
  create: [TagCreateInput!]
  connect: [TagWhereUniqueInput!]
  set: [TagWhereUniqueInput!]
  disconnect: [TagWhereUniqueInput!]
  delete: [TagWhereUniqueInput!]
  update: [TagUpdateWithWhereUniqueNestedInput!]
  deleteMany: [TagScalarWhereInput!]
  upsert: [TagUpsertWithWhereUniqueNestedInput!]
}

input TagUpdateWithWhereUniqueNestedInput {
  where: TagWhereUniqueInput!
  data: TagUpdateDataInput!
}

input TagUpsertWithWhereUniqueNestedInput {
  where: TagWhereUniqueInput!
  update: TagUpdateDataInput!
  create: TagCreateInput!
}

type TagValue implements Node {
  id: ID!
  value: String!
}

"""A connection to a list of items."""
type TagValueConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [TagValueEdge]!
  aggregate: AggregateTagValue!
}

input TagValueCreateInput {
  id: ID
  value: String!
}

input TagValueCreateOneInput {
  create: TagValueCreateInput
  connect: TagValueWhereUniqueInput
}

"""An edge in a connection."""
type TagValueEdge {
  """The item at the end of the edge."""
  node: TagValue!

  """A cursor for use in pagination."""
  cursor: String!
}

enum TagValueOrderByInput {
  id_ASC
  id_DESC
  value_ASC
  value_DESC
}

type TagValuePreviousValues {
  id: ID!
  value: String!
}

type TagValueSubscriptionPayload {
  mutation: MutationType!
  node: TagValue
  updatedFields: [String!]
  previousValues: TagValuePreviousValues
}

input TagValueSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [TagValueSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagValueSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagValueSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: TagValueWhereInput
}

input TagValueUpdateDataInput {
  value: String
}

input TagValueUpdateInput {
  value: String
}

input TagValueUpdateManyMutationInput {
  value: String
}

input TagValueUpdateOneRequiredInput {
  create: TagValueCreateInput
  connect: TagValueWhereUniqueInput
  update: TagValueUpdateDataInput
  upsert: TagValueUpsertNestedInput
}

input TagValueUpsertNestedInput {
  update: TagValueUpdateDataInput!
  create: TagValueCreateInput!
}

input TagValueWhereInput {
  """Logical AND on all given filters."""
  AND: [TagValueWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagValueWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagValueWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  value: String

  """All values that are not equal to given value."""
  value_not: String

  """All values that are contained in given list."""
  value_in: [String!]

  """All values that are not contained in given list."""
  value_not_in: [String!]

  """All values less than the given value."""
  value_lt: String

  """All values less than or equal the given value."""
  value_lte: String

  """All values greater than the given value."""
  value_gt: String

  """All values greater than or equal the given value."""
  value_gte: String

  """All values containing the given string."""
  value_contains: String

  """All values not containing the given string."""
  value_not_contains: String

  """All values starting with the given string."""
  value_starts_with: String

  """All values not starting with the given string."""
  value_not_starts_with: String

  """All values ending with the given string."""
  value_ends_with: String

  """All values not ending with the given string."""
  value_not_ends_with: String
}

input TagValueWhereUniqueInput {
  id: ID
  value: String
}

input TagWhereInput {
  """Logical AND on all given filters."""
  AND: [TagWhereInput!]

  """Logical OR on all given filters."""
  OR: [TagWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [TagWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  key: TagKeyWhereInput
  value: TagValueWhereInput
}

input TagWhereUniqueInput {
  id: ID
}

enum TimePeriod {
  SEC
  MIN
  HOUR
  DAY
  WEEK
  MONTH
  YEAR
}

type User implements Node {
  id: ID!
  name: String!
  lastName: String
  email: String!
  password: String!
  token: String
  isActive: Boolean!
  registrationToken: String
  roles: [Roles!]!
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  id: ID
  name: String!
  lastName: String
  email: String!
  password: String!
  token: String
  isActive: Boolean
  registrationToken: String
  roles: UserCreaterolesInput
}

input UserCreateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
}

input UserCreaterolesInput {
  set: [Roles!]
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  lastName_ASC
  lastName_DESC
  email_ASC
  email_DESC
  password_ASC
  password_DESC
  token_ASC
  token_DESC
  isActive_ASC
  isActive_DESC
  registrationToken_ASC
  registrationToken_DESC
}

type UserPreviousValues {
  id: ID!
  name: String!
  lastName: String
  email: String!
  password: String!
  token: String
  isActive: Boolean!
  registrationToken: String
  roles: [Roles!]!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """The subscription event gets dispatched when it's listed in mutation_in"""
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateDataInput {
  name: String
  lastName: String
  email: String
  password: String
  token: String
  isActive: Boolean
  registrationToken: String
  roles: UserUpdaterolesInput
}

input UserUpdateInput {
  name: String
  lastName: String
  email: String
  password: String
  token: String
  isActive: Boolean
  registrationToken: String
  roles: UserUpdaterolesInput
}

input UserUpdateManyMutationInput {
  name: String
  lastName: String
  email: String
  password: String
  token: String
  isActive: Boolean
  registrationToken: String
  roles: UserUpdaterolesInput
}

input UserUpdateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: UserUpdateDataInput
  upsert: UserUpsertNestedInput
}

input UserUpdaterolesInput {
  set: [Roles!]
}

input UserUpsertNestedInput {
  update: UserUpdateDataInput!
  create: UserCreateInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  lastName: String

  """All values that are not equal to given value."""
  lastName_not: String

  """All values that are contained in given list."""
  lastName_in: [String!]

  """All values that are not contained in given list."""
  lastName_not_in: [String!]

  """All values less than the given value."""
  lastName_lt: String

  """All values less than or equal the given value."""
  lastName_lte: String

  """All values greater than the given value."""
  lastName_gt: String

  """All values greater than or equal the given value."""
  lastName_gte: String

  """All values containing the given string."""
  lastName_contains: String

  """All values not containing the given string."""
  lastName_not_contains: String

  """All values starting with the given string."""
  lastName_starts_with: String

  """All values not starting with the given string."""
  lastName_not_starts_with: String

  """All values ending with the given string."""
  lastName_ends_with: String

  """All values not ending with the given string."""
  lastName_not_ends_with: String
  email: String

  """All values that are not equal to given value."""
  email_not: String

  """All values that are contained in given list."""
  email_in: [String!]

  """All values that are not contained in given list."""
  email_not_in: [String!]

  """All values less than the given value."""
  email_lt: String

  """All values less than or equal the given value."""
  email_lte: String

  """All values greater than the given value."""
  email_gt: String

  """All values greater than or equal the given value."""
  email_gte: String

  """All values containing the given string."""
  email_contains: String

  """All values not containing the given string."""
  email_not_contains: String

  """All values starting with the given string."""
  email_starts_with: String

  """All values not starting with the given string."""
  email_not_starts_with: String

  """All values ending with the given string."""
  email_ends_with: String

  """All values not ending with the given string."""
  email_not_ends_with: String
  password: String

  """All values that are not equal to given value."""
  password_not: String

  """All values that are contained in given list."""
  password_in: [String!]

  """All values that are not contained in given list."""
  password_not_in: [String!]

  """All values less than the given value."""
  password_lt: String

  """All values less than or equal the given value."""
  password_lte: String

  """All values greater than the given value."""
  password_gt: String

  """All values greater than or equal the given value."""
  password_gte: String

  """All values containing the given string."""
  password_contains: String

  """All values not containing the given string."""
  password_not_contains: String

  """All values starting with the given string."""
  password_starts_with: String

  """All values not starting with the given string."""
  password_not_starts_with: String

  """All values ending with the given string."""
  password_ends_with: String

  """All values not ending with the given string."""
  password_not_ends_with: String
  token: String

  """All values that are not equal to given value."""
  token_not: String

  """All values that are contained in given list."""
  token_in: [String!]

  """All values that are not contained in given list."""
  token_not_in: [String!]

  """All values less than the given value."""
  token_lt: String

  """All values less than or equal the given value."""
  token_lte: String

  """All values greater than the given value."""
  token_gt: String

  """All values greater than or equal the given value."""
  token_gte: String

  """All values containing the given string."""
  token_contains: String

  """All values not containing the given string."""
  token_not_contains: String

  """All values starting with the given string."""
  token_starts_with: String

  """All values not starting with the given string."""
  token_not_starts_with: String

  """All values ending with the given string."""
  token_ends_with: String

  """All values not ending with the given string."""
  token_not_ends_with: String
  isActive: Boolean

  """All values that are not equal to given value."""
  isActive_not: Boolean
  registrationToken: String

  """All values that are not equal to given value."""
  registrationToken_not: String

  """All values that are contained in given list."""
  registrationToken_in: [String!]

  """All values that are not contained in given list."""
  registrationToken_not_in: [String!]

  """All values less than the given value."""
  registrationToken_lt: String

  """All values less than or equal the given value."""
  registrationToken_lte: String

  """All values greater than the given value."""
  registrationToken_gt: String

  """All values greater than or equal the given value."""
  registrationToken_gte: String

  """All values containing the given string."""
  registrationToken_contains: String

  """All values not containing the given string."""
  registrationToken_not_contains: String

  """All values starting with the given string."""
  registrationToken_starts_with: String

  """All values not starting with the given string."""
  registrationToken_not_starts_with: String

  """All values ending with the given string."""
  registrationToken_ends_with: String

  """All values not ending with the given string."""
  registrationToken_not_ends_with: String
}

input UserWhereUniqueInput {
  id: ID
  email: String
  registrationToken: String
}
`

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({typeDefs})

/**
 * Types
*/

export type AddressOrderByInput =   'id_ASC' |
  'id_DESC' |
  'uuid_ASC' |
  'uuid_DESC' |
  'street_ASC' |
  'street_DESC' |
  'houseNumber_ASC' |
  'houseNumber_DESC' |
  'city_ASC' |
  'city_DESC' |
  'countryCode_ASC' |
  'countryCode_DESC' |
  'lng_ASC' |
  'lng_DESC' |
  'lat_ASC' |
  'lat_DESC' |
  'cityDistrict_ASC' |
  'cityDistrict_DESC' |
  'continent_ASC' |
  'continent_DESC' |
  'neighbourhood_ASC' |
  'neighbourhood_DESC' |
  'postcode_ASC' |
  'postcode_DESC' |
  'state_ASC' |
  'state_DESC' |
  'suburb_ASC' |
  'suburb_DESC' |
  'road_ASC' |
  'road_DESC' |
  'importance_ASC' |
  'importance_DESC'

export type CountryCode =   'DE' |
  'US'

export type FeatureOrderByInput =   'id_ASC' |
  'id_DESC' |
  'type_ASC' |
  'type_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC'

export type FeatureType =   'FeatureCollection' |
  'Feature'

export type GeometryOrderByInput =   'id_ASC' |
  'id_DESC' |
  'type_ASC' |
  'type_DESC'

export type GeometryType =   'LineString' |
  'Point' |
  'Polygon'

export type LineStringCoordinateOrderByInput =   'id_ASC' |
  'id_DESC' |
  'lng_ASC' |
  'lng_DESC' |
  'lat_ASC' |
  'lat_DESC'

export type LineStringOrderByInput =   'id_ASC' |
  'id_DESC'

export type MutationType =   'CREATED' |
  'UPDATED' |
  'DELETED'

export type PointOrderByInput =   'id_ASC' |
  'id_DESC' |
  'lng_ASC' |
  'lng_DESC' |
  'lat_ASC' |
  'lat_DESC'

export type PolygonLineCoordinateOrderByInput =   'id_ASC' |
  'id_DESC' |
  'lng_ASC' |
  'lng_DESC' |
  'lat_ASC' |
  'lat_DESC'

export type PolygonLineOrderByInput =   'id_ASC' |
  'id_DESC'

export type PolygonOrderByInput =   'id_ASC' |
  'id_DESC'

export type PricePeriodOrderByInput =   'id_ASC' |
  'id_DESC' |
  'startTime_ASC' |
  'startTime_DESC' |
  'endTime_ASC' |
  'endTime_DESC' |
  'startDate_ASC' |
  'startDate_DESC' |
  'endDate_ASC' |
  'endDate_DESC' |
  'repeat_ASC' |
  'repeat_DESC' |
  'price_ASC' |
  'price_DESC' |
  'priceUnit_ASC' |
  'priceUnit_DESC'

export type PropertyChangeSetOrderByInput =   'id_ASC' |
  'id_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC' |
  'version_ASC' |
  'version_DESC' |
  'value_ASC' |
  'value_DESC'

export type PropertyOrderByInput =   'id_ASC' |
  'id_DESC' |
  'createdAt_ASC' |
  'createdAt_DESC' |
  'updatedAt_ASC' |
  'updatedAt_DESC'

export type Roles =   'ADMIN' |
  'SUPER_ADMIN' |
  'USER'

export type TagKeyOrderByInput =   'id_ASC' |
  'id_DESC' |
  'value_ASC' |
  'value_DESC'

export type TagOrderByInput =   'id_ASC' |
  'id_DESC'

export type TagValueOrderByInput =   'id_ASC' |
  'id_DESC' |
  'value_ASC' |
  'value_DESC'

export type TimePeriod =   'SEC' |
  'MIN' |
  'HOUR' |
  'DAY' |
  'WEEK' |
  'MONTH' |
  'YEAR'

export type UserOrderByInput =   'id_ASC' |
  'id_DESC' |
  'name_ASC' |
  'name_DESC' |
  'lastName_ASC' |
  'lastName_DESC' |
  'email_ASC' |
  'email_DESC' |
  'password_ASC' |
  'password_DESC' |
  'token_ASC' |
  'token_DESC' |
  'isActive_ASC' |
  'isActive_DESC' |
  'registrationToken_ASC' |
  'registrationToken_DESC'

export interface AddressCreateInput {
  id?: ID_Input | null
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

export interface AddressCreateOneInput {
  create?: AddressCreateInput | null
  connect?: AddressWhereUniqueInput | null
}

export interface AddressSubscriptionWhereInput {
  AND?: AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput | null
  OR?: AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput | null
  NOT?: AddressSubscriptionWhereInput[] | AddressSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: AddressWhereInput | null
}

export interface AddressUpdateDataInput {
  uuid?: String | null
  street?: String | null
  houseNumber?: String | null
  city?: String | null
  countryCode?: CountryCode | null
  lng?: Float | null
  lat?: Float | null
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

export interface AddressUpdateInput {
  uuid?: String | null
  street?: String | null
  houseNumber?: String | null
  city?: String | null
  countryCode?: CountryCode | null
  lng?: Float | null
  lat?: Float | null
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

export interface AddressUpdateManyMutationInput {
  uuid?: String | null
  street?: String | null
  houseNumber?: String | null
  city?: String | null
  countryCode?: CountryCode | null
  lng?: Float | null
  lat?: Float | null
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

export interface AddressUpdateOneInput {
  create?: AddressCreateInput | null
  connect?: AddressWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: AddressUpdateDataInput | null
  upsert?: AddressUpsertNestedInput | null
}

export interface AddressUpsertNestedInput {
  update: AddressUpdateDataInput
  create: AddressCreateInput
}

export interface AddressWhereInput {
  AND?: AddressWhereInput[] | AddressWhereInput | null
  OR?: AddressWhereInput[] | AddressWhereInput | null
  NOT?: AddressWhereInput[] | AddressWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  uuid?: String | null
  uuid_not?: String | null
  uuid_in?: String[] | String | null
  uuid_not_in?: String[] | String | null
  uuid_lt?: String | null
  uuid_lte?: String | null
  uuid_gt?: String | null
  uuid_gte?: String | null
  uuid_contains?: String | null
  uuid_not_contains?: String | null
  uuid_starts_with?: String | null
  uuid_not_starts_with?: String | null
  uuid_ends_with?: String | null
  uuid_not_ends_with?: String | null
  street?: String | null
  street_not?: String | null
  street_in?: String[] | String | null
  street_not_in?: String[] | String | null
  street_lt?: String | null
  street_lte?: String | null
  street_gt?: String | null
  street_gte?: String | null
  street_contains?: String | null
  street_not_contains?: String | null
  street_starts_with?: String | null
  street_not_starts_with?: String | null
  street_ends_with?: String | null
  street_not_ends_with?: String | null
  houseNumber?: String | null
  houseNumber_not?: String | null
  houseNumber_in?: String[] | String | null
  houseNumber_not_in?: String[] | String | null
  houseNumber_lt?: String | null
  houseNumber_lte?: String | null
  houseNumber_gt?: String | null
  houseNumber_gte?: String | null
  houseNumber_contains?: String | null
  houseNumber_not_contains?: String | null
  houseNumber_starts_with?: String | null
  houseNumber_not_starts_with?: String | null
  houseNumber_ends_with?: String | null
  houseNumber_not_ends_with?: String | null
  city?: String | null
  city_not?: String | null
  city_in?: String[] | String | null
  city_not_in?: String[] | String | null
  city_lt?: String | null
  city_lte?: String | null
  city_gt?: String | null
  city_gte?: String | null
  city_contains?: String | null
  city_not_contains?: String | null
  city_starts_with?: String | null
  city_not_starts_with?: String | null
  city_ends_with?: String | null
  city_not_ends_with?: String | null
  countryCode?: CountryCode | null
  countryCode_not?: CountryCode | null
  countryCode_in?: CountryCode[] | CountryCode | null
  countryCode_not_in?: CountryCode[] | CountryCode | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
  cityDistrict?: String | null
  cityDistrict_not?: String | null
  cityDistrict_in?: String[] | String | null
  cityDistrict_not_in?: String[] | String | null
  cityDistrict_lt?: String | null
  cityDistrict_lte?: String | null
  cityDistrict_gt?: String | null
  cityDistrict_gte?: String | null
  cityDistrict_contains?: String | null
  cityDistrict_not_contains?: String | null
  cityDistrict_starts_with?: String | null
  cityDistrict_not_starts_with?: String | null
  cityDistrict_ends_with?: String | null
  cityDistrict_not_ends_with?: String | null
  continent?: String | null
  continent_not?: String | null
  continent_in?: String[] | String | null
  continent_not_in?: String[] | String | null
  continent_lt?: String | null
  continent_lte?: String | null
  continent_gt?: String | null
  continent_gte?: String | null
  continent_contains?: String | null
  continent_not_contains?: String | null
  continent_starts_with?: String | null
  continent_not_starts_with?: String | null
  continent_ends_with?: String | null
  continent_not_ends_with?: String | null
  neighbourhood?: String | null
  neighbourhood_not?: String | null
  neighbourhood_in?: String[] | String | null
  neighbourhood_not_in?: String[] | String | null
  neighbourhood_lt?: String | null
  neighbourhood_lte?: String | null
  neighbourhood_gt?: String | null
  neighbourhood_gte?: String | null
  neighbourhood_contains?: String | null
  neighbourhood_not_contains?: String | null
  neighbourhood_starts_with?: String | null
  neighbourhood_not_starts_with?: String | null
  neighbourhood_ends_with?: String | null
  neighbourhood_not_ends_with?: String | null
  postcode?: Int | null
  postcode_not?: Int | null
  postcode_in?: Int[] | Int | null
  postcode_not_in?: Int[] | Int | null
  postcode_lt?: Int | null
  postcode_lte?: Int | null
  postcode_gt?: Int | null
  postcode_gte?: Int | null
  state?: String | null
  state_not?: String | null
  state_in?: String[] | String | null
  state_not_in?: String[] | String | null
  state_lt?: String | null
  state_lte?: String | null
  state_gt?: String | null
  state_gte?: String | null
  state_contains?: String | null
  state_not_contains?: String | null
  state_starts_with?: String | null
  state_not_starts_with?: String | null
  state_ends_with?: String | null
  state_not_ends_with?: String | null
  suburb?: String | null
  suburb_not?: String | null
  suburb_in?: String[] | String | null
  suburb_not_in?: String[] | String | null
  suburb_lt?: String | null
  suburb_lte?: String | null
  suburb_gt?: String | null
  suburb_gte?: String | null
  suburb_contains?: String | null
  suburb_not_contains?: String | null
  suburb_starts_with?: String | null
  suburb_not_starts_with?: String | null
  suburb_ends_with?: String | null
  suburb_not_ends_with?: String | null
  road?: String | null
  road_not?: String | null
  road_in?: String[] | String | null
  road_not_in?: String[] | String | null
  road_lt?: String | null
  road_lte?: String | null
  road_gt?: String | null
  road_gte?: String | null
  road_contains?: String | null
  road_not_contains?: String | null
  road_starts_with?: String | null
  road_not_starts_with?: String | null
  road_ends_with?: String | null
  road_not_ends_with?: String | null
  importance?: Float | null
  importance_not?: Float | null
  importance_in?: Float[] | Float | null
  importance_not_in?: Float[] | Float | null
  importance_lt?: Float | null
  importance_lte?: Float | null
  importance_gt?: Float | null
  importance_gte?: Float | null
}

export interface AddressWhereUniqueInput {
  id?: ID_Input | null
  uuid?: String | null
}

export interface FeatureCreateInput {
  id?: ID_Input | null
  type: FeatureType
  geometry: GeometryCreateOneInput
  properties: PropertyCreateOneInput
}

export interface FeatureSubscriptionWhereInput {
  AND?: FeatureSubscriptionWhereInput[] | FeatureSubscriptionWhereInput | null
  OR?: FeatureSubscriptionWhereInput[] | FeatureSubscriptionWhereInput | null
  NOT?: FeatureSubscriptionWhereInput[] | FeatureSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: FeatureWhereInput | null
}

export interface FeatureUpdateInput {
  type?: FeatureType | null
  geometry?: GeometryUpdateOneRequiredInput | null
  properties?: PropertyUpdateOneRequiredInput | null
}

export interface FeatureUpdateManyMutationInput {
  type?: FeatureType | null
}

export interface FeatureWhereInput {
  AND?: FeatureWhereInput[] | FeatureWhereInput | null
  OR?: FeatureWhereInput[] | FeatureWhereInput | null
  NOT?: FeatureWhereInput[] | FeatureWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  type?: FeatureType | null
  type_not?: FeatureType | null
  type_in?: FeatureType[] | FeatureType | null
  type_not_in?: FeatureType[] | FeatureType | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  geometry?: GeometryWhereInput | null
  properties?: PropertyWhereInput | null
}

export interface FeatureWhereUniqueInput {
  id?: ID_Input | null
}

export interface GeometryCreateInput {
  id?: ID_Input | null
  type: GeometryType
  lineString?: LineStringCreateOneInput | null
  point?: PointCreateOneInput | null
  polygon?: PolygonCreateOneInput | null
}

export interface GeometryCreateOneInput {
  create?: GeometryCreateInput | null
  connect?: GeometryWhereUniqueInput | null
}

export interface GeometrySubscriptionWhereInput {
  AND?: GeometrySubscriptionWhereInput[] | GeometrySubscriptionWhereInput | null
  OR?: GeometrySubscriptionWhereInput[] | GeometrySubscriptionWhereInput | null
  NOT?: GeometrySubscriptionWhereInput[] | GeometrySubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: GeometryWhereInput | null
}

export interface GeometryUpdateDataInput {
  type?: GeometryType | null
  lineString?: LineStringUpdateOneInput | null
  point?: PointUpdateOneInput | null
  polygon?: PolygonUpdateOneInput | null
}

export interface GeometryUpdateInput {
  type?: GeometryType | null
  lineString?: LineStringUpdateOneInput | null
  point?: PointUpdateOneInput | null
  polygon?: PolygonUpdateOneInput | null
}

export interface GeometryUpdateManyMutationInput {
  type?: GeometryType | null
}

export interface GeometryUpdateOneRequiredInput {
  create?: GeometryCreateInput | null
  connect?: GeometryWhereUniqueInput | null
  update?: GeometryUpdateDataInput | null
  upsert?: GeometryUpsertNestedInput | null
}

export interface GeometryUpsertNestedInput {
  update: GeometryUpdateDataInput
  create: GeometryCreateInput
}

export interface GeometryWhereInput {
  AND?: GeometryWhereInput[] | GeometryWhereInput | null
  OR?: GeometryWhereInput[] | GeometryWhereInput | null
  NOT?: GeometryWhereInput[] | GeometryWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  type?: GeometryType | null
  type_not?: GeometryType | null
  type_in?: GeometryType[] | GeometryType | null
  type_not_in?: GeometryType[] | GeometryType | null
  lineString?: LineStringWhereInput | null
  point?: PointWhereInput | null
  polygon?: PolygonWhereInput | null
}

export interface GeometryWhereUniqueInput {
  id?: ID_Input | null
}

export interface LineStringCoordinateCreateInput {
  id?: ID_Input | null
  lng: Float
  lat: Float
}

export interface LineStringCoordinateCreateManyInput {
  create?: LineStringCoordinateCreateInput[] | LineStringCoordinateCreateInput | null
  connect?: LineStringCoordinateWhereUniqueInput[] | LineStringCoordinateWhereUniqueInput | null
}

export interface LineStringCoordinateScalarWhereInput {
  AND?: LineStringCoordinateScalarWhereInput[] | LineStringCoordinateScalarWhereInput | null
  OR?: LineStringCoordinateScalarWhereInput[] | LineStringCoordinateScalarWhereInput | null
  NOT?: LineStringCoordinateScalarWhereInput[] | LineStringCoordinateScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
}

export interface LineStringCoordinateSubscriptionWhereInput {
  AND?: LineStringCoordinateSubscriptionWhereInput[] | LineStringCoordinateSubscriptionWhereInput | null
  OR?: LineStringCoordinateSubscriptionWhereInput[] | LineStringCoordinateSubscriptionWhereInput | null
  NOT?: LineStringCoordinateSubscriptionWhereInput[] | LineStringCoordinateSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: LineStringCoordinateWhereInput | null
}

export interface LineStringCoordinateUpdateDataInput {
  lng?: Float | null
  lat?: Float | null
}

export interface LineStringCoordinateUpdateInput {
  lng?: Float | null
  lat?: Float | null
}

export interface LineStringCoordinateUpdateManyDataInput {
  lng?: Float | null
  lat?: Float | null
}

export interface LineStringCoordinateUpdateManyInput {
  create?: LineStringCoordinateCreateInput[] | LineStringCoordinateCreateInput | null
  connect?: LineStringCoordinateWhereUniqueInput[] | LineStringCoordinateWhereUniqueInput | null
  set?: LineStringCoordinateWhereUniqueInput[] | LineStringCoordinateWhereUniqueInput | null
  disconnect?: LineStringCoordinateWhereUniqueInput[] | LineStringCoordinateWhereUniqueInput | null
  delete?: LineStringCoordinateWhereUniqueInput[] | LineStringCoordinateWhereUniqueInput | null
  update?: LineStringCoordinateUpdateWithWhereUniqueNestedInput[] | LineStringCoordinateUpdateWithWhereUniqueNestedInput | null
  updateMany?: LineStringCoordinateUpdateManyWithWhereNestedInput[] | LineStringCoordinateUpdateManyWithWhereNestedInput | null
  deleteMany?: LineStringCoordinateScalarWhereInput[] | LineStringCoordinateScalarWhereInput | null
  upsert?: LineStringCoordinateUpsertWithWhereUniqueNestedInput[] | LineStringCoordinateUpsertWithWhereUniqueNestedInput | null
}

export interface LineStringCoordinateUpdateManyMutationInput {
  lng?: Float | null
  lat?: Float | null
}

export interface LineStringCoordinateUpdateManyWithWhereNestedInput {
  where: LineStringCoordinateScalarWhereInput
  data: LineStringCoordinateUpdateManyDataInput
}

export interface LineStringCoordinateUpdateWithWhereUniqueNestedInput {
  where: LineStringCoordinateWhereUniqueInput
  data: LineStringCoordinateUpdateDataInput
}

export interface LineStringCoordinateUpsertWithWhereUniqueNestedInput {
  where: LineStringCoordinateWhereUniqueInput
  update: LineStringCoordinateUpdateDataInput
  create: LineStringCoordinateCreateInput
}

export interface LineStringCoordinateWhereInput {
  AND?: LineStringCoordinateWhereInput[] | LineStringCoordinateWhereInput | null
  OR?: LineStringCoordinateWhereInput[] | LineStringCoordinateWhereInput | null
  NOT?: LineStringCoordinateWhereInput[] | LineStringCoordinateWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
}

export interface LineStringCoordinateWhereUniqueInput {
  id?: ID_Input | null
}

export interface LineStringCreateInput {
  id?: ID_Input | null
  coordinates?: LineStringCoordinateCreateManyInput | null
}

export interface LineStringCreateOneInput {
  create?: LineStringCreateInput | null
  connect?: LineStringWhereUniqueInput | null
}

export interface LineStringSubscriptionWhereInput {
  AND?: LineStringSubscriptionWhereInput[] | LineStringSubscriptionWhereInput | null
  OR?: LineStringSubscriptionWhereInput[] | LineStringSubscriptionWhereInput | null
  NOT?: LineStringSubscriptionWhereInput[] | LineStringSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: LineStringWhereInput | null
}

export interface LineStringUpdateDataInput {
  coordinates?: LineStringCoordinateUpdateManyInput | null
}

export interface LineStringUpdateInput {
  coordinates?: LineStringCoordinateUpdateManyInput | null
}

export interface LineStringUpdateOneInput {
  create?: LineStringCreateInput | null
  connect?: LineStringWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: LineStringUpdateDataInput | null
  upsert?: LineStringUpsertNestedInput | null
}

export interface LineStringUpsertNestedInput {
  update: LineStringUpdateDataInput
  create: LineStringCreateInput
}

export interface LineStringWhereInput {
  AND?: LineStringWhereInput[] | LineStringWhereInput | null
  OR?: LineStringWhereInput[] | LineStringWhereInput | null
  NOT?: LineStringWhereInput[] | LineStringWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  coordinates_every?: LineStringCoordinateWhereInput | null
  coordinates_some?: LineStringCoordinateWhereInput | null
  coordinates_none?: LineStringCoordinateWhereInput | null
}

export interface LineStringWhereUniqueInput {
  id?: ID_Input | null
}

export interface PointCreateInput {
  id?: ID_Input | null
  lng: Float
  lat: Float
}

export interface PointCreateOneInput {
  create?: PointCreateInput | null
  connect?: PointWhereUniqueInput | null
}

export interface PointSubscriptionWhereInput {
  AND?: PointSubscriptionWhereInput[] | PointSubscriptionWhereInput | null
  OR?: PointSubscriptionWhereInput[] | PointSubscriptionWhereInput | null
  NOT?: PointSubscriptionWhereInput[] | PointSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PointWhereInput | null
}

export interface PointUpdateDataInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PointUpdateInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PointUpdateManyMutationInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PointUpdateOneInput {
  create?: PointCreateInput | null
  connect?: PointWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: PointUpdateDataInput | null
  upsert?: PointUpsertNestedInput | null
}

export interface PointUpsertNestedInput {
  update: PointUpdateDataInput
  create: PointCreateInput
}

export interface PointWhereInput {
  AND?: PointWhereInput[] | PointWhereInput | null
  OR?: PointWhereInput[] | PointWhereInput | null
  NOT?: PointWhereInput[] | PointWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
}

export interface PointWhereUniqueInput {
  id?: ID_Input | null
}

export interface PolygonCreateInput {
  id?: ID_Input | null
  lines?: PolygonLineCreateManyInput | null
}

export interface PolygonCreateOneInput {
  create?: PolygonCreateInput | null
  connect?: PolygonWhereUniqueInput | null
}

export interface PolygonLineCoordinateCreateInput {
  id?: ID_Input | null
  lng: Float
  lat: Float
}

export interface PolygonLineCoordinateCreateManyInput {
  create?: PolygonLineCoordinateCreateInput[] | PolygonLineCoordinateCreateInput | null
  connect?: PolygonLineCoordinateWhereUniqueInput[] | PolygonLineCoordinateWhereUniqueInput | null
}

export interface PolygonLineCoordinateScalarWhereInput {
  AND?: PolygonLineCoordinateScalarWhereInput[] | PolygonLineCoordinateScalarWhereInput | null
  OR?: PolygonLineCoordinateScalarWhereInput[] | PolygonLineCoordinateScalarWhereInput | null
  NOT?: PolygonLineCoordinateScalarWhereInput[] | PolygonLineCoordinateScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
}

export interface PolygonLineCoordinateSubscriptionWhereInput {
  AND?: PolygonLineCoordinateSubscriptionWhereInput[] | PolygonLineCoordinateSubscriptionWhereInput | null
  OR?: PolygonLineCoordinateSubscriptionWhereInput[] | PolygonLineCoordinateSubscriptionWhereInput | null
  NOT?: PolygonLineCoordinateSubscriptionWhereInput[] | PolygonLineCoordinateSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PolygonLineCoordinateWhereInput | null
}

export interface PolygonLineCoordinateUpdateDataInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PolygonLineCoordinateUpdateInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PolygonLineCoordinateUpdateManyDataInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PolygonLineCoordinateUpdateManyInput {
  create?: PolygonLineCoordinateCreateInput[] | PolygonLineCoordinateCreateInput | null
  connect?: PolygonLineCoordinateWhereUniqueInput[] | PolygonLineCoordinateWhereUniqueInput | null
  set?: PolygonLineCoordinateWhereUniqueInput[] | PolygonLineCoordinateWhereUniqueInput | null
  disconnect?: PolygonLineCoordinateWhereUniqueInput[] | PolygonLineCoordinateWhereUniqueInput | null
  delete?: PolygonLineCoordinateWhereUniqueInput[] | PolygonLineCoordinateWhereUniqueInput | null
  update?: PolygonLineCoordinateUpdateWithWhereUniqueNestedInput[] | PolygonLineCoordinateUpdateWithWhereUniqueNestedInput | null
  updateMany?: PolygonLineCoordinateUpdateManyWithWhereNestedInput[] | PolygonLineCoordinateUpdateManyWithWhereNestedInput | null
  deleteMany?: PolygonLineCoordinateScalarWhereInput[] | PolygonLineCoordinateScalarWhereInput | null
  upsert?: PolygonLineCoordinateUpsertWithWhereUniqueNestedInput[] | PolygonLineCoordinateUpsertWithWhereUniqueNestedInput | null
}

export interface PolygonLineCoordinateUpdateManyMutationInput {
  lng?: Float | null
  lat?: Float | null
}

export interface PolygonLineCoordinateUpdateManyWithWhereNestedInput {
  where: PolygonLineCoordinateScalarWhereInput
  data: PolygonLineCoordinateUpdateManyDataInput
}

export interface PolygonLineCoordinateUpdateWithWhereUniqueNestedInput {
  where: PolygonLineCoordinateWhereUniqueInput
  data: PolygonLineCoordinateUpdateDataInput
}

export interface PolygonLineCoordinateUpsertWithWhereUniqueNestedInput {
  where: PolygonLineCoordinateWhereUniqueInput
  update: PolygonLineCoordinateUpdateDataInput
  create: PolygonLineCoordinateCreateInput
}

export interface PolygonLineCoordinateWhereInput {
  AND?: PolygonLineCoordinateWhereInput[] | PolygonLineCoordinateWhereInput | null
  OR?: PolygonLineCoordinateWhereInput[] | PolygonLineCoordinateWhereInput | null
  NOT?: PolygonLineCoordinateWhereInput[] | PolygonLineCoordinateWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lng?: Float | null
  lng_not?: Float | null
  lng_in?: Float[] | Float | null
  lng_not_in?: Float[] | Float | null
  lng_lt?: Float | null
  lng_lte?: Float | null
  lng_gt?: Float | null
  lng_gte?: Float | null
  lat?: Float | null
  lat_not?: Float | null
  lat_in?: Float[] | Float | null
  lat_not_in?: Float[] | Float | null
  lat_lt?: Float | null
  lat_lte?: Float | null
  lat_gt?: Float | null
  lat_gte?: Float | null
}

export interface PolygonLineCoordinateWhereUniqueInput {
  id?: ID_Input | null
}

export interface PolygonLineCreateInput {
  id?: ID_Input | null
  coordinates?: PolygonLineCoordinateCreateManyInput | null
}

export interface PolygonLineCreateManyInput {
  create?: PolygonLineCreateInput[] | PolygonLineCreateInput | null
  connect?: PolygonLineWhereUniqueInput[] | PolygonLineWhereUniqueInput | null
}

export interface PolygonLineScalarWhereInput {
  AND?: PolygonLineScalarWhereInput[] | PolygonLineScalarWhereInput | null
  OR?: PolygonLineScalarWhereInput[] | PolygonLineScalarWhereInput | null
  NOT?: PolygonLineScalarWhereInput[] | PolygonLineScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
}

export interface PolygonLineSubscriptionWhereInput {
  AND?: PolygonLineSubscriptionWhereInput[] | PolygonLineSubscriptionWhereInput | null
  OR?: PolygonLineSubscriptionWhereInput[] | PolygonLineSubscriptionWhereInput | null
  NOT?: PolygonLineSubscriptionWhereInput[] | PolygonLineSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PolygonLineWhereInput | null
}

export interface PolygonLineUpdateDataInput {
  coordinates?: PolygonLineCoordinateUpdateManyInput | null
}

export interface PolygonLineUpdateInput {
  coordinates?: PolygonLineCoordinateUpdateManyInput | null
}

export interface PolygonLineUpdateManyInput {
  create?: PolygonLineCreateInput[] | PolygonLineCreateInput | null
  connect?: PolygonLineWhereUniqueInput[] | PolygonLineWhereUniqueInput | null
  set?: PolygonLineWhereUniqueInput[] | PolygonLineWhereUniqueInput | null
  disconnect?: PolygonLineWhereUniqueInput[] | PolygonLineWhereUniqueInput | null
  delete?: PolygonLineWhereUniqueInput[] | PolygonLineWhereUniqueInput | null
  update?: PolygonLineUpdateWithWhereUniqueNestedInput[] | PolygonLineUpdateWithWhereUniqueNestedInput | null
  deleteMany?: PolygonLineScalarWhereInput[] | PolygonLineScalarWhereInput | null
  upsert?: PolygonLineUpsertWithWhereUniqueNestedInput[] | PolygonLineUpsertWithWhereUniqueNestedInput | null
}

export interface PolygonLineUpdateWithWhereUniqueNestedInput {
  where: PolygonLineWhereUniqueInput
  data: PolygonLineUpdateDataInput
}

export interface PolygonLineUpsertWithWhereUniqueNestedInput {
  where: PolygonLineWhereUniqueInput
  update: PolygonLineUpdateDataInput
  create: PolygonLineCreateInput
}

export interface PolygonLineWhereInput {
  AND?: PolygonLineWhereInput[] | PolygonLineWhereInput | null
  OR?: PolygonLineWhereInput[] | PolygonLineWhereInput | null
  NOT?: PolygonLineWhereInput[] | PolygonLineWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  coordinates_every?: PolygonLineCoordinateWhereInput | null
  coordinates_some?: PolygonLineCoordinateWhereInput | null
  coordinates_none?: PolygonLineCoordinateWhereInput | null
}

export interface PolygonLineWhereUniqueInput {
  id?: ID_Input | null
}

export interface PolygonSubscriptionWhereInput {
  AND?: PolygonSubscriptionWhereInput[] | PolygonSubscriptionWhereInput | null
  OR?: PolygonSubscriptionWhereInput[] | PolygonSubscriptionWhereInput | null
  NOT?: PolygonSubscriptionWhereInput[] | PolygonSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PolygonWhereInput | null
}

export interface PolygonUpdateDataInput {
  lines?: PolygonLineUpdateManyInput | null
}

export interface PolygonUpdateInput {
  lines?: PolygonLineUpdateManyInput | null
}

export interface PolygonUpdateOneInput {
  create?: PolygonCreateInput | null
  connect?: PolygonWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: PolygonUpdateDataInput | null
  upsert?: PolygonUpsertNestedInput | null
}

export interface PolygonUpsertNestedInput {
  update: PolygonUpdateDataInput
  create: PolygonCreateInput
}

export interface PolygonWhereInput {
  AND?: PolygonWhereInput[] | PolygonWhereInput | null
  OR?: PolygonWhereInput[] | PolygonWhereInput | null
  NOT?: PolygonWhereInput[] | PolygonWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  lines_every?: PolygonLineWhereInput | null
  lines_some?: PolygonLineWhereInput | null
  lines_none?: PolygonLineWhereInput | null
}

export interface PolygonWhereUniqueInput {
  id?: ID_Input | null
}

export interface PricePeriodCreateInput {
  id?: ID_Input | null
  startTime: DateTime
  endTime: DateTime
  startDate: DateTime
  endDate?: DateTime | null
  repeat: TimePeriod
  price: Int
  priceUnit: TimePeriod
}

export interface PricePeriodSubscriptionWhereInput {
  AND?: PricePeriodSubscriptionWhereInput[] | PricePeriodSubscriptionWhereInput | null
  OR?: PricePeriodSubscriptionWhereInput[] | PricePeriodSubscriptionWhereInput | null
  NOT?: PricePeriodSubscriptionWhereInput[] | PricePeriodSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PricePeriodWhereInput | null
}

export interface PricePeriodUpdateInput {
  startTime?: DateTime | null
  endTime?: DateTime | null
  startDate?: DateTime | null
  endDate?: DateTime | null
  repeat?: TimePeriod | null
  price?: Int | null
  priceUnit?: TimePeriod | null
}

export interface PricePeriodUpdateManyMutationInput {
  startTime?: DateTime | null
  endTime?: DateTime | null
  startDate?: DateTime | null
  endDate?: DateTime | null
  repeat?: TimePeriod | null
  price?: Int | null
  priceUnit?: TimePeriod | null
}

export interface PricePeriodWhereInput {
  AND?: PricePeriodWhereInput[] | PricePeriodWhereInput | null
  OR?: PricePeriodWhereInput[] | PricePeriodWhereInput | null
  NOT?: PricePeriodWhereInput[] | PricePeriodWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  startTime?: DateTime | null
  startTime_not?: DateTime | null
  startTime_in?: DateTime[] | DateTime | null
  startTime_not_in?: DateTime[] | DateTime | null
  startTime_lt?: DateTime | null
  startTime_lte?: DateTime | null
  startTime_gt?: DateTime | null
  startTime_gte?: DateTime | null
  endTime?: DateTime | null
  endTime_not?: DateTime | null
  endTime_in?: DateTime[] | DateTime | null
  endTime_not_in?: DateTime[] | DateTime | null
  endTime_lt?: DateTime | null
  endTime_lte?: DateTime | null
  endTime_gt?: DateTime | null
  endTime_gte?: DateTime | null
  startDate?: DateTime | null
  startDate_not?: DateTime | null
  startDate_in?: DateTime[] | DateTime | null
  startDate_not_in?: DateTime[] | DateTime | null
  startDate_lt?: DateTime | null
  startDate_lte?: DateTime | null
  startDate_gt?: DateTime | null
  startDate_gte?: DateTime | null
  endDate?: DateTime | null
  endDate_not?: DateTime | null
  endDate_in?: DateTime[] | DateTime | null
  endDate_not_in?: DateTime[] | DateTime | null
  endDate_lt?: DateTime | null
  endDate_lte?: DateTime | null
  endDate_gt?: DateTime | null
  endDate_gte?: DateTime | null
  repeat?: TimePeriod | null
  repeat_not?: TimePeriod | null
  repeat_in?: TimePeriod[] | TimePeriod | null
  repeat_not_in?: TimePeriod[] | TimePeriod | null
  price?: Int | null
  price_not?: Int | null
  price_in?: Int[] | Int | null
  price_not_in?: Int[] | Int | null
  price_lt?: Int | null
  price_lte?: Int | null
  price_gt?: Int | null
  price_gte?: Int | null
  priceUnit?: TimePeriod | null
  priceUnit_not?: TimePeriod | null
  priceUnit_in?: TimePeriod[] | TimePeriod | null
  priceUnit_not_in?: TimePeriod[] | TimePeriod | null
}

export interface PricePeriodWhereUniqueInput {
  id?: ID_Input | null
}

export interface PropertyChangeSetCreateInput {
  id?: ID_Input | null
  version?: Int | null
  value?: String | null
  changedBy?: UserCreateOneInput | null
}

export interface PropertyChangeSetCreateManyInput {
  create?: PropertyChangeSetCreateInput[] | PropertyChangeSetCreateInput | null
  connect?: PropertyChangeSetWhereUniqueInput[] | PropertyChangeSetWhereUniqueInput | null
}

export interface PropertyChangeSetScalarWhereInput {
  AND?: PropertyChangeSetScalarWhereInput[] | PropertyChangeSetScalarWhereInput | null
  OR?: PropertyChangeSetScalarWhereInput[] | PropertyChangeSetScalarWhereInput | null
  NOT?: PropertyChangeSetScalarWhereInput[] | PropertyChangeSetScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  version?: Int | null
  version_not?: Int | null
  version_in?: Int[] | Int | null
  version_not_in?: Int[] | Int | null
  version_lt?: Int | null
  version_lte?: Int | null
  version_gt?: Int | null
  version_gte?: Int | null
  value?: String | null
  value_not?: String | null
  value_in?: String[] | String | null
  value_not_in?: String[] | String | null
  value_lt?: String | null
  value_lte?: String | null
  value_gt?: String | null
  value_gte?: String | null
  value_contains?: String | null
  value_not_contains?: String | null
  value_starts_with?: String | null
  value_not_starts_with?: String | null
  value_ends_with?: String | null
  value_not_ends_with?: String | null
}

export interface PropertyChangeSetSubscriptionWhereInput {
  AND?: PropertyChangeSetSubscriptionWhereInput[] | PropertyChangeSetSubscriptionWhereInput | null
  OR?: PropertyChangeSetSubscriptionWhereInput[] | PropertyChangeSetSubscriptionWhereInput | null
  NOT?: PropertyChangeSetSubscriptionWhereInput[] | PropertyChangeSetSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PropertyChangeSetWhereInput | null
}

export interface PropertyChangeSetUpdateDataInput {
  version?: Int | null
  value?: String | null
  changedBy?: UserUpdateOneInput | null
}

export interface PropertyChangeSetUpdateInput {
  version?: Int | null
  value?: String | null
  changedBy?: UserUpdateOneInput | null
}

export interface PropertyChangeSetUpdateManyDataInput {
  version?: Int | null
  value?: String | null
}

export interface PropertyChangeSetUpdateManyInput {
  create?: PropertyChangeSetCreateInput[] | PropertyChangeSetCreateInput | null
  connect?: PropertyChangeSetWhereUniqueInput[] | PropertyChangeSetWhereUniqueInput | null
  set?: PropertyChangeSetWhereUniqueInput[] | PropertyChangeSetWhereUniqueInput | null
  disconnect?: PropertyChangeSetWhereUniqueInput[] | PropertyChangeSetWhereUniqueInput | null
  delete?: PropertyChangeSetWhereUniqueInput[] | PropertyChangeSetWhereUniqueInput | null
  update?: PropertyChangeSetUpdateWithWhereUniqueNestedInput[] | PropertyChangeSetUpdateWithWhereUniqueNestedInput | null
  updateMany?: PropertyChangeSetUpdateManyWithWhereNestedInput[] | PropertyChangeSetUpdateManyWithWhereNestedInput | null
  deleteMany?: PropertyChangeSetScalarWhereInput[] | PropertyChangeSetScalarWhereInput | null
  upsert?: PropertyChangeSetUpsertWithWhereUniqueNestedInput[] | PropertyChangeSetUpsertWithWhereUniqueNestedInput | null
}

export interface PropertyChangeSetUpdateManyMutationInput {
  version?: Int | null
  value?: String | null
}

export interface PropertyChangeSetUpdateManyWithWhereNestedInput {
  where: PropertyChangeSetScalarWhereInput
  data: PropertyChangeSetUpdateManyDataInput
}

export interface PropertyChangeSetUpdateWithWhereUniqueNestedInput {
  where: PropertyChangeSetWhereUniqueInput
  data: PropertyChangeSetUpdateDataInput
}

export interface PropertyChangeSetUpsertWithWhereUniqueNestedInput {
  where: PropertyChangeSetWhereUniqueInput
  update: PropertyChangeSetUpdateDataInput
  create: PropertyChangeSetCreateInput
}

export interface PropertyChangeSetWhereInput {
  AND?: PropertyChangeSetWhereInput[] | PropertyChangeSetWhereInput | null
  OR?: PropertyChangeSetWhereInput[] | PropertyChangeSetWhereInput | null
  NOT?: PropertyChangeSetWhereInput[] | PropertyChangeSetWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  version?: Int | null
  version_not?: Int | null
  version_in?: Int[] | Int | null
  version_not_in?: Int[] | Int | null
  version_lt?: Int | null
  version_lte?: Int | null
  version_gt?: Int | null
  version_gte?: Int | null
  value?: String | null
  value_not?: String | null
  value_in?: String[] | String | null
  value_not_in?: String[] | String | null
  value_lt?: String | null
  value_lte?: String | null
  value_gt?: String | null
  value_gte?: String | null
  value_contains?: String | null
  value_not_contains?: String | null
  value_starts_with?: String | null
  value_not_starts_with?: String | null
  value_ends_with?: String | null
  value_not_ends_with?: String | null
  changedBy?: UserWhereInput | null
}

export interface PropertyChangeSetWhereUniqueInput {
  id?: ID_Input | null
}

export interface PropertyCreateInput {
  id?: ID_Input | null
  tags?: TagCreateManyInput | null
  address?: AddressCreateOneInput | null
  changeSet?: PropertyChangeSetCreateManyInput | null
}

export interface PropertyCreateOneInput {
  create?: PropertyCreateInput | null
  connect?: PropertyWhereUniqueInput | null
}

export interface PropertySubscriptionWhereInput {
  AND?: PropertySubscriptionWhereInput[] | PropertySubscriptionWhereInput | null
  OR?: PropertySubscriptionWhereInput[] | PropertySubscriptionWhereInput | null
  NOT?: PropertySubscriptionWhereInput[] | PropertySubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: PropertyWhereInput | null
}

export interface PropertyUpdateDataInput {
  tags?: TagUpdateManyInput | null
  address?: AddressUpdateOneInput | null
  changeSet?: PropertyChangeSetUpdateManyInput | null
}

export interface PropertyUpdateInput {
  tags?: TagUpdateManyInput | null
  address?: AddressUpdateOneInput | null
  changeSet?: PropertyChangeSetUpdateManyInput | null
}

export interface PropertyUpdateOneRequiredInput {
  create?: PropertyCreateInput | null
  connect?: PropertyWhereUniqueInput | null
  update?: PropertyUpdateDataInput | null
  upsert?: PropertyUpsertNestedInput | null
}

export interface PropertyUpsertNestedInput {
  update: PropertyUpdateDataInput
  create: PropertyCreateInput
}

export interface PropertyWhereInput {
  AND?: PropertyWhereInput[] | PropertyWhereInput | null
  OR?: PropertyWhereInput[] | PropertyWhereInput | null
  NOT?: PropertyWhereInput[] | PropertyWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  createdAt?: DateTime | null
  createdAt_not?: DateTime | null
  createdAt_in?: DateTime[] | DateTime | null
  createdAt_not_in?: DateTime[] | DateTime | null
  createdAt_lt?: DateTime | null
  createdAt_lte?: DateTime | null
  createdAt_gt?: DateTime | null
  createdAt_gte?: DateTime | null
  updatedAt?: DateTime | null
  updatedAt_not?: DateTime | null
  updatedAt_in?: DateTime[] | DateTime | null
  updatedAt_not_in?: DateTime[] | DateTime | null
  updatedAt_lt?: DateTime | null
  updatedAt_lte?: DateTime | null
  updatedAt_gt?: DateTime | null
  updatedAt_gte?: DateTime | null
  tags_every?: TagWhereInput | null
  tags_some?: TagWhereInput | null
  tags_none?: TagWhereInput | null
  address?: AddressWhereInput | null
  changeSet_every?: PropertyChangeSetWhereInput | null
  changeSet_some?: PropertyChangeSetWhereInput | null
  changeSet_none?: PropertyChangeSetWhereInput | null
}

export interface PropertyWhereUniqueInput {
  id?: ID_Input | null
}

export interface TagCreateInput {
  id?: ID_Input | null
  key: TagKeyCreateOneInput
  value: TagValueCreateOneInput
}

export interface TagCreateManyInput {
  create?: TagCreateInput[] | TagCreateInput | null
  connect?: TagWhereUniqueInput[] | TagWhereUniqueInput | null
}

export interface TagKeyCreateInput {
  id?: ID_Input | null
  value: String
}

export interface TagKeyCreateOneInput {
  create?: TagKeyCreateInput | null
  connect?: TagKeyWhereUniqueInput | null
}

export interface TagKeySubscriptionWhereInput {
  AND?: TagKeySubscriptionWhereInput[] | TagKeySubscriptionWhereInput | null
  OR?: TagKeySubscriptionWhereInput[] | TagKeySubscriptionWhereInput | null
  NOT?: TagKeySubscriptionWhereInput[] | TagKeySubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: TagKeyWhereInput | null
}

export interface TagKeyUpdateDataInput {
  value?: String | null
}

export interface TagKeyUpdateInput {
  value?: String | null
}

export interface TagKeyUpdateManyMutationInput {
  value?: String | null
}

export interface TagKeyUpdateOneRequiredInput {
  create?: TagKeyCreateInput | null
  connect?: TagKeyWhereUniqueInput | null
  update?: TagKeyUpdateDataInput | null
  upsert?: TagKeyUpsertNestedInput | null
}

export interface TagKeyUpsertNestedInput {
  update: TagKeyUpdateDataInput
  create: TagKeyCreateInput
}

export interface TagKeyWhereInput {
  AND?: TagKeyWhereInput[] | TagKeyWhereInput | null
  OR?: TagKeyWhereInput[] | TagKeyWhereInput | null
  NOT?: TagKeyWhereInput[] | TagKeyWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  value?: String | null
  value_not?: String | null
  value_in?: String[] | String | null
  value_not_in?: String[] | String | null
  value_lt?: String | null
  value_lte?: String | null
  value_gt?: String | null
  value_gte?: String | null
  value_contains?: String | null
  value_not_contains?: String | null
  value_starts_with?: String | null
  value_not_starts_with?: String | null
  value_ends_with?: String | null
  value_not_ends_with?: String | null
}

export interface TagKeyWhereUniqueInput {
  id?: ID_Input | null
  value?: String | null
}

export interface TagScalarWhereInput {
  AND?: TagScalarWhereInput[] | TagScalarWhereInput | null
  OR?: TagScalarWhereInput[] | TagScalarWhereInput | null
  NOT?: TagScalarWhereInput[] | TagScalarWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
}

export interface TagSubscriptionWhereInput {
  AND?: TagSubscriptionWhereInput[] | TagSubscriptionWhereInput | null
  OR?: TagSubscriptionWhereInput[] | TagSubscriptionWhereInput | null
  NOT?: TagSubscriptionWhereInput[] | TagSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: TagWhereInput | null
}

export interface TagUpdateDataInput {
  key?: TagKeyUpdateOneRequiredInput | null
  value?: TagValueUpdateOneRequiredInput | null
}

export interface TagUpdateInput {
  key?: TagKeyUpdateOneRequiredInput | null
  value?: TagValueUpdateOneRequiredInput | null
}

export interface TagUpdateManyInput {
  create?: TagCreateInput[] | TagCreateInput | null
  connect?: TagWhereUniqueInput[] | TagWhereUniqueInput | null
  set?: TagWhereUniqueInput[] | TagWhereUniqueInput | null
  disconnect?: TagWhereUniqueInput[] | TagWhereUniqueInput | null
  delete?: TagWhereUniqueInput[] | TagWhereUniqueInput | null
  update?: TagUpdateWithWhereUniqueNestedInput[] | TagUpdateWithWhereUniqueNestedInput | null
  deleteMany?: TagScalarWhereInput[] | TagScalarWhereInput | null
  upsert?: TagUpsertWithWhereUniqueNestedInput[] | TagUpsertWithWhereUniqueNestedInput | null
}

export interface TagUpdateWithWhereUniqueNestedInput {
  where: TagWhereUniqueInput
  data: TagUpdateDataInput
}

export interface TagUpsertWithWhereUniqueNestedInput {
  where: TagWhereUniqueInput
  update: TagUpdateDataInput
  create: TagCreateInput
}

export interface TagValueCreateInput {
  id?: ID_Input | null
  value: String
}

export interface TagValueCreateOneInput {
  create?: TagValueCreateInput | null
  connect?: TagValueWhereUniqueInput | null
}

export interface TagValueSubscriptionWhereInput {
  AND?: TagValueSubscriptionWhereInput[] | TagValueSubscriptionWhereInput | null
  OR?: TagValueSubscriptionWhereInput[] | TagValueSubscriptionWhereInput | null
  NOT?: TagValueSubscriptionWhereInput[] | TagValueSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: TagValueWhereInput | null
}

export interface TagValueUpdateDataInput {
  value?: String | null
}

export interface TagValueUpdateInput {
  value?: String | null
}

export interface TagValueUpdateManyMutationInput {
  value?: String | null
}

export interface TagValueUpdateOneRequiredInput {
  create?: TagValueCreateInput | null
  connect?: TagValueWhereUniqueInput | null
  update?: TagValueUpdateDataInput | null
  upsert?: TagValueUpsertNestedInput | null
}

export interface TagValueUpsertNestedInput {
  update: TagValueUpdateDataInput
  create: TagValueCreateInput
}

export interface TagValueWhereInput {
  AND?: TagValueWhereInput[] | TagValueWhereInput | null
  OR?: TagValueWhereInput[] | TagValueWhereInput | null
  NOT?: TagValueWhereInput[] | TagValueWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  value?: String | null
  value_not?: String | null
  value_in?: String[] | String | null
  value_not_in?: String[] | String | null
  value_lt?: String | null
  value_lte?: String | null
  value_gt?: String | null
  value_gte?: String | null
  value_contains?: String | null
  value_not_contains?: String | null
  value_starts_with?: String | null
  value_not_starts_with?: String | null
  value_ends_with?: String | null
  value_not_ends_with?: String | null
}

export interface TagValueWhereUniqueInput {
  id?: ID_Input | null
  value?: String | null
}

export interface TagWhereInput {
  AND?: TagWhereInput[] | TagWhereInput | null
  OR?: TagWhereInput[] | TagWhereInput | null
  NOT?: TagWhereInput[] | TagWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  key?: TagKeyWhereInput | null
  value?: TagValueWhereInput | null
}

export interface TagWhereUniqueInput {
  id?: ID_Input | null
}

export interface UserCreateInput {
  id?: ID_Input | null
  name: String
  lastName?: String | null
  email: String
  password: String
  token?: String | null
  isActive?: Boolean | null
  registrationToken?: String | null
  roles?: UserCreaterolesInput | null
}

export interface UserCreateOneInput {
  create?: UserCreateInput | null
  connect?: UserWhereUniqueInput | null
}

export interface UserCreaterolesInput {
  set?: Roles[] | Roles | null
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null
  mutation_in?: MutationType[] | MutationType | null
  updatedFields_contains?: String | null
  updatedFields_contains_every?: String[] | String | null
  updatedFields_contains_some?: String[] | String | null
  node?: UserWhereInput | null
}

export interface UserUpdateDataInput {
  name?: String | null
  lastName?: String | null
  email?: String | null
  password?: String | null
  token?: String | null
  isActive?: Boolean | null
  registrationToken?: String | null
  roles?: UserUpdaterolesInput | null
}

export interface UserUpdateInput {
  name?: String | null
  lastName?: String | null
  email?: String | null
  password?: String | null
  token?: String | null
  isActive?: Boolean | null
  registrationToken?: String | null
  roles?: UserUpdaterolesInput | null
}

export interface UserUpdateManyMutationInput {
  name?: String | null
  lastName?: String | null
  email?: String | null
  password?: String | null
  token?: String | null
  isActive?: Boolean | null
  registrationToken?: String | null
  roles?: UserUpdaterolesInput | null
}

export interface UserUpdateOneInput {
  create?: UserCreateInput | null
  connect?: UserWhereUniqueInput | null
  disconnect?: Boolean | null
  delete?: Boolean | null
  update?: UserUpdateDataInput | null
  upsert?: UserUpsertNestedInput | null
}

export interface UserUpdaterolesInput {
  set?: Roles[] | Roles | null
}

export interface UserUpsertNestedInput {
  update: UserUpdateDataInput
  create: UserCreateInput
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput | null
  OR?: UserWhereInput[] | UserWhereInput | null
  NOT?: UserWhereInput[] | UserWhereInput | null
  id?: ID_Input | null
  id_not?: ID_Input | null
  id_in?: ID_Output[] | ID_Output | null
  id_not_in?: ID_Output[] | ID_Output | null
  id_lt?: ID_Input | null
  id_lte?: ID_Input | null
  id_gt?: ID_Input | null
  id_gte?: ID_Input | null
  id_contains?: ID_Input | null
  id_not_contains?: ID_Input | null
  id_starts_with?: ID_Input | null
  id_not_starts_with?: ID_Input | null
  id_ends_with?: ID_Input | null
  id_not_ends_with?: ID_Input | null
  name?: String | null
  name_not?: String | null
  name_in?: String[] | String | null
  name_not_in?: String[] | String | null
  name_lt?: String | null
  name_lte?: String | null
  name_gt?: String | null
  name_gte?: String | null
  name_contains?: String | null
  name_not_contains?: String | null
  name_starts_with?: String | null
  name_not_starts_with?: String | null
  name_ends_with?: String | null
  name_not_ends_with?: String | null
  lastName?: String | null
  lastName_not?: String | null
  lastName_in?: String[] | String | null
  lastName_not_in?: String[] | String | null
  lastName_lt?: String | null
  lastName_lte?: String | null
  lastName_gt?: String | null
  lastName_gte?: String | null
  lastName_contains?: String | null
  lastName_not_contains?: String | null
  lastName_starts_with?: String | null
  lastName_not_starts_with?: String | null
  lastName_ends_with?: String | null
  lastName_not_ends_with?: String | null
  email?: String | null
  email_not?: String | null
  email_in?: String[] | String | null
  email_not_in?: String[] | String | null
  email_lt?: String | null
  email_lte?: String | null
  email_gt?: String | null
  email_gte?: String | null
  email_contains?: String | null
  email_not_contains?: String | null
  email_starts_with?: String | null
  email_not_starts_with?: String | null
  email_ends_with?: String | null
  email_not_ends_with?: String | null
  password?: String | null
  password_not?: String | null
  password_in?: String[] | String | null
  password_not_in?: String[] | String | null
  password_lt?: String | null
  password_lte?: String | null
  password_gt?: String | null
  password_gte?: String | null
  password_contains?: String | null
  password_not_contains?: String | null
  password_starts_with?: String | null
  password_not_starts_with?: String | null
  password_ends_with?: String | null
  password_not_ends_with?: String | null
  token?: String | null
  token_not?: String | null
  token_in?: String[] | String | null
  token_not_in?: String[] | String | null
  token_lt?: String | null
  token_lte?: String | null
  token_gt?: String | null
  token_gte?: String | null
  token_contains?: String | null
  token_not_contains?: String | null
  token_starts_with?: String | null
  token_not_starts_with?: String | null
  token_ends_with?: String | null
  token_not_ends_with?: String | null
  isActive?: Boolean | null
  isActive_not?: Boolean | null
  registrationToken?: String | null
  registrationToken_not?: String | null
  registrationToken_in?: String[] | String | null
  registrationToken_not_in?: String[] | String | null
  registrationToken_lt?: String | null
  registrationToken_lte?: String | null
  registrationToken_gt?: String | null
  registrationToken_gte?: String | null
  registrationToken_contains?: String | null
  registrationToken_not_contains?: String | null
  registrationToken_starts_with?: String | null
  registrationToken_not_starts_with?: String | null
  registrationToken_ends_with?: String | null
  registrationToken_not_ends_with?: String | null
}

export interface UserWhereUniqueInput {
  id?: ID_Input | null
  email?: String | null
  registrationToken?: String | null
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output
}

export interface Address extends Node {
  id: ID_Output
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

/*
 * A connection to a list of items.

 */
export interface AddressConnection {
  pageInfo: PageInfo
  edges: Array<AddressEdge | null>
  aggregate: AggregateAddress
}

/*
 * An edge in a connection.

 */
export interface AddressEdge {
  node: Address
  cursor: String
}

export interface AddressPreviousValues {
  id: ID_Output
  uuid: String
  street: String
  houseNumber: String
  city: String
  countryCode: CountryCode
  lng: Float
  lat: Float
  cityDistrict?: String | null
  continent?: String | null
  neighbourhood?: String | null
  postcode?: Int | null
  state?: String | null
  suburb?: String | null
  road?: String | null
  importance?: Float | null
}

export interface AddressSubscriptionPayload {
  mutation: MutationType
  node?: Address | null
  updatedFields?: Array<String> | null
  previousValues?: AddressPreviousValues | null
}

export interface AggregateAddress {
  count: Int
}

export interface AggregateFeature {
  count: Int
}

export interface AggregateGeometry {
  count: Int
}

export interface AggregateLineString {
  count: Int
}

export interface AggregateLineStringCoordinate {
  count: Int
}

export interface AggregatePoint {
  count: Int
}

export interface AggregatePolygon {
  count: Int
}

export interface AggregatePolygonLine {
  count: Int
}

export interface AggregatePolygonLineCoordinate {
  count: Int
}

export interface AggregatePricePeriod {
  count: Int
}

export interface AggregateProperty {
  count: Int
}

export interface AggregatePropertyChangeSet {
  count: Int
}

export interface AggregateTag {
  count: Int
}

export interface AggregateTagKey {
  count: Int
}

export interface AggregateTagValue {
  count: Int
}

export interface AggregateUser {
  count: Int
}

export interface BatchPayload {
  count: Long
}

export interface Feature extends Node {
  id: ID_Output
  type: FeatureType
  geometry: Geometry
  properties: Property
  createdAt: DateTime
  updatedAt: DateTime
}

/*
 * A connection to a list of items.

 */
export interface FeatureConnection {
  pageInfo: PageInfo
  edges: Array<FeatureEdge | null>
  aggregate: AggregateFeature
}

/*
 * An edge in a connection.

 */
export interface FeatureEdge {
  node: Feature
  cursor: String
}

export interface FeaturePreviousValues {
  id: ID_Output
  type: FeatureType
  createdAt: DateTime
  updatedAt: DateTime
}

export interface FeatureSubscriptionPayload {
  mutation: MutationType
  node?: Feature | null
  updatedFields?: Array<String> | null
  previousValues?: FeaturePreviousValues | null
}

export interface Geometry extends Node {
  id: ID_Output
  type: GeometryType
  lineString?: LineString | null
  point?: Point | null
  polygon?: Polygon | null
}

/*
 * A connection to a list of items.

 */
export interface GeometryConnection {
  pageInfo: PageInfo
  edges: Array<GeometryEdge | null>
  aggregate: AggregateGeometry
}

/*
 * An edge in a connection.

 */
export interface GeometryEdge {
  node: Geometry
  cursor: String
}

export interface GeometryPreviousValues {
  id: ID_Output
  type: GeometryType
}

export interface GeometrySubscriptionPayload {
  mutation: MutationType
  node?: Geometry | null
  updatedFields?: Array<String> | null
  previousValues?: GeometryPreviousValues | null
}

export interface LineString extends Node {
  id: ID_Output
  coordinates?: Array<LineStringCoordinate> | null
}

/*
 * A connection to a list of items.

 */
export interface LineStringConnection {
  pageInfo: PageInfo
  edges: Array<LineStringEdge | null>
  aggregate: AggregateLineString
}

export interface LineStringCoordinate extends Node {
  id: ID_Output
  lng: Float
  lat: Float
}

/*
 * A connection to a list of items.

 */
export interface LineStringCoordinateConnection {
  pageInfo: PageInfo
  edges: Array<LineStringCoordinateEdge | null>
  aggregate: AggregateLineStringCoordinate
}

/*
 * An edge in a connection.

 */
export interface LineStringCoordinateEdge {
  node: LineStringCoordinate
  cursor: String
}

export interface LineStringCoordinatePreviousValues {
  id: ID_Output
  lng: Float
  lat: Float
}

export interface LineStringCoordinateSubscriptionPayload {
  mutation: MutationType
  node?: LineStringCoordinate | null
  updatedFields?: Array<String> | null
  previousValues?: LineStringCoordinatePreviousValues | null
}

/*
 * An edge in a connection.

 */
export interface LineStringEdge {
  node: LineString
  cursor: String
}

export interface LineStringPreviousValues {
  id: ID_Output
}

export interface LineStringSubscriptionPayload {
  mutation: MutationType
  node?: LineString | null
  updatedFields?: Array<String> | null
  previousValues?: LineStringPreviousValues | null
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean
  hasPreviousPage: Boolean
  startCursor?: String | null
  endCursor?: String | null
}

export interface Point extends Node {
  id: ID_Output
  lng: Float
  lat: Float
}

/*
 * A connection to a list of items.

 */
export interface PointConnection {
  pageInfo: PageInfo
  edges: Array<PointEdge | null>
  aggregate: AggregatePoint
}

/*
 * An edge in a connection.

 */
export interface PointEdge {
  node: Point
  cursor: String
}

export interface PointPreviousValues {
  id: ID_Output
  lng: Float
  lat: Float
}

export interface PointSubscriptionPayload {
  mutation: MutationType
  node?: Point | null
  updatedFields?: Array<String> | null
  previousValues?: PointPreviousValues | null
}

export interface Polygon extends Node {
  id: ID_Output
  lines?: Array<PolygonLine> | null
}

/*
 * A connection to a list of items.

 */
export interface PolygonConnection {
  pageInfo: PageInfo
  edges: Array<PolygonEdge | null>
  aggregate: AggregatePolygon
}

/*
 * An edge in a connection.

 */
export interface PolygonEdge {
  node: Polygon
  cursor: String
}

export interface PolygonLine extends Node {
  id: ID_Output
  coordinates?: Array<PolygonLineCoordinate> | null
}

/*
 * A connection to a list of items.

 */
export interface PolygonLineConnection {
  pageInfo: PageInfo
  edges: Array<PolygonLineEdge | null>
  aggregate: AggregatePolygonLine
}

export interface PolygonLineCoordinate extends Node {
  id: ID_Output
  lng: Float
  lat: Float
}

/*
 * A connection to a list of items.

 */
export interface PolygonLineCoordinateConnection {
  pageInfo: PageInfo
  edges: Array<PolygonLineCoordinateEdge | null>
  aggregate: AggregatePolygonLineCoordinate
}

/*
 * An edge in a connection.

 */
export interface PolygonLineCoordinateEdge {
  node: PolygonLineCoordinate
  cursor: String
}

export interface PolygonLineCoordinatePreviousValues {
  id: ID_Output
  lng: Float
  lat: Float
}

export interface PolygonLineCoordinateSubscriptionPayload {
  mutation: MutationType
  node?: PolygonLineCoordinate | null
  updatedFields?: Array<String> | null
  previousValues?: PolygonLineCoordinatePreviousValues | null
}

/*
 * An edge in a connection.

 */
export interface PolygonLineEdge {
  node: PolygonLine
  cursor: String
}

export interface PolygonLinePreviousValues {
  id: ID_Output
}

export interface PolygonLineSubscriptionPayload {
  mutation: MutationType
  node?: PolygonLine | null
  updatedFields?: Array<String> | null
  previousValues?: PolygonLinePreviousValues | null
}

export interface PolygonPreviousValues {
  id: ID_Output
}

export interface PolygonSubscriptionPayload {
  mutation: MutationType
  node?: Polygon | null
  updatedFields?: Array<String> | null
  previousValues?: PolygonPreviousValues | null
}

export interface PricePeriod extends Node {
  id: ID_Output
  startTime: DateTime
  endTime: DateTime
  startDate: DateTime
  endDate?: DateTime | null
  repeat: TimePeriod
  price: Int
  priceUnit: TimePeriod
}

/*
 * A connection to a list of items.

 */
export interface PricePeriodConnection {
  pageInfo: PageInfo
  edges: Array<PricePeriodEdge | null>
  aggregate: AggregatePricePeriod
}

/*
 * An edge in a connection.

 */
export interface PricePeriodEdge {
  node: PricePeriod
  cursor: String
}

export interface PricePeriodPreviousValues {
  id: ID_Output
  startTime: DateTime
  endTime: DateTime
  startDate: DateTime
  endDate?: DateTime | null
  repeat: TimePeriod
  price: Int
  priceUnit: TimePeriod
}

export interface PricePeriodSubscriptionPayload {
  mutation: MutationType
  node?: PricePeriod | null
  updatedFields?: Array<String> | null
  previousValues?: PricePeriodPreviousValues | null
}

export interface Property extends Node {
  id: ID_Output
  tags?: Array<Tag> | null
  address?: Address | null
  changeSet?: Array<PropertyChangeSet> | null
  createdAt: DateTime
  updatedAt: DateTime
}

export interface PropertyChangeSet extends Node {
  id: ID_Output
  createdAt: DateTime
  updatedAt: DateTime
  version: Int
  value?: String | null
  changedBy?: User | null
}

/*
 * A connection to a list of items.

 */
export interface PropertyChangeSetConnection {
  pageInfo: PageInfo
  edges: Array<PropertyChangeSetEdge | null>
  aggregate: AggregatePropertyChangeSet
}

/*
 * An edge in a connection.

 */
export interface PropertyChangeSetEdge {
  node: PropertyChangeSet
  cursor: String
}

export interface PropertyChangeSetPreviousValues {
  id: ID_Output
  createdAt: DateTime
  updatedAt: DateTime
  version: Int
  value?: String | null
}

export interface PropertyChangeSetSubscriptionPayload {
  mutation: MutationType
  node?: PropertyChangeSet | null
  updatedFields?: Array<String> | null
  previousValues?: PropertyChangeSetPreviousValues | null
}

/*
 * A connection to a list of items.

 */
export interface PropertyConnection {
  pageInfo: PageInfo
  edges: Array<PropertyEdge | null>
  aggregate: AggregateProperty
}

/*
 * An edge in a connection.

 */
export interface PropertyEdge {
  node: Property
  cursor: String
}

export interface PropertyPreviousValues {
  id: ID_Output
  createdAt: DateTime
  updatedAt: DateTime
}

export interface PropertySubscriptionPayload {
  mutation: MutationType
  node?: Property | null
  updatedFields?: Array<String> | null
  previousValues?: PropertyPreviousValues | null
}

export interface Tag extends Node {
  id: ID_Output
  key: TagKey
  value: TagValue
}

/*
 * A connection to a list of items.

 */
export interface TagConnection {
  pageInfo: PageInfo
  edges: Array<TagEdge | null>
  aggregate: AggregateTag
}

/*
 * An edge in a connection.

 */
export interface TagEdge {
  node: Tag
  cursor: String
}

export interface TagKey extends Node {
  id: ID_Output
  value: String
}

/*
 * A connection to a list of items.

 */
export interface TagKeyConnection {
  pageInfo: PageInfo
  edges: Array<TagKeyEdge | null>
  aggregate: AggregateTagKey
}

/*
 * An edge in a connection.

 */
export interface TagKeyEdge {
  node: TagKey
  cursor: String
}

export interface TagKeyPreviousValues {
  id: ID_Output
  value: String
}

export interface TagKeySubscriptionPayload {
  mutation: MutationType
  node?: TagKey | null
  updatedFields?: Array<String> | null
  previousValues?: TagKeyPreviousValues | null
}

export interface TagPreviousValues {
  id: ID_Output
}

export interface TagSubscriptionPayload {
  mutation: MutationType
  node?: Tag | null
  updatedFields?: Array<String> | null
  previousValues?: TagPreviousValues | null
}

export interface TagValue extends Node {
  id: ID_Output
  value: String
}

/*
 * A connection to a list of items.

 */
export interface TagValueConnection {
  pageInfo: PageInfo
  edges: Array<TagValueEdge | null>
  aggregate: AggregateTagValue
}

/*
 * An edge in a connection.

 */
export interface TagValueEdge {
  node: TagValue
  cursor: String
}

export interface TagValuePreviousValues {
  id: ID_Output
  value: String
}

export interface TagValueSubscriptionPayload {
  mutation: MutationType
  node?: TagValue | null
  updatedFields?: Array<String> | null
  previousValues?: TagValuePreviousValues | null
}

export interface User extends Node {
  id: ID_Output
  name: String
  lastName?: String | null
  email: String
  password: String
  token?: String | null
  isActive: Boolean
  registrationToken?: String | null
  roles: Array<Roles>
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo
  edges: Array<UserEdge | null>
  aggregate: AggregateUser
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User
  cursor: String
}

export interface UserPreviousValues {
  id: ID_Output
  name: String
  lastName?: String | null
  email: String
  password: String
  token?: String | null
  isActive: Boolean
  registrationToken?: String | null
  roles: Array<Roles>
}

export interface UserSubscriptionPayload {
  mutation: MutationType
  node?: User | null
  updatedFields?: Array<String> | null
  previousValues?: UserPreviousValues | null
}

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean

export type DateTime = Date | string

/*
The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).
*/
export type Float = number

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number
export type ID_Output = string

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.
*/
export type Int = number

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string