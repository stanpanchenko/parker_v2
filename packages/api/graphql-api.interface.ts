/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export enum AddressOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  city_ASC = 'city_ASC',
  city_DESC = 'city_DESC',
  city_district_ASC = 'city_district_ASC',
  city_district_DESC = 'city_district_DESC',
  continent_ASC = 'continent_ASC',
  continent_DESC = 'continent_DESC',
  country_ASC = 'country_ASC',
  country_DESC = 'country_DESC',
  country_code_ASC = 'country_code_ASC',
  country_code_DESC = 'country_code_DESC',
  house_number_ASC = 'house_number_ASC',
  house_number_DESC = 'house_number_DESC',
  neighbourhood_ASC = 'neighbourhood_ASC',
  neighbourhood_DESC = 'neighbourhood_DESC',
  postcode_ASC = 'postcode_ASC',
  postcode_DESC = 'postcode_DESC',
  state_ASC = 'state_ASC',
  state_DESC = 'state_DESC',
  suburb_ASC = 'suburb_ASC',
  suburb_DESC = 'suburb_DESC',
  road_ASC = 'road_ASC',
  road_DESC = 'road_DESC',
  lng_ASC = 'lng_ASC',
  lng_DESC = 'lng_DESC',
  lat_ASC = 'lat_ASC',
  lat_DESC = 'lat_DESC',
  importance_ASC = 'importance_ASC',
  importance_DESC = 'importance_DESC',
}

export enum CountryCode {
  DE = 'DE',
  US = 'US',
}

export enum FeatureType {
  FeatureCollection = 'FeatureCollection',
  Feature = 'Feature',
}

export enum GeometryType {
  LineString = 'LineString',
  Point = 'Point',
  Polygon = 'Polygon',
}

export enum LineStringCoordinateOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  lng_ASC = 'lng_ASC',
  lng_DESC = 'lng_DESC',
  lat_ASC = 'lat_ASC',
  lat_DESC = 'lat_DESC',
}

export enum PolygonLineCoordinateOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  lng_ASC = 'lng_ASC',
  lng_DESC = 'lng_DESC',
  lat_ASC = 'lat_ASC',
  lat_DESC = 'lat_DESC',
}

export enum PolygonLineOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
}

export enum PricePeriodOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  startTime_ASC = 'startTime_ASC',
  startTime_DESC = 'startTime_DESC',
  endTime_ASC = 'endTime_ASC',
  endTime_DESC = 'endTime_DESC',
  startDate_ASC = 'startDate_ASC',
  startDate_DESC = 'startDate_DESC',
  endDate_ASC = 'endDate_ASC',
  endDate_DESC = 'endDate_DESC',
  repeat_ASC = 'repeat_ASC',
  repeat_DESC = 'repeat_DESC',
  price_ASC = 'price_ASC',
  price_DESC = 'price_DESC',
  priceUnit_ASC = 'priceUnit_ASC',
  priceUnit_DESC = 'priceUnit_DESC',
}

export enum PropertyChangeSetOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  createdAt_ASC = 'createdAt_ASC',
  createdAt_DESC = 'createdAt_DESC',
  updatedAt_ASC = 'updatedAt_ASC',
  updatedAt_DESC = 'updatedAt_DESC',
  version_ASC = 'version_ASC',
  version_DESC = 'version_DESC',
  value_ASC = 'value_ASC',
  value_DESC = 'value_DESC',
}

export enum Roles {
  ADMIN = 'ADMIN',
  SUPER_ADMIN = 'SUPER_ADMIN',
  USER = 'USER',
}

export enum TagKeyOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  value_ASC = 'value_ASC',
  value_DESC = 'value_DESC',
}

export enum TagOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
}

export enum TagValueOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  value_ASC = 'value_ASC',
  value_DESC = 'value_DESC',
}

export enum TimePeriod {
  SEC = 'SEC',
  MIN = 'MIN',
  HOUR = 'HOUR',
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
  YEAR = 'YEAR',
}

export enum UserOrderByInput {
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  name_ASC = 'name_ASC',
  name_DESC = 'name_DESC',
  lastName_ASC = 'lastName_ASC',
  lastName_DESC = 'lastName_DESC',
  email_ASC = 'email_ASC',
  email_DESC = 'email_DESC',
  password_ASC = 'password_ASC',
  password_DESC = 'password_DESC',
  token_ASC = 'token_ASC',
  token_DESC = 'token_DESC',
  isActive_ASC = 'isActive_ASC',
  isActive_DESC = 'isActive_DESC',
  registrationToken_ASC = 'registrationToken_ASC',
  registrationToken_DESC = 'registrationToken_DESC',
}

export interface AddressCreateInput {
  id?: string;
  city?: string;
  city_district?: string;
  continent?: string;
  country?: string;
  country_code?: CountryCode;
  house_number?: string;
  neighbourhood?: string;
  postcode?: number;
  state?: string;
  suburb?: string;
  road?: string;
  lng?: number;
  lat?: number;
  importance?: number;
}

export interface AddressUpdateInput {
  city?: string;
  city_district?: string;
  continent?: string;
  country?: string;
  country_code?: CountryCode;
  house_number?: string;
  neighbourhood?: string;
  postcode?: number;
  state?: string;
  suburb?: string;
  road?: string;
  lng?: number;
  lat?: number;
  importance?: number;
}

export interface AddressWhereInput {
  AND?: AddressWhereInput[];
  OR?: AddressWhereInput[];
  NOT?: AddressWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  city?: string;
  city_not?: string;
  city_in?: string[];
  city_not_in?: string[];
  city_lt?: string;
  city_lte?: string;
  city_gt?: string;
  city_gte?: string;
  city_contains?: string;
  city_not_contains?: string;
  city_starts_with?: string;
  city_not_starts_with?: string;
  city_ends_with?: string;
  city_not_ends_with?: string;
  city_district?: string;
  city_district_not?: string;
  city_district_in?: string[];
  city_district_not_in?: string[];
  city_district_lt?: string;
  city_district_lte?: string;
  city_district_gt?: string;
  city_district_gte?: string;
  city_district_contains?: string;
  city_district_not_contains?: string;
  city_district_starts_with?: string;
  city_district_not_starts_with?: string;
  city_district_ends_with?: string;
  city_district_not_ends_with?: string;
  continent?: string;
  continent_not?: string;
  continent_in?: string[];
  continent_not_in?: string[];
  continent_lt?: string;
  continent_lte?: string;
  continent_gt?: string;
  continent_gte?: string;
  continent_contains?: string;
  continent_not_contains?: string;
  continent_starts_with?: string;
  continent_not_starts_with?: string;
  continent_ends_with?: string;
  continent_not_ends_with?: string;
  country?: string;
  country_not?: string;
  country_in?: string[];
  country_not_in?: string[];
  country_lt?: string;
  country_lte?: string;
  country_gt?: string;
  country_gte?: string;
  country_contains?: string;
  country_not_contains?: string;
  country_starts_with?: string;
  country_not_starts_with?: string;
  country_ends_with?: string;
  country_not_ends_with?: string;
  country_code?: CountryCode;
  country_code_not?: CountryCode;
  country_code_in?: CountryCode[];
  country_code_not_in?: CountryCode[];
  house_number?: string;
  house_number_not?: string;
  house_number_in?: string[];
  house_number_not_in?: string[];
  house_number_lt?: string;
  house_number_lte?: string;
  house_number_gt?: string;
  house_number_gte?: string;
  house_number_contains?: string;
  house_number_not_contains?: string;
  house_number_starts_with?: string;
  house_number_not_starts_with?: string;
  house_number_ends_with?: string;
  house_number_not_ends_with?: string;
  neighbourhood?: string;
  neighbourhood_not?: string;
  neighbourhood_in?: string[];
  neighbourhood_not_in?: string[];
  neighbourhood_lt?: string;
  neighbourhood_lte?: string;
  neighbourhood_gt?: string;
  neighbourhood_gte?: string;
  neighbourhood_contains?: string;
  neighbourhood_not_contains?: string;
  neighbourhood_starts_with?: string;
  neighbourhood_not_starts_with?: string;
  neighbourhood_ends_with?: string;
  neighbourhood_not_ends_with?: string;
  postcode?: number;
  postcode_not?: number;
  postcode_in?: number[];
  postcode_not_in?: number[];
  postcode_lt?: number;
  postcode_lte?: number;
  postcode_gt?: number;
  postcode_gte?: number;
  state?: string;
  state_not?: string;
  state_in?: string[];
  state_not_in?: string[];
  state_lt?: string;
  state_lte?: string;
  state_gt?: string;
  state_gte?: string;
  state_contains?: string;
  state_not_contains?: string;
  state_starts_with?: string;
  state_not_starts_with?: string;
  state_ends_with?: string;
  state_not_ends_with?: string;
  suburb?: string;
  suburb_not?: string;
  suburb_in?: string[];
  suburb_not_in?: string[];
  suburb_lt?: string;
  suburb_lte?: string;
  suburb_gt?: string;
  suburb_gte?: string;
  suburb_contains?: string;
  suburb_not_contains?: string;
  suburb_starts_with?: string;
  suburb_not_starts_with?: string;
  suburb_ends_with?: string;
  suburb_not_ends_with?: string;
  road?: string;
  road_not?: string;
  road_in?: string[];
  road_not_in?: string[];
  road_lt?: string;
  road_lte?: string;
  road_gt?: string;
  road_gte?: string;
  road_contains?: string;
  road_not_contains?: string;
  road_starts_with?: string;
  road_not_starts_with?: string;
  road_ends_with?: string;
  road_not_ends_with?: string;
  lng?: number;
  lng_not?: number;
  lng_in?: number[];
  lng_not_in?: number[];
  lng_lt?: number;
  lng_lte?: number;
  lng_gt?: number;
  lng_gte?: number;
  lat?: number;
  lat_not?: number;
  lat_in?: number[];
  lat_not_in?: number[];
  lat_lt?: number;
  lat_lte?: number;
  lat_gt?: number;
  lat_gte?: number;
  importance?: number;
  importance_not?: number;
  importance_in?: number[];
  importance_not_in?: number[];
  importance_lt?: number;
  importance_lte?: number;
  importance_gt?: number;
  importance_gte?: number;
}

export interface AddressWhereUniqueInput {
  id?: string;
}

export interface CoordinatesWhereInput {
  AND?: CoordinatesWhereInput[];
  OR?: CoordinatesWhereInput[];
  NOT?: CoordinatesWhereInput[];
  lat?: number;
  lat_lt?: number;
  lat_lte?: number;
  lat_gt?: number;
  lat_gte?: number;
  lng?: number;
  lng_lt?: number;
  lng_lte?: number;
  lng_gt?: number;
  lng_gte?: number;
}

export interface FeatureInput {
  type: FeatureType;
  geometry: GeometryInput;
  properties: PropertiesInput;
}

export interface FeatureWhereInput {
  id?: string;
  AND?: FeatureWhereInput[];
  OR?: FeatureWhereInput[];
  NOT?: FeatureWhereInput[];
  geometry?: GeometryWhereInput;
  properties?: PropertiesWhereInput;
}

export interface FeatureWhereUniqueInput {
  id?: string;
}

export interface GeometryInput {
  type: GeometryType;
  coordinates: Coordinates;
}

export interface GeometryWhereInput {
  id?: string;
  AND?: GeometryWhereInput[];
  OR?: GeometryWhereInput[];
  NOT?: GeometryWhereInput[];
  type?: GeometryType;
  coordinates?: CoordinatesWhereInput;
}

export interface GeometryWhereUniqueInput {
  id?: string;
}

export interface LineStringCoordinateWhereInput {
  AND?: LineStringCoordinateWhereInput[];
  OR?: LineStringCoordinateWhereInput[];
  NOT?: LineStringCoordinateWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  lng?: number;
  lng_not?: number;
  lng_in?: number[];
  lng_not_in?: number[];
  lng_lt?: number;
  lng_lte?: number;
  lng_gt?: number;
  lng_gte?: number;
  lat?: number;
  lat_not?: number;
  lat_in?: number[];
  lat_not_in?: number[];
  lat_lt?: number;
  lat_lte?: number;
  lat_gt?: number;
  lat_gte?: number;
}

export interface LoginInput {
  email: string;
  password: string;
}

export interface PolygonLineCoordinateWhereInput {
  AND?: PolygonLineCoordinateWhereInput[];
  OR?: PolygonLineCoordinateWhereInput[];
  NOT?: PolygonLineCoordinateWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  lng?: number;
  lng_not?: number;
  lng_in?: number[];
  lng_not_in?: number[];
  lng_lt?: number;
  lng_lte?: number;
  lng_gt?: number;
  lng_gte?: number;
  lat?: number;
  lat_not?: number;
  lat_in?: number[];
  lat_not_in?: number[];
  lat_lt?: number;
  lat_lte?: number;
  lat_gt?: number;
  lat_gte?: number;
}

export interface PolygonLineWhereInput {
  AND?: PolygonLineWhereInput[];
  OR?: PolygonLineWhereInput[];
  NOT?: PolygonLineWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  coordinates_every?: PolygonLineCoordinateWhereInput;
  coordinates_some?: PolygonLineCoordinateWhereInput;
  coordinates_none?: PolygonLineCoordinateWhereInput;
}

export interface PricePeriodWhereInput {
  AND?: PricePeriodWhereInput[];
  OR?: PricePeriodWhereInput[];
  NOT?: PricePeriodWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  startTime?: DateTime;
  startTime_not?: DateTime;
  startTime_in?: DateTime[];
  startTime_not_in?: DateTime[];
  startTime_lt?: DateTime;
  startTime_lte?: DateTime;
  startTime_gt?: DateTime;
  startTime_gte?: DateTime;
  endTime?: DateTime;
  endTime_not?: DateTime;
  endTime_in?: DateTime[];
  endTime_not_in?: DateTime[];
  endTime_lt?: DateTime;
  endTime_lte?: DateTime;
  endTime_gt?: DateTime;
  endTime_gte?: DateTime;
  startDate?: DateTime;
  startDate_not?: DateTime;
  startDate_in?: DateTime[];
  startDate_not_in?: DateTime[];
  startDate_lt?: DateTime;
  startDate_lte?: DateTime;
  startDate_gt?: DateTime;
  startDate_gte?: DateTime;
  endDate?: DateTime;
  endDate_not?: DateTime;
  endDate_in?: DateTime[];
  endDate_not_in?: DateTime[];
  endDate_lt?: DateTime;
  endDate_lte?: DateTime;
  endDate_gt?: DateTime;
  endDate_gte?: DateTime;
  repeat?: TimePeriod;
  repeat_not?: TimePeriod;
  repeat_in?: TimePeriod[];
  repeat_not_in?: TimePeriod[];
  price?: number;
  price_not?: number;
  price_in?: number[];
  price_not_in?: number[];
  price_lt?: number;
  price_lte?: number;
  price_gt?: number;
  price_gte?: number;
  priceUnit?: TimePeriod;
  priceUnit_not?: TimePeriod;
  priceUnit_in?: TimePeriod[];
  priceUnit_not_in?: TimePeriod[];
}

export interface PropertiesInput {
  tags?: TagInput[];
  address?: AddressCreateInput;
}

export interface PropertiesWhereInput {
  id?: string;
  tags?: TagWhereInput;
}

export interface PropertyChangeSetWhereInput {
  AND?: PropertyChangeSetWhereInput[];
  OR?: PropertyChangeSetWhereInput[];
  NOT?: PropertyChangeSetWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  createdAt?: DateTime;
  createdAt_not?: DateTime;
  createdAt_in?: DateTime[];
  createdAt_not_in?: DateTime[];
  createdAt_lt?: DateTime;
  createdAt_lte?: DateTime;
  createdAt_gt?: DateTime;
  createdAt_gte?: DateTime;
  updatedAt?: DateTime;
  updatedAt_not?: DateTime;
  updatedAt_in?: DateTime[];
  updatedAt_not_in?: DateTime[];
  updatedAt_lt?: DateTime;
  updatedAt_lte?: DateTime;
  updatedAt_gt?: DateTime;
  updatedAt_gte?: DateTime;
  version?: number;
  version_not?: number;
  version_in?: number[];
  version_not_in?: number[];
  version_lt?: number;
  version_lte?: number;
  version_gt?: number;
  version_gte?: number;
  value?: string;
  value_not?: string;
  value_in?: string[];
  value_not_in?: string[];
  value_lt?: string;
  value_lte?: string;
  value_gt?: string;
  value_gte?: string;
  value_contains?: string;
  value_not_contains?: string;
  value_starts_with?: string;
  value_not_starts_with?: string;
  value_ends_with?: string;
  value_not_ends_with?: string;
  changedBy?: UserWhereInput;
}

export interface SignUpInput {
  name: string;
  email: string;
  password: string;
  confPassword: string;
}

export interface TagInput {
  key: string;
  value: string;
}

export interface TagKeyWhereInput {
  AND?: TagKeyWhereInput[];
  OR?: TagKeyWhereInput[];
  NOT?: TagKeyWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  value?: string;
  value_not?: string;
  value_in?: string[];
  value_not_in?: string[];
  value_lt?: string;
  value_lte?: string;
  value_gt?: string;
  value_gte?: string;
  value_contains?: string;
  value_not_contains?: string;
  value_starts_with?: string;
  value_not_starts_with?: string;
  value_ends_with?: string;
  value_not_ends_with?: string;
}

export interface TagKeyWhereUniqueInput {
  id?: string;
  value?: string;
}

export interface TagValueWhereInput {
  AND?: TagValueWhereInput[];
  OR?: TagValueWhereInput[];
  NOT?: TagValueWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  value?: string;
  value_not?: string;
  value_in?: string[];
  value_not_in?: string[];
  value_lt?: string;
  value_lte?: string;
  value_gt?: string;
  value_gte?: string;
  value_contains?: string;
  value_not_contains?: string;
  value_starts_with?: string;
  value_not_starts_with?: string;
  value_ends_with?: string;
  value_not_ends_with?: string;
}

export interface TagValueWhereUniqueInput {
  id?: string;
  value?: string;
}

export interface TagWhereInput {
  id?: string;
  AND?: TagWhereInput[];
  OR?: TagWhereInput[];
  NOT?: TagWhereInput[];
  key?: string;
  value?: string;
}

export interface TagWhereUniqueInput {
  id?: string;
}

export interface UserCreateInput {
  id?: string;
  name: string;
  lastName?: string;
  email: string;
  password: string;
  token?: string;
  isActive?: boolean;
  registrationToken?: string;
  roles?: UserCreaterolesInput;
}

export interface UserCreaterolesInput {
  set?: Roles[];
}

export interface UserWhereInput {
  AND?: UserWhereInput[];
  OR?: UserWhereInput[];
  NOT?: UserWhereInput[];
  id?: string;
  id_not?: string;
  id_in?: string[];
  id_not_in?: string[];
  id_lt?: string;
  id_lte?: string;
  id_gt?: string;
  id_gte?: string;
  id_contains?: string;
  id_not_contains?: string;
  id_starts_with?: string;
  id_not_starts_with?: string;
  id_ends_with?: string;
  id_not_ends_with?: string;
  name?: string;
  name_not?: string;
  name_in?: string[];
  name_not_in?: string[];
  name_lt?: string;
  name_lte?: string;
  name_gt?: string;
  name_gte?: string;
  name_contains?: string;
  name_not_contains?: string;
  name_starts_with?: string;
  name_not_starts_with?: string;
  name_ends_with?: string;
  name_not_ends_with?: string;
  lastName?: string;
  lastName_not?: string;
  lastName_in?: string[];
  lastName_not_in?: string[];
  lastName_lt?: string;
  lastName_lte?: string;
  lastName_gt?: string;
  lastName_gte?: string;
  lastName_contains?: string;
  lastName_not_contains?: string;
  lastName_starts_with?: string;
  lastName_not_starts_with?: string;
  lastName_ends_with?: string;
  lastName_not_ends_with?: string;
  email?: string;
  email_not?: string;
  email_in?: string[];
  email_not_in?: string[];
  email_lt?: string;
  email_lte?: string;
  email_gt?: string;
  email_gte?: string;
  email_contains?: string;
  email_not_contains?: string;
  email_starts_with?: string;
  email_not_starts_with?: string;
  email_ends_with?: string;
  email_not_ends_with?: string;
  password?: string;
  password_not?: string;
  password_in?: string[];
  password_not_in?: string[];
  password_lt?: string;
  password_lte?: string;
  password_gt?: string;
  password_gte?: string;
  password_contains?: string;
  password_not_contains?: string;
  password_starts_with?: string;
  password_not_starts_with?: string;
  password_ends_with?: string;
  password_not_ends_with?: string;
  token?: string;
  token_not?: string;
  token_in?: string[];
  token_not_in?: string[];
  token_lt?: string;
  token_lte?: string;
  token_gt?: string;
  token_gte?: string;
  token_contains?: string;
  token_not_contains?: string;
  token_starts_with?: string;
  token_not_starts_with?: string;
  token_ends_with?: string;
  token_not_ends_with?: string;
  isActive?: boolean;
  isActive_not?: boolean;
  registrationToken?: string;
  registrationToken_not?: string;
  registrationToken_in?: string[];
  registrationToken_not_in?: string[];
  registrationToken_lt?: string;
  registrationToken_lte?: string;
  registrationToken_gt?: string;
  registrationToken_gte?: string;
  registrationToken_contains?: string;
  registrationToken_not_contains?: string;
  registrationToken_starts_with?: string;
  registrationToken_not_starts_with?: string;
  registrationToken_ends_with?: string;
  registrationToken_not_ends_with?: string;
}

export interface UserWhereUniqueInput {
  id?: string;
  email?: string;
  registrationToken?: string;
}

export interface Node {
  id: string;
}

export interface Address extends Node {
  id: string;
  city?: string;
  city_district?: string;
  continent?: string;
  country?: string;
  country_code?: CountryCode;
  house_number?: string;
  neighbourhood?: string;
  postcode?: number;
  state?: string;
  suburb?: string;
  road?: string;
  lng?: number;
  lat?: number;
  importance?: number;
}

export interface Feature {
  id: string;
  type: FeatureType;
  geometry: Geometry;
  properties: Properties;
}

export interface Geometry {
  id?: string;
  type: GeometryType;
  coordinates: Coordinates;
}

export interface LineString extends Node {
  id: string;
  coordinates?: LineStringCoordinate[];
}

export interface LineStringCoordinate extends Node {
  id: string;
  lng: number;
  lat: number;
}

export interface LoginOutput {
  expiresIn: number;
  token: string;
  roles: Roles[];
  user: UserCredentialsLoginOutput;
}

export interface IMutation {
  updateAddress(
    data: AddressUpdateInput,
    where: AddressWhereUniqueInput,
  ): Address | Promise<Address>;
  createUser(data: UserCreateInput): User | Promise<User>;
  login(loginInput: LoginInput): LoginOutput | Promise<LoginOutput>;
  validateToken(tokenInput?: string): boolean | Promise<boolean>;
  loginWithToken(tokenInput?: string): LoginOutput | Promise<LoginOutput>;
  signup(signUpInput: SignUpInput): User | Promise<User>;
  deleteFeature(where: FeatureWhereUniqueInput): Feature | Promise<Feature>;
  createFeatures(data: FeatureInput[]): Feature[] | Promise<Feature[]>;
  createFeature(data: FeatureInput): Feature | Promise<Feature>;
  deleteFeatures(
    where: FeatureWhereUniqueInput[],
  ): Feature[] | Promise<Feature[]>;
  createTag(data: TagInput): Tag | Promise<Tag>;
  createTags(data: TagInput[]): Tag[] | Promise<Tag[]>;
}

export interface Point extends Node {
  id: string;
  lng: number;
  lat: number;
}

export interface Polygon extends Node {
  id: string;
  lines?: PolygonLine[];
}

export interface PolygonLine extends Node {
  id: string;
  coordinates?: PolygonLineCoordinate[];
}

export interface PolygonLineCoordinate extends Node {
  id: string;
  lng: number;
  lat: number;
}

export interface PricePeriod extends Node {
  id: string;
  startTime: DateTime;
  endTime: DateTime;
  startDate: DateTime;
  endDate?: DateTime;
  repeat: TimePeriod;
  price: number;
  priceUnit: TimePeriod;
}

export interface Properties {
  id?: string;
  tags: Tag[];
  address?: Address;
}

export interface Property extends Node {
  id: string;
  tags?: Tag[];
  address?: Address;
  changeSet?: PropertyChangeSet[];
  createdAt: DateTime;
  updatedAt: DateTime;
}

export interface PropertyChangeSet extends Node {
  id: string;
  createdAt: DateTime;
  updatedAt: DateTime;
  version: number;
  value?: string;
  changedBy?: User;
}

export interface IQuery {
  pricePeriods(
    where?: PricePeriodWhereInput,
    orderBy?: PricePeriodOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): PricePeriod[] | Promise<PricePeriod[]>;
  addresses(
    where?: AddressWhereInput,
    orderBy?: AddressOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): Address[] | Promise<Address[]>;
  address(where: AddressWhereUniqueInput): Address | Promise<Address>;
  user(where: UserWhereUniqueInput): User | Promise<User>;
  users(
    where?: UserWhereInput,
    orderBy?: UserOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): User[] | Promise<User[]>;
  search(query: string): SearchResult[] | Promise<SearchResult[]>;
  tag(where: TagWhereUniqueInput): Tag | Promise<Tag>;
  tags(
    where?: TagWhereInput,
    orderBy?: TagOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): Tag[] | Promise<Tag[]>;
  tagKey(where: TagKeyWhereUniqueInput): TagKey | Promise<TagKey>;
  tagKeys(
    where?: TagKeyWhereInput,
    orderBy?: TagKeyOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): TagKey[] | Promise<TagKey[]>;
  tagValue(where: TagValueWhereUniqueInput): TagValue | Promise<TagValue>;
  tagValues(
    where?: TagValueWhereInput,
    orderBy?: TagValueOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): TagValue[] | Promise<TagValue[]>;
  geometries(where: GeometryWhereInput): Geometry[] | Promise<Geometry[]>;
  geometry(where: GeometryWhereUniqueInput): Geometry | Promise<Geometry>;
  feature(where: FeatureWhereUniqueInput): Feature | Promise<Feature>;
  features(where: FeatureWhereInput): Feature[] | Promise<Feature[]>;
}

export interface SearchResult {
  place_id?: number;
  licence?: string;
  osm_type?: string;
  osm_id?: number;
  boundingbox?: number[];
  lat?: number;
  lon?: number;
  display_name?: string;
  class?: string;
  type?: string;
  importance?: number;
  icon?: string;
  address?: Address;
}

export interface Tag {
  id?: string;
  key: string;
  value: string;
}

export interface TagKey extends Node {
  id: string;
  value: string;
}

export interface TagValue extends Node {
  id: string;
  value: string;
}

export interface User extends Node {
  id: string;
  name: string;
  lastName?: string;
  email: string;
  password: string;
  token?: string;
  isActive: boolean;
  registrationToken?: string;
  roles: Roles[];
}

export interface UserCredentialsLoginOutput {
  email: string;
  id: string;
}

export type Coordinates = any;
export type DateTime = any;
