<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">


## Description
[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Dependencies
- Docker
- Node

## Installation
execute from the root folder
```bash
$ sh setup.sh
```

## Running the app

```bash
# database
$ npm run db

# nest webpack
$ npm run webpack

# nest server
$ npm run server

# react client
$ npm run client

```

## Support

This project is a private project and you should not be able to contribute if not allowed.

## Stay in touch

- Author - [Stanislav Panchenko](http://stanislavpanchenko.de)

## License

  
