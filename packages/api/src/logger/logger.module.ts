import { Logger, Module, Provider } from "@nestjs/common";
import { LoggerService } from './logger.service';
import { LoggerProvider } from "./providers/logger.provider";


@Module({
  providers: [
    LoggerService,
    LoggerProvider
  ],
  exports: [LoggerService]
})
export class LoggerModule {}
