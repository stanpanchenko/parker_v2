import { Logger, Provider } from "@nestjs/common";

export const LoggerByToken = 'PARKER_LOGGER_TOKEN';
export const LoggerProvider: Provider = {
  provide: LoggerByToken,
  useClass: Logger
};
