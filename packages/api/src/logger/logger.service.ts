import { Inject, Injectable, Logger } from '@nestjs/common';
import { LoggerByToken } from './providers/logger.provider';

interface LoggerServiceI {
  log(message: any, context?: string): any;
  error(message: any, trace?: string, context?: string): any;
  warn(message: any, context?: string): any;
}

@Injectable()
export class LoggerService implements LoggerServiceI {
  constructor(@Inject(LoggerByToken) private readonly logger: Logger) {}

  log(message: any, context?: string): any {
    // implement custom logging here
    this.logger.log(message, context);
  }
  error(message: any, trace?: string, context?: string): any {
    // implement custom logging here
    this.logger.error(message, trace, context);
  }
  warn(message: any, context?: string): any {
    // implement custom logging here
    this.logger.warn(message, context);
  }
}
