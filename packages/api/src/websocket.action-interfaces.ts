type SpaceID = string;
type UserID = string;
export enum WebSocketActionTypes {
  connect = "[WebSocketAction]CONNECTED",
  freeParkingSpace = "[WebSocketAction]FREE_PARKING_SPACE",
  occupyParkingSpace = "[WebSocketAction]OCCUPY_PARKING_SPACE",
  authenticate = "[WebSocketAction]AUTHENTICATE",
  reconnect = '[WebSocketAction]RECONNECT'
}

export interface AuthenticateWebSocketPayload {
  authorization: string;
}

export interface OccupySpaceActionPayloadI {
  id: SpaceID;
  userID: UserID;
}
export interface OccupySpaceActionI {
  type: WebSocketActionTypes.occupyParkingSpace;
  payload: OccupySpaceActionPayloadI;
}

export interface FreeSpaceActionPayloadI {
  id: SpaceID;
  userID: UserID;
}
export interface FreeSpaceActionI {
  type: WebSocketActionTypes.freeParkingSpace;
  payload: FreeSpaceActionPayloadI;
}
export type WebSocketActionsI = FreeSpaceActionI | OccupySpaceActionI;
