import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Test } from '@nestjs/testing';

describe('AppModule', () => {
  let appController: AppController;
  let appService: AppService;

  // beforeEach(() => {
  //   appService = new AppService();
  //   appController = new AppController(appService);
  // });

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appService = module.get<AppService>(AppService);
    appController = module.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return Hallo World', async () => {
      const result = 'Hello World!';
      jest.spyOn(appService, 'root').mockImplementation(() => result);

      expect(await appController.root()).toBe(result);
    });
  });
});