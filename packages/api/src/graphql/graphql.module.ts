import { Module } from '@nestjs/common';
import { GraphQLModule as NestNativeGraphQLModule } from '@nestjs/graphql';
import { join, resolve } from 'path';
import { importSchema } from 'graphql-import';

const apiSchemaPath = resolve(
  process.cwd() + '/src/graphql/api.schema.graphql',
);
const apiSchema = importSchema(apiSchemaPath);
console.log(`Pulling api-schema from: ${apiSchemaPath}`);

const definitionsPath = join(process.cwd(), '/src/graphql-api.interface.ts');
console.log(`generating api typescript interfaces to: ${definitionsPath}`);

@Module({
  imports: [
    NestNativeGraphQLModule.forRoot({
      typeDefs: [apiSchema],
      definitions: {
        path: definitionsPath,
      },
      resolverValidationOptions: {
        requireResolversForResolveType: false,
      },
      context: ({ req }) => ({ req }),
      playground: {
        settings: {
          'editor.theme': 'light',
        },
      },
    }),
  ],
})
export class GraphQLModule {}
