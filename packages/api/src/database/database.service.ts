import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import {
  ID_Output,
  User,
  UserCreateInput,
  UserWhereInput,
  UserWhereUniqueInput,
} from '../prisma/prisma.interfaces';
import { Address } from '../graphql-api.interface';
import { GraphQLResolveInfo } from 'graphql';

@Injectable()
export class DatabaseService {
  constructor(private readonly prismaService: PrismaService) {}

  async findUserByEmail(email: string): Promise<any> {
    return new Promise<any>(() => {});
  }

  async updateAddress(args, info?): Promise<Address> {
    return this.prismaService.mutation.updateAddress(args, info);
  }

  async getUserWithEmail(email: string): Promise<User> {
    // TODO: make a generic query
    const a = await this.prismaService.query.user(
      { where: { email: email } },
      `{
      id
      roles
      owns{
        id
      }
      isActive
      email
      name
      lastName
      token
      password
      registrationToken
      createdParkingSpaces{
        id
      }
      }`,
    );
    return a;
  }

  async getSpaceById(id: string) {}

  async setSpaceAddress(id: string, address: any) {}

  async setSpaceAvailable(id: string, isAvailable: boolean) {}

  async deleteSpace(id: string) {}

  async createUser(
    data: UserCreateInput,
    info?: GraphQLResolveInfo,
  ): Promise<ID_Output> {
    return await this.prismaService.mutation.createUser({ data }, info);
  }
  async getUsers(where: UserWhereInput) {
    return this.prismaService.query.users({ where });
  }
  async getUser(where: UserWhereUniqueInput): Promise<User> {
    return this.prismaService.query.user({
      where,
    });
  }

  async pricePeriods(args, info) {
    return this.prismaService.query.pricePeriods(args, info);
  }
}
