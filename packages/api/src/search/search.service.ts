import { HttpService, Injectable } from '@nestjs/common';
import * as URL from 'urlencode';
import { GraphQLResolveInfo } from 'graphql';

@Injectable()
export class SearchService {
  constructor(public readonly http: HttpService) {}
  public async search(args, info?: GraphQLResolveInfo) {
    const url =
      'https://nominatim.openstreetmap.org/search/?format=json&addressdetails=1&q=';
    const { data } = await this.http
      .get(`${url}${URL.encode(args.query)}`)
      .toPromise();
    console.log(data);
    return data;
  }
}
