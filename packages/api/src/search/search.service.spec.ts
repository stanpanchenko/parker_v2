import { Test, TestingModule } from '@nestjs/testing';
import { SearchService } from './search.service';
import {HttpModule} from "@nestjs/common";
import {SearchResolver} from "./search.resolver";

describe('SearchService', () => {
  let service: SearchService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[HttpModule],
      providers: [SearchService, SearchResolver]
    }).compile();

    service = module.get<SearchService>(SearchService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
