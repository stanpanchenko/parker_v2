import { Test, TestingModule } from '@nestjs/testing';
import { SearchResolver } from './search.resolver';
import { HttpModule } from '@nestjs/common';
import { SearchService } from './search.service';


describe('SearchResolver', () => {
  let resolver: SearchResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
      ],
      providers: [SearchService, SearchResolver],
    }).compile();

    resolver = module.get<SearchResolver>(SearchResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
