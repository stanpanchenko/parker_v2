import { Args, Info, Query, Resolver } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { SearchService } from './search.service';

@Resolver('Search')
export class SearchResolver {
  constructor(private readonly searchService: SearchService) {}

  @Query('search')
  async search(
    @Args() args: string,
    @Info() info?: GraphQLResolveInfo,
  ) {
    return this.searchService.search(args, info);
  }
}
