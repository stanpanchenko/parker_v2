import { Injectable } from '@nestjs/common';
import { Prisma } from './prisma.interfaces';
import {env} from "../env";

const prismaEndPoint = env.PRISMA_END_POINT;


@Injectable()
export class PrismaService extends Prisma {
  constructor() {
    const endpoint = `${prismaEndPoint || 'http://localhost:4466'}`;
    super({
      endpoint,
      // debug: false,
    });
  }
}
