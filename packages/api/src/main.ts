import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import 'dotenv/config';
declare const module: any;
import { env } from './env';

const port = env.API_PORT || 8080;
const websocketPort = env.WEBSOCKET_PORT;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  // app.use(
  //   rateLimit({
  //     windowMs: 15 * 60 * 1000, // 15 minutes
  //     max: 1000, // limit each IP to 100 requests per windowMs
  //     message: 'Max limit of request reached!',
  //   }),
  // );

  Logger.log(`API serving from port: ${port}`, `Bootstrap`);
  Logger.log(`Websocket serving from port: ${websocketPort}`, `Bootstrap`);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }

  await app.listen(port);
}

bootstrap();
