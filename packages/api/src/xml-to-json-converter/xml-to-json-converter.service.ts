import { Injectable } from '@nestjs/common';
import { parseString } from 'xml2js';

@Injectable()
export class XmlToJsonConverterService {
  public async toJSON(xml: string) {
    return new Promise((resolves, rejects) => {
      parseString(
        xml,
        (err, json) => {
          if (err) rejects(err);
          if (json) resolves(json);
        },
      );
    });
  }
}
