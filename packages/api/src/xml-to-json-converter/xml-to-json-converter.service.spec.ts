import { Test, TestingModule } from '@nestjs/testing';
import { XmlToJsonConverterService } from './xml-to-json-converter.service';

describe('XmlToJsonConverterService', () => {
  let service: XmlToJsonConverterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [XmlToJsonConverterService],
    }).compile();

    service = module.get<XmlToJsonConverterService>(XmlToJsonConverterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
