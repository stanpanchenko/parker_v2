import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { HashService } from './hash.service';
import { DatabaseService } from '../database/database.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { LoginInput, LoginOutput, Roles } from '../graphql-api.interface';
import { env } from '../env';
import {User} from "../prisma/prisma.interfaces";

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly hashService: HashService,
    private readonly databaseService: DatabaseService,
  ) {}
  // async signIn() {
  //   // In the real-world app you shouldn't expose this method publicly
  //   // instead, return a token once you verify user credentials
  //   const user: JwtPayload = this.testPayload;
  //   const accessToken = this.jwtService.sign(user);
  //   return {
  //     expiresIn: 3600,
  //     accessToken,
  //   };
  // }

  // async createToken() {
  //   const payload: JwtPayload = this.testPayload;
  //   const accessToken = this.jwtService.sign(payload);
  //   return {
  //     expiresIn: 3600,
  //     accessToken,
  //   };
  // }
  //
  // async createTokenForUser(user: User): Promise<JWT> {
  //   const jwtPayload = await this.createJwtPayloadFromUser(user);
  //   return this.jwtService.sign(jwtPayload);
  // }
  //
  // async validateToken(jwt: JWT): Promise<boolean> {
  //   try {
  //     const valid = this.jwtService.verify(jwt);
  //     return !!valid;
  //   } catch (e) {
  //     throw new UnauthorizedException(e.message, e.name);
  //   }
  // }

  public async validateJWTPayload(payload: JwtPayload) {
    // const user = this.databaseService.getUserWithEmail(payload.email);
    // TODO: extend payload validation
    // console.log('hier');
    return true;
  }

  public async loginWithToken(token: string): Promise<LoginOutput> {
    // check if token is valid
    const decryptedToken = this.jwtService.verify(token);

    if (!decryptedToken) throw UnauthorizedException;

    const user = await this.databaseService.getUserWithEmail(
      decryptedToken.email,
    );

    const loginOutput: LoginOutput = {
      roles: user.roles as Roles[],
      expiresIn: 0,
      token: token,
      user: {
        email: user.email,
        id: user.id,
      },
    };
    return loginOutput;
  }

  async validateToken(token: string): Promise<boolean> {
    // check if the token is valid
    const decryptedToken = this.jwtService.verify(token);
    if (!decryptedToken) throw UnauthorizedException;

    return !!decryptedToken;
  }

  private async createJwtPayloadFromUser(user: User): Promise<JwtPayload> {
    // email: string;
    // uuid: string;
    // nbf: number; // (not before time): Time before which the JWT must not be accepted for processing
    // jti: string; //  (JWT ID): Unique identifier; can be used to prevent the JWT from being replayed (allows a token to be used only once)
    const payload: JwtPayload = {
      email: 'mail@mail.de',
      roles: [Roles.USER],
      userID: 'some_id',
      nbf: 0,
      jti: null,
    };
    return payload;
  }

  async decode(token) {
    return this.jwtService.decode(token);
  }
  async verifyAsync(token) {
    return this.jwtService.verifyAsync(token);
  }

  async login({ email, password }: LoginInput): Promise<LoginOutput> {
    const user = await this.databaseService.getUserWithEmail(email);
    if (!user) throw new UnauthorizedException();
    const passwordValid = await this.hashService.compare(
      password,
      user.password,
    );
    if (!passwordValid) throw new UnauthorizedException();

    const jwtPayload: JwtPayload = {
      roles: user.roles,
      userID: user.id,
      email: user.email,
      nbf: 42,
      jti: '',
    };

    const token = await this.jwtService.sign(jwtPayload, {
      expiresIn: env.JWT_EXPIRES_IN,
    });
    const loginOutput: LoginOutput = {
      roles: user.roles as Roles[],
      expiresIn: 0,
      token: token,
      user: {
        email: user.email,
        id: user.id,
      },
    };

    return loginOutput;
  }
}
