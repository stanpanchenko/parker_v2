import { Args, Info, Mutation, Resolver } from '@nestjs/graphql';
import { AuthenticationService } from './authentication.service';
import { LoginInput, LoginOutput } from '../graphql-api.interface';
import { GraphQLResolveInfo } from 'graphql';

@Resolver('Authentication')
export class AuthenticationResolver {
  constructor(public readonly authenticationService: AuthenticationService) {}

  @Mutation('login')
  async login(
    @Args('loginInput') loginInput: LoginInput,
    @Info() info?: GraphQLResolveInfo,
  ): Promise<LoginOutput> {
    return this.authenticationService.login(loginInput);
  }

  @Mutation('validateToken')
  async validateToken(
    @Args('tokenInput') tokenInput: string,
  ): Promise<Boolean> {
    return this.authenticationService.validateToken(tokenInput);
  }

  @Mutation('loginWithToken')
  async loginWithToken(
    @Args('tokenInput') tokenInput: string,
  ): Promise<LoginOutput> {
    return this.authenticationService.loginWithToken(tokenInput);
  }
}
