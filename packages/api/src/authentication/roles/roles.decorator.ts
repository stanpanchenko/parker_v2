import { SetMetadata } from '@nestjs/common';

export const RolesAttributeKey = 'roles';
export const Roles = (...roles: string[]) => SetMetadata(RolesAttributeKey, roles);
