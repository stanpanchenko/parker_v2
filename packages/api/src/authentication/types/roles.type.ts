type AdminType = 'admin';
type UserType = 'user';
type SuperAdminType = 'super_admin';

const Admin: AdminType = 'admin';
const User: UserType = 'user';
const SuperAdmin: SuperAdminType = 'super_admin';

export const Roles = {
  admin: Admin,
  user: User,
  super_admin: SuperAdmin
};

export type RoleType = AdminType | UserType | SuperAdminType;
