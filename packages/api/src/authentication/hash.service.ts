import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';

export type Hash = string;
interface HashingI {
  hash: (value: string) => Promise<string>;
  compare: (value: string, hash: string) => Promise<boolean>;
}

@Injectable()
export class HashService implements HashingI {
  constructor() {}

  async hash(value: string): Promise<Hash> {
    return await bcrypt.hash(value, 10);
  }
  async compare(value: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(value, hash);
  }
}
