import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy, Type } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthenticationService } from '../authentication.service';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { env } from '../../env';

const JWT_SECRET_KEY = env.JWT_SECRET_KEY

@Injectable()
export class JwtStrategy extends PassportStrategy<Type>(Strategy) {
  constructor(private readonly authService: AuthenticationService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: JWT_SECRET_KEY,
    });
  }

  public async validate(jwtPayload: JwtPayload): Promise<boolean> {
    const valid = await this.authService.validateJWTPayload(jwtPayload);
    if (!valid) {
      throw new UnauthorizedException();
    }

    return true;
  }
}
