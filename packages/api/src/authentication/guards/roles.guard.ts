import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { RolesAttributeKey } from '../roles/roles.decorator';
import { AuthenticationService } from '../authentication.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthenticationService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    // get req object from GraphQL incoming request
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();

    // extract jwtPayload from incoming request
    const token = req.headers.authorization.replace('Bearer ', '');
    const jwtPayload: JwtPayload = (await this.authService.decode(
      token,
    )) as JwtPayload;

    // roles attached to request handler function
    const roles = this.reflector.get<string[]>(
      RolesAttributeKey,
      context.getHandler(),
    );
    if (!roles) return true; // if no roles on handler function attached

    const hasRole = () => jwtPayload.roles.some(role => roles.includes(role));
    return hasRole();
  }
}
