import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthenticationService } from '../authentication.service';

@Injectable()
export class WsHasSessionGuard implements CanActivate {
  constructor(
    private readonly authService: AuthenticationService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = request.handshake.query.authorization || '';
    // console.log(request.handshake.query.authorization);
    const valid = await this.authService.verifyAsync(token).catch(e => {
      console.log(e);
    });
    return !!valid;
  }
}
