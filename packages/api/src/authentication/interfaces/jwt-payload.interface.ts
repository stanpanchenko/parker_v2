import { Roles } from '../../prisma/prisma.interfaces';

export type JWT = string;

export interface JwtPayload {
  email: string;
  userID: string;
  roles: Roles[];
  nbf?: number; // (not before time): Time before which the JWT must not be accepted for processing
  jti?: string; //  (JWT ID): Unique identifier; can be used to prevent the JWT from being replayed (allows a token to be used only once)
}
