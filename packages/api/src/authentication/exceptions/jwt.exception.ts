export class JwtException extends Error {
  name: string;
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    // this.stack = new Error().stack;
  }
}
