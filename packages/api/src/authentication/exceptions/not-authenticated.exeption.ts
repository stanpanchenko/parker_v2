import { HttpException, HttpStatus } from '@nestjs/common';

export class NotAuthenticatedExeption extends HttpException {
  constructor(message?: string | object | any, error?: string){
    super(message || 'Invalid credentials!', HttpStatus.UNAUTHORIZED);
  }
}
