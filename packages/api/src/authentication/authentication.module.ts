import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { PassportModule } from '@nestjs/passport';

import { LoggerModule } from '../logger/logger.module';
import { HashService } from './hash.service';
import { AuthenticationResolver } from './authentication.resolver';
import { DatabaseModule } from '../database/database.module';
import { HasSessionGuard } from './guards/has-session.guard';
import { GqlAuthGuard } from './guards/grapqh-auth.guard';
import { env } from '../env';
import { WsHasSessionGuard } from './guards/ws-has-session.guard';
import { RolesGuard } from './guards/roles.guard';

const jwtSecretAuthKey = env.JWT_SECRET_KEY;
const Passport = PassportModule.register({
  defaultStrategy: 'jwt',
  session: true,
});
@Module({
  imports: [
    LoggerModule,
    DatabaseModule,
    Passport,
    JwtModule.register({
      secret: jwtSecretAuthKey,
      signOptions: { expiresIn: env.JWT_EXPIRES_IN },
    }),
  ],
  providers: [
    AuthenticationService,
    JwtStrategy,
    HashService,
    HasSessionGuard,
    AuthenticationResolver,
    GqlAuthGuard,
    WsHasSessionGuard,
    RolesGuard,
  ],
  exports: [HasSessionGuard, Passport, AuthenticationService],
})
export class AuthenticationModule {}
