import {IsString} from "class-validator";


export class CreateParkingSpaceDto {
	@IsString()
	readonly id: string;
	
	@IsString()
	readonly address: string;
}