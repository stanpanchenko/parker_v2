import { Module } from '@nestjs/common';
import { AuthenticationModule } from '../../authentication/authentication.module';
import { ParkingSpaceGateway } from './parking-space.gateway';
import { loggerProvider } from '../provides/logger.provider';
import { ParkingSpacesService } from '../parking-spaces.service';
// import { WsHasSessionGuard } from '../../authentication/guards/ws-has-session.guard';
import { DatabaseModule } from '../../database/database.module';
import {PassportModule} from "@nestjs/passport";

@Module({
  imports: [AuthenticationModule, DatabaseModule,PassportModule.register({ defaultStrategy: 'jwt', session: true })],
  providers: [ParkingSpaceGateway, loggerProvider, ParkingSpacesService],
  exports: [],
})
export class ParkingSpaceGatewayModule {}
