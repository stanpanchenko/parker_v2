import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsException,
  WsResponse,
} from '@nestjs/websockets';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ParkingSpacesService } from '../parking-spaces.service';
import { Logger, UseGuards } from '@nestjs/common';
import { env } from '../../env';
import {
  AuthenticateWebSocketPayload,
  FreeSpaceActionPayloadI,
  OccupySpaceActionPayloadI,
  WebSocketActionTypes,
} from '../../websocket.action-interfaces';
import { Client, Server, Socket } from 'socket.io';
import { WsHasSessionGuard } from '../../authentication/guards/ws-has-session.guard';
import { AuthenticationService } from '../../authentication/authentication.service';

const websocketPort = parseInt(env.WEBSOCKET_PORT, 10);

@WebSocketGateway(websocketPort)
export class ParkingSpaceGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  private logger = new Logger('ParkingSpaceGateway');
  @WebSocketServer() webSocket: Server;

  constructor(
    private readonly parkingSpacesService: ParkingSpacesService,
    private readonly authService: AuthenticationService,
  ) {
    this.logger.log(`WebSocket served at port: ${websocketPort}`);
  }

  handleConnection(socket: Socket) {
    // if user does not provide a token in some time after connection
    // or the token is invalid, he will be disconnected
    this.logger.log('New WS client connected');
    let authenticated = false;
    socket.on(
      WebSocketActionTypes.authenticate,
      (payload: AuthenticateWebSocketPayload) => {
        this.authService
          .validateToken(payload.authorization)
          .then(valid => {
            authenticated = valid;
          })
          .catch(e => {
            socket.disconnect(true);
          });
      },
    );
    setTimeout(() => {
      if (!authenticated) {
        this.logger.log('WS client disconnected');
        socket.disconnect(true);
      }
    }, 3000);

    socket.emit(WebSocketActionTypes.connect, { data: 'connectedd' });
  }

  handleDisconnect(socket: Socket) {
    this.logger.log('Client Disconnected');
  }
  @SubscribeMessage(WebSocketActionTypes.authenticate)
  async authenticate(client, data) {
    // console.log('authenticate');
    return { authenticated: true };
  }

  @SubscribeMessage('events')
  findAll(client, data): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  @SubscribeMessage('identity')
  async identity(client, data: number): Promise<number> {
    return data;
  }

  @UseGuards(WsHasSessionGuard)
  @SubscribeMessage(WebSocketActionTypes.freeParkingSpace)
  async freeSpace(client, payload: FreeSpaceActionPayloadI) {
    // hold response for testing purposes. remove in production
    // TODO: Add try catch block
    const response = await this.parkingSpacesService.freeSpace(payload.id);
    this.webSocket.emit(WebSocketActionTypes.freeParkingSpace, response);
  }

  @UseGuards(WsHasSessionGuard)
  @SubscribeMessage(WebSocketActionTypes.occupyParkingSpace)
  async occupySpace(client, payload: OccupySpaceActionPayloadI) {
    // hold response for testing purposes. remove in production

    // TODO: add try catch block
    const response = await this.parkingSpacesService
      .occupySpace(payload.id, payload.userID)
      .catch(e => e); // returns asked data or HttpError

    this.webSocket.emit(WebSocketActionTypes.occupyParkingSpace, response);
  }
}
