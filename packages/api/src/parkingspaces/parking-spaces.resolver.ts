import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ParkingSpacesService } from './parking-spaces.service';
import { Address, Roles as UserRoles } from '../graphql-api.interface';
import { DatabaseService } from '../database/database.service';
import { GraphQLArgs, GraphQLResolveInfo } from 'graphql';
import { PRISMA } from '../globals';
import { UseGuards } from '@nestjs/common';
import { HasSessionGuard } from '../authentication/guards/has-session.guard';
import { RolesGuard } from '../authentication/guards/roles.guard';
import { Roles } from '../authentication/roles/roles.decorator';

@Resolver('ParkingSpace')
export class ParkingSpacesResolver {
  constructor(
    private readonly parkingSpacesService: ParkingSpacesService,
    private readonly databaseService: DatabaseService,
  ) {}

  // @UseGuards(HasSessionGuard, RolesGuard)
  // @Roles(UserRoles.ADMIN)
  // @Mutation('createParkingSpace')
  // async createParkingSpace(
  //   @Args({ name: 'data', type: PRISMA }) data: ParkingSpaceCreateInput,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace> {
  //   return await this.databaseService.createParkingSpace(data, info);
  // }

  // @UseGuards(HasSessionGuard, RolesGuard)
  // @Roles(UserRoles.ADMIN)
  // @Mutation('upsertParkingSpace')
  // async upsertParkingSpace(
  //   @Args() args: any,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace> {
  //   return await this.databaseService.upsertParkingSpace(args, info);
  // }
  // @UseGuards(HasSessionGuard)
  // @Mutation('createOccupation')
  // async createOccupation(@Args() args: any, @Info() info: GraphQLResolveInfo) {
  //   return this.parkingSpacesService.createOccupation(args, info);
  // }

  // @UseGuards(HasSessionGuard, RolesGuard)
  // @Roles(UserRoles.ADMIN)
  // @Mutation('deleteParkingSpace')
  // async deleteParkingSpace(
  //   @Args() args: any,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace> {
  //   return await this.databaseService.deleteParkingSpace(args, info);
  // }

  // @UseGuards(HasSessionGuard)
  // @Query('address')
  // async getAddress(
  //   @Args({ type: PRISMA }) args: GraphQLArgs,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace[]> {
  //   return await this.databaseService.getAddress(args, info);
  // }

  // @Query('parkingSpaces')
  // @UseGuards(HasSessionGuard)
  // async parkingSpaces(
  //   @Args({ type: PRISMA }) args: GraphQLArgs,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace[]> {
  //   return await this.databaseService.parkingSpaces(args, info);
  // }

  // @UseGuards(HasSessionGuard)
  // @Query('parkingSpaceFeatures')
  // async parkingSpaceFeatures(
  //   @Args({ type: PRISMA }) args: GraphQLArgs,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpaceFeature[]> {
  //   return this.databaseService.parkingSpaceFeatures(args, info);
  // }

  // @Query('parkingSpace')
  // async parkingSpace(
  //   @Args() args: GraphQLArgs,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<ParkingSpace[]> {
  //   return this.databaseService.parkingSpace(args, info);
  // }

  // @UseGuards(HasSessionGuard, RolesGuard)
  // @Roles(UserRoles.ADMIN)
  // @Mutation('updateAddress')
  // async updateAddress(
  //   @Args() args: any,
  //   @Info() info?: GraphQLResolveInfo,
  // ): Promise<Address> {
  //   return this.databaseService.updateAddress(args, info);
  // }

  // @UseGuards(HasSessionGuard, RolesGuard)
  // @Roles(UserRoles.ADMIN)
  // @Mutation('updateParkingSpace')
  // async updateParkingSpace(
  //   @Args('args') args: any,
  //   @Info() info?: GraphQLResolveInfo,
  // ) {
  //   return this.databaseService.updateParkingSpace(args, info);
  // }

  @Query('pricePeriods')
  async pricePeriods(
    @Args({ type: PRISMA }) args: GraphQLArgs,
    @Info() info?: GraphQLResolveInfo,
  ) {
    return this.databaseService.pricePeriods(args, info);
  }
}
