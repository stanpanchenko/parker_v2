import { Module } from '@nestjs/common';
import { ParkingSpacesService } from './parking-spaces.service';
import { ParkingSpacesResolver } from './parking-spaces.resolver';
import { DatabaseModule } from '../database/database.module';
import { ParkingSpacesController } from './parking-spaces.controller';
import { AuthenticationModule } from '../authentication/authentication.module';
import { loggerProvider } from './provides/logger.provider';
import { ParkingSpaceGatewayModule } from './gateways/parking-space-gateway.module';
import { RolesGuard } from '../authentication/guards/roles.guard';

@Module({
  imports: [DatabaseModule, ParkingSpaceGatewayModule, AuthenticationModule],
  controllers: [ParkingSpacesController],
  providers: [
    ParkingSpacesService,
    ParkingSpacesResolver,
    loggerProvider,
    RolesGuard,
  ],
  exports: [ParkingSpacesService],
})
export class ParkingSpacesModule {}
