import {
  ForbiddenException,
  Injectable,
  Logger,
  UseFilters,
} from '@nestjs/common';
import { DatabaseService } from '../database/database.service';

import { GraphQLArgs, GraphQLResolveInfo } from 'graphql';

@Injectable()
export class ParkingSpacesService {
  logger = new Logger();
  constructor(private readonly databaseService: DatabaseService) {}

  async getAll() {}

  async delete(id: string) {
    // return await this.ParkingSpaceModel.findOneAndRemove({ _id: id });
    return await this.databaseService.deleteSpace(id);
  }
  async createParkingSpace(parkingSpace: any) {}

  async occupySpace(id: string, userID: string) {}

  async freeSpace(id: string) {
    // TODO: hook up payments here
  }

  async createOccupation(args: GraphQLArgs, info?: GraphQLResolveInfo) {
    console.log('can occupy!');
    return null;
  }
}
