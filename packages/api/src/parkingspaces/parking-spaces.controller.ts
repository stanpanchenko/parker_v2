import { Get, Controller, Post, Body, UsePipes, Logger } from '@nestjs/common';
import { ParkingSpacesService } from './parking-spaces.service';
import { ValidationPipe } from '../pipes/validation.pipe';

@Controller('parkingSpaces')
export class ParkingSpacesController {
  logger = new Logger('ParkingSpacesController');
  constructor(private readonly parkingSpacesService: ParkingSpacesService) {}

  @Get()
  async getAll() {
    return await this.parkingSpacesService.getAll();
  }

  // @Post()
  // @UsePipes(ValidationPipe)
  // async createParkingSpace(@Body() parkingSpace: ParkingSpace) {
  //   this.logger.log(
  //     JSON.stringify(parkingSpace),
  //     'UsersController : createParkingSpace',
  //   );
  //
  //   const space = await this.parkingSpacesService.createParkingSpace(
  //     parkingSpace,
  //   );
  //   // if(!space) {}
  //   return space;
  // }
  // @Post('/save')
  // @UsePipes(ValidationPipe)
  // async saveParkingSpace(@Body() parkingSpace: ParkingSpace) {
  // console.log(parkingSpace);
  // const space = await this.parkingSpacesService.createParkingSpace(
  //   parkingSpace,
  // );
  // const id = { id: space.id };
  // return id;
  // }

  @Post('/delete')
  @UsePipes(ValidationPipe)
  async deleteParkingSpace(@Body() id: { id: string }) {
    return await this.parkingSpacesService.delete(id.id);
  }
}
