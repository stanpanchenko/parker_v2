import { Logger, Provider } from '@nestjs/common';

export const loggerProvider: Provider = {
  provide: 'LOGGER',
  useClass: Logger,
};
