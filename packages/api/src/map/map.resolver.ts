import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql';
import { MapService } from './map.service';
import {Address, AddressInput, AddressWhereUniqueInput, Feature} from '../graphql-api.interface';
import { GraphQLResolveInfo } from 'graphql';

@Resolver()
export class MapResolver {
  loggerContext = 'MapResolver';
  constructor(private readonly mapService: MapService) {}

  @Query('features')
  async features(
    @Args('where') where: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<[Feature]> {
    return this.mapService.features(where, info);
  }

  @Query('feature')
  async feature(
    @Args('where') where: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<Feature> {
    return this.mapService.feature(where, info);
  }

  @Mutation('createFeature')
  async createFeature(
    @Args('data') data: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.createFeature(data, info);
  }

  @Mutation('createFeatures')
  async createFeatures(
    @Args('data') data: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.createFeatures(data, info);
  }

  @Mutation('deleteFeature')
  async deleteFeature(
    @Args('data') data: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.deleteFeature(data, info);
  }

  @Mutation('deleteFeatures')
  async deleteFeatures(
    @Args('where') where: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.deleteFeatures(where, info);
  }

  @Mutation('createTag')
  async createTag(
    @Args('data') data: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.createTag(data, info);
  }

  @Mutation('createTags')
  async createTags(
    @Args('data') data: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return this.mapService.createTags(data, info);
  }

  @Query('address')
  async address(
    @Args('where') where: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<Address> {
    // @ts-ignore
    return this.mapService.address(where, info);
  }

  @Query('addresses')
  async addresses(
    @Args('where') where: any,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<Address[]> {
    // @ts-ignore
    return this.mapService.addresses(where, info);
  }

  @Mutation('createAddress')
  async createAddress(
    @Args('data') data: AddressInput,
    @Info()
    info: GraphQLResolveInfo,
  ): Promise<Address> {
    // @ts-ignore
    return this.mapService.createAddress(data, info);
  }

  @Mutation('deleteAddress')
  async deleteAddress(
    @Args('where') where: AddressWhereUniqueInput,
    @Info()
      info: GraphQLResolveInfo,
  ): Promise<Address> {
    // @ts-ignore
    return this.mapService.deleteAddress(where, info);
  }
  
  @Mutation('updateAddress')
  async updateAddress(
    @Args('data') data: AddressInput,
    @Args('where') where: AddressWhereUniqueInput,
    @Info()
      info: GraphQLResolveInfo,
  ): Promise<Address> {
    // @ts-ignore
    return this.mapService.updateAddress(data, where, info);
  }
  
  @Mutation('upsertAddress')
  async upsertAddress(
    @Args('where') where: AddressWhereUniqueInput,
    @Args('create') create: AddressInput,
    @Args('update') update: AddressInput,
    @Info()
      info: GraphQLResolveInfo,
  ): Promise<Address> {
    // @ts-ignore
    return this.mapService.upsertAddress(where, create, update, info);
  }

}
