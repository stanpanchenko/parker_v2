import { Test, TestingModule } from '@nestjs/testing';
import { MapService } from './map.service';
import { MapResolver } from './map.resolver';
import { CoordinatesScalar } from './coordinates.scalar';
import { PrismaModule } from '../database/prisma/prisma.module';
import { PrismaAdapterModule } from '../prisma-adapter/prisma-adapter.module';

describe('MapService', () => {
  let service: MapService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule, PrismaAdapterModule],
      providers: [MapService, MapResolver, CoordinatesScalar],
    }).compile();

    service = module.get<MapService>(MapService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
