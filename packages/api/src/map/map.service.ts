import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import {
  AddressInput,
  Feature,
  FeatureWhereInput as ApiFeatureWhereInput,
  FeatureWhereUniqueInput,
  Tag,
  TagInput,
} from '../graphql-api.interface';
import { GraphQLResolveInfo } from 'graphql';
import { PrismaAdapterService } from '../prisma-adapter/prisma-adapter.service';
import { asyncForEach } from '../utils/utils';
import { injectAddressUUID } from './utils';
import { queryFields } from '../prisma-adapter/prisma-adapter-utils/prisma-adapter-utils';

@Injectable()
export class MapService {
  constructor(
    private readonly prismaService: PrismaService,
    // PrismaAdapterService translates GeoJSON featureWhereInput query to prisma specific format see the geo-json.prisma file
    private readonly prismaAdapter: PrismaAdapterService,
  ) {}

  async features(
    featureWhereInput: ApiFeatureWhereInput,
    info: GraphQLResolveInfo,
  ): Promise<any> {
    return await this.prismaAdapter.query.features(featureWhereInput, info);
  }

  async feature(
    where: FeatureWhereUniqueInput,
    info: GraphQLResolveInfo,
  ): Promise<Feature> {
    return await this.prismaAdapter.query.feature(where, info);
  }

  async createFeature(data: any, info: GraphQLResolveInfo): Promise<Feature> {
    return await this.prismaAdapter.mutation.createFeature(data, info);
  }

  async createFeatures(
    data: any,
    info: GraphQLResolveInfo,
  ): Promise<Feature[]> {
    return this.prismaAdapter.mutation.createFeatures(data, info);
  }

  async deleteFeature(data, info) {
    return this.prismaService.mutation.deleteFeature(data, info);
  }

  async deleteFeatures(where, info) {
    const res = [];
    await asyncForEach(where, async whereData => {
      const r = await this.prismaService.mutation.deleteFeature(
        { where: { ...whereData } },
        info,
      );
      res.push(r);
    });
    return res;
  }

  async createTag(data: TagInput, info: GraphQLResolveInfo): Promise<Tag> {
    return this.prismaAdapter.mutation.createTag(data, info);
  }

  async createTags(data: TagInput[], info: GraphQLResolveInfo): Promise<Tag[]> {
    return this.prismaAdapter.mutation.createTags(data, info);
  }

  async createAddress(data: AddressInput, info: GraphQLResolveInfo) {
    /*
     * To prevent identical addresses in DB a uuid is needed.
     * street, houseNumber city and countryCode are required fields on AddressInput
     * uuid: `${street}+${houseNumber}+${city}+${countryCode}`
     * */
    const addressWithUUID = injectAddressUUID(data);
    let resp;
    try {
      resp = await this.prismaService.mutation.createAddress(
        { data: addressWithUUID },
        info,
      );
    } catch (e) {
      if (e.message.includes('Details: Field name = uuid')) {
        throw new Error(
          `Error: A unique constraint would be violated for Address. Details: Combination of fields: street="${data.street}", houseNumber="${data.houseNumber}", city="${data.city}", countryCode="${data.countryCode}" already exist!`,
        );
      }
    }
    return resp;
  }

  async address(where, info) {
    return this.prismaService.query.address({ where }, info);
  }

  async addresses(where, info) {
    return this.prismaService.query.addresses({ where }, info);
  }

  async deleteAddress(where, info) {
    return this.prismaService.mutation.deleteAddress({ where }, info);
  }

  async updateAddress(data, where, info) {
    const addressWithUUID = injectAddressUUID(data);
    return this.prismaService.mutation.updateAddress(
      { data: addressWithUUID, where },
      info,
    );
  }

  async upsertAddress(where, create, update, info) {
    const createAddressWithUUID = injectAddressUUID(create);
    const updateAddressWithUUID = injectAddressUUID(update);
    return this.prismaService.mutation.upsertAddress(
      { where, create: createAddressWithUUID, update: updateAddressWithUUID },
      info,
    );
  }
}
