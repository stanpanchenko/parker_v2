import { Test, TestingModule } from '@nestjs/testing';
import { MapResolver } from './map.resolver';
import { PrismaModule } from '../database/prisma/prisma.module';
import { PrismaAdapterModule } from '../prisma-adapter/prisma-adapter.module';
import { MapService } from './map.service';
import { CoordinatesScalar } from './coordinates.scalar';

describe('MapResolver', () => {
  let resolver: MapResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule, PrismaAdapterModule],
      providers: [MapService, MapResolver, CoordinatesScalar],
    }).compile();

    resolver = module.get<MapResolver>(MapResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
