import { AddressInput } from '../graphql-api.interface';

export const injectAddressUUID = (address: AddressInput) => {
  const { street, houseNumber, city, countryCode, lat, lng } = address;
  const uuid = {
    street,
    houseNumber,
    city,
    countryCode,
  };
  return {
    ...address,
    // uuid: `${street}+${houseNumber}+${city}+${countryCode}+${lat}+${lng}`,
    uuid: JSON.stringify(uuid),
  };
};
