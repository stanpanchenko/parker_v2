import { Scalar, CustomScalar } from '@nestjs/graphql';
import { GraphQLError, Kind, ValueNode } from 'graphql';

function convertToNumber(ast: any) {
  switch (ast.kind) {
    case Kind.INT:
      return parseInt(ast.value, 10);
    case Kind.FLOAT:
      return parseFloat(ast.value);
    default:
      throw new GraphQLError('Value is not a number.');
  }
}
function convertToPoint(ast: any) {
  return [convertToNumber(ast.values[0]), convertToNumber(ast.values[1])];
}
function convertToLine(ast: any) {
  return ast.values.map(value => convertToPoint(value));
}
function convertToPolygon(ast: any) {
  return ast.values.map(value => convertToLine(value));
}

function isNumber(kind: string) {
  return kind === Kind.INT || kind === Kind.FLOAT;
}

function isPoint(ast: any): boolean {
  if (ast.kind !== Kind.LIST || ast.values.length !== 2) return false;
  return !ast.values.some(({ kind }) => !isNumber(kind));
}

function isLine(ast: any) {
  if (ast.kind !== Kind.LIST) return false;
  return !ast.values.some(value => !isPoint(value));
}

function isPolygon(ast: any) {
  if (ast.kind !== Kind.LIST) return false;
  return !ast.values.some(value => !isLine(value));
}

/**
 *
 * @param {any} ast
 * @return {Array} [0,0](Point) | [[0,0],[0,0]](Line) | [[[0,0],[0,0]],[[0,0],[0,0]]](Polygon)
 */
function parseToCoordinates(ast: any) {
  if (ast.kind !== Kind.LIST) throw new GraphQLError('Value is not Array.');
  // [number,number]
  if (isPoint(ast)) {
    return convertToPoint(ast);
  }
  if (isLine(ast)) {
    return convertToLine(ast);
  }
  if (isPolygon(ast)) {
    return convertToPolygon(ast);
  }

  throw new GraphQLError(
    'Coordinates needs to be of shape: [number,number] | [[number,number]] | [[[number,number]]]',
  );
}

@Scalar('Coordinates')
export class CoordinatesScalar implements CustomScalar<number, any> {
  description =
    'Point:[number,number] | LineString:[[number,number]] | Polygon:[[[number,number]]]';

  parseValue(value: any): any {
    return value; // value from the client
  }

  serialize(value: any): any {
    return value; // value sent to the client
  }

  parseLiteral(ast: any): any {
    return parseToCoordinates(ast); // value from the clients mutation
  }
}
