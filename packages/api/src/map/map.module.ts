import { Module } from '@nestjs/common';
import { MapService } from './map.service';
import { MapResolver } from './map.resolver';
import { CoordinatesScalar } from './coordinates.scalar';
import { PrismaModule } from '../prisma/prisma.module';
import { PrismaAdapterModule } from '../prisma-adapter/prisma-adapter.module';
import { APP_PIPE } from '@nestjs/core';
import { GraphqlQueryFieldsObjectPipe } from '../prisma-adapter/graphql-query-fields-object.pipe';

const GraphQLQueryFieldPipe = {
  provide: APP_PIPE,
  useClass: GraphqlQueryFieldsObjectPipe,
};

@Module({
  imports: [PrismaModule, PrismaAdapterModule],
  providers: [MapService, MapResolver, CoordinatesScalar],
})
export class MapModule {}
