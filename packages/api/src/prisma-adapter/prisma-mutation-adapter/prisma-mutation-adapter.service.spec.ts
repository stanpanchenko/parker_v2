import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from '../../prisma/prisma.module';
import { PrismaQueryAdapterService } from '../prisma-query-adapter/prisma-query-adapter.service';
import { PrismaResponseAdapterService } from '../prisma-response-adapter/prisma-response-adapter.service';
import { PrismaMutationAdapterService } from './prisma-mutation-adapter.service';
import { PrismaAdapterService } from '../prisma-adapter.service';
import { MutationDataParser } from '../query-data-parser/mutation-data-parser.service';
import { QueryFieldsParser } from '../query-fields-parser/query-fields-parser';

describe('PrismaMutationAdapterService', () => {
  let service: PrismaMutationAdapterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [
        MutationDataParser,
        QueryFieldsParser,
        PrismaQueryAdapterService,
        PrismaResponseAdapterService,
        PrismaMutationAdapterService,
        PrismaAdapterService,
      ],
      exports: [PrismaAdapterService],
    }).compile();

    service = module.get<PrismaMutationAdapterService>(
      PrismaMutationAdapterService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  // TODO: test the createFeature and createFeatures functions
});
