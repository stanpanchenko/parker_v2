import { Injectable } from '@nestjs/common';
import { GraphQLResolveInfo } from 'graphql';
import { GraphqlQueryFieldsObject } from '../../map/graphql-query-fields-object.type';
import * as graphqlFields from 'graphql-fields';
import { PrismaService } from '../../prisma/prisma.service';
import { PrismaResponseAdapterService } from '../prisma-response-adapter/prisma-response-adapter.service';
import { Feature, Tag, TagInput } from '../../graphql-api.interface';
import { QueryFieldsParser } from '../query-fields-parser/query-fields-parser';
import { MutationDataParser } from '../query-data-parser/mutation-data-parser.service';
import { asyncForEach } from '../../utils/utils';
import { TagCreateInput } from '../../prisma/prisma.interfaces';

@Injectable()
export class PrismaMutationAdapterService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly prismaResponseAdapterService: PrismaResponseAdapterService,
    private readonly queryFieldsParser: QueryFieldsParser,
    private readonly mutationDataParser: MutationDataParser,
  ) {}

  /**
   * @desc Adapts Api graphql createFeature mutation to Prisma createFeature mutation
   * @param data
   * @param info
   * @return Promise<Feature>
   */
  public async createFeature(
    data: any,
    info: GraphQLResolveInfo,
  ): Promise<Feature> {
    let queryFieldsObject: GraphqlQueryFieldsObject = graphqlFields(info);

    queryFieldsObject = await this.queryFieldsParser.injectGeometryQueryObject(
      queryFieldsObject,
      data.geometry.type,
    );
    queryFieldsObject = await this.queryFieldsParser.injectPropertiesQueryFieldObject(
      queryFieldsObject,
    );

    let prismaComplientDataObj = await this.mutationDataParser.injectGeometryCoordinatesData(
      data,
    );
    prismaComplientDataObj = await this.mutationDataParser.injectPropertiesData(
      prismaComplientDataObj,
    );

    const response = await this.prismaService.mutation.createFeature(
      { data: prismaComplientDataObj },
      this.queryFieldsParser.queryFieldsObjectToQueryString(queryFieldsObject),
    );

    return this.prismaResponseAdapterService.createFeature(response);
  }

  public async createFeatures(
    data: any[],
    info: GraphQLResolveInfo,
  ): Promise<Feature[]> {
    const res = [];
    await asyncForEach(data, async feature => {
      const newFeature = await this.createFeature(feature, info);
      res.push(newFeature);
      return;
    });
    return res;
  }

  public async createTag(
    data: TagInput,
    info: GraphQLResolveInfo,
  ): Promise<Tag> {
    const queryFieldsObject = graphqlFields(info);
    const queryFields = this.queryFieldsParser.injectTagQueryFieldsObject(
      queryFieldsObject,
    );
    const tagCreateInput: TagCreateInput = await this.mutationDataParser.injectTagInputData(
      data,
    );

    const res = await this.prismaService.mutation.createTag(
      { data: tagCreateInput },
      this.queryFieldsParser.queryFieldsObjectToQueryString(queryFields),
    );
    return this.prismaResponseAdapterService.createTag(res);
  }

  public async createTags(data: TagInput[], info: GraphQLResolveInfo) {
    const res = [];
    await asyncForEach(data, async tag => {
      const t = await this.createTag(tag, info);
      res.push(t);
    });
    return res;
  }
}
