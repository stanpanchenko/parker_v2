import { Injectable } from '@nestjs/common';
import { PrismaQueryAdapterService } from './prisma-query-adapter/prisma-query-adapter.service';
import { PrismaResponseAdapterService } from './prisma-response-adapter/prisma-response-adapter.service';
import { PrismaMutationAdapterService } from './prisma-mutation-adapter/prisma-mutation-adapter.service';

@Injectable()
export class PrismaAdapterService {
  constructor(
    private readonly prismaQueryAdapter: PrismaQueryAdapterService,
    private readonly prismaMutationAdapter: PrismaMutationAdapterService,
    private readonly prismaResponseService: PrismaResponseAdapterService,
  ) {}

  public query = this.prismaQueryAdapter;
  public mutation = this.prismaMutationAdapter;
  public response = this.prismaResponseService;
}
