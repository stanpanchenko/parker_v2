import { QueryFieldsParser } from './query-fields-parser';
import { GeometryType } from '../../graphql-api.interface';

describe('QueryFieldsParser', () => {
  let queryFieldsParser: QueryFieldsParser;

  beforeEach(() => {
    queryFieldsParser = new QueryFieldsParser();
  });

  it('should exist', () => {
    expect(queryFieldsParser).toBeInstanceOf(QueryFieldsParser);
  });

  describe('injectGeometryCoordinatesQueryObject', () => {
    it('should return point geometry coordinates query object if geometry type Point', () => {
      const queryObject = {
        geometry: {
          coordinates: {},
        },
      };

      expect(
        queryFieldsParser.injectGeometryQueryObject(
          queryObject,
          GeometryType.Point,
        ),
      ).resolves.toEqual({
        geometry: {
          point: {
            lng: {},
            lat: {},
          },
        },
      });
    });

    it('should return line string geometry coordinates query object if geometry type LineString is given', () => {
      const queryObject = {
        geometry: {
          coordinates: {},
        },
      };

      expect(
        queryFieldsParser.injectGeometryQueryObject(
          queryObject,
          GeometryType.LineString,
        ),
      ).resolves.toEqual({
        geometry: {
          lineString: {
            coordinates: {
              lng: {},
              lat: {},
            },
          },
        },
      });
    });

    it('should return Polygon geometry coordinates query object if provided geometry type is Polygon', () => {
      const queryObject = {
        geometry: {
          coordinates: {},
        },
      };

      expect(
        queryFieldsParser.injectGeometryQueryObject(
          queryObject,
          GeometryType.Polygon,
        ),
      ).resolves.toEqual({
        geometry: {
          polygon: {
            lines: {
              coordinates: {
                lng: {},
                lat: {},
              },
            },
          },
        },
      });
    });

    it('should return Polygon, LineString and Point geometry coordinates query object no known geometry type is given', () => {
      const queryObject = {
        geometry: {
          coordinates: {},
        },
      };

      expect(
        queryFieldsParser.injectGeometryQueryObject(
          queryObject,
          '' as GeometryType,
        ),
      ).resolves.toEqual({
        geometry: {
          point: {
            lng: {},
            lat: {},
          },
          lineString: {
            coordinates: {
              lng: {},
              lat: {},
            },
          },
          polygon: {
            lines: {
              coordinates: {
                lng: {},
                lat: {},
              },
            },
          },
        },
      });
    });
  });

  describe('objectToQueryString', () => {
    it('should return valid graphql query string', () => {
      const queryFieldObject = {
        type: {},
        geometry: {
          coordinates: {},
          undef: undefined,
        },
      };
      const queryString = queryFieldsParser.queryFieldsObjectToQueryString(
        queryFieldObject,
      );
      expect(queryString).toEqual('{ type     geometry  { coordinates   }}');
    });
  });

  describe('injectPropertiesTagsQueryObject', () => {
    it('should return valid query field object with tags key and value query if asked for key and value', async () => {
      expect(
        await queryFieldsParser.injectPropertiesTagsQueryObject({
          properties: {
            id: {},
            tags: {
              key: {},
              value: {},
            },
          },
        }),
      ).toEqual({
        properties: {
          id: {},
          tags: {
            key: {
              value: {},
            },
            value: {
              value: {},
            },
          },
        },
      });
    });

    it('should return the passed query fields object if properties filed is not in query object', async () => {
      expect(
        await queryFieldsParser.injectPropertiesTagsQueryObject({
          geometry: {
            type: GeometryType.Polygon,
          },
        }),
      ).toEqual({
        geometry: {
          type: GeometryType.Polygon,
        },
      });
    });

    it('should return the passed query fields object if tags field is not in query object', async () => {
      expect(
        await queryFieldsParser.injectPropertiesTagsQueryObject({
          properties: {
            id: {},
          },
        }),
      ).toEqual({
        properties: {
          id: {},
        },
      });
    });
  });

  describe('injectTagQueryFieldsObject', () => {
    it('should return tag value query', function() {
      const query = {
        id: {},
        value: {},
      };
      const res = queryFieldsParser.injectTagQueryFieldsObject(query);
      expect(res).toEqual({
        id: {},
        value: {
          value: {},
        },
      });
    });
    it('should return tag key query', function() {
      const query = {
        id: {},
        key: {},
      };
      const res = queryFieldsParser.injectTagQueryFieldsObject(query);
      expect(res).toEqual({
        id: {},
        key: {
          value: {},
        },
      });
    });
    it('should return tag key and tag value query', function() {
      const query = {
        id: {},
        key: {},
        value: {},
      };
      const res = queryFieldsParser.injectTagQueryFieldsObject(query);
      expect(res).toEqual({
        id: {},
        key: {
          value: {},
        },
        value: {
          value: {},
        },
      });
    });
  });
});
