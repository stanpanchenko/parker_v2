import { Injectable } from '@nestjs/common';
import { GeometryType } from '../../graphql-api.interface';
import { mapObject, removeProperties } from '../../utils/utils';

@Injectable()
export class QueryFieldsParser {
  async injectGeometryQueryObject(
    queryFieldsObject: any,
    geometryType: GeometryType,
  ) {
    if (
      !(queryFieldsObject.geometry && queryFieldsObject.geometry.coordinates)
    ) {
      return queryFieldsObject;
    }

    // replace coordinates with Geometry specific query object
    const polygonCoordinatesQueryObj = {
      polygon: {
        lines: {
          coordinates: {
            lat: {},
            lng: {},
          },
        },
      },
    };
    const lineStringCoordinatesQueryObj = {
      lineString: {
        coordinates: {
          lat: {},
          lng: {},
        },
      },
    };
    const pointCoordinatesQueryObj = {
      point: {
        lat: {},
        lng: {},
      },
    };
    switch (geometryType) {
      case 'LineString': {
        return mapObject(queryFieldsObject, (key: string, value: any) => {
          if (key === 'geometry')
            return {
              ...queryFieldsObject.geometry,
              coordinates: undefined, // will be removed from the merged geometry object
              ...lineStringCoordinatesQueryObj,
            };
          return value;
        });
      }
      case 'Point': {
        return mapObject(queryFieldsObject, (key, value) => {
          if (key === 'geometry')
            return {
              ...queryFieldsObject.geometry,
              coordinates: undefined,
              ...pointCoordinatesQueryObj,
            };
          return value;
        });
      }
      case 'Polygon': {
        return mapObject(queryFieldsObject, (key, value) => {
          if (key === 'geometry')
            return {
              ...queryFieldsObject.geometry,
              coordinates: undefined,
              ...polygonCoordinatesQueryObj,
            };
          return value;
        });
      }

      default: {
        return mapObject(queryFieldsObject, (key, value) => {
          if (key === 'geometry')
            return {
              ...queryFieldsObject.geometry,
              coordinates: undefined,
              ...pointCoordinatesQueryObj,
              ...lineStringCoordinatesQueryObj,
              ...polygonCoordinatesQueryObj,
            };
          return value;
        });
      }
    }
  }

  async injectPropertiesTagsQueryObject(queryFieldsObject: any): Promise<any> {
    if (!queryFieldsObject.properties || !queryFieldsObject.properties.tags) {
      return queryFieldsObject;
    }
    // TODO: consider to add key and value separately if asked for one, not both
    return {
      ...queryFieldsObject,
      properties: {
        ...queryFieldsObject.properties,
        tags: {
          key: {
            value: {},
          },
          value: {
            value: {},
          },
        },
      },
    };
  }

  async injectPropertiesQueryFieldObject(queryFieldObject: any) {
    if (!queryFieldObject.properties) return queryFieldObject;
    let newQueryObject = { ...queryFieldObject };
    if (newQueryObject.properties && newQueryObject.properties.tags) {
      newQueryObject = this.injectPropertiesTagsQueryObject(newQueryObject);
    }
    return newQueryObject;
  }

  public injectTagQueryFieldsObject(queryFieldObject: any) {
    let res = { ...queryFieldObject };
    if (res.value) {
      res = {
        ...res,
        value: {
          value: {},
        },
      };
    }
    if (res.key) {
      res = {
        ...res,
        key: {
          value: {},
        },
      };
    }
    return res;
  }

  public queryFieldsObjectToQueryString(object): string {
    return JSON.stringify(object).replace(/:|"|,|{}/g, ' ');
  }
}
