import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from '../../prisma/prisma.module';
import { PrismaResponseAdapterService } from './prisma-response-adapter.service';
import { PrismaQueryAdapterService } from '../prisma-query-adapter/prisma-query-adapter.service';
import { PrismaMutationAdapterService } from '../prisma-mutation-adapter/prisma-mutation-adapter.service';
import { PrismaAdapterService } from '../prisma-adapter.service';
import { MutationDataParser } from '../query-data-parser/mutation-data-parser.service';
import { QueryFieldsParser } from '../query-fields-parser/query-fields-parser';

describe('PrismaResponseAdapterService', () => {
  let responseService: PrismaResponseAdapterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [
        MutationDataParser,
        QueryFieldsParser,
        PrismaQueryAdapterService,
        PrismaResponseAdapterService,
        PrismaMutationAdapterService,
        PrismaAdapterService,
      ],
      exports: [PrismaAdapterService],
    }).compile();

    responseService = module.get<PrismaResponseAdapterService>(
      PrismaResponseAdapterService,
    );
  });
  it('should be defined', () => {
    expect(responseService).toBeDefined();
  });
});
