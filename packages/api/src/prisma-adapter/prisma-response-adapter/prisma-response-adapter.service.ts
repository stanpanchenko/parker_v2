import { Injectable } from '@nestjs/common';
import { asyncForEach, asyncMap, removeProperties } from '../../utils/utils';
import { Tag } from '../../prisma/prisma.interfaces';

@Injectable()
export class PrismaResponseAdapterService {
  constructor() {}

  private async parseProperties(property) {
    let tags = [];
    if (property['tags']) {
      tags = await asyncMap(property['tags'], async tag => ({
        key: tag.key.value,
        value: tag.value.value,
      }));
    }
    return {
      ...property,
      tags,
    };
  }

  private async parseGeometry(geometry) {
    // remove point, lineString, polygon from geometry and replace is with coordinates property
    // adjusted to the figure coordinates interface
    let coordinates = [];
    if (geometry['point']) {
      coordinates = [geometry['point'].lng, geometry['point'].lat];
    }
    
    if (geometry['lineString']) {
      coordinates = geometry['lineString'].coordinates.map(({ lng, lat }) => {
        return [lng, lat];
      });
    }
    
    if (geometry['polygon']) {
      coordinates = geometry['polygon'].lines.map(({ coordinates }) => {
        return coordinates.map(({ lat, lng }) => [lng, lat]);
      });
    }

    return {
      ...(await removeProperties(['point', 'lineString', 'polygon'], geometry)),
      coordinates,
    };
  }
  private async parseFeature(feature: any) {
    if (feature.geometry) {
      feature.geometry = await this.parseGeometry(feature.geometry);
    }
    if (feature.properties) {
      feature.properties = await this.parseProperties(feature.properties);
    }
    return feature;
  }

  public async features(features) {
    await asyncForEach(features, async feature => {
      if (feature.geometry) {
        feature.geometry = await this.parseGeometry(feature.geometry);
      }
      if (feature.properties) {
        feature.properties = await this.parseProperties(feature.properties);
      }
      return feature;
    });
    return features;
  }

  public async feature(feature: any) {
    return this.parseFeature(feature);
  }
  
  public createFeature(response: any) {
    return this.parseFeature(response);
  }

  public createTag(tag: Tag): any{
    let res:any = {
      ...tag,
    }
    if (tag.key){
      res = {
        ...res,
        key: tag.key.value,
      }
    }
    if (tag.value){
      res = {
        ...res,
        value: tag.value.value
      }
    }
    return res;
  }
  
}
