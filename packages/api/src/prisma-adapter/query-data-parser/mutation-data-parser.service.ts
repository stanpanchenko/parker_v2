import { GeometryType, TagInput } from '../../graphql-api.interface';
import { GraphQLError } from 'graphql';
import {
  isNumber,
  LineLngLat,
  LngLat,
  PolygonLngLat,
} from '../prisma-adapter-utils/prisma-adapter-utils';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { asyncForEach } from '../../utils/utils';
import { TagCreateInput } from '../../prisma/prisma.interfaces';

@Injectable()
export class MutationDataParser {
  constructor(private readonly prismaService: PrismaService) {}

  async injectPropertiesData(data: any) {
    if (!data.properties)
      throw new GraphQLError('Feature needs to have a properties attribute!');

    const injectedPropertiesTags = await this.injectDataPropertiesTags(
      data.properties,
    );
    const newProperties: any = {
      create: {
        ...injectedPropertiesTags,
      },
    };
    return {
      ...data,
      properties: {
        ...newProperties,
      },
    };
  }

  async tagKeyExist(key: string) {
    return this.prismaService.exists.TagKey({ value: key });
  }
  async tagValueExist(value: string) {
    return this.prismaService.exists.TagValue({ value });
  }

  async injectDataPropertiesTags(properties: any) {
    if (!properties.tags) return {};

    const tags = {
      create: [],
    };

    await asyncForEach(properties.tags, async (tag, i, array) => {
      const { key, value } = tag;
      tags.create = [
        ...tags.create,
        {
          ...this.getTagKeyDataObject(await this.tagKeyExist(key), key),
        },
      ];
      tags.create[i] = {
        ...tags.create[i],
        ...this.getTagValueDataObject(await this.tagValueExist(value), value),
      };

      return;
    });
    return {
      ...properties,
      tags: {
        ...tags,
      },
    };
  }

  isPointCoordinates(coordinates: LngLat) {
    if (coordinates.length !== 2) return false;
    return !coordinates.some(value => !isNumber(value));
  }
  isLineCoordinates(coords: LineLngLat): boolean {
    return !coords.some(value => !this.isPointCoordinates(value));
  }

  isPolygonCoordinates(coords: PolygonLngLat) {
    return !coords.some(value => !this.isLineCoordinates(value));
  }

  private invalidLineCoordsErrorMessage =
    'Line coordinates needs to be of shape: [[number,number],[number,number]].';
  injectLineGeometryDataObject(data: any) {
    if (!this.isLineCoordinates(data.geometry.coordinates))
      throw new GraphQLError(this.invalidLineCoordsErrorMessage);
    return {
      ...data,
      geometry: {
        create: {
          type: GeometryType.LineString,
          lineString: {
            create: {
              coordinates: {
                create: data.geometry.coordinates.map(c => ({
                  lng: c[0],
                  lat: c[1],
                })),
              },
            },
          },
        },
      },
    };
  }

  getCreateTagKeyValueDataObject(
    withValue: string,
  ): {
    value: {
      create: {
        value: string;
      };
    };
  } {
    return {
      value: {
        create: {
          value: withValue,
        },
      },
    };
  }

  getConnectTagKeyValueDataObject(toValue: string) {
    return {
      value: {
        connect: {
          value: toValue,
        },
      },
    };
  }

  getCreateTagKeyDataObject(withValue: string) {
    return {
      key: {
        create: {
          value: withValue,
        },
      },
    };
  }
  getConnectTagKeyDataObject(toValue: string) {
    return {
      key: {
        connect: {
          value: toValue,
        },
      },
    };
  }

  getTagKeyDataObject(tagKeyExist: boolean, keyValue: string) {
    return tagKeyExist
      ? this.getConnectTagKeyDataObject(keyValue)
      : this.getCreateTagKeyDataObject(keyValue);
  }

  getTagValueDataObject(valueExist: boolean, keyValue: string) {
    return valueExist
      ? this.getConnectTagKeyValueDataObject(keyValue)
      : this.getCreateTagKeyValueDataObject(keyValue);
  }

  private invalidPolygonCoordsErrorMessage =
    'Polygon coordinates should be of shape: [[[number,number]]].';
  injectPolygonGeometryDataObject(data: any) {
    if (!this.isPolygonCoordinates(data.geometry.coordinates))
      throw new GraphQLError(this.invalidPolygonCoordsErrorMessage);
    return {
      ...data,
      geometry: {
        create: {
          type: GeometryType.Polygon,
          polygon: {
            create: {
              lines: {
                create: data.geometry.coordinates.map(lines => {
                  return {
                    coordinates: {
                      create: lines.map(point => {
                        return {
                          lng: point[0],
                          lat: point[1],
                        };
                      }),
                    },
                  };
                }),
              },
            },
          },
        },
      },
    };
  }

  private noSuchGeometryTypeKnownError = 'No such geometry type know yet!';
  /**
   * @desc Injects prisma complient createFeature() geometry coordinates
   * @param data
   */
  async injectGeometryCoordinatesData(data: any) {
    // TODO: handle other types of geometry types like Multi- Polygons,LineStrings,Points
    const type = data.geometry.type;
    switch (type) {
      case GeometryType.Point: {
        return this.injectPointDataGeometryCoordinates(data);
      }
      case GeometryType.LineString: {
        return this.injectLineGeometryDataObject(data);
      }
      case GeometryType.Polygon: {
        return this.injectPolygonGeometryDataObject(data);
      }
      default:
        throw new TypeError(this.noSuchGeometryTypeKnownError);
    }
  }

  private inValidPointCoordinatesMessage =
    'Point coordinates needs to be of shape: [number,number].';
  injectPointDataGeometryCoordinates(data: any) {
    if (!this.isPointCoordinates(data.geometry.coordinates))
      throw new GraphQLError(this.inValidPointCoordinatesMessage);
    return {
      ...data,
      geometry: {
        create: {
          type: GeometryType.Point,
          point: {
            create: {
              lng: data.geometry.coordinates[0],
              lat: data.geometry.coordinates[1],
            },
          },
        },
      },
    };
  }

  public async injectTagInputData(data: TagInput): Promise<TagCreateInput> {
    const { key, value } = data;
    const keyData = this.getTagKeyDataObject(await this.tagKeyExist(key), key);
    const valueData = this.getTagValueDataObject(
      await this.tagValueExist(value),
      value,
    );
    return {
      ...data,
      ...keyData,
      ...valueData,
    };
  }
}
