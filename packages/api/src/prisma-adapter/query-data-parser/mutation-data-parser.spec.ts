import { MutationDataParser } from './mutation-data-parser.service';
import { FeatureType, GeometryType } from '../../graphql-api.interface';
import { GraphQLError } from 'graphql';
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaModule } from '../../prisma/prisma.module';

describe('MutationDataParser', () => {
  let parser: MutationDataParser;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [MutationDataParser],
    }).compile();

    parser = module.get<MutationDataParser>(MutationDataParser);
  });

  describe('', () => {
    it('should inject connect data properties tags if key and value exist', async () => {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(true);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(true);

      const properties = {
        tags: [
          {
            key: 'some',
            value: 'some',
          },
        ],
      };

      return expect(
        parser.injectDataPropertiesTags(properties),
      ).resolves.toEqual({
        tags: {
          create: [
            {
              key: {
                connect: {
                  value: 'some',
                },
              },
              value: {
                connect: {
                  value: 'some',
                },
              },
            },
          ],
        },
      });
    });

    it('should inject create data properties tags if key and value not exist', async () => {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(false);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(false);

      const properties = {
        tags: [
          {
            key: 'some',
            value: 'some',
          },
        ],
      };

      expect(await parser.injectDataPropertiesTags(properties)).toEqual({
        tags: {
          create: [
            {
              key: {
                create: {
                  value: 'some',
                },
              },
              value: {
                create: {
                  value: 'some',
                },
              },
            },
          ],
        },
      });
    });

    it('should inject create and connect data properties tags if key exist and value not exist', async () => {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(true);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(false);

      const properties = {
        tags: [
          {
            key: 'some',
            value: 'some',
          },
        ],
      };

      expect(await parser.injectDataPropertiesTags(properties)).toEqual({
        tags: {
          create: [
            {
              key: {
                connect: {
                  value: 'some',
                },
              },
              value: {
                create: {
                  value: 'some',
                },
              },
            },
          ],
        },
      });
    });

    it('should inject create and connect data properties tags if key not exist and value exist', async () => {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(false);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(true);

      const properties = {
        tags: [
          {
            key: 'some',
            value: 'some',
          },
        ],
      };

      expect(await parser.injectDataPropertiesTags(properties)).toEqual({
        tags: {
          create: [
            {
              key: {
                create: {
                  value: 'some',
                },
              },
              value: {
                connect: {
                  value: 'some',
                },
              },
            },
          ],
        },
      });
    });
  });

  describe('isLineCoordinates', () => {
    it('should be true if coordinates are valid', function() {
      const coords = [[0, 0], [0, 0]] as any;
      expect(parser.isLineCoordinates(coords)).toBeTruthy();
    });
    it('should return false if coordinates are not valid', function() {
      const coords = [[0, 0], [0, 0], 0] as any;
      expect(parser.isLineCoordinates(coords)).toBeFalsy();
    });
  });

  describe('isPointCoordinates', () => {
    it('should validate point coordinates', function() {
      const isPoint = parser.isPointCoordinates([0, 0]);
      expect(isPoint).toBeTruthy();
    });

    it('should fail if coordinates contain a string', function() {
      const c = [0, 'a'] as any;
      const isPoint = parser.isPointCoordinates(c);
      expect(isPoint).toBeFalsy();
    });

    it('should fail if coordinates contain array', function() {
      const c = [0, [0]] as any;
      const isPoint = parser.isPointCoordinates(c);
      expect(isPoint).toBeFalsy();
    });
    it('should fail if coordinates contain more then 2 numbers', function() {
      const c = [0, 0, 0] as any;
      const isPoint = parser.isPointCoordinates(c);
      expect(isPoint).toBeFalsy();
    });
  });

  describe('getCreateTagKeyValueDataObject', () => {
    it('should return valid object', () => {
      const keyValueObject = parser.getCreateTagKeyValueDataObject('value');
      expect(keyValueObject).toEqual({
        value: {
          create: {
            value: 'value',
          },
        },
      });
    });
  });

  describe('getConnectTagKeyValueDataObject', () => {
    it('should return valid object', () => {
      const keyValueObject = parser.getConnectTagKeyValueDataObject('value');
      expect(keyValueObject).toEqual({
        value: {
          connect: {
            value: 'value',
          },
        },
      });
    });
  });

  describe('getCreateTagKeyDataObject', () => {
    it('should return valid object', () => {
      const keyValueObject = parser.getCreateTagKeyDataObject('value');
      expect(keyValueObject).toEqual({
        key: {
          create: {
            value: 'value',
          },
        },
      });
    });
  });

  describe('getConnectTagKeyDataObject', () => {
    it('should return valid object', () => {
      const keyValueObject = parser.getConnectTagKeyDataObject('value');
      expect(keyValueObject).toEqual({
        key: {
          connect: {
            value: 'value',
          },
        },
      });
    });
  });

  describe('injectPointDataGeometryCoordinates', () => {
    it('should return valid object', () => {
      const data = {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [0, 0],
        },
        properties: {},
      };
      const keyValueObject = parser.injectPointDataGeometryCoordinates(data);
      expect(keyValueObject).toEqual({
        type: 'Feature',
        geometry: {
          create: {
            type: 'Point',
            point: {
              create: {
                lng: 0,
                lat: 0,
              },
            },
          },
        },
        properties: {},
      });
    });
    it('should throw GraphQLError if coordinates are not valid', () => {
      const data = {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [0, 0, 0],
        },
        properties: {},
      };
      try {
        parser.injectPointDataGeometryCoordinates(data);
      } catch (e) {
        expect(e).toBeInstanceOf(GraphQLError);
      }
    });
  });

  describe('injectGeometryCoordinatesData', () => {
    it('should return valid geometry data for a point', () => {
      const data = {
        type: FeatureType.Feature,
        geometry: {
          type: GeometryType.Point,
          coordinates: [0, 0],
        },
        properties: {},
      };

      return expect(
        parser.injectGeometryCoordinatesData(data),
      ).resolves.toEqual({
        type: FeatureType.Feature,
        geometry: {
          create: {
            type: GeometryType.Point,
            point: {
              create: {
                lng: 0,
                lat: 0,
              },
            },
          },
        },
        properties: {},
      });
    });

    it('should return valid geometry data for a LineString', async () => {
      const data = {
        type: FeatureType.Feature,
        geometry: {
          type: GeometryType.LineString,
          coordinates: [[0, 0], [0, 0]],
        },
        properties: {},
      };

      return expect(
        parser.injectGeometryCoordinatesData(data),
      ).resolves.toEqual({
        type: FeatureType.Feature,
        geometry: {
          create: {
            type: GeometryType.LineString,
            lineString: {
              create: {
                coordinates: {
                  create: [
                    {
                      lng: 0,
                      lat: 0,
                    },
                    {
                      lng: 0,
                      lat: 0,
                    },
                  ],
                },
              },
            },
          },
        },

        properties: {},
      });
    });

    it('should return valid geometry data for a Polygon', async () => {
      const data = {
        type: FeatureType.Feature,
        geometry: {
          type: GeometryType.Polygon,
          coordinates: [[[0, 0], [0, 0]], [[0, 0], [0, 0]]],
        },
        properties: {},
      };

      return expect(
        parser.injectGeometryCoordinatesData(data),
      ).resolves.toEqual({
        type: FeatureType.Feature,
        geometry: {
          create: {
            type: GeometryType.Polygon,
            polygon: {
              create: {
                lines: {
                  create: [
                    {
                      coordinates: {
                        create: [{ lng: 0, lat: 0 }, { lng: 0, lat: 0 }],
                      },
                    },
                    {
                      coordinates: {
                        create: [{ lng: 0, lat: 0 }, { lng: 0, lat: 0 }],
                      },
                    },
                  ],
                },
              },
            },
          },
        },
        properties: {},
      });
    });

    it('should throw Error if geometry type not supported', async () => {
      const data = {
        geometry: {
          type: 'Some',
        },
      };
      return expect(
        parser.injectGeometryCoordinatesData(data),
      ).rejects.toThrow();
    });
  });

  describe('injectPolygonGeometryDataObject', () => {
    it('should throw GraphQLError if coordinates are invalid', function() {
      const data = {
        type: 'Feature',
        geometry: {
          type: GeometryType.Polygon,
          coordinates: [
            [[[30, 20], [45, 40], [10, 40], [30, 20]]],
            [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]],
          ],
        },
        properties: {},
      };
      try {
        parser.injectPolygonGeometryDataObject(data);
      } catch (e) {
        expect(e).toBeInstanceOf(GraphQLError);
      }
    });
    it('should return valid data object', function() {
      const data = {
        type: 'Feature',
        geometry: {
          type: GeometryType.Polygon,
          coordinates: [
            [[0, 0], [0, 0]],
            [[0, 0], [0, 0], [0, 0]],
            [[0, 0], [0, 0]],
          ],
        },
        properties: {},
      };
      const res = parser.injectPolygonGeometryDataObject(data);
      expect(res).toEqual({
        type: 'Feature',
        properties: {},
        geometry: {
          create: {
            type: GeometryType.Polygon,
            polygon: {
              create: {
                lines: {
                  create: [
                    {
                      coordinates: {
                        create: [{ lng: 0, lat: 0 }, { lng: 0, lat: 0 }],
                      },
                    },
                    {
                      coordinates: {
                        create: [
                          { lng: 0, lat: 0 },
                          { lng: 0, lat: 0 },
                          { lng: 0, lat: 0 },
                        ],
                      },
                    },
                    {
                      coordinates: {
                        create: [{ lng: 0, lat: 0 }, { lng: 0, lat: 0 }],
                      },
                    },
                  ],
                },
              },
            },
          },
        },
      });
    });
  });

  describe('getTagKeyDataObject', () => {
    it('should returns connect tag key data object if key exist', function() {
      expect(parser.getTagKeyDataObject(true, 'key')).toEqual({
        key: {
          connect: {
            value: 'key',
          },
        },
      });
    });
    it('should returns create tag key data object if key not exist', function() {
      expect(parser.getTagKeyDataObject(false, 'key')).toEqual({
        key: {
          create: {
            value: 'key',
          },
        },
      });
    });
  });

  describe('getTagValueDataObject', () => {
    it('should returns connect tag key value data object if value exist', function() {
      expect(parser.getTagValueDataObject(true, 'value')).toEqual({
        value: {
          connect: {
            value: 'value',
          },
        },
      });
    });
    it('should returns create tag key value data object if value not exist', function() {
      expect(parser.getTagValueDataObject(false, 'value')).toEqual({
        value: {
          create: {
            value: 'value',
          },
        },
      });
    });
  });

  describe('injectLineGeometryDataObject', () => {
    it('should throw TypeError if coordinates are invalid', () => {
      const coords = [[0, 0], [0, 0], 0];
      try {
        parser.injectLineGeometryDataObject(coords);
      } catch (e) {
        expect(e).toBeInstanceOf(TypeError);
      }
    });

    it('should return valid object', () => {
      const data = {
        type: 'Feature',
        geometry: {
          type: 'LineString',
          coordinates: [[0, 0], [0, 0]],
        },
        properties: {},
      };

      const keyValueObject = parser.injectLineGeometryDataObject(data);

      expect(keyValueObject).toEqual({
        type: 'Feature',
        geometry: {
          create: {
            type: 'LineString',
            lineString: {
              create: {
                coordinates: {
                  create: [
                    {
                      lng: 0,
                      lat: 0,
                    },
                    {
                      lng: 0,
                      lat: 0,
                    },
                  ],
                },
              },
            },
          },
        },
        properties: {},
      });
    });
  });
  describe('injectTagInputData', () => {
    it('should return create tag key and tag value if key and value not exist', function() {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(false);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(false);
      const data = {
        id: 'someID',
        key: 'someKey',
        value: 'someValue',
      };
      const res = parser.injectTagInputData(data);
      expect(res).resolves.toEqual({
        id: 'someID',
        key: {
          create: {
            value: 'someKey',
          },
        },
        value: {
          create: {
            value: 'someValue',
          },
        },
      });
    });
    it('should return connect tag key and tag value if key and value exist', function() {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(true);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(true);
      const data = {
        id: 'someID',
        key: 'someKey',
        value: 'someValue',
      };
      const res = parser.injectTagInputData(data);
      expect(res).resolves.toEqual({
        id: 'someID',
        key: {
          connect: {
            value: 'someKey',
          },
        },
        value: {
          connect: {
            value: 'someValue',
          },
        },
      });
    });

    it('should return create tag key and connect tag value if key not exist and value exist', function() {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(false);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(true);
      const data = {
        id: 'someID',
        key: 'someKey',
        value: 'someValue',
      };
      const res = parser.injectTagInputData(data);
      expect(res).resolves.toEqual({
        id: 'someID',
        key: {
          create: {
            value: 'someKey',
          },
        },
        value: {
          connect: {
            value: 'someValue',
          },
        },
      });
    });

    it('should return connect tag key and create tag value if key not exist and value exist', function() {
      jest.spyOn(parser, 'tagKeyExist').mockResolvedValue(true);
      jest.spyOn(parser, 'tagValueExist').mockResolvedValue(false);
      const data = {
        id: 'someID',
        key: 'someKey',
        value: 'someValue',
      };
      const res = parser.injectTagInputData(data);
      expect(res).resolves.toEqual({
        id: 'someID',
        key: {
          connect: {
            value: 'someKey',
          },
        },
        value: {
          create: {
            value: 'someValue',
          },
        },
      });
    });
  });
});
