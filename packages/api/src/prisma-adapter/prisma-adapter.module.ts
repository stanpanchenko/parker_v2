import { Module } from '@nestjs/common';
import { PrismaAdapterService } from './prisma-adapter.service';
import { PrismaResponseAdapterService } from './prisma-response-adapter/prisma-response-adapter.service';
import { PrismaQueryAdapterService } from './prisma-query-adapter/prisma-query-adapter.service';
import { PrismaMutationAdapterService } from './prisma-mutation-adapter/prisma-mutation-adapter.service';
import { PrismaModule } from '../prisma/prisma.module';
import { MutationDataParser } from './query-data-parser/mutation-data-parser.service';
import { QueryFieldsParser } from './query-fields-parser/query-fields-parser';

@Module({
  imports: [PrismaModule],
  providers: [
    MutationDataParser,
    QueryFieldsParser,
    PrismaQueryAdapterService,
    PrismaResponseAdapterService,
    PrismaMutationAdapterService,
    PrismaAdapterService,
  ],
  exports: [PrismaAdapterService],
})
export class PrismaAdapterModule {}
