import { Injectable } from '@nestjs/common';
import { removeProperties } from '../../utils/utils';
import {
  Feature,
  FeatureWhereInput as ApiFeatureWhereInput,
  FeatureWhereUniqueInput,
  GeometryType,
} from '../../graphql-api.interface';
import { FeatureWhereInput as PrismaFeatureWhereInput } from '../../prisma/prisma.interfaces';
import { GraphQLResolveInfo } from 'graphql';
import * as graphqlFields from 'graphql-fields';
import { PrismaService } from '../../prisma/prisma.service';
import { PrismaResponseAdapterService } from '../prisma-response-adapter/prisma-response-adapter.service';
import { QueryFieldsParser } from '../query-fields-parser/query-fields-parser';

@Injectable()
export class PrismaQueryAdapterService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly prismaResponseAdapter: PrismaResponseAdapterService,
    private readonly queryFieldsParser: QueryFieldsParser,
  ) {}

  private getGeometryType(where: any): GeometryType | string {
    if (where.geometry && where.geometry.type) {
      return where.geometry.type;
    }
    return '';
  }

  private injectCoordinatesWhereQuery(whereQuery: any) {
    const geometryType = this.getGeometryType(whereQuery);
    if (!(whereQuery.geometry && whereQuery.geometry.coordinates)) {
      return whereQuery;
    }
    const coordinatesSubquery = { ...whereQuery.geometry.coordinates };
    whereQuery.geometry = removeProperties(
      ['coordinates'],
      whereQuery.geometry,
    );
    const pointWhereQuery = {
      point: {
        ...coordinatesSubquery,
      },
    };

    const lineStringWhereQuery = {
      lineString: {
        coordinates_every: {
          ...coordinatesSubquery,
        },
      },
    };

    const polygonWhereQuery = {
      polygon: {
        lines_every: {
          coordinates_every: {
            ...coordinatesSubquery,
          },
        },
      },
    };

    switch (geometryType) {
      case GeometryType.LineString: {
        return {
          ...whereQuery,
          geometry: {
            ...whereQuery.geometry,
            ...lineStringWhereQuery,
          },
        };
      }
      case GeometryType.Point: {
        return {
          ...whereQuery,
          geometry: {
            ...whereQuery.geometry,
            ...pointWhereQuery,
          },
        };
      }
      case GeometryType.Polygon: {
        return {
          ...whereQuery,
          geometry: {
            ...whereQuery.geometry,
            ...polygonWhereQuery,
          },
        };
      }
      default:
        return {
          ...whereQuery,
          geometry: {
            ...whereQuery.geometry,
            ...pointWhereQuery,
            ...polygonWhereQuery,
            ...lineStringWhereQuery,
          },
        };
    }
  }

  private async translateToPrismaFeatureWhereQuery(
    where: ApiFeatureWhereInput,
  ): Promise<PrismaFeatureWhereInput> {
    let newWhereInput: any = {
      ...where,
    };
    newWhereInput = this.injectCoordinatesWhereQuery(newWhereInput);

    // handle properties
    if (where.properties) {
      // TODO: handle properties where query
      newWhereInput['properties'] = {};
    }
    return newWhereInput;
  }

  async features(
    where: ApiFeatureWhereInput,
    info: GraphQLResolveInfo,
  ): Promise<[Feature]> {
    let queryFieldObject = graphqlFields(info);
    // TODO: handle OR, AND, NOT where queries
    const prismaWhereQueryObject = await this.translateToPrismaFeatureWhereQuery(
      where,
    );

    queryFieldObject = await this.queryFieldsParser.injectGeometryQueryObject(
      queryFieldObject,
      this.getGeometryType(where) as GeometryType,
    );

    queryFieldObject = await this.queryFieldsParser.injectPropertiesTagsQueryObject(
      queryFieldObject,
    );

    const response = await this.prismaService.query.features(
      { where: prismaWhereQueryObject },
      this.queryFieldsParser.queryFieldsObjectToQueryString(queryFieldObject),
    );

    return this.prismaResponseAdapter.features(response);
  }

  async feature(
    where: FeatureWhereUniqueInput,
    info: GraphQLResolveInfo,
  ): Promise<Feature> {
    let adaptedQueryFieldsObject = graphqlFields(info);

    adaptedQueryFieldsObject = await this.queryFieldsParser.injectGeometryQueryObject(
      adaptedQueryFieldsObject,
      this.getGeometryType(where) as GeometryType,
    );

    adaptedQueryFieldsObject = await this.queryFieldsParser.injectPropertiesTagsQueryObject(
      adaptedQueryFieldsObject,
    );

    const response = await this.prismaService.query.feature(
      { where },
      this.queryFieldsParser.queryFieldsObjectToQueryString(
        adaptedQueryFieldsObject,
      ),
    );

    return this.prismaResponseAdapter.feature(response);
  }
}
