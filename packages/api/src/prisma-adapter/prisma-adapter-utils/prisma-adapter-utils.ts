import { replaceProperty } from '../../utils/utils';
import {GraphQLResolveInfo} from "graphql";
import * as graphqlFields from 'graphql-fields';

export function isNumber(value) {
  return (
    !isNaN(value) && !Array.isArray(value) && value !== '' && value !== null
  );
}
export type LngLat = [number, number];
export type LineLngLat = LngLat[];
export type PolygonLngLat = LineLngLat[];

export async function injectPropertyTagsWhere(where: any): Promise<any> {
  if (!where.properties || !where.properties.tags) return where;
  const prop = {
    tags_some: {
      key: {
        value: where.properties.tags.key,
      },
      value: {
        value: where.properties.tags.value,
      },
    },
  };
  const newProperties = replaceProperty('tags', where.properties, prop);
  return {
    ...where,
    properties: {
      ...newProperties,
    },
  };
}
export const queryFields = (info: GraphQLResolveInfo) => {
  return graphqlFields(info);
}
