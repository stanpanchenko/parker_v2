import { injectPropertyTagsWhere, isNumber } from './prisma-adapter-utils';

describe('PrismaAdapterUtils', () => {
  describe('isNumber', () => {
    it('should return true if number passed', function() {
      const n = 2;
      expect(isNumber(n)).toBeTruthy();
    });

    it('should be false if string passed', function() {
      const n = 'q';
      expect(isNumber(n)).toBeFalsy();
    });
    it('should be false if array passed', function() {
      const n = [2];
      expect(isNumber(n)).toBeFalsy();
    });
    it('should be false if empty string passed', function() {
      const n = '';
      expect(isNumber(n)).toBeFalsy();
    });
    it('should be false if empty null passed', function() {
      const n = null;
      expect(isNumber(n)).toBeFalsy();
    });
  });

  describe('injectPropertyTagsWhere', () => {
    it('should return keys value where query if tags are present in query', async () => {
      const query = {
        properties: {
          tags: {
            key: 'key',
            value: 'value',
          },
        },
      };

      expect(await injectPropertyTagsWhere(query)).toEqual({
        properties: {
          tags_some: {
            key: {
              value: 'key',
            },
            value: {
              value: 'value',
            },
          },
        },
      });
    });
  });
});
