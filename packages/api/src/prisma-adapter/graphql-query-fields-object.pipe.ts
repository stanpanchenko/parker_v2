import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import * as graphqlFields from 'graphql-fields';
import { GraphQLResolveInfo } from 'graphql';

@Injectable()
export class GraphqlQueryFieldsObjectPipe implements PipeTransform {
  async transform(value: GraphQLResolveInfo, metadata: ArgumentMetadata) {
    // transforms GraphQLResolveInfo to a queryFieldsObject
    // gets: GraphQLResolveInfo with requested field: {geometry { type  coordinates: { lat lng }}}
    // returns: object: {geometry:{ type:{}, coordinates:{ lat:{}, lng:{} }}}
    return graphqlFields(value);
  }
}
