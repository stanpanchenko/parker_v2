import {
	PipeTransform,
	Injectable,
	ArgumentMetadata,
	HttpException,
	HttpStatus, Logger,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
	async transform(value, { metatype }: ArgumentMetadata) {
		
		// if valid return
		if (!metatype || !this.toValidate(metatype)) {
			return value;
		}
		
		const object = plainToClass(metatype, value); // transforms JSON object to Class object
		const errors = await validate(object); // validates the Class object
		
		if (errors.length > 0) {
			const parsedErrors = this.parseErrors(errors);
			Logger.log(parsedErrors, 'ValidationPipe');
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST, // 400
				timestamp: new Date().toLocaleTimeString(),
				message: 'Validation Error',
				errors: parsedErrors,
			}, HttpStatus.BAD_REQUEST); // 400
		}
		return value;
	}
	
	private toValidate(metatype): boolean {
		const types = [String, Boolean, Number, Array, Object];
		return !types.find((type) => metatype === type);
	}
	
	private parseErrors(errors: any[]){
		return errors.map(error => {
			return {
				message: `Passed invalid ${error.target.constructor.name} object!`,
				error: error,
			};
		});
	}
}