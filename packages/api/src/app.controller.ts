import { Get, Controller, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  // @UseGuards(AuthGuard())
  root() {
    return this.appService.root();
  }
  @Get('protected')
  @UseGuards(AuthGuard())
  async protectedRoute() {
    return 'reached protected route here';
  }
}
