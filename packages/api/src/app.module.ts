import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ParkingSpacesModule } from './parkingspaces/parking-spaces.module';
import { UsersModule } from './users/users.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { LoggerModule } from './logger/logger.module';
import { GraphQLModule } from './graphql/graphql.module';
import { MapModule } from './map/map.module';
import { XmlToJsonConverterService } from './xml-to-json-converter/xml-to-json-converter.service';
import { SearchModule } from './search/search.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    AuthenticationModule,
    ParkingSpacesModule,
    UsersModule,
    DatabaseModule,
    GraphQLModule,
    LoggerModule,
    MapModule,
    SearchModule,
  ],
  controllers: [AppController],
  providers: [AppService, XmlToJsonConverterService],
})
export class AppModule {}
