import 'dotenv/config';

export const env: any = {
  API_PORT: process.env.API_PORT,
  PRISMA_END_POINT: process.env.PRISMA_END_POINT,
  WEBSOCKET_PORT: process.env.WEBSOCKET_PORT,
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
  JWT_EXPIRES_IN: process.env.JWT_EXPIRES_IN,
};
