export const asyncForEach = async (
  array: any[],
  asyncCallback: (elem: any, index: number, array: any[]) => Promise<any>,
) => {
  for (let index = 0; index < array.length; index++) {
    await asyncCallback(array[index], index, array);
  }
};
export const mapObject = (
  targetObject: any,
  callbackFn: (key: string, value: any) => any,
): any => {
  if (!targetObject) return targetObject;
  if (typeof targetObject === 'number' || typeof targetObject === 'string')
    if (Array.isArray(targetObject)) {
      return targetObject.map(v => mapObject(v, callbackFn));
    }
  return Object.entries(targetObject).reduce((acc, [key, value]) => {
    const res = callbackFn(key, value);
    if (!Array.isArray(res) && typeof res === 'object') {
      return { ...acc, [key]: mapObject(res, callbackFn) };
    }
    if (Array.isArray(res)) {
      return { ...acc, [key]: res.map(v => mapObject(v, callbackFn)) };
    }
    return { ...acc, [key]: res };
  }, {});
};

export async function asyncMap<ElemType = any>(
  array: ElemType[],
  asyncCallback: (
    elem: ElemType,
    index: number,
    array: ElemType[],
  ) => Promise<any>,
) {
  return Promise.all(array.map(asyncCallback));
}

export function removeProperties(
  properties: string[],
  fromObject: { [key: string]: any },
): { [key: string]: any } {
  return Object.entries(fromObject).reduce(
    (acc, [key, value]: [string, any]) => {
      if (properties.includes(key)) return acc;
      return { ...acc, [key]: value };
    },
    {},
  );
}

export function replaceProperty(
  oldPropertyName: string,
  hostObject: { [key: string]: any },
  guestObject: { [key: string]: any },
) {
  if (!hostObject[oldPropertyName]) return hostObject;
  const { [oldPropertyName]: toDelete, ...rest } = hostObject;
  return {
    ...rest,
    ...guestObject,
  };
}

/**
 * @param { any } input
 * @param { Array } asyncCallbacks
 * @desc The result of a function is propagated to the next function.
 */
export async function chain(
  input: any,
  asyncCallbacks: ((input: any) => Promise<any>)[],
) {
  let res = input;
  for (const callback of asyncCallbacks) {
    res = await callback(res);
  }
  return res;
}
