import { mapObject } from './utils';

describe('utilities', () => {
  describe('mapObject', () => {
    it('should remove attribute if callbackFn returns undefined', () => {
      const data = {
        id: 'id',
        geometry: {
          id: 'id',
          coordinates: [{ id: 'id' }, { id: 'id' }, { id: 'id' }],
        },
      };
      const res = mapObject(data, (key, value) => {
        if (key === 'id') return;
        return value;
      });
      expect(res).toEqual({
        geometry: {
          coordinates: [{}, {}, {}],
        },
      });
    });

    it('should replace attribute if callbackFn returns new value for a key', () => {
      const data = {
        id: 'id',
        geometry: {
          id: 'id',
          coordinates: [{ id: 'id' }, { id: 'id' }, { id: 'id' }],
        },
      };
      const res = mapObject(data, (key, value) => {
        if (key === 'id') return 1;
        return value;
      });
      expect(res).toEqual({
        id: 1,
        geometry: {
          id: 1,
          coordinates: [
            {
              id: 1,
            },
            { id: 1 },
            { id: 1 },
          ],
        },
      });
    });
  });
});
