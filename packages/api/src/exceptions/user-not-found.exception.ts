import { NotFoundException } from '@nestjs/common';

export class UserNotFoundException extends NotFoundException {
  constructor(message?: string | object | any, error?: string) {
    super(message, error);
  }
}
