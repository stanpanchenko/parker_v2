export interface LocationI {
  street: string;
  city: string;
  state: string;
  postcode: number;
  coordinates: {
    latitude: number;
    longitude: number;
  };
  timezone: {
    offset: string;
    description: string;
  };
}
