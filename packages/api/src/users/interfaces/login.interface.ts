export interface LoginI {
  username: string;
  password: string;
  history: number[]; // login times in ms
}
