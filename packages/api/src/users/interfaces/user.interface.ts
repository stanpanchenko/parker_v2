import { LocationI } from './location.interface';
import { LoginI } from './login.interface';
import { RoleType } from '../../authentication/types/roles.type';
import { IsString } from 'class-validator';

export interface UserI {
  uuid: string;
  gender: string;
  email: string;
  name: {
    first: string;
    last: string;
  };
  location: LocationI;
  login: LoginI;
  registered: {
    date: number;
    age: number;
  };
  roles: RoleType[];
  phone: string;
  picture: {
    large: string;
    medium: string;
    thumbnail: string;
  };
}
