import { Injectable } from '@nestjs/common';
import { DatabaseService } from '../database/database.service';
import { HashService } from '../authentication/hash.service';
import { UserCreateInput } from '../graphql-api.interface';
import { ID_Output } from '../prisma/prisma.interfaces';

import { GraphQLResolveInfo } from 'graphql';

@Injectable()
export class UsersService {
  constructor(
    private readonly databaseService: DatabaseService,
    private readonly hashService: HashService,
  ) {}

  async createUser(
    userCreateInput: UserCreateInput,
    info?: GraphQLResolveInfo,
  ): Promise<ID_Output> {
    const hashedPassword = await this.hashService.hash(
      userCreateInput.password,
    );
    return this.databaseService.createUser(
      {
        name: userCreateInput.name,
        email: userCreateInput.email,
        password: hashedPassword,
        isActive: true,
        roles: userCreateInput.roles,
      },
      info,
    );
  }

  async getUser() {
    console.log('hier');
    return await this.databaseService.getUser({ email: 'user2@mail.de' });
  }
  async findOneByEmail(email: string): Promise<any> {
    const user = await this.databaseService.findUserByEmail(email);
    if (!user) return null;
    const newUser = {
      ...user,
    };
    return newUser;
  }

  async saveUser() {
    return;
  }

  async findOneById(id: string): Promise<null> {
    return null;
  }
}
