import { Args, Info, Mutation, Query, Resolver } from '@nestjs/graphql';
import { DatabaseService } from '../database/database.service';
import {
  User,
  UserWhereInput,
  UserWhereUniqueInput,
} from '../prisma/prisma.interfaces';
import { Roles as UserRoles, UserCreateInput } from '../graphql-api.interface';
import { UsersService } from './users.service';
import { GraphQLResolveInfo } from 'graphql';
import { UseGuards } from '@nestjs/common';
import { HasSessionGuard } from '../authentication/guards/has-session.guard';
import { RolesGuard } from '../authentication/guards/roles.guard';
import { Roles } from '../authentication/roles/roles.decorator';

@Resolver()
export class UsersResolver {
  constructor(
    private readonly databaseService: DatabaseService,
    private readonly userService: UsersService,
  ) {}

  @UseGuards(HasSessionGuard, RolesGuard)
  @Roles(UserRoles.ADMIN)
  @Query('user')
  async user(@Args('where') where: UserWhereUniqueInput): Promise<User> {
    return await this.databaseService.getUser(where);
  }

  @UseGuards(HasSessionGuard, RolesGuard)
  @Roles(UserRoles.ADMIN)
  @Query('users')
  async users(@Args('where') where: UserWhereInput): Promise<User[]> {
    return await this.databaseService.getUsers(where);
  }

  @Mutation('createUser')
  async createUser(
    @Args('data') data: UserCreateInput,
    @Info() info?: GraphQLResolveInfo,
  ) {
    return await this.userService.createUser(data, info);
  }
}
