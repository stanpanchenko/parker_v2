import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { DatabaseModule } from '../database/database.module';
import { LoggerModule } from '../logger/logger.module';
import { HashService } from '../authentication/hash.service';
import { UsersResolver } from './users.resolver';
import { AuthenticationModule } from '../authentication/authentication.module';

@Module({
  imports: [DatabaseModule, LoggerModule, AuthenticationModule],
  providers: [UsersResolver, UsersService, HashService],
  exports: [UsersService],
})
export class UsersModule {}
