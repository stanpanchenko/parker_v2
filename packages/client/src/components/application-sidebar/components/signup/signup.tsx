import React from "react";
import Input, { InputPropsI } from "../../../../shared/components/input/input";
import Button from "../../../shared/buttons/Button";

import { ApplicationStateI } from "../../../../redux/states";
import { connect } from "react-redux";
import {
  Roles,
  SignUpInput,
  User,
  UserCreateInput
} from "../../../../api/graphql/graphql-api.interface";
import { isEmail } from "../../../../utils/validation";
import { ErrorsSelectors } from "../../../../redux/states/errors/errors.selectors";
import { ErrorI } from "../../../../redux/states/errors/errors.reducer";
import { Form } from "../../../../shared/components/form";
import { UserActionCreators } from "../../../../redux/states/user/actions/creators";
import { UserSignUpActionI } from "../../../../redux/states/user/actions/interfaces/user.action-interfaces";
import { newID } from "../../../../pages/public/map/components/parker-map/components/defaultSpaceArea";

interface IndicatorInputPropsI extends InputPropsI {
  highlight: boolean;
}

function IndicatorInput({
  highlight,
  onChange,
  value,
  type,
  placeholder
}: IndicatorInputPropsI) {
  return (
    <Input
      style={
        highlight
          ? {
              borderColor: "red"
            }
          : null
      }
      onChange={onChange}
      type={type}
      value={value}
      placeholder={placeholder}
    />
  );
}

interface SignUpPropsI {
  createUser: (userCreateInput: SignUpInput) => UserSignUpActionI;
  error: ErrorI | null;
}

function Signup({ createUser, error }: SignUpPropsI) {
  function handleSubmit(formValues) {
    const { email, password, name } = formValues;
    createUser({
      email: email,
      password: password,
      name: name,
      confPassword: password
    });
  }

  function validate(values) {
    // return true if valid
    const { email, password, confPassword, name } = values;
    if (!name) return false;
    if (password !== confPassword) return false;
    if (!password) return false;
    if (password && password.length < 6) return false;
    if (!isEmail(email)) return false;

    return true;
  }

  return (
    <div>
      <Form
        onSubmit={handleSubmit}
        validate={validate}
        initialValid={true}
        initialValues={{
          email: "",
          password: "",
          confPassword: "",
          name: ""
        }}
      >
        {({
          setValue,
          formValues: { email, password, confPassword, name },
          valid
        }) => (
          <div>
            <div>
              <Input
                name={"name"}
                id={"name"}
                placeholder={"name"}
                value={name || ""}
                type={"text"}
                required
                onChange={e => setValue("name", e)}
              />
            </div>
            <div>
              <Input
                name={"email"}
                id={"email"}
                placeholder={"email"}
                value={email || ""}
                type={"email"}
                required
                onChange={e => setValue("email", e)}
              />
            </div>
            <div>
              <Input
                name={"password"}
                id={"password"}
                placeholder={"password"}
                value={password || ""}
                type={"password"}
                required
                onChange={e => setValue("password", e)}
              />
            </div>
            <div>
              <Input
                name={"confPassword"}
                id={"confPassword"}
                placeholder={"confirm"}
                value={confPassword || ""}
                type={"password"}
                required
                onChange={e => setValue("confPassword", e)}
              />
            </div>
            <Button disabled={!valid}>Sign Up</Button>
          </div>
        )}
      </Form>
    </div>
  );
}

const props = (state: ApplicationStateI) => ({
  error: ErrorsSelectors.errorSelector(state)
});

const actions = dispatch => ({
  createUser: (userCreateInput: SignUpInput) =>
    dispatch(UserActionCreators.createUserAction(userCreateInput))
});

export default connect(
  props,
  actions
)(Signup);
