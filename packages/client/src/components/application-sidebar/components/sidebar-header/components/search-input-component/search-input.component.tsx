import React from "react";
import { CSSTransition } from "react-transition-group";
import "./search-input-style.scss";
import IconButton from "../../../../../../shared/components/icon-button/icon-button.component";
import Input from "../../../../../../shared/components/input/input";

interface PropsI {}
interface StateI {
  inputVisible: boolean;
}
class SearchInputComponent extends React.Component<PropsI, StateI> {
  state = {
    inputVisible: false
  };

  componentDidMount() {}

  handleClick = () => {
    this.setState(s => ({
      ...s,
      inputVisible: !s.inputVisible
    }));
  };

  render() {
    return (
      <div
        className={"search-input-component"}
        style={{
          display: "flex",
          alignItems: "center",
          marginLeft: "20px"
        }}
      >
        <div className={"icon-wrapper"}>
          <IconButton
            classNames={[""]}
            onClick={() => this.handleClick()}
            name={"search"}
          />
        </div>
        <CSSTransition
          classNames={"input-wrapper-animation"}
          timeout={400}
          in={this.state.inputVisible}
          mountOnEnter
          unmountOnExit
        >
          <div className={"input-wrapper"}>
            <Input
              value={""}
              type={"text"}
              onChange={value => {}}
              placeholder={"Suche"}
            />
          </div>
        </CSSTransition>
      </div>
    );
  }
}

export default SearchInputComponent;
