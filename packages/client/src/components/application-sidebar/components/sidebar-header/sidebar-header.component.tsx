import React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import IconButton from "../../../../shared/components/icon-button/icon-button.component";
import SearchInputComponent from "./components/search-input-component/search-input.component";
import styled from "styled-components";
import { Theme } from "../../../../shared/providers/theme/Theme";
import { ApplicationStateI } from "../../../../redux/states";
import { connect } from "react-redux";
import ThemeI from "../../../../shared/providers/theme/theme.interface";
import { Login } from "../../../../shared/components/login/login";
import { LogoutButton } from "../../../../shared/components/login/logout-button";
import "./sidebar-header-component.style.scss";
import Button from "../../../shared/buttons/Button";
import { ThemeUISelectors } from "../../../../redux/states/ui/states/theme/selectors";
import { UserAuthenticationSelectors } from "../../../../redux/states/user/substates/authentication/selectors";

interface StyledHeaderPropsI {
  theme: ThemeI;
}
const StyledHeader = styled.div(
  {
    height: "55px",
    position: "absolute",
    top: 0,
    zIndex: 100,
    width: "100%",
    display: "flex",
    paddingLeft: "1.3vw"
  },
  (props: StyledHeaderPropsI) => ({
    backgroundColor: props.theme.backgroundColorSecond,
    borderBottom: `1px solid ${props.theme.borderColorPrime}`
  })
);

interface PropsI {
  theme: Theme;
  isAuthenticated: boolean;
}

const SidebarHeaderComponent = ({
  isAuthenticated,
  history
}: PropsI & RouteComponentProps) => {
  return (
    <StyledHeader>
      <IconButton
        classNames={[]}
        onClick={() => history.push("/")}
        name={"times"}
      />
      <SearchInputComponent />
      {!isAuthenticated ? <Login /> : <LogoutButton />}
      <div>
        <Button
          onClick={() => {
            history.push("/signup");
          }}
        >
          Sign Up
        </Button>
      </div>
    </StyledHeader>
  );
};
const props = (state: ApplicationStateI) => ({
  theme: ThemeUISelectors.currentTheme(state),
  isAuthenticated: UserAuthenticationSelectors.isAuthenticated(state)
});
const actions = dispatch => ({});

export default withRouter(
  connect(
    props,
    actions
  )(SidebarHeaderComponent)
);
