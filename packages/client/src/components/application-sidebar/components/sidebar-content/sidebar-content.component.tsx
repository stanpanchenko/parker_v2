import React from "react";

import "./sidebar-content-component.style.scss";
import { Route, RouteComponentProps, Switch, withRouter } from "react-router";
import AboutPage from "../../../../pages/public/about/about-page.component";
import styled from "styled-components";
import Signup from "../signup/signup";

interface PropsI {
  theme?: any;
}

const ContentStyled = styled.div`
  overflow-y: scroll;
  height: calc(100% - 55px);
  margin-top: 55px;
`;

const SidebarContent_ = (props: PropsI & RouteComponentProps) => {
  const { location } = props;
  return (
    <ContentStyled>
      <Switch location={location}>
        <Route path={"/about"} component={AboutPage} />
        <Route path={"/signup"} component={Signup} />
      </Switch>
    </ContentStyled>
  );
};

const SidebarContent = withRouter(SidebarContent_);
export default SidebarContent;
