import { createSelector } from 'reselect';

import {FREE_OF_CHARGE} from './parking-spaces-types';
const getParkingSpaces = state => state.parkingSpaces.spaces;
export const freeParkingSpacesSelector = createSelector([getParkingSpaces], (parkingSpaces) => {
	return parkingSpaces.filter(space => space.type === FREE_OF_CHARGE);
});