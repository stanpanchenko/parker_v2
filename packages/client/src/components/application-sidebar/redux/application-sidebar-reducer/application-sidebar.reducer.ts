import { Reducer } from "redux";
import { ApplicationSidebarActionIs } from "../application-sidebar-actions/application-sidebar.action-interfaces";
import { ApplicationSidebarActionTypes } from "../application-sidebar-actions/application-sidebar.action-types";
import { ApplicationSidebarStateI } from "./application-sidebar.state-interface";
import ApplicationSidebarReducerFunctions from "./application-sidebar.reducer-functions";

const initState: ApplicationSidebarStateI = {
  visible: false
};

const reducer: Reducer<
  ApplicationSidebarStateI,
  ApplicationSidebarActionIs
> = (state = initState, action) => {
  switch (action.type) {
    case ApplicationSidebarActionTypes.hideApplicationSidebar:
      return ApplicationSidebarReducerFunctions.hide(state);
    case ApplicationSidebarActionTypes.showApplicationSidebar:
      return ApplicationSidebarReducerFunctions.show(state);
    default:
      return state;
  }
};
export { reducer as ApplicationSidebarReducer };
