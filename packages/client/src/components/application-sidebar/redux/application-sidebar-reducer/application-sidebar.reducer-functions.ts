import {ApplicationSidebarStateI} from "./application-sidebar.state-interface";


const getNewState = (state: ApplicationSidebarStateI): ApplicationSidebarStateI => {
    const newState = {
        ...state,
    };
    return newState;
};

const hide = (state: ApplicationSidebarStateI): ApplicationSidebarStateI => {
    const newState = getNewState(state);
    newState.visible = false;
    return newState;
};

const show = (state: ApplicationSidebarStateI): ApplicationSidebarStateI => {
    const newState = getNewState(state);
    newState.visible = true;
    return newState;
};

const ApplicationSidebarReducerFunctions = {
    show,
    hide,
};

export default ApplicationSidebarReducerFunctions;
