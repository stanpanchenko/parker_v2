export enum ApplicationSidebarActionTypes {
    showApplicationSidebar = '[APPLICATION_SIDEBAR]SHOW_PARKING_SPACE_SIDEBAR',
    hideApplicationSidebar = '[APPLICATION_SIDEBAR]HIDE_PARKING_SPACE_SIDEBAR',
    toggleApplicationSidebar = '[APPLICATION_SIDEBAR]TOGGLE_PARKING_SPACE_SIDEBAR',
}