import {
	HideApplicationSidebarActionI,
	ShowApplicationSidebarActionI
} from "./application-sidebar.action-interfaces";
import {ApplicationSidebarActionTypes} from "./application-sidebar.action-types";


const hideApplicationSidebar = (): HideApplicationSidebarActionI => ({
	type: ApplicationSidebarActionTypes.hideApplicationSidebar,
});

const showApplicationSidebar = (): ShowApplicationSidebarActionI => ({
	type: ApplicationSidebarActionTypes.showApplicationSidebar,
});

export const ApplicationSidebarActions = {
	hide: hideApplicationSidebar,
	show: showApplicationSidebar,
};

export default ApplicationSidebarActions;
