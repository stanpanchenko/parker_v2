import {ApplicationSidebarActionTypes} from "./application-sidebar.action-types";

export interface HideApplicationSidebarActionI {
    type: ApplicationSidebarActionTypes.hideApplicationSidebar;
}

export interface ShowApplicationSidebarActionI {
    type: ApplicationSidebarActionTypes.showApplicationSidebar;
}

export type ApplicationSidebarActionIs = (
    HideApplicationSidebarActionI |
    ShowApplicationSidebarActionI
);
