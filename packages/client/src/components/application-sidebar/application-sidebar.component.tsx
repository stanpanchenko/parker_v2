import React from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../redux/states/";
import { AnyAction, Dispatch } from "redux";
import { CSSTransition } from "react-transition-group";
import SidebarHeaderComponent from "./components/sidebar-header/sidebar-header.component";
import ApplicationSidebarActions from "./redux/application-sidebar-actions/application-sidebar.actions";
import "./sidebar.style.scss";
import SidebarContent from "./components/sidebar-content/sidebar-content.component";
import { Theme } from "../../shared/providers/theme/Theme";
import styled from "styled-components";
import ThemeI from "../../shared/providers/theme/theme.interface";
import { RouteComponentProps, withRouter } from "react-router";
import { ThemeUISelectors } from "../../redux/states/ui/states/theme/selectors";
import { ThemeUIActionCreators } from "../../redux/states/ui/states/theme/actions/creators";

interface ThemePropsI {
  theme: ThemeI;
}
const SidebarStyled = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  bottom: 5px;
  // height: 100vh;
  overflow: hidden;
  box-shadow: ${(props: ThemePropsI) => props.theme.shadowPrime};
  background-color: ${(props: ThemePropsI) => props.theme.backgroundColor};
`;

interface PropsI extends RouteComponentProps {
  showSidebar: () => AnyAction;
  hideSidebar: () => AnyAction;
  theme: Theme;
  changeTheme: (theme: Theme) => AnyAction;
  history: any;
}

const SideBar = ({
  showSidebar,
  theme,
  hideSidebar,
  history,
  changeTheme,
  location
}: PropsI) => {
  const paths = ["/about", "/signup"];
  const visiblePaths = () => paths.includes(location.pathname);

  return (
    <CSSTransition
      classNames={"sidebar-component-animation"}
      timeout={{ enter: 1200, exit: 400 }}
      appear={true}
      in={visiblePaths()}
      unmountOnExit={true}
      onEnter={showSidebar}
      onExit={hideSidebar}
      mountOnEnter={true}
    >
      <SidebarStyled className={"sidebar-component"}>
        <SidebarHeaderComponent />
        <SidebarContent />
      </SidebarStyled>
    </CSSTransition>
  );
};

const props = (state: ApplicationStateI) => ({
  theme: ThemeUISelectors.currentTheme(state)
});

const actions = (dispatch: Dispatch) => ({
  showSidebar: () => dispatch(ApplicationSidebarActions.show()),
  hideSidebar: () => dispatch(ApplicationSidebarActions.hide()),
  changeTheme: (theme: Theme) =>
    dispatch(ThemeUIActionCreators.changeTheme(theme))
});

export const ApplicationSidebar = withRouter(
  connect(
    props,
    actions
  )(SideBar)
);
