import React from "react";
import { Route, Switch } from "react-router";
import MapPage from "../../pages/public/map/map.page";
import { NotFound } from "../../pages/public/not-found/NotFound";

enum AppRoutes {
  MAP = "/"
}

export function ApplicationRoutes() {
  return (
    <Switch>
      <Route path={AppRoutes.MAP} component={MapPage} />
      <Route component={NotFound} />
    </Switch>
  );
}
