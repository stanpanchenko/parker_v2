import styled from "styled-components";
import Button from "./Button";
import React, { ReactChild, ReactNode } from "react";
import ThemeI from "../../../shared/providers/theme/theme.interface";

interface PropsI {
  disabled?: boolean;
  theme: ThemeI;
}

const ButtonTertiaryStyled = styled(Button)`
  background: transparent;
  border: none;
  font-size: 0.8rem;
  font-weight: 600;
  margin: 0.2rem;
  &:disabled {
    background: none;
    color: grey;
    cursor: default;
  }
  color: ${(props: PropsI) =>
    props.disabled === true ? "grey" : props.theme.colorPrime};
`;

const ButtonTertiary = React.memo(function({
  disabled,
  onClick,
  children
}: {
  disabled?: boolean;
  onClick: () => any;
  children: ReactNode;
}) {
  return (
    <ButtonTertiaryStyled disabled={disabled} onClick={onClick}>
      {children}
    </ButtonTertiaryStyled>
  );
});
export default ButtonTertiary;
