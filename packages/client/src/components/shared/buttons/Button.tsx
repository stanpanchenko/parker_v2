import styled from "styled-components";

const Button = styled.button`
  cursor: pointer;

  padding: 5px 15px 7px 15px;
  font-size: 0.7rem;
  border-radius: 7px;
  background: linear-gradient(
    0deg,
    rgb(25, 120, 241) 22%,
    rgb(53, 167, 249) 100%
  );
  color: white;

  border: 1px solid #34b8d8;
  position: relative;
  outline: none;
  font-weight: 500;
  text-align: center;
  margin: 5px;
  will-change: background;
  transition: background 2000ms linear;

  &:disabled {
    color: #61bae8;
    cursor: not-allowed;
    background: linear-gradient(
      0deg,
      rgb(25, 120, 241) 22%,
      rgb(25, 128, 202) 100%
    );
  }
`;
export default Button;
