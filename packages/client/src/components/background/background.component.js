import React from 'react';
import './background.style.scss';
import {CSSTransition} from "react-transition-group";


const Background = (props) => {
	
	return(
		<CSSTransition
			in={props.in}
			unmountOnExit={true}
			timeout={100}
			classNames={'fade'}>
			<div className={'background'}/>
		</CSSTransition>
	)
	
};

export default Background;