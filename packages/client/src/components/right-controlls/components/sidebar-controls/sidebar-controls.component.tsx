import React from "react";
import { AnyAction, Dispatch } from "redux";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import "./sidebar-controls.style.scss";
import { ApplicationStateI } from "../../../../redux/states";
import ApplicationSidebarActions from "../../../application-sidebar/redux/application-sidebar-actions/application-sidebar.actions";
import styled from "styled-components";
import { Theme, Themes } from "../../../../shared/providers/theme/Theme";
import ThemeI from "../../../../shared/providers/theme/theme.interface";
import { Icon } from "../../../../shared/components/icon";
import { ThemeUIActionCreators } from "../../../../redux/states/ui/states/theme/actions/creators";
import { ThemeUISelectors } from "../../../../redux/states/ui/states/theme/selectors";

interface StyledComponentPropsI {
  theme: ThemeI;
  [name: string]: any;
}

const ButtonStyled = styled("button")(
  {
    cursor: "pointer",
    border: "none",
    fontSize: ".7rem",
    padding: "6px",
    fontWeight: 600,
    outline: "none"
  },
  (props: StyledComponentPropsI) => {
    return {
      backgroundColor: props.theme.buttonPrimaryBackgroundColor,
      color: props.theme.buttonPrimeTextColor
    };
  }
);

interface ButtonGrpupStyledPropsI {
  theme: StyledComponentPropsI;
  sidebarOpen: boolean;
}
const ButtonGroupStyled = styled("div")(
  {
    borderRadius: "5px",
    margin: "5px",
    padding: "5px",
    transition: "box-shadow 400ms"
  },
  (props: ButtonGrpupStyledPropsI) => ({
    backgroundColor: props.theme.buttonPrimaryBackgroundColor,
    border: `1px solid ${props.theme.borderColorPrime}`,
    boxShadow: props.sidebarOpen ? null : props.theme.shadowPrime
  })
);

interface PropsI {
  hide: () => any;
  visible: boolean;
  show: () => any;
  changeTheme: (theme: Theme) => AnyAction;
  theme: Theme;
}

const SidebarControls_ = (props: PropsI & RouteComponentProps) => {
  const { theme } = props;
  return (
    <div className={"sidebar-controls"}>
      <ButtonGroupStyled sidebarOpen={props.visible}>
        <ButtonStyled
          onClick={() => {
            const { pathname } = props.history.location;
            if (pathname === "/about") {
              props.history.push("/");
            } else {
              props.history.replace("/about");
            }
          }}
        >
          About
        </ButtonStyled>
        {theme === Themes.dark ? (
          <ButtonStyled onClick={() => props.changeTheme(Themes.default)}>
            <Icon name={"sun"} />
          </ButtonStyled>
        ) : (
          <ButtonStyled onClick={() => props.changeTheme(Themes.dark)}>
            <Icon name={"moon"} />
          </ButtonStyled>
        )}
      </ButtonGroupStyled>
    </div>
  );
};

const props = (state: ApplicationStateI) => ({
  visible: state.ui.applicationSidebar.visible,
  theme: ThemeUISelectors.currentTheme(state)
});

const actions = (dispatch: Dispatch) => ({
  hide: () => dispatch(ApplicationSidebarActions.hide()),
  show: () => dispatch(ApplicationSidebarActions.show()),
  changeTheme: (theme: Theme) =>
    dispatch(ThemeUIActionCreators.changeTheme(theme))
});

const SidebarControls = withRouter(
  connect(
    props,
    actions
  )(SidebarControls_)
);
export default SidebarControls;
