import styled from "styled-components";
import { ThemePropsI } from "../../shared/providers/theme/interfaces/theme-props.interface";

export const ToastWrapper = styled.div`
  position: absolute;
  width: 40vw;
  min-width: 335px;
  height: 80px;
  top: 20px;
  z-index: 100;
`;

export const ToastContent = styled.div`
  padding: 5px 35px 5px 35px;

  color: ${({ theme }: ThemePropsI) => theme.textColorPrime};
`;
