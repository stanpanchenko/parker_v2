import React, { useEffect, useState } from "react";
import { ToastAppearAnimation } from "./toast.animation";
import { Backdrop } from "../../shared/components/blur";
import { ToastContent } from "./toast.style";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../redux/states";
import { ToastMessage } from "../../redux/states/ui/states/toast/toast.state-reducer";
import IconButton from "../../shared/components/icon-button/icon-button.component";
import { ToastActionCreators } from "../../redux/states/ui/states/toast/actions/creators";
import { HideToastActionI } from "../../redux/states/ui/states/toast/actions/interfaces/toast.action-interfaces";

interface ReduxProps {
  message: ToastMessage;
  visible: boolean;
  hideToast: () => HideToastActionI;
}

function Toast({ hideToast, message, visible }: ReduxProps) {
  useEffect(() => {
    if (visible) {
      setTimeout(hideToast, 4000);
    }
  }, [visible]);
  return (
    <ToastAppearAnimation pose={visible ? "visible" : "hidden"}>
      <Backdrop blur={"35px"} borderRadius={60} withShadow>
        <ToastContent>
          <div>{message}</div>
        </ToastContent>
      </Backdrop>
    </ToastAppearAnimation>
  );
}

const props = (state: ApplicationStateI) => ({
  visible: state.ui.toast.visible,
  message: state.ui.toast.message
});
const actions = dispatch => ({
  hideToast: () => dispatch(ToastActionCreators.hideToast())
});
export default connect(
  props,
  actions
)(Toast);
