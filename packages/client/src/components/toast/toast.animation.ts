import posed from "react-pose";
import { ToastWrapper } from "./toast.style";

export const ToastAppearAnimation = posed(ToastWrapper)({
  init: {
    left: "50%",
    x: "-50%",
    opacity: 0.7
  },
  visible: {
    y: "0%",
    scale: 1,
    opacity: 1
  },
  hidden: {
    y: "-110%",
    scale: 0.5,
    opacity: 0.7
  }
});
