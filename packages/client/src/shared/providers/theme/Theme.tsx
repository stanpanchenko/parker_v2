import React from "react";
import { ThemeProvider } from "styled-components";
import ThemeI from "./theme.interface";
import { darkTheme as dark, defaultTheme as def } from "./themes";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../redux/states";
import { ThemeUISelectors } from "../../../redux/states/ui/states/theme/selectors";

// 1. add new enum to make the theme name available in the application
export enum Themes {
  default = "default",
  dark = "dark"
}

// 2. create new type for a theme
export type defaultTheme = "default";
export type darkTheme = "dark";
// 3. add new theme type the the Theme
export type Theme = darkTheme | defaultTheme /* | newThemeType*/;

interface PropsI {
  theme: Theme;
  children?: any;
}
interface ThemesContainer {
  [themeName: string]: ThemeI;
}
export const themesContainer: ThemesContainer = {
  [Themes.default]: def,
  [Themes.dark]: dark
};

const SwitchTheme = ({ theme, children }: PropsI) => {
  // 3. import theme to themeContainer

  if (!(theme in themesContainer)) {
    console.warn(
      `
      [SwitchTheme]: Theme with name: "${theme}" does not exist!
      You need to create a theme type and use the themes provided in "Themes" enum.`
    );
  }
  // const themeFromContainer =
  //   theme in themesContainer ? themesContainer[theme] : {};
  return (
    <ThemeProvider theme={themesContainer[theme]}>{children}</ThemeProvider>
  );
};
const props = (state: ApplicationStateI) => ({
  theme: ThemeUISelectors.currentTheme(state)
});
export default connect(
  props,
  null
)(SwitchTheme);
