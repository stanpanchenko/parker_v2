import ThemeI from "../theme.interface";
import commonTheme from "./comon.theme";

const defaultTheme: ThemeI = {
  name: "default",

  ...commonTheme,
  colors: {
    blue: "#004afe",
    green: {
      prime: "#21c058"
    },
    icon: {
      color: "#2d2d2d",
      hoverColor: "#5b5b5b"
    }
  },

  colorThird: "#6e6e6e",

  backgroundColor: "white",
  backgroundColorPrimeTransparent: "rgba(255,255,255,0.96)",

  backgroundColorSecond: "#f3f3f3",
  backgroundColorSecondTransparent: "rgba(243,243,243,0.9)",

  backgroundColorThird: "#d4d4d4e8",
  backgroundIconPrime: "#5f5f5f",

  backgroundGlassEffect: "rgba(255,255,255,0.9)",

  hoverPrime: "#e4e4e4",

  buttonPrimaryBackgroundColor: "white",
  buttonPrimeTextColor: "black",

  borderColorPrime: "#f3f3f3",
  borderColorSecond: "#dfdfdf",
  borderColorThird: "#d2d2d2",

  inputBackgroundPrime: "#e4e4e4",
  inputBackgroundSecond: "#dcdcdc",
  inputTextColor: "black",
  inputPlaceholderColor: "#afafaf",

  selectInputBackgroundColor: "white",

  textColorPrime: "#2d2d2d",
  textColorSecond: "#404040"
};

export default defaultTheme;
