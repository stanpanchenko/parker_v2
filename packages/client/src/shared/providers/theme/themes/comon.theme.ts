const commonTheme = {
  shadowPrime: "0 0 3px 0 rgba(0,0,0,0.1), 0 3px 5px 0 rgba(0,0,0,0.2)",
  colorPrime: "#004afe",

  animation: {
    times: {
      faster: "70ms",
      fast: "200ms",
      medium: "400ms",
      slow: "700ms"
    }
  },
  radius: {
    prime: 6,
    second: 7
  }
};

export default commonTheme;
