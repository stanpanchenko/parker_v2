import ThemeI from "../theme.interface";
import commonTheme from "./comon.theme";

const darkTheme: ThemeI = {
  name: "dark",

  ...commonTheme,
  colors: {
    blue: "#0070c9",
    green: {
      prime: "#21c058"
    },
    icon: {
      color: "#b9b9b9",
      hoverColor: "#e8e8e8"
    }
  },

  colorThird: "#6e6e6e",

  backgroundColor: "rgb(30,30,32)",
  backgroundColorPrimeTransparent: "rgba(30,30,32,0.95)",

  backgroundColorSecond: "#2e2e2f",
  backgroundColorSecondTransparent: "#1e1e20f2",

  backgroundColorThird: "#343434",
  backgroundIconPrime: "#808080",

  backgroundGlassEffect: "rgba(0,0,0,0.51)",

  hoverPrime: "#2d2d2ff2",

  buttonPrimaryBackgroundColor: "#383838",
  buttonPrimeTextColor: "white",

  borderColorPrime: "black",
  borderColorSecond: "#1d1d1d",
  borderColorThird: "#4c4c4c",

  inputBackgroundPrime: "rgb(100, 100, 101)",

  inputBackgroundSecond: "#4c4c4c",
  inputTextColor: "white",
  inputPlaceholderColor: "#afafaf",
  selectInputBackgroundColor: "white",

  textColorPrime: "white",
  textColorSecond: "#a7a7a7"
};

export default darkTheme;
