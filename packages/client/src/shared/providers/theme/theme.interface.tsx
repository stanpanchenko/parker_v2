interface ThemeI {
  name: string;

  colors: {
    blue: string;
    green: {
      prime: string;
    };
    icon: {
      color: string;
      hoverColor: string;
    };
  };
  radius: {
    prime: number;
    second: number;
  };
  colorPrime: string;
  colorThird: string;

  backgroundColor: string;
  backgroundColorPrimeTransparent: string;

  backgroundColorSecond: string;
  backgroundColorSecondTransparent: string;

  backgroundColorThird: string;
  backgroundIconPrime: string;

  backgroundGlassEffect: string;

  hoverPrime: string;

  buttonPrimaryBackgroundColor: string;
  buttonPrimeTextColor: string;

  borderColorPrime: string;
  borderColorSecond: string;
  borderColorThird: string;

  inputBackgroundPrime: string;

  inputBackgroundSecond: string;
  selectInputBackgroundColor: string;

  shadowPrime: string;

  inputTextColor: string;
  textColorPrime: string;
  textColorSecond: string;
  inputPlaceholderColor: string;

  animation: {
    times: {
      faster: string;
      fast: string;
      medium: string;
      slow: string;
    };
  };
}
export default ThemeI;
