import ThemeI from '../theme.interface';

export interface ThemePropsI {
  theme: ThemeI;
}
