import React from "react";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";
import Backdrop from "../blur/backdrop";

interface StyledPropsI {
  theme: ThemeI;
  left: number;
  top: number;
}
// background-color: ${(props: StyledPropsI) => props.theme.backgroundColor};
// box-shadow: ${(props: StyledPropsI) => props.theme.shadowPrime};
const DropdownStyle = styled.div`
  position: absolute;
  left: ${(props: StyledPropsI) => props.left};
  top: ${(props: StyledPropsI) => props.top};
  z-index: 5;
`;

const Content = styled.div`
  padding: 2px;
`;

export default function Dropdown({ left, top, children }) {
  return (
    <DropdownStyle left={left} top={top}>
      <Backdrop blur={"15px"} borderRadius={8} withShadow>
        <Content>{children}</Content>
      </Backdrop>
    </DropdownStyle>
  );
}
