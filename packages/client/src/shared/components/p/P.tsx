import React from "react";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";

interface StyledParagraphPropsI {
  theme: ThemeI;
  center: boolean;
}

const StyledParagraph = styled.p(
  {
    lineHeight: 1.3,
    fontSize: "1.1rem"
  },
  (props: StyledParagraphPropsI) => ({
    color: props.theme.textColorPrime,
    textAlign: props.center ? "center" : "left"
  })
);

interface PropsI {
  children: any;
  center?: boolean;
}
const P = (props: PropsI) => {
  return (
    <StyledParagraph center={props.center}>{props.children}</StyledParagraph>
  );
};

export default P;
