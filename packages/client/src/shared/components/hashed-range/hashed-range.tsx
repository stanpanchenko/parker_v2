import React, { useEffect, useState } from "react";

interface Option {
  value: number;
  label?: string;
}
interface PropsI {
  options: Option[];
  onChange: (e) => any;
  name?: string;
  step: number;
  defaultValue: number;
}

export default function({
  defaultValue,
  step,
  options,
  onChange,
  name
}: PropsI) {
  const [currentValue, setCurrentValue] = useState<number>(defaultValue);
  const [values, setValues] = useState<Option[]>(
    options.sort((a, b) => (a.value > b.value ? 1 : a.value < b.value ? -1 : 0))
  );
  function sort(options: Option[]) {
    return options.sort((a, b) =>
      a.value > b.value ? 1 : a.value < b.value ? -1 : 0
    );
  }
  function handleChange(e) {
    const value = e.target.value;
    setCurrentValue(parseInt(value, 10));
  }

  useEffect(() => {
    setValues(sort(options));
  }, [options]);

  useEffect(() => {
    const valueIsOnHash = !!values.find(v => v.value === currentValue);
    if (valueIsOnHash) onChange(currentValue);
  }, [currentValue]);

  return (
    <span>
      <label>
        <div>{name}</div>
      </label>
      <input
        value={currentValue}
        //step={step}
        min={values[0].value}
        max={values[options.length - 1].value}
        type="range"
        list={name}
        onChange={handleChange}
        name={"hashed-range"}
      />
      <datalist id={name}>
        {values.map(({ value, label }, idx) => (
          <option key={`option-${value}-${idx}`} value={value} label={"some"} />
        ))}
      </datalist>
    </span>
  );
}
