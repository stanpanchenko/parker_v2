import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBicycle,
  faBiking,
  faTimes,
  faAngleDown,
  faAngleUp,
  faCar,
  faBus,
  faTruckMoving,
  faWheelchair,
  faWarehouse,
  faFemale,
  faChargingStation,
  faMotorcycle,
  faRoad,
  faStopwatch,
  faClock,
  faAd,
  faMoon,
  faSun,
  faSearch,
  faLocationArrow,
  faCheck,
  faRedoAlt,
  faPlus,
  faMapMarkedAlt,
  faSortUp,
  faSortDown,
  faMapMarker,
  faMapPin
} from "@fortawesome/free-solid-svg-icons";
import { lineIconSvg, polygonIconSvg } from "./icons.svg";
import styled, { css } from "styled-components";

const IconSVGStyled = styled.span<any>`
  & > svg {
    width: 1rem;
    height: 1rem;
  }
`;
export type IconNames =
  | "angleDown"
  | "angleUp"
  | "mapMarker"
  | "mapMarkerAlt"
  | "warehouse"
  | "road"
  | "search"
  | "times"
  | "wheelchair"
  | "stopwatch"
  | "female"
  | "bus"
  | "car"
  | "bus"
  | "bicycle"
  | "motorcycle"
  | "biking"
  | "default"
  | "moon"
  | "sun"
  | "locationArrow"
  | "check"
  | "redoAlt"
  | "plus"
  | "sortUp"
  | "sortDown"
  | "mapPin"
  | "mapLine"
  | "polygon";

interface IconPropsI {
  name: IconNames;
  style?: {
    [property: string]: any;
  };
}
export default function Icon({ name, style }: IconPropsI) {
  const svgIcons = {
    mapLine: lineIconSvg,
    polygon: polygonIconSvg
  };
  const iconsMap = {
    sortUp: faSortUp,
    angleDown: faAngleDown,
    angleUp: faAngleUp,
    mapMarkerAlt: faMapMarkedAlt,
    mapPin: faMapPin,
    mapMarker: faMapMarker,
    motorcycle: faMotorcycle,
    sortDown: faSortDown,
    biking: faBiking,
    bicycle: faBicycle,
    clock: faClock,
    plus: faPlus,
    check: faCheck,
    redoAlt: faRedoAlt,
    search: faSearch,
    locationArrow: faLocationArrow,
    car: faCar,
    bus: faBus,
    truckMoving: faTruckMoving,
    wheelchair: faWheelchair,
    warehouse: faWarehouse,
    female: faFemale,
    chargingStation: faChargingStation,
    road: faRoad,
    stopwatch: faStopwatch,
    times: faTimes,
    default: faAd,
    moon: faMoon,
    sun: faSun
  };
  useEffect(() => {}, [name]);
  const iconRef = (name: IconNames) => {
    return iconsMap[name];
  };
  return (
    <>
      {svgIcons[name] ? (
        <IconSVGStyled style={style ? style : {}}>
          {svgIcons[name]}
        </IconSVGStyled>
      ) : (
        <FontAwesomeIcon icon={iconsMap[name]} {...style} />
      )}
    </>
  );
}
