import React from "react";
import styled from "styled-components";

interface PropsI {
  classNames?: string[];
  name: string;
  onClick?: () => any;
}

const IconStyled = styled.i``;

export default ({ name, classNames, onClick }: PropsI) => (
  <IconStyled
    onClick={() => (onClick ? onClick() : null)}
    className={["material-icons", classNames ? classNames.join(" ") : ""].join(
      " "
    )}
  >
    {name}
  </IconStyled>
);
