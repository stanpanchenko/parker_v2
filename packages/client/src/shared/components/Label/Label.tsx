import React from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";

const StyledLabel = styled.label`
  color: ${(props: ThemePropsI) => props.theme.textColorSecond}
  font-size: 10px;
  font-weight: 500;
  display: block;
  padding: .6rem .3rem;
`;

interface LabelPropsI {
  text: string;
}
const Label = ({ text }: LabelPropsI) => {
  return <StyledLabel>{text.toUpperCase()}</StyledLabel>;
};

export default Label;
