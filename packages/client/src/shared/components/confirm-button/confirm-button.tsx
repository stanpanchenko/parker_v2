import React, { useState } from "react";
import { TetriaryButton } from "../tetriary-buttons";

interface PropsI {
  text: string;
}

export default function({ text }: PropsI) {
  const [shouldConfirm, setShouldConfirm] = useState<boolean>(false);

  function handleClick(e) {
    setShouldConfirm(true);
  }
  function onConfirm(e) {}

  return (
    <>
      {!shouldConfirm ? (
        <TetriaryButton onClick={handleClick} highlighted={false}>
          REMOVE
        </TetriaryButton>
      ) : (
        <TetriaryButton onClick={onConfirm} highlighted={true}>
          SURE!
        </TetriaryButton>
      )}
    </>
  );
}
