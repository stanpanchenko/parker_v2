import React from "react";
import SwitchTheme, { Theme } from "../../providers/theme/Theme";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../redux/states";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";
import { ThemeUISelectors } from "../../../redux/states/ui/states/theme/selectors";

interface StyledWrapperPropsI {
  theme: ThemeI;
}
const StyledWrapper = styled("div")({}, (props: StyledWrapperPropsI) => ({
  color: props.theme.textColorPrime
}));

interface PropsI {
  children: any;
  theme: Theme;
}
const Text_ = (props: PropsI) => {
  const { theme } = props;

  return (
    <SwitchTheme>
      <StyledWrapper>{props.children}</StyledWrapper>
    </SwitchTheme>
  );
};
const props = (state: ApplicationStateI) => ({
  theme: ThemeUISelectors.currentTheme(state)
});
const SwitchColor = connect(
  props,
  null
)(Text_);

export default SwitchColor;
