import React, { ReactChild, ReactChildren, ReactNode } from "react";
import ThemeI from "../../providers/theme/theme.interface";
import styled from "styled-components";

interface StyledButtonPropsI {
  theme: ThemeI;
}
const StyledButton = styled("button")(
  {
    border: "none",
    backgroundColor: "transparent",
    outline: "none",
    fontSize: ".8rem",
    margin: "7px"
  },
  ({ theme }: StyledButtonPropsI) => ({
    color: theme.textColorPrime
  })
);

const Button = React.memo(function({
  onClick,
  children
}: {
  children?: ReactNode;
  onClick: (e: any) => void;
}) {
  function handleEditClick(e: any) {
    onClick(e);
  }
  return <StyledButton onClick={handleEditClick}>{children}</StyledButton>;
});

interface PropsI {
  children?: ReactNode;
  onClick: (e: any) => void;
}
export function WordButton({ children, onClick }: PropsI) {
  return <Button onClick={onClick}>{children}</Button>;
}
