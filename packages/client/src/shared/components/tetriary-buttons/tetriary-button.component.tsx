import React, { ReactNode } from "react";
import styled from "styled-components";

interface TetriaryButtonPropsI {
  highlighted: boolean;
}
const TetriaryButton = styled("button")(
  {
    background: "transparent",
    border: "none",
    fontSize: ".8rem",
    fontWeight: 600,
    margin: ".2rem"
  },
  (props: TetriaryButtonPropsI) => ({
    color: props.highlighted ? "blue" : "gray"
  })
);

interface PropsI {
  highlighted: boolean;
  children: ReactNode;
  onClick: (e) => void;
}
export default function({ children, highlighted, onClick }: PropsI) {
  return (
    <TetriaryButton onClick={onClick} highlighted={highlighted}>
      {children}
    </TetriaryButton>
  );
}
