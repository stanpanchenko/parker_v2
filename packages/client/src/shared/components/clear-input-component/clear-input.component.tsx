import React from "react";
import IconButton from "../icon-button/icon-button.component";
import "./clear-input-component.style.scss";
import { CSSTransition } from "react-transition-group";

interface PropsI {
  placeholder: string;
  onFocus: () => void;
  onChange: (value: string) => void;
}
interface StateI {
  inputValue: string;
}

export class ClearInputComponent extends React.Component<PropsI, StateI> {
  constructor(props: PropsI) {
    super(props);
  }

  state = {
    inputValue: ""
  };

  componentDidMount(): void {}

  handleClearEvent() {
    this.handleInputValueChange("");
  }

  handleInputValueChange(value: string) {
    this.props.onChange(value);
    this.setInputValue(value);
  }

  setInputValue(value: string) {
    this.setState(s => ({
      ...s,
      inputValue: value
    }));
  }
  getInputValue() {
    return this.state.inputValue;
  }

  inputHasValue() {
    return this.getInputValue() !== "";
  }

  render() {
    const { placeholder, onFocus } = this.props;
    return (
      <div className={"clear-input-component"}>
        <div className={"clear-input-component__controls-wrapper"}>
          <input
            className={"input"}
            placeholder={placeholder}
            type={"text"}
            value={this.getInputValue()}
            onChange={e => this.handleInputValueChange(e.target.value)}
            onFocus={() => onFocus()}
          />

          <CSSTransition
            classNames={"clear-input-component-button-appear-animation"}
            timeout={400}
            in={this.inputHasValue()}
          >
            <div className={"clear-input-component__controls-wrapper__button"}>
              <IconButton
                classNames={[""]}
                onClick={() => this.handleClearEvent()}
                name={"times"}
              />
            </div>
          </CSSTransition>
        </div>
      </div>
    );
  }
}
