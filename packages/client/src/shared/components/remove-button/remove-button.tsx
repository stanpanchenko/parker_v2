import React from "react";
import IconButton from "../icon-button/icon-button.component";

interface RemoveButtonPropsI {
  onClick: (e: any) => void;
}
export function RemoveButton({ onClick }: RemoveButtonPropsI) {
  return (
    <IconButton
      onClick={onClick}
      name={"times"}
      style={{
        color: "grey"
      }}
    />
  );
}
