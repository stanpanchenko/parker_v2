import React from "react";
import GoogleIcon from "../google-icon/google-icon";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";

interface StyledButtonPropsI {
  theme: ThemeI;
}

const ButtonStyled = styled.button`
  background-color: ${(props: StyledButtonPropsI) =>
    props.theme.backgroundColor};
  height: 45px;
  width: 45px;
  color: ${(props: StyledButtonPropsI) => props.theme.colors.icon.color};
  border: none;
  border-radius: 25px;
  outline: none;
`;

export const HoveredButton = styled(ButtonStyled)`
  &:hover {
    color: ${(props: StyledButtonPropsI) => props.theme.colorPrime};
  }
`;

export const ShadowedButton = styled(HoveredButton)`
  box-shadow: ${(props: StyledButtonPropsI) => props.theme.shadowPrime};
`;

export default function RoundButton(props) {
  const { name, onClick } = props;
  return (
    <ShadowedButton onClick={onClick}>
      <GoogleIcon name={name} />
    </ShadowedButton>
  );
}
