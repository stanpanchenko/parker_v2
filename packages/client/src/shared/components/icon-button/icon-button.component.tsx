import React from "react";
import "./icon-button-component.style.scss";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";
import { GoogleIconComponent } from "../google-icon-component";
import { Icon } from "../icon";
import { IconNames } from "../icon/icon";

interface IconButtonStyledPropsI {
  round: boolean;
  theme: ThemeI;
  style?: { [attribute: string]: string | number };
  shadow: boolean;
}
const IconButtonStyled = styled("button")`
    border: none;
    outline: none;
    will-change: background-color, color, left, top, transform;
    border-radius: ${(props: IconButtonStyledPropsI) =>
      props.round ? "40px" : "5px"};
    width: ${(props: IconButtonStyledPropsI) => (props.round ? "45px" : null)};
    height: ${(props: IconButtonStyledPropsI) => (props.round ? "45px" : null)};
    margin: 7px;
    box-shadow: ${(props: IconButtonStyledPropsI) => {
      return props.shadow ? props.theme.shadowPrime : "";
    }};
    background-color: ${(props: IconButtonStyledPropsI) =>
      props.theme.backgroundColorSecond};
    color: ${(props: IconButtonStyledPropsI) => props.theme.textColorPrime};
    border-color: ${(props: IconButtonStyledPropsI) =>
      props.theme.borderColorPrime}
    transition: color 800ms, background-color 200ms
 
`;

interface IconButtonPropsI {
  classNames?: string[];
  onClick: (e: any) => any;
  name: IconNames;
  shadow?: boolean;
  round?: boolean;
  small?: boolean;
  style?: { [attribute: string]: string | number };
}

const IconButton = ({
  shadow,
  name,
  style,
  round,
  onClick
}: IconButtonPropsI) => {
  return (
    <IconButtonStyled
      style={{ ...style }}
      shadow={shadow}
      round={round}
      onClick={e => onClick(e)}
    >
      <Icon name={name} />
    </IconButtonStyled>
  );
};

export default IconButton;
