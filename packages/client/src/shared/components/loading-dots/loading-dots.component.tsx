import React from "react";
import "./loading-dots.style.scss";

interface LoadingWheelPropsI {
  color?: string;
}

const LoadingDotsComponent = (props: LoadingWheelPropsI) => {
  return (
    <div className={"lds-ellipsis"}>
      <div style={{ backgroundColor: props.color ? props.color : "#d9d9d9" }} />
      <div style={{ backgroundColor: props.color ? props.color : "#d9d9d9" }} />
      <div style={{ backgroundColor: props.color ? props.color : "#d9d9d9" }} />
      <div style={{ backgroundColor: props.color ? props.color : "#d9d9d9" }} />
    </div>
  );
};

export default LoadingDotsComponent;
