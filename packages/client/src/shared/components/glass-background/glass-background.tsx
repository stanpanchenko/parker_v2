import React from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";
import Backdrop from "../blur/backdrop";

const BG = styled.div`
  width: 100%;
  position: relative;
  height: 100%;
`;

export default function GlassBackground({ children }: any) {
  return (
    <>
      <Backdrop blur={"10px"}>{children}</Backdrop>
    </>
  );
}
