import React, { CSSProperties, ReactNode } from "react";
import { Sticky } from "../index";

const StickyBottom = ({
  distance,
  children,
  style
}: {
  distance: string;
  children: ReactNode;
  style?: CSSProperties;
}) => {
  return (
    <Sticky style={{ ...style }} position={{ bottom: distance }}>
      {children}
    </Sticky>
  );
};

export default StickyBottom;
