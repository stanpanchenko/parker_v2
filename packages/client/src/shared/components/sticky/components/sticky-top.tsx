import React, { CSSProperties, ReactNode } from "react";
import { Sticky } from "../index";

const StickyTop = ({
  distance,
  children,
  style
}: {
  style?: CSSProperties;
  distance: string;
  children: ReactNode;
}) => {
  return (
    <Sticky style={{ ...style }} position={{ top: distance }}>
      {children}
    </Sticky>
  );
};

export default StickyTop;
