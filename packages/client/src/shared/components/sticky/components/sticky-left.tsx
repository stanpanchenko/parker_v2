import React, { CSSProperties, ReactNode } from "react";
import { Sticky } from "../index";

const StickyLeft = ({
  distance,
  children,
  style
}: {
  distance: string;
  children: ReactNode;
  style?: CSSProperties;
}) => {
  return (
    <Sticky style={{ ...style }} position={{ left: distance }}>
      {children}
    </Sticky>
  );
};

export default StickyLeft;
