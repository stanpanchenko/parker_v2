import React, { CSSProperties, ReactNode } from "react";
import { Sticky } from "../index";

const StickyRight = ({
  distance,
  children,
  style
}: {
  distance: string;
  children: ReactNode;
  style?: CSSProperties;
}) => {
  return (
    <Sticky style={{ ...style }} position={{ right: distance }}>
      {children}
    </Sticky>
  );
};

export default StickyRight;
