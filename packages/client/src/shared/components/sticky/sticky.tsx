import React, { Component, CSSProperties, ReactNode } from "react";
import styled from "styled-components";
import StickyBottom from "./components/sticky.bottom";
import StickyTop from "./components/sticky-top";
import StickyLeft from "./components/sticky-left";
import StickyRight from "./components/sticky-right";

const StickyStyled = styled.div<{}>`
  display: inline-block;
  position: sticky;
`;
interface StickyPropsI {
  position: {
    left?: string;
    right?: string;
    bottom?: string;
    top?: string;
  };
  style?: CSSProperties;
  children: ReactNode;
}
class Sticky extends Component<StickyPropsI, any> {
  static Bottom = StickyBottom;
  static Top = StickyTop;
  static Left = StickyLeft;
  static Right = StickyRight;

  render() {
    const { style, position, children } = this.props;

    return (
      <StickyStyled
        style={{
          ...position,
          ...style
        }}
      >
        {children}
      </StickyStyled>
    );
  }
}

export default Sticky;
