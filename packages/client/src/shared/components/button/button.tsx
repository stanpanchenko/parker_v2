import React, { CSSProperties, ReactNode } from "react";

type EventHandler = (e: any) => void;
interface IButtonProps {
  active: boolean;
  onClick: EventHandler;
  onMouseOver?: EventHandler;
  style?: CSSProperties;
  children: ReactNode;
  className?: string[];
}

const Button = ({
  className,
  onMouseOver,
  onClick,
  style,
  children,
  active
}: IButtonProps) => {
  return (
    <button
      style={style ? style : {}}
      onClick={onClick}
      onMouseOver={onMouseOver ? onMouseOver : null}
      className={
        className
          ? [...className, active ? "button-active" : "button-non-active"].join(
              " "
            )
          : active
          ? "button-active"
          : "button-non-active"
      }
    >
      {children}
    </button>
  );
};

export default Button;
