import React from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../redux/states";
import { UserSelectors } from "../../../redux/states/user/selectors/user.selectors";
import Button from "../../../components/shared/buttons/Button";
import { AuthActionsCreators } from "../../../redux/states/user/substates/authentication/actions";
import { LogoutActionI } from "../../../redux/states/user/substates/authentication/actions/interfaces/auth.actions-interface";

interface PropsI {
  logout: () => LogoutActionI;
}
function LogoutButton_(props: PropsI) {
  const { logout } = props;
  function handleClick(e) {
    logout();
  }
  return <Button onClick={handleClick}>Logout</Button>;
}
const props = (state: ApplicationStateI) => ({
  isAuthenticated: UserSelectors.authentication(state)
});
const actions = dispatch => ({
  logout: () => dispatch(AuthActionsCreators.logout())
});
export const LogoutButton = connect(
  props,
  actions
)(LogoutButton_);
