import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { AnyAction } from "redux";
import Input from "../input/input";
import Button from "../../../components/shared/buttons/Button";
import { ApplicationStateI } from "../../../redux/states";
import { AuthActionsCreators } from "../../../redux/states/user/substates/authentication/actions";
import { LoginInput } from "../../../api/graphql/graphql-api.interface";
import { ErrorI } from "../../../redux/states/errors/errors.reducer";
import { ErrorsSelectors } from "../../../redux/states/errors/errors.selectors";
import { Form } from "../form";
import { isEmail } from "../../../utils/validation";
import { ShakeAnimation } from "../../animations/shake";

interface PropsI {
  login: (loginInput: LoginInput) => AnyAction;
  setAuthentication: (value: boolean) => AnyAction;
  error: ErrorI | null;
}
function Login_({ setAuthentication, login, error }: PropsI) {
  const [animate, setAnimate] = useState<boolean>(false);

  useEffect(() => {
    const a = error && error.statusCode === 401;
    if (a) {
      setAnimate(true);
      setTimeout(() => {
        setAnimate(false);
      }, 1000);
    }
  }, [error]);

  function handleSubmit(values: any): void {
    const { email, password } = values;
    login({ email, password });
  }
  function validate(values: any): boolean {
    const { email, password } = values;
    if (!password) return false;
    // if (password && password.length < 6) return false;
    if (!isEmail(email)) return false;
    return true;
  }

  return (
    <div>
      <Form
        onSubmit={handleSubmit}
        validate={validate}
        initialValid={true}
        initialValues={{
          email: "email",
          password: ""
        }}
      >
        {({ setValue, valid, formValues: { email, password } }) => (
          <div>
            <ShakeAnimation animate={animate}>
              <Input
                value={email || ""}
                placeholder={"Email"}
                type={"email"}
                autocomplete={"username"}
                onChange={value => setValue<string>("email", value)}
              />

              <Input
                value={password || ""}
                autocomplete={"current-password"}
                type={"password"}
                placeholder={"Password"}
                onChange={value => setValue<string>("password", value)}
              />

              <Button disabled={!valid}>Login</Button>
            </ShakeAnimation>
          </div>
        )}
      </Form>
    </div>
  );
}
const props = (state: ApplicationStateI) => ({
  error: ErrorsSelectors.errorSelector(state)
});

const actions = dispatch => ({
  login: (loginInput: LoginInput) =>
    dispatch(AuthActionsCreators.login(loginInput)),
  setAuthentication: (value: boolean) =>
    dispatch(AuthActionsCreators.setAuth(value))
});
export const Login = connect(
  props,
  actions
)(Login_);
