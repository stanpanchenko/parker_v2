import React from "react";
import styled from "styled-components";

const IStyle = styled.i``;

interface PropsI {
  name: string;
}
export default function GoogleIcon(props: PropsI) {
  const { name, ...rest } = props;
  return (
    <IStyle {...rest} className={"material-icons"}>
      {name}
    </IStyle>
  );
}
