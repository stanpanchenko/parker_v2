import React, { useEffect, useState } from "react";
import styled from "styled-components";
import ThemeI from "../../providers/theme/theme.interface";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../redux/states";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";

interface StyledInputPropsI {
  theme: ThemeI;
}
const StyledInput = styled.input`
  text-overflow: ellipsis;
  "-webkit-appearance":none ;
  "-moz-appearance":none ;
  &:focus {
    border: 1px solid #848484e0;
  }
  padding: 0.4rem 0.8rem 0.5rem 0.8rem;
  font-size: 0.9rem;
  font-weight: 300;
  border-radius: 6px;
  margin: 1px;
  outline: none;
  background-color: ${(props: ThemePropsI) => props.theme.inputBackgroundPrime};
  color: ${(props: ThemePropsI) => props.theme.inputTextColor};
  border: 1px solid ${(props: ThemePropsI) => props.theme.borderColorThird};
  transition: background-color 200ms;
  will-change: background-color;
`;

export interface InputPropsI {
  placeholder?: string;
  onChange: (value: any) => any;
  onFocus?: () => any;
  style?: {
    [name: string]: string | number;
  };
  value?: string | number;
  type: "text" | "number" | "email" | "password";
  autocomplete?: string;
  step?: number;
  onMouseOver?: () => any;
  name?: string;
  required?: boolean;
  id?: string;
  ref?: any;
}
function Input({
  placeholder,
  onChange,
  name,
  id,
  value,
  onFocus,
  type,
  style,
  step,
  ref,
  onMouseOver,
  required
}: InputPropsI) {
  function handleChange({ target: { value } }) {
    onChange(type === "number" ? parseFloat(value) : value);
  }
  function handleFocus(e) {
    if (onFocus) onFocus();
  }
  return (
    <span style={{ position: "relative" }}>
      <StyledInput
        placeholder={placeholder}
        ref={ref}
        onMouseOver={() => (onMouseOver ? onMouseOver() : null)}
        type={type}
        id={id}
        required={required}
        value={value}
        style={style}
        step={step ? step : null}
        onChange={handleChange}
        onFocus={handleFocus}
      />
    </span>
  );
}

export default Input;
