import React, { useEffect, useState } from "react";
import { GoogleIconComponent } from "../google-icon-component";
import styled from "styled-components";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";

interface WrapperProps extends ThemePropsI {
  active: boolean;
}

const StyledWrapper = styled.span`
  cursor: pointer;
  color: ${({ active, theme }: WrapperProps) =>
    active ? theme.colorPrime : theme.backgroundIconPrime};
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface PropsI {
  name: string;
  onStateChange: (state: boolean) => any;
}

export default function ToggleIcon({ onStateChange, name }: PropsI) {
  const [active, setActive] = useState(false);
  function handleClick() {
    onStateChange(active);
    setActive(!active);
  }
  useEffect(() => {}, [name, onStateChange]);
  return (
    <StyledWrapper active={active} onClick={handleClick}>
      <GoogleIconComponent onClick={handleClick} name={name} />
    </StyledWrapper>
  );
}
