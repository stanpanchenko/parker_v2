import React, { CSSProperties, ReactNode } from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";

export const BackDrop = styled.div`
  background-color: ${({ theme }: ThemePropsI) =>
    theme.backgroundColorPrimeTransparent};
  height: 100%;
  width: 100%;

  @supports (
    (backdrop-filter: blur(25px)) or (-webkit-backdrop-filter: blur(25px))
  ) {
    -webkit-backdrop-filter: blur(25px);
    backdrop-filter: saturate(180%) blur(25px);
    background-color: ${({ theme }: ThemePropsI) =>
      theme.backgroundGlassEffect};
  }
`;

const BlurBG = styled.div<any>`
  overflow: hidden;
  pointer-events: none;
  :after {
    z-index: -1;
    position: absolute;
    border-radius: ${({ radius }: any) => radius}px;
    will-change: backdrop-filter;
    transform: scale(0.5);
    top: -50%;
    left: -50%;
    right: -50%;
    bottom: -50%;
    content: " ";
    pointer-events: none;

    background-color: ${({ theme }: ThemePropsI) =>
      theme.backgroundColorPrimeTransparent};

    @supports (
      (backdrop-filter: blur(25px)) or (-webkit-backdrop-filter: blur(25px))
    ) {
      -webkit-backdrop-filter: blur(25px);
      backdrop-filter: saturate(180%)
        blur(${(props: { blur: string }) => props.blur});
      background-color: ${({ theme }: ThemePropsI) =>
        theme.backgroundGlassEffect};
    }
  }
`;

const Wrapper = styled.div<any>`
  box-shadow: ${({ theme, withShadow }: any) =>
    withShadow ? theme.shadowPrime : 0};

  border-radius: ${({ radius }: any) => radius}px;
  position: relative;
  height: 100%;
  width: 100%;
`;

// box-shadow: ${({ theme }: any) => theme.shadowPrime};
interface OwnPropsI {
  children: ReactNode;
  blur: string;
  borderRadius?: number;
  withShadow?: boolean;
}
export default function Backdrop({
  children,
  blur,
  borderRadius,
  withShadow
}: OwnPropsI) {
  return (
    <Wrapper
      className={"Backdrop"}
      radius={borderRadius ? borderRadius : "0px"}
      withShadow={withShadow}
    >
      <BlurBG blur={blur} radius={borderRadius ? borderRadius * 1.5 : 0} />
      {children}
    </Wrapper>
  );
}
