import React, { ReactNode } from "react";
import styled from "styled-components";

const Styled = styled.div`
  filter: blur(${(props: { radius: number }) => props.radius}px);
  will-change: filter;
`;

interface PropsI {
  children: ReactNode;
  blurRadius: number;
}
export default function Blur({ children, blurRadius }: PropsI) {
  return <Styled radius={blurRadius}>{children}</Styled>;
}
