import React, { ReactNode } from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../providers/theme/interfaces/theme-props.interface";

const LabelStyled = styled.label`
  // customize the label
  display: inline-block;
  position: relative;
  cursor: pointer;
  font-weight: 300;
  font-size: 0.9rem;

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  // hide the browser's default checkbox
  & input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    // when the checkbox is checked, add style
    &:checked ~ span.checkbox {
      background-color: ${({ theme }: ThemePropsI) => theme.colorPrime};
    }
    // show the checkmark when checked
    &:checked ~ span.checkbox:after {
      display: block;
    }
  }
  // on mouse-over, add style
  &:hover input ~ span.checkbox {
    background-color: #ccc;
  }
  // style the checkmark/indicator
  span.checkbox:after {
    top: 3px;
    left: 5px;
    width: 3px;
    height: 5px;
    border: solid white;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;

const CheckboxStyled = styled.span`
  // Create a custom checkbox
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  left: 8px;
  height: 15px;
  width: 15px;
  border-radius: 5px;
  background-color: #eee;
  // create the checkmark/indicator (hidden when not checked)
  &:after {
    content: "";
    position: absolute;
    display: none;
  }
`;

interface OwnPropsI {
  onChange: (e) => any;
  name?: string;
  value: string;
  checked: boolean;
  children?: ReactNode;
}

export default function Checkbox({
  onChange,
  name,
  value,
  checked,
  children
}: OwnPropsI) {
  return (
    <LabelStyled>
      <input
        value={value}
        checked={checked}
        name={name}
        onChange={onChange}
        type="checkbox"
      />
      {children}
      <CheckboxStyled className={"checkbox"} />
    </LabelStyled>
  );
}
