import React from "react";
import "./section-component.style.scss";

interface PropsI {
  children: any;
}
const Section = (props: PropsI) => {
  return (
    <section>
      <div className={"section-content"}>{props.children}</div>
    </section>
  );
};

export default Section;
