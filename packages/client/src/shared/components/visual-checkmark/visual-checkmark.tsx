import React from "react";
import styled from "styled-components";

const Icon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="-4 -4 150 150"
    className={"visual-check-mark"}
  >
    <circle className="path circle" cx="65.1" cy="65.1" r="62.1" />
    <polyline className="path check" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
  </svg>
);

const StyledIcon = styled.span`
  .visual-check-mark {
    width: 45px;
    display: block;

    .path {
      stroke-dasharray: 1000;
      stroke-dashoffset: 0;
      &.circle {
        fill: none;
        stroke: white;
        stroke-width: 10px;
        stroke-miterlimit: 10;
        -webkit-animation: dash 0.9s ease-in-out;
        animation: dash 0.9s ease-in-out;
      }
      &.check {
        stroke-width: 10px;
        fill: none;
        stroke: white;
        stroke-linecap: round;
        stroke-miterlimit: 10;
        stroke-dashoffset: -100;
        -webkit-animation: dash-check 0.9s 0.35s ease-in-out forwards;
        animation: dash-check 0.9s 0.35s ease-in-out forwards;
      }
    }

    @-webkit-keyframes dash {
      0% {
        stroke-dashoffset: 1000;
      }
      100% {
        stroke-dashoffset: 0;
      }
    }

    @keyframes dash {
      0% {
        stroke-dashoffset: 1000;
      }
      100% {
        stroke-dashoffset: 0;
      }
    }

    @-webkit-keyframes dash-check {
      0% {
        stroke-dashoffset: -100;
      }
      100% {
        stroke-dashoffset: 900;
      }
    }

    @keyframes dash-check {
      0% {
        stroke-dashoffset: -100;
      }
      100% {
        stroke-dashoffset: 900;
      }
    }
  }
`;

export function VisualCheckmark() {
  return (
    <StyledIcon>
      <Icon />
    </StyledIcon>
  );
}
