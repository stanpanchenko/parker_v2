import React, { ReactNode, useState } from "react";
import styled from "styled-components";
import Dropdown from "../dropdown/dropdown";
import GoogleIcon from "../google-icon/google-icon";
import ThemeI from "../../providers/theme/theme.interface";
import { Option } from "./components/option";
import { Icon } from "../icon";

interface ThemePropsI {
  theme: ThemeI;
}

const SelectStyle = styled.div`
  display: inline-block;
  position: relative;
  margin: 5px;
`;

const SelectedValueStyle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  width: 135px;
  cursor: pointer;
  padding: 0 7px 0 15px;

  background-color: ${(props: ThemePropsI) => props.theme.inputBackgroundPrime};
  color: ${(props: ThemePropsI) => props.theme.inputTextColor};
  border: 1px solid ${(props: ThemePropsI) => props.theme.borderColorPrime};
  border-radius: ${(props: ThemePropsI) => props.theme.radius.prime}px;
  font-size: 0.8rem;
  height: 36px;
`;

function SelectedValue({ value }) {
  return (
    <SelectedValueStyle>
      <span
        style={{
          height: "15px",
          lineHeight: "15px",
          paddingRight: "15px"
        }}
      >
        {value}
      </span>
      <span
        style={{
          height: "24px"
        }}
      >
        <Icon name={"sortDown"} />
      </span>
    </SelectedValueStyle>
  );
}

function SOption({ children, onClick }) {
  const childType = children.type.name;
  if (childType !== "Option") {
    console.error("All children of Select Component must be of type Option!");
    return <span />;
  }
  const value = children.props.value;
  return <div onClick={() => onClick(value)}>{children}</div>;
}

interface SelectPropsI {
  children: ReactNode[];
  onSelect: (value: any) => any;
  selected: string;
}
export default function Select({ children, selected, onSelect }: SelectPropsI) {
  const [isOpen, setOpen] = useState(false);
  function open() {
    if (!isOpen) setOpen(true);
  }
  function close() {
    if (isOpen) setOpen(false);
  }
  function handleSelectClick(value) {
    onSelect(value);
    close();
  }
  return (
    <SelectStyle onClick={open}>
      <SelectedValue value={selected} />
      {isOpen ? (
        <Dropdown left={0} top={0}>
          {children.map((child, idx) => (
            <SOption
              onClick={value => handleSelectClick(value)}
              key={`soption-${idx}`}
            >
              {child}
            </SOption>
          ))}
        </Dropdown>
      ) : null}
    </SelectStyle>
  );
}
