import React, { ReactNode } from "react";
import ThemeI from "../../../../providers/theme/theme.interface";
import styled from "styled-components";

interface ThemePropsI {
  theme: ThemeI;
}
const OptionStyled = styled.div`
  cursor: pointer;
  color: ${(props: ThemePropsI) => props.theme.inputTextColor};
  min-width: 100px;
  border-radius: 7px;
  font-size: 0.8rem;
  padding: 0.7rem 0.7rem 0.7rem 0.7rem;
  background-color: ${(props: ThemePropsI) => props.theme.inputBackgroundPrime};
  &:hover {
    background-color: ${(props: ThemePropsI) =>
      props.theme.inputBackgroundSecond};
  }
  margin: 3px;
`;

export default function Option({ value, children }) {
  return <OptionStyled>{children}</OptionStyled>;
}
