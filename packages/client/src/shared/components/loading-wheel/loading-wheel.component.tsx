import React from "react";
import "./loading-wheel.style.scss";

interface LoadingWheelPropsI {}

const LoadingWheelComponent = (props: LoadingWheelPropsI) => {
  return (
    <div className={"lds-spinner"}>
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
  );
};

export default LoadingWheelComponent;
