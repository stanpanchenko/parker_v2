import React, { useEffect, useState } from "react";

interface FormStateI {
  [key: string]: any;
}
interface RenderPropI {
  setValue: <T>(inputName: string, value: T) => void;
  getValue: <T>(inputName: string) => T;
  formValues: { [key: string]: any };
  valid: boolean;
}
interface FormPropsI {
  children: ({ setValue, getValue, formValues, valid }: RenderPropI) => any;
  onSubmit: (e: any) => void;
  initialValid: boolean;
  validate?: (values: any) => boolean;
  initialValues: {
    [key: string]: any;
  };
}
function Form({
  children,
  onSubmit,
  validate,
  initialValid,
  initialValues
}: FormPropsI) {
  const [formValues, setFormValues] = useState<FormStateI>({});
  const [formIsValid, setFormIsValid] = useState<boolean>(false);

  function getValue(name: string): any {
    return formValues && formValues[name];
  }

  function setValue(name, value): void {
    setFormValues({
      ...formValues,
      [name]: value
    });
  }

  const Children = children({
    setValue,
    getValue,
    formValues,
    valid: formIsValid
  });

  function handleInitialValuesSet() {
    setFormIsValid(initialValid);
    setFormValues(initialValues);
  }

  useEffect(handleInitialValuesSet, []);

  useEffect(() => {
    const valid = validate(formValues);
    setFormIsValid(valid);
  }, [formValues]);

  function handleSubmit(e) {
    e.preventDefault();
    if (formIsValid) {
      onSubmit(formValues);
    }
  }

  return <form onSubmit={handleSubmit}>{Children}</form>;
}
export default Form;
