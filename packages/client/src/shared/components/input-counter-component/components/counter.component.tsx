import React from "react";
import IconButton from "../../icon-button/icon-button.component";
import "./counter-component.style.scss";

interface PropsI {
  onClick: (direction: string) => void;
}

export const CounterComponent = (props: PropsI) => {
  const { onClick } = props;
  return (
    <div className={"counter-component"}>
      <IconButton
        classNames={[]}
        onClick={() => onClick("up")}
        name={"sortUp"}
      />
      <IconButton
        classNames={[]}
        onClick={() => onClick("down")}
        name={"sortDown"}
      />
    </div>
  );
};
