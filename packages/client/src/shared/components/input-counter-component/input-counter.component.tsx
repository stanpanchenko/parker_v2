import React from "react";
import { CounterComponent } from "./components/counter.component";
import "./input-counter-component.style.scss";

interface PropsI {
  step: number;
  onChange: (value: number) => void;
  numberType: string;
}

interface StateI {
  value: string;
}

export class InputCounterComponent extends React.Component<PropsI, StateI> {
  constructor(props) {
    super(props);
  }

  state = {
    value: ""
  };

  componentDidMount(): void {}

  handleCounterClick(direction: string) {
    const value = this.state.value;
    if (direction === "up") {
      this.props.numberType === "float"
        ? this.setValue(
            `${parseFloat(value ? value : `${0}`) + this.props.step}`
          )
        : this.setValue(
            `${parseInt(value ? value : `${0}`, 10) + this.props.step}`
          );
    } else {
      this.props.numberType === "float"
        ? this.setValue(
            `${parseFloat(value ? value : `${0}`) + this.props.step}`
          )
        : this.setValue(
            `${parseInt(value ? value : `${0}`, 10) + this.props.step}`
          );
    }
  }

  setValue(value: string) {
    this.setState(s => ({
      ...s,
      value
    }));
    this.props.onChange(parseFloat(value));
  }

  handleChange(e) {
    const newValue = e.target.value;
    this.setValue(newValue);
  }

  render() {
    const { value } = this.state;
    const { step } = this.props;
    return (
      <div className={"input-counter-component"}>
        <div className={"input-counter-component__content-wrapper"}>
          <input
            type={"number"}
            value={value}
            step={step}
            onChange={e => this.handleChange(e)}
          />
          <CounterComponent
            onClick={direction => this.handleCounterClick(direction)}
          />
        </div>
      </div>
    );
  }
}
