import React from "react";
import { ReactNode } from "react";
import { Roles } from "../../../api/graphql/graphql-api.interface";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../redux/states";
import { UserAuthenticationSelectors } from "../../../redux/states/user/substates/authentication/selectors";

interface ReduxProps {
  userRoles: Roles[];
  isAuthenticated: boolean;
}

interface OwnPropsI {
  children: ReactNode;
  roles: Roles[];
}
function RoleGuard({
  children,
  roles,
  userRoles,
  isAuthenticated
}: OwnPropsI & ReduxProps) {
  return (
    <>
      {isAuthenticated && userRoles.some(role => roles.includes(role))
        ? children
        : null}
    </>
  );
}
const props = (state: ApplicationStateI) => ({
  userRoles: state.user.roles,
  isAuthenticated: UserAuthenticationSelectors.isAuthenticated(state)
});
export default connect(
  props,
  null
)(RoleGuard);
