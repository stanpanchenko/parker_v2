import { ILngLat } from './LatLng.interface';

export interface MapBoundsRectI {
  center: ILngLat;
  southEast: ILngLat;
  southWest: ILngLat;
  northWest: ILngLat;
  northEast: ILngLat;
}
