import ThemeI from "../providers/theme/theme.interface";

interface ThemedPropsI {
	theme: ThemeI;
}
export default ThemedPropsI;
