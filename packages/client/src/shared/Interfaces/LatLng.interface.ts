export interface ILngLat {
  id?: string;
  lat: number;
  lng: number;
}
