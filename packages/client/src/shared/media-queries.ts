interface SizesI {
  [sttr: string]: number;
}
const sizes: SizesI = {
  mobile: 320,
  iphone5: 320,
  iphone6_7_8: 414,
  iPad: 768,
  tablet: 768,
  iPadPro: 1024,
  desktop: 1224
};

interface MediaQuery {
  iPhone5: string;
  iPhone6_7_8: string;
  iPad: string;
  iPadPro: string;
  mobile: string;
  tablet: string;
  desktop: string;
}

/**
 * @return {string} (min-width: [number]px)
 * @desc Returns media-query css string of kind: (min-width: [number]px). Used in styled-components
 */
const media = Object.keys(sizes).reduce((acc, cur) => {
  acc[cur] = `(min-width: ${sizes[cur]}px)`;

  return acc;
}, {});

export default media as MediaQuery;
