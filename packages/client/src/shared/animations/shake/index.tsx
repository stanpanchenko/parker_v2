import React from "react";
import posed from "react-pose";

const Animation = posed.span({
  init: {
    display: "inline-block"
  },
  shake: {
    x: 10,
    transition: {
      type: "keyframes",
      values: [-10, 0, 10, 0, -10, 0, 10, 0],
      duration: 400
    }
  }
});

export function ShakeAnimation({ children, animate }) {
  return <Animation pose={animate ? "shake" : null}>{children}</Animation>;
}
