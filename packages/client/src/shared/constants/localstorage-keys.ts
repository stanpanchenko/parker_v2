export enum LocalStorageKeys {
  themeKey = 'theme',
  tokenKey = 'token'
}
