import React from "react";
import { CSSTransition } from "react-transition-group";
import "./fadein-animation.style.scss";

interface PropsI {
  children: any;
}

const PageFadeinAnimation = (props: PropsI) => {
  return (
    <CSSTransition classNames={"fadein"} timeout={3000} in={true} appear={true}>
      <>{props.children}</>
    </CSSTransition>
  );
};

export default PageFadeinAnimation;
