import {css} from "styled-components";

export const scrollbarStyle = css`
  ::-webkit-scrollbar {
    width: 3px;
  }
  /* Track */
  ::-webkit-scrollbar-track {
    background: #f0f0f0;
  }
  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #c7c7c7;
  }
`;