import { from, merge, Observable } from "rxjs";
import { AnyAction } from "redux";

export function createObservableMiddleware(
  obervableHandler: (
    obs: Observable<AnyAction>,
    state?: any
  ) => Observable<AnyAction | void>
) {
  // use redux middleware interface
  return ({ getState, dispatch }) => next => action => {
    if (typeof action === "function") return next(action); // let thunk actions pass

    const observable = from([action]);
    const handledObservable = obervableHandler(observable, getState());
    const handledObservableSubscription = handledObservable.subscribe(
      dispatch,
      error => {
        console.error(error);
      },
      () => {
        handledObservableSubscription &&
          handledObservableSubscription.unsubscribe();
      }
    );

    return next(action);
  };
}

function combineObservablesMiddleware(...observables) {
  return (action$, state) => merge(...observables);
}
