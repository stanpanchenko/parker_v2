import { Observable } from "rxjs";
import { filter, map } from "rxjs/operators";
import { AnyAction } from "redux";
import { MapActionTypes } from "../../states/map/actions/types/map.action-types";
import { createObservableMiddleware } from "./createObservableMiddleware";

const mapMovedHandler = (
  action$: Observable<AnyAction>,
  state?: any
): Observable<AnyAction | void> =>
  action$.pipe(
    filter(action => action.type === MapActionTypes.mapMoved),
    map(action => {
      return { type: "[NOOP-ACTION]" };
    })
  );
const mapMovedMiddleware = createObservableMiddleware(mapMovedHandler);

export const ObservablesMiddleware = [mapMovedMiddleware];
