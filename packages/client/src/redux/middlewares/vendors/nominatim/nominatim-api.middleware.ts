import { INominatimFetchAddressByCoordinatesAction } from "./actions/interfaces/nominatim.action-interfaces";
import { NominatimActionTypes } from "./actions/nominatim.action-types";

const fetchAddress = () => next => (
  action: INominatimFetchAddressByCoordinatesAction
) => {
  if (action.type !== NominatimActionTypes.fetchAddressByCoordinates)
    return next(action);
  const { lat, lng } = action.payload;
  const queryURL = `https://nominatim.openstreetmap.org/reverse?format=geojson&lat=${lat}&lon=${lng}&zoom=18&addressdetails=1`;
  fetch(queryURL)
    .then(res => res.json())
    .then(data => {
      console.log("fetchAddress: ", data.features[0].properties.address);
    })
    .catch(err => console.error(err));

  return next(action);
};

export const NominatimMiddleware = [fetchAddress];
