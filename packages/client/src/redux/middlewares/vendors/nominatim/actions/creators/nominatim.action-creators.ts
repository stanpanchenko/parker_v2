import { NominatimActionTypes } from "../nominatim.action-types";
import {INominatimFetchAddressByCoordinatesAction} from "../interfaces/nominatim.action-interfaces";
import {ILngLat} from "../../../../../../shared/Interfaces/LatLng.interface";

export const fetchAddressByCoordinatesActionCreator = (
  lngLat: ILngLat
): INominatimFetchAddressByCoordinatesAction => ({
	type: NominatimActionTypes.fetchAddressByCoordinates,
	payload: lngLat
});
