import {NominatimActionTypes} from "../nominatim.action-types";

export interface INominatimFetchAddressByCoordinatesAction {
	type: NominatimActionTypes.fetchAddressByCoordinates;
	payload: {
		lat: number,
		lng: number
	}
	
}
