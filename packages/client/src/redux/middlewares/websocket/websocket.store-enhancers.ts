import io from "socket.io-client";
import env from "../../../env";
import {
  AuthenticateWebSocketPayload,
  WebSocketActionsI,
  WebSocketActionTypes
} from "../../../api/websocket/websocket.action-interfaces";
import { LocalStorageKeys } from "../../../shared/constants/localstorage-keys";
import { Store } from "redux";

const webSocketURL = env.API_SOCKET_ADDRESS;
const WsSocket = io(webSocketURL, {
  query: {
    // TODO: if token is invalid at application start websocket will not renew it after login
    authorization: localStorage.getItem(LocalStorageKeys.tokenKey)
  }
});

console.log(`websocket runs on: ${webSocketURL}`);

/**
 * @desc Maps WebSocket incoming actions to store actions.
 */
export function withWebSocketEvents(store: Store): Store<any, any> {
  const { dispatch } = store;
  // registers a WsSocket call defined in WebSocketActionTypes on application start
  Object.entries(WebSocketActionTypes).forEach(([actionName, type]) => {
    // map incoming WS event to store action
    WsSocket.on(type, payload => dispatch({ type, payload }));
  });

  WsSocket.on("connect", event => {
    // when client connects to WS, client will immediately send authorization event to WS with the token in payload,
    // if token is not valid, client will be disconnected
    console.log("WS Connected");
    const token = localStorage.getItem(LocalStorageKeys.tokenKey) || "";
    const payload: AuthenticateWebSocketPayload = { authorization: token };
    WsSocket.emit(WebSocketActionTypes.authenticate, payload);
  });

  WsSocket.on("reconnect_attempt", () => {
    console.log("WS Reconnection");
  });

  WsSocket.on("disconnect", reason => {
    console.log(`WS Disconnected: ${reason}`);
  });

  return store;
}

// make websocket emit function in a actionCreator thunk function
export const websocketDispatch = (action: WebSocketActionsI) => {
  WsSocket.emit(action.type, action.payload);
};

const webSocketReconnectMiddleware = ({
  getState,
  dispatch
}) => next => action => {
  if (action.type === WebSocketActionTypes.reconnect) WsSocket.connect();
  return next(action);
};

export const WebSocketMiddlewares = [webSocketReconnectMiddleware];
