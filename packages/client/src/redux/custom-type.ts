export type PropsProvider<S, P = any> = (
  state: S,
  componentOwnProps?: P
) => {
  [property: string]: any;
};
export type ActionHandler<S, A> = (state: S, action: A) => S;
