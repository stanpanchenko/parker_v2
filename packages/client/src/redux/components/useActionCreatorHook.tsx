import React from "react";
import { useDispatch } from "react-redux";
import { useCallback } from "react";

export function useActionCreator(actionCreator) {
  const dispatch = useDispatch();
  const handler = useCallback(
    (...args) => dispatch(actionCreator(...args)),
    [actionCreator]
  );
  return [handler];
}
