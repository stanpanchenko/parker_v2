import React from "react";
import { Provider } from "react-redux";
import applicationStore from "../../store";

export default ({ children }) => {
  return <Provider store={applicationStore}>{children}</Provider>;
};
