import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import {
  websocketDispatch,
  WebSocketMiddlewares,
  withWebSocketEvents
} from "./middlewares/websocket/websocket.store-enhancers";
import { composeWithDevTools } from "redux-devtools-extension";
import { applyMiddleware, createStore, Store } from "redux";
import { ApplicationStateI, rootReducer } from "./states";
import multiActions from "redux-multi";
import { ApiMiddlewares } from "../api/graphql/middlewares";
import { MessageMiddlewares } from "./states/messages/middleware";
import { MetaMiddleware } from "./middlewares/meta";
import { DispatchQueueMiddlewares } from "./states/dispatch-queue/middlewares";
import { PopupDialogMiddleware } from "./states/ui/states/popup-dialog/middlewares";
import { UserMiddlewares } from "./states/user/middlewares";
import { UserAuthenticationMiddleware } from "./states/user/substates/authentication/middlewares";
import { ThemeMiddlewares } from "./states/ui/states/theme/middlewares";
import { QueryMiddlewares } from "./states/query/middlewares";
import { ObservablesMiddleware } from "./middlewares/observables/observables.middleware";
import { FeatureCollectionMiddlewares } from "./states/feature-collection/middlewares";
import { ApplicationMiddleware } from "./states/application/middlewares";
import { NominatimMiddleware } from "./middlewares/vendors/nominatim/nominatim-api.middleware";
import {MapMiddlewares} from "./states/map/middlewars/map.middlewares";

const inDevelopment = process.env.NODE_ENV === "development";

const logger = createLogger({
  collapsed: true,
  diff: true
});

const middlewares = [
  ...ObservablesMiddleware,
  multiActions,
  thunk.withExtraArgument(websocketDispatch), // keep thunk at the start of the array
  inDevelopment && logger,
  // ------ vendor API -------
  ...NominatimMiddleware,
  // -------------------------
  ...MapMiddlewares,
  // -------------------------
  ...MessageMiddlewares, // keep first
  ...ApplicationMiddleware,
  ...PopupDialogMiddleware,
  ...DispatchQueueMiddlewares,
  ...MetaMiddleware,
  // ----------------------
  ...FeatureCollectionMiddlewares,
  // ----------------------
  ...WebSocketMiddlewares,
  ...UserAuthenticationMiddleware,
  ...UserMiddlewares,
  ...ThemeMiddlewares,
  ...QueryMiddlewares
].filter(Boolean);

function configureStore(): Store<ApplicationStateI, any> {
  const composeEnhancers = composeWithDevTools({ trace: true });
  const appliedMiddleware = applyMiddleware(...middlewares);
  const storeOptions = composeEnhancers(appliedMiddleware);
  return withWebSocketEvents(createStore(rootReducer, storeOptions));
}

export default configureStore();
