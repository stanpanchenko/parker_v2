import { Reducer } from "redux";
import {
  MessageActionsI,
  SetActionToRedispatchActionI
} from "./action-creators/message.actions-interface";
import { MessageActionTypes } from "./action-creators/message.action-types";
import { PayloadAction } from "../custom-interfaces";

export interface MessagesStateI {
  curMessage: string;
  hasMessage: boolean;
  lastMessages: any[];
  redispatchAction: PayloadAction;
}

const initState: MessagesStateI = {
  curMessage: "No Messages!",
  hasMessage: false,
  lastMessages: [],
  redispatchAction: null
};

function show(state: MessagesStateI, action) {
  return {
    ...state,
    curMessage: action.message,
    hasMessage: true
  };
}

function hide(state: MessagesStateI, action) {
  return {
    ...state,
    lastMessages: [...state.lastMessages, state.curMessage], // save last message
    curMessage: "", // reset message
    hasMessage: false // hide pop up
  };
}

function setActionToRedispatch(
  state: MessagesStateI,
  action: SetActionToRedispatchActionI
): MessagesStateI {
  return {
    ...state,
    redispatchAction: {
      ...action.payload.redispatchAction
    }
  };
}

function reduce(
  state: MessagesStateI,
  action: MessageActionsI
): MessagesStateI {
  switch (action.type) {
    case MessageActionTypes.setActionToConfirm:
      return setActionToRedispatch(state, action);
    case MessageActionTypes.showMessage:
      return show(state, action);
    case MessageActionTypes.hideMessage:
      return hide(state, action);
    default:
      return state;
  }
}

export const messageReducer: Reducer<MessagesStateI, MessageActionsI> = (
  state = initState,
  action
) => {
  return reduce(state, action);
};
