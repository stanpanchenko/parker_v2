import { MessageHideI, MessageShowI, SetActionToRedispatchActionI } from './message.actions-interface';
import { MessageActionTypes } from './message.action-types';
import { PayloadAction } from '../../custom-interfaces';



export const showMessage = (message: string): MessageShowI => {
  return {
    type: MessageActionTypes.showMessage,
    message
  };
};


export const hideMessage = (): MessageHideI => {
  return {
    type: MessageActionTypes.hideMessage
  };
};

export const setActionToRedispatch = (action: PayloadAction): SetActionToRedispatchActionI => {
  return {
    type: MessageActionTypes.setActionToConfirm,
    payload: {
      redispatchAction: {...action}
    }
  }
};

