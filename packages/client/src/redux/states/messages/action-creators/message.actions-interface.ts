import { MessageActionTypes } from "./message.action-types";
import { AnyAction } from 'redux';

export interface MessageShowI {
  type: MessageActionTypes.showMessage;
  message: string;
}
export interface MessageHideI {
  type: MessageActionTypes.hideMessage;
}

export interface SetActionToRedispatchActionI extends AnyAction {
  type: MessageActionTypes.setActionToConfirm;
  payload: {
    redispatchAction: {
      type: string;
      payload: any
    };
  }
}

export type MessageActionsI = MessageHideI | MessageShowI | SetActionToRedispatchActionI;
