export enum MessageActionTypes {
  showMessage = "[MessageAction]SHOW_MESSAGE",
  hideMessage = "[MessageAction]HIDE_MESSAGE",
  clearMessage = '[MESSAGE_ACTION]CLEAR_ACTION',
  setActionToConfirm = '[MESSAGE_ACTION]SET_ACTION_TO_CONFIRM'
}
