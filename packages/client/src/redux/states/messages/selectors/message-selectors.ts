import { ApplicationStateI } from '../../index';
import { createSelector } from 'reselect';

const hasMessage = (state: ApplicationStateI) => state.message.hasMessage;


export const hasMessageSelector = createSelector([hasMessage], (has):boolean=> has);
