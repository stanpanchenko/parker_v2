import { combineReducers } from "redux";
import {
  UserCredentialsStateI,
  UserCredentialsStateReducer
} from "./substates/credentials/user-credentials.state.reducer";
import { PositionReducer, UserPositionI } from "./substates/position/reducer";
import {
  UserAuthenticationReducer,
  UserAuthenticationStateI,
} from './substates/authentication/user-authentication.state-reducer';
import { UserRolesStateI, UserRolesStateReducer } from './substates/roles/user-roles.state-reducer';

export interface UserStateI {
  credentials: UserCredentialsStateI;
  position: UserPositionI;
  authenticated: UserAuthenticationStateI,
  roles: UserRolesStateI,
}

export const UserCombinedReducer = combineReducers({
  credentials: UserCredentialsStateReducer,
  position: PositionReducer,
  authenticated: UserAuthenticationReducer,
  roles: UserRolesStateReducer
});
