import { ApplicationStateI } from "../..";

import { createSelector, Selector } from "reselect";
import { UserMapPosition, UserPositionI } from "../substates/position/reducer";

const getUserPosition = (state: ApplicationStateI): UserPositionI =>
  state.user.position;

const currentPosition: Selector<
  ApplicationStateI,
  UserMapPosition
> = createSelector(
  [getUserPosition],
  (position: UserPositionI): UserMapPosition => {
    return position.current;
  }
);

const getAuthentication = (state: ApplicationStateI): boolean => {
  return state.user.authenticated;
};
const authentication = createSelector(
  [getAuthentication],
  authentication => {
    return authentication;
  }
);

export const UserSelectors = {
  currentPosition: (state: ApplicationStateI): UserMapPosition =>
    currentPosition(state),
  authentication: (state: ApplicationStateI) => authentication(state)
};
