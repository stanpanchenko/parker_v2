import * as AuthActionsCreators from "./creators/user-authentication.action-creators";
import {
  LoginActionI,
  SetAuthActionI,
  ValidateTokenActionI
} from "./interfaces/auth.actions-interface";
export { AuthActionsCreators };

export type UserAuthenticationActionsI =
  | SetAuthActionI
  | LoginActionI
  | ValidateTokenActionI;
