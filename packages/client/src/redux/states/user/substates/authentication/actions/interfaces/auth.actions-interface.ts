import { UserAuthenticationActionTypes } from '../types/user-authentication.action-types';
import { AnyAction } from 'redux';
import { LoginInput } from '../../../../../../../api/graphql/graphql-api.interface';

export interface SetAuthActionI {
  type: UserAuthenticationActionTypes.setAuth;
  authenticated: boolean;
}
export interface LoginActionI extends AnyAction {
  type: UserAuthenticationActionTypes.login;
  loginInput: LoginInput;
}

export interface LogoutActionI {
  type: UserAuthenticationActionTypes.logout
}

// export interface SetUserRoleActionI {
//   type: UserAuthenticationActionTypes.setUserRole;
//   role: Roles;
// }
export interface ValidateTokenActionI extends AnyAction {
  type: UserAuthenticationActionTypes.validateToken;
  token: string;
}
export interface LoginWithTokenActionI {
  type: UserAuthenticationActionTypes.loginWithToken;
  token: string;
}
