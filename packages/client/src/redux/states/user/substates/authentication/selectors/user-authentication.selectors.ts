import { ApplicationStateI } from "../../../../index";
import { createSelector, Selector } from "reselect";

export const isAuthenticated: Selector<ApplicationStateI, boolean> = createSelector(
  [(state: ApplicationStateI) => state.user.authenticated],
  (isAuth) => {
    return isAuth;
  }
);

// const userRoleSelector = createSelector(
//   [
//     (state: ApplicationStateI) => {
//       return state.auth.role;
//     }
//   ],
//   role => {
//     return role;
//   }
// );


