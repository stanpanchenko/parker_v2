import { UserAuthenticationActionTypes } from "../types/user-authentication.action-types";
import { LoginInput } from "../../../../../../../api/graphql/graphql-api.interface";

import {
  LoginActionI,
  LogoutActionI,
  SetAuthActionI,
  ValidateTokenActionI
} from "../interfaces/auth.actions-interface";

export function setAuth(authenticated: boolean): SetAuthActionI {
  return {
    type: UserAuthenticationActionTypes.setAuth,
    authenticated
  };
}

export function login(loginInput: LoginInput): LoginActionI {
  return {
    type: UserAuthenticationActionTypes.login,
    loginInput
  };
}

export function loginWithToken(token: string) {
  return {
    type: UserAuthenticationActionTypes.loginWithToken,
    token
  }
}

export function logout(): LogoutActionI {
  return {
    type: UserAuthenticationActionTypes.logout
  };
}

// export function setUserRole(role: Roles): SetUserRoleActionI {
//   return {
//     type: UserAuthenticationActionTypes.setUserRole,
//     role
//   };
// }

export function validateToken(token: string): ValidateTokenActionI {
  return {
    type: UserAuthenticationActionTypes.validateToken,
    token
  };
}
// export function setUserLoginCredentials(credentials: UserCredentialsLoginOutput): AuthSetUserLoginCredentialsActionI {
//   return {
//     type: UserAuthenticationActionTypes.setLoginCredentials,
//     payload: {
//       ...credentials
//     }
//   }
// }
