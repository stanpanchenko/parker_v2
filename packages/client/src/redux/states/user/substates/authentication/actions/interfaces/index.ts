import { LoginActionI, LogoutActionI, SetAuthActionI, ValidateTokenActionI } from './auth.actions-interface';

export type UserAuthenticationActionsI = SetAuthActionI | LoginActionI | LogoutActionI | ValidateTokenActionI
