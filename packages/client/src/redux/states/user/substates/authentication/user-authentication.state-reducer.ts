import { Reducer } from "redux";
import { UserAuthenticationActionTypes } from "./actions/types/user-authentication.action-types";
import { AuthReduceFunctions } from "./action-handlers";
import { UserAuthenticationActionsI } from "./actions";

export type UserAuthenticationStateI = boolean;

const initialState: UserAuthenticationStateI = false;

function reduce(
  state: UserAuthenticationStateI,
  action: UserAuthenticationActionsI
): UserAuthenticationStateI {
  switch (action.type) {
    // case UserAuthenticationActionTypes.setLoginCredentials:
    //   return AuthReduceFunctions.setUserLoginCredentials(state, action);
    case UserAuthenticationActionTypes.setAuth:
      return AuthReduceFunctions.setAuth(state, action.authenticated);
    // case UserAuthenticationActionTypes.setUserRole:
    //   return AuthReduceFunctions.setUserRole(state, action);
    default:
      return state;
  }
}

export const UserAuthenticationReducer: Reducer<
  UserAuthenticationStateI,
  any
> = (state = initialState, actions: UserAuthenticationActionsI) => {
  return reduce(state, actions);
};
