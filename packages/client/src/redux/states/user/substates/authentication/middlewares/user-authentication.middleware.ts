import { UserAuthenticationActionTypes } from "../actions/types/user-authentication.action-types";
import Api from "../../../../../../api/graphql";
import { LoginOutput } from "../../../../../../api/graphql/graphql-api.interface";
import { ErrorsActionCreators } from "../../../../errors/errors.action-creators";
import { handleError } from "../../../../../../error-handler/error-handler";
import { AuthActionsCreators } from "../actions";
import {
  LoginActionI,
  LoginWithTokenActionI,
  LogoutActionI,
  ValidateTokenActionI
} from "../actions/interfaces/auth.actions-interface";
import { UserCredentialsActionCreators } from "../../credentials/actions/creators";
import { UserRolesActionCreators } from "../../roles/actions/creators";
import { LocalStorageKeys } from "../../../../../../shared/constants/localstorage-keys";
import { WebSocketActionTypes } from "../../../../../../api/websocket/websocket.action-interfaces";

const login = ({ getStore, dispatch }) => next => (action: LoginActionI) => {
  if (action.type !== UserAuthenticationActionTypes.login) return next(action);
  Api.login(action.loginInput)
    .then((loginOutput: LoginOutput) => {
      localStorage.setItem(LocalStorageKeys.tokenKey, loginOutput.token);
      dispatch(
        UserCredentialsActionCreators.setUserCredentials({
          email: loginOutput.user.email,
          id: loginOutput.user.id
        })
      );
      dispatch({ type: WebSocketActionTypes.reconnect });
      dispatch(UserRolesActionCreators.setUserRoles(loginOutput.roles));
      dispatch(AuthActionsCreators.setAuth(true));
    })
    .catch(({ graphQLErrors }) => {
      const unauthorizedError = graphQLErrors.find(
        e => e.message.statusCode === 401
      );

      handleError(unauthorizedError);
      dispatch(
        ErrorsActionCreators.setError(
          unauthorizedError && unauthorizedError.message
        )
      );
    });
  return next(action);
};

const logout = ({ getStore, dispatch }) => next => (action: LogoutActionI) => {
  if (action.type !== UserAuthenticationActionTypes.logout) return next(action);
  localStorage.removeItem(LocalStorageKeys.tokenKey);
  dispatch(AuthActionsCreators.setAuth(false));
  return next(action); // pass action along
};

const validateToken = ({ getStore, dispatch }) => next => (
  action: ValidateTokenActionI
) => {
  if (action.type !== UserAuthenticationActionTypes.validateToken)
    return next(action);
  Api.validateToken(action.token)
    .then(({ valid }) => {
      if (valid) {
        // dispatch(UserCredentialsActionCreators.setUserCredentials(valid));
        // dispatch(AuthActionsCreators.setAuth(true));
        console.log("token valid");
      }
    })
    .catch(err => console.log(err));
};

const loginWithToken = ({ getStore, dispatch }) => next => (
  action: LoginWithTokenActionI
) => {
  if (action.type !== UserAuthenticationActionTypes.loginWithToken) {
    return next(action);
  }
  Api.loginWithToken(action.token)
    .then(({ user, roles }) => {
      if (user) {
        dispatch(UserCredentialsActionCreators.setUserCredentials(user));
        dispatch(AuthActionsCreators.setAuth(true));
        dispatch(UserRolesActionCreators.setUserRoles(roles));
      }
    })
    .catch(err => console.log(err));
  return next(action);
};

export default [login, validateToken, logout, loginWithToken];
