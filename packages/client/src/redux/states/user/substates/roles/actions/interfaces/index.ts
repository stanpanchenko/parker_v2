import { SetUserRolesActionI } from './user-roles.action-interfaces';

export type UserRolesActionsI = SetUserRolesActionI;
