import { UserRolesActionsI } from '../interfaces';
import { UserRolesStateI } from '../../user-roles.state-reducer';

export function setRoles(state:UserRolesStateI, action:UserRolesActionsI) {
  return action.payload.roles;
}
