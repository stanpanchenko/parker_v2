import { Roles } from '../../../../../../../api/graphql/graphql-api.interface';
import { UserRolesActionTypes } from '../types/user-roles.action-types';

export function setUserRoles(roles: Roles[]) {
  return {
    type: UserRolesActionTypes.setRoles,
    payload: {
      roles
    }
  }
}
