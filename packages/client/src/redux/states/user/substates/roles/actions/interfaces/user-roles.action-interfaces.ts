import { UserRolesActionTypes } from '../types/user-roles.action-types';
import { Roles } from '../../../../../../../api/graphql/graphql-api.interface';

export interface SetUserRolesActionI {
  type: UserRolesActionTypes.setRoles;
  payload: {
    roles: Roles[]
  }
}
