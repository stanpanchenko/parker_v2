import { Roles } from '../../../../../api/graphql/graphql-api.interface';
import { UserRolesActionTypes } from './actions/types/user-roles.action-types';
import { UserRolesActionReducers } from './actions/reducers';
import { UserRolesActionsI } from './actions/interfaces';

export type UserRolesStateI = Roles[]

const initState:UserRolesStateI = [];


const reduce = (state: UserRolesStateI, action:UserRolesActionsI) => {
  switch (action.type) {
    case UserRolesActionTypes.setRoles:
      return UserRolesActionReducers.setRoles(state, action);
    default: return state;
  }
};

export const UserRolesStateReducer = (state = [], action: UserRolesActionsI) => {
  return reduce(state,action)
};
