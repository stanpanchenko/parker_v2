import { UserCredentialsActionTypes } from '../types/user-credentials.action-types';
import { UserCredentialsStateI } from '../../user-credentials.state.reducer';

export interface SetUserEmailActionsI {
  type: UserCredentialsActionTypes.setUserCredentials;
  payload: {email: string}
}

export interface SetCredentialsActionI {
  type: UserCredentialsActionTypes.setUserCredentials;
  payload: UserCredentialsStateI
}
