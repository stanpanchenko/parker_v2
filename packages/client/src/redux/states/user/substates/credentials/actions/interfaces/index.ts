import { SetCredentialsActionI, SetUserEmailActionsI } from './user-credentials.actions-interface';

export type UserCredentialsActionsI = SetUserEmailActionsI | SetCredentialsActionI;
