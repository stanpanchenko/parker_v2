import { UserCredentialsStateI } from '../../user-credentials.state.reducer';
import { UserCredentialsActionTypes } from '../types/user-credentials.action-types';

export function setUserCredentials(credentials: UserCredentialsStateI) {
  return {
    type: UserCredentialsActionTypes.setUserCredentials,
    payload: credentials
  };
}
