import { Reducer } from "redux";
import { UserCredentialsActionsI } from "./actions/interfaces";
import { UserCredentialsActionReducers } from "./actions/reduces";
import { UserCredentialsActionTypes } from "./actions/types/user-credentials.action-types";

export interface UserCredentialsStateI {
  email: string;
  id: string;
}
const initialState: UserCredentialsStateI = {
  email: "",
  id: ''
};

const reduce = (
  state: UserCredentialsStateI,
  action: UserCredentialsActionsI
): UserCredentialsStateI => {
  switch (action.type) {
    case UserCredentialsActionTypes.setUserCredentials:
      return UserCredentialsActionReducers.setCreadentials(state, action);
    default:
      return state;
  }
};

export const UserCredentialsStateReducer: Reducer<
  UserCredentialsStateI,
  UserCredentialsActionsI
> = (
  state = initialState,
  action: UserCredentialsActionsI
): UserCredentialsStateI => reduce(state, action);
