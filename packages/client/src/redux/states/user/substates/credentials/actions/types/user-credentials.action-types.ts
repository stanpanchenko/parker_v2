export enum UserCredentialsActionTypes {
  setUserCredentials = '[USER_CREDENTIALS_ACTION]SET_USER_CREDENTIALS'
}
