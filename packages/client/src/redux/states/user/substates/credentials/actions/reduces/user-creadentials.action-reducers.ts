import { UserCredentialsStateI } from '../../user-credentials.state.reducer';

export function setCreadentials(state:UserCredentialsStateI,action):UserCredentialsStateI {
  return {
    ...state,
    ...action.payload,
  }
}
export function setUserCredentials(state, action) {
  return state;
}
