import { Reducer } from "redux";
import ReducerFunctions from "./reducer.functions";
import { LocationActionsI } from "./action.interface";
import { ActionTypes } from "./action.types";
import { ILngLat } from '../../../../../shared/Interfaces/LatLng.interface';


export type UserMapPosition = ILngLat;
export interface UserPositionI {
  city: string;
  country: string;
  current: UserMapPosition | null;
}

const initState: UserPositionI = {
  city: "",
  country: "",
  current: null
};

const reducer: Reducer<UserPositionI, LocationActionsI> = (
  state = initState,
  action
) => {
  switch (action.type) {
    case ActionTypes.setUserPosition:
      return ReducerFunctions.setUserPosition(state, action.location);
    default:
      return state;
  }
};
export { reducer as PositionReducer };
