import { ActionTypes } from "./action.types";
import { SetLocationActionI } from "./action.interface";
import { UserMapPosition } from './reducer';

export const UserPositionActions = {
  setPosition: (location: UserMapPosition): SetLocationActionI => ({
    type: ActionTypes.setUserPosition,
    location
  })
};
