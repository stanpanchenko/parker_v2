import { UserMapPosition, UserPositionI } from './reducer';


function setUserPosition(
  oldState: UserPositionI,
  location: UserMapPosition
): UserPositionI {
  const newState = { ...oldState };
  newState.current = {...location};
  return newState;
}

const ReducerFunctions = {
  setUserPosition
};

export default ReducerFunctions;
