import { ActionTypes } from "./action.types";
import { UserMapPosition } from './reducer';

export interface SetLocationActionI {
  location: UserMapPosition;
  type: ActionTypes.setUserPosition;
}

export type LocationActionsI = SetLocationActionI;
