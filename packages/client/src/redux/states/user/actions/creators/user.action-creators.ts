import { UserActionType } from "../types/user.action-type";
import { UserSignUpActionI } from '../interfaces/user.action-interfaces';
import { SignUpInput } from '../../../../../api/graphql/graphql-api.interface';

export function createUserAction(
  user: SignUpInput
): UserSignUpActionI {
  return {
    type: UserActionType.createUser,
    payload: user
  };
}
