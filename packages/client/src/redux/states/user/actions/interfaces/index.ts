import { UserSignUpActionI } from './user.action-interfaces';

export type UserActionsInterface = UserSignUpActionI;
