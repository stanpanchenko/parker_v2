import { UserActionType } from '../types/user.action-type';
import { SignUpInput } from '../../../../../api/graphql/graphql-api.interface';

export interface UserSignUpActionI {
  type: UserActionType.createUser;
  payload: SignUpInput;
}
