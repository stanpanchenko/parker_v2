import { UserActionType } from "../actions/types/user.action-type";
import Api from "../../../../api/graphql";
import { UserSignUpActionI } from "../actions/interfaces/user.action-interfaces";

const signUp = ({ getStore, dispatch }) => next => (
  action: UserSignUpActionI
) => {
  if (action.type !== UserActionType.createUser) return next(action);

  Api.createUser(action.payload)
    .then(res => console.log(res))
    .catch((error: Error) => {
      console.log("error");
      console.error(error.message);
    });

  return next(action);
};

export default [signUp];
