import { combineReducers } from "redux";
import {
  messageReducer as message,
  MessagesStateI
} from "./messages/messages.reducer";
import { MapReducer, MapStateI } from "./map/map.reducer";
import {
  ApplicationModeReducer,
  ApplicationModeStateI
} from "./application/application.state-reducer";
import { UIReducer, UIStateI } from "./ui";
import { UserCombinedReducer, UserStateI } from "./user";
import { ErrorsReducer, ErrorsStateI } from "./errors/errors.reducer";
import {
  DispatchQueueReducer,
  DispatchQueueStateI
} from "./dispatch-queue/dispatch-queue.reducer";
import {
  FeatureCollectionReducer,
  IFeatureCollectionState
} from "./feature-collection/features-collection.reducer";

export interface ApplicationStateI {
  message: MessagesStateI;
  map: MapStateI;
  user: UserStateI;
  applicationMode: ApplicationModeStateI;
  ui: UIStateI;
  errors: ErrorsStateI;
  dispatchQueue: DispatchQueueStateI;
  featureCollection: IFeatureCollectionState;
}

export const rootReducer = combineReducers({
  user: UserCombinedReducer,
  message,
  errors: ErrorsReducer,
  map: MapReducer,
  applicationMode: ApplicationModeReducer,
  ui: UIReducer,
  dispatchQueue: DispatchQueueReducer,
  featureCollection: FeatureCollectionReducer
});
