export enum DispatchQueueActionTypes {
  addToDispatchQueue = '[DISPATCH_QUEUE_ACTION]ADD_TO_QUEUE',
  removeFromDispatchQueue = '[DISPATCH_QUEUE_ACTION]REMOVE_FROM_QUEUE',
  redispatch = '[DISPATCH_QUEUE_ACTION]REDISPATCH',
}
