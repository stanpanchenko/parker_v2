import { AnyAction } from "redux";
import { DispatchQueueActionTypes } from "../action-types/dispatch-queue.action-types";
import { ActionType } from "../dispatch-queue.reducer";
import {
  AddToDispatchQueueActionsI,
  RedispatchActionActionI,
  RemoveFromDispatchQueueActionI
} from "../action-interfaces/dispatch-queue.actions-interface";

export function addToDispatchQueue(
  action: AnyAction
): AddToDispatchQueueActionsI {
  return {
    type: DispatchQueueActionTypes.addToDispatchQueue,
    payload: {
      ...action
    }
  };
}
export function removeFromDispatchQueue(
  type: ActionType
): RemoveFromDispatchQueueActionI {
  return {
    type: DispatchQueueActionTypes.removeFromDispatchQueue,
    payload: {
      type
    }
  };
}
export function reDispatch(type: ActionType): RedispatchActionActionI {
  // This action will be captured by the DispatchMiddleware. see DispatchMiddleware ...
  return {
    type: DispatchQueueActionTypes.redispatch,
    payload: {
      type
    }
  };
}
