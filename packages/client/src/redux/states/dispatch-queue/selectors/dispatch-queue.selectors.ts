import { ApplicationStateI } from '../../index';
import { createSelector } from 'reselect';
import { ActionType } from '../dispatch-queue.reducer';

const types = (state: ApplicationStateI) => state.dispatchQueue.types;

export const actionTypeToRedispatch = createSelector([types], (types: ActionType[]): ActionType => {
  if (types.length === 0) return '';
  return types[types.length-1];
});
