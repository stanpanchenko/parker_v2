import { DispatchQueueActionTypes } from "../action-types/dispatch-queue.action-types";
import { PayloadAction } from "../../custom-interfaces";
import { AnyAction } from "redux";
import { ActionType } from "../dispatch-queue.reducer";

export interface AddToDispatchQueueActionsI extends PayloadAction {
  type: DispatchQueueActionTypes.addToDispatchQueue;
  payload: AnyAction;
}

export interface RemoveFromDispatchQueueActionI extends PayloadAction {
  type: DispatchQueueActionTypes.removeFromDispatchQueue;
  payload: {
    type: ActionType;
  };
}

export interface RedispatchActionActionI {
  type: DispatchQueueActionTypes.redispatch;
  payload: {
    type: ActionType
  }
}
