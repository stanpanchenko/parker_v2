import {
  AddToDispatchQueueActionsI,
  RedispatchActionActionI,
  RemoveFromDispatchQueueActionI
} from "./dispatch-queue.actions-interface";

export type DispatchQueueActions =
  | AddToDispatchQueueActionsI
  | RemoveFromDispatchQueueActionI
  | RedispatchActionActionI;
