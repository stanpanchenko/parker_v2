import { DispatchQueueStateI } from "../dispatch-queue.reducer";
import {
  AddToDispatchQueueActionsI,
  RedispatchActionActionI
} from "../action-interfaces/dispatch-queue.actions-interface";
import { removeProperty } from "../../../../utils/objects";

export function addToQueue(
  state: DispatchQueueStateI,
  action: AddToDispatchQueueActionsI
): DispatchQueueStateI {
  // action was stopped by MetaMiddleware and should be added for re-dispatching
  if (
    state.types.includes(action.payload.type) &&
    state.actions[action.payload.type]
  )
    return state;

  return {
    ...state,
    types: [...state.types, action.payload.type],
    actions: {
      ...state.actions,
      [action.payload.type]: {
        ...action.payload
      }
    }
  };
}

export function redispatch(
  state: DispatchQueueStateI,
  action: RedispatchActionActionI
): DispatchQueueStateI {
  // action was re-dispatched by the DispatchQueueMiddleware at this point and can be removed from the state
  return {
    ...state,
    types: state.types.filter(type => type !== action.payload.type),
    actions: removeProperty(state.actions, action.payload.type)
  };
}
