import { AnyAction, Reducer } from 'redux';
import { DispatchQueueActions } from './action-interfaces';
import { DispatchQueueActionTypes } from './action-types/dispatch-queue.action-types';
import { DispatchActionHandlers } from './action-handlers';


export type ActionType = string;
export interface DispatchQueueStateI {
  types: ActionType[];
  actions: {
    [actionType: string]: AnyAction
  }
}

const initState:DispatchQueueStateI = {
  types: [],
  actions: {}
};

function reduce(state:DispatchQueueStateI, action: DispatchQueueActions): DispatchQueueStateI {
  switch (action.type) {
    case DispatchQueueActionTypes.redispatch:
      return DispatchActionHandlers.redispatch(state, action);
    case DispatchQueueActionTypes.addToDispatchQueue:
      return DispatchActionHandlers.addToQueue(state, action);
    default:
      return state;
  }
}

export const DispatchQueueReducer: Reducer<DispatchQueueStateI, any> = (state = initState, action: DispatchQueueActions):DispatchQueueStateI =>{
  return reduce(state, action);
};
