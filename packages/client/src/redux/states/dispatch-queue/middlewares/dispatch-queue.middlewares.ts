import { RedispatchActionActionI } from "../action-interfaces/dispatch-queue.actions-interface";
import { DispatchQueueActionTypes } from "../action-types/dispatch-queue.action-types";
import { ApplicationStateI } from "../../index";

const reDispatchMiddleware = ({ getState, dispatch }) => next => (
  action: RedispatchActionActionI
) => {
  if (action.type !== DispatchQueueActionTypes.redispatch) return next(action);

  const state: ApplicationStateI = getState();
  if (!state.dispatchQueue.actions[action.payload.type]) return next(action);

  // get function for re-dispatch from the state
  const actionToRedispatch = state.dispatchQueue.actions[action.payload.type];
  // dispatch action from the state
  dispatch({ ...actionToRedispatch });

  // pass the action (not the action to re-dispatch) a long,
  // this will be handled by the DispatchQueueReducer and the re-dispatch action will be removed from the state
  return next(action);
};
export default [reDispatchMiddleware];
