// @ts-ignore
import {
  FeatureType,
  GeometryType
} from "../../../api/graphql/graphql-api.interface";
import { FeatureCollectionReducer } from "./features-collection.reducer";
import { ISaveToStateNormalizedFeaturesAction } from "./actions/interfaces/feature-collection.interfaces";
import { FeatureCollectionActionTypes } from "./actions/types/feature-collection.action-types";

describe("featureCollectionReducer", () => {
  it("should handle saveNormalizedFeaturesToState action", function() {
    const action: ISaveToStateNormalizedFeaturesAction = {
      type: FeatureCollectionActionTypes.saveNormalizedFeaturesToState,
      payload: {
        normalizedFeatures: {
          features: {
            f1: {
              id: "f1",
              type: FeatureType.Feature,
              geometry: "f1_g1",
              properties: "f1_p1"
            }
          },
          geometry: {
            f1_g1: {
              id: "f1_g1",
              type: GeometryType.Point,
              coordinates: [0, 0]
            }
          },
          properties: {
            f1_p1: {
              id: "f1_p1"
            }
          }
        }
      }
    };
    const state = {
      features: {},
      geometry: {},
      properties: {}
    };
    expect(FeatureCollectionReducer(state, action)).toEqual({
      features: {
        f1: {
          id: "f1",
          type: FeatureType.Feature,
          geometry: "f1_g1",
          properties: "f1_p1"
        }
      },
      geometry: {
        f1_g1: {
          id: "f1_g1",
          type: GeometryType.Point,
          coordinates: [0, 0]
        }
      },
      properties: {
        f1_p1: {
          id: "f1_p1"
        }
      }
    });
  });
});
