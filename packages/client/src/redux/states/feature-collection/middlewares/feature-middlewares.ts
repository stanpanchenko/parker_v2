import {
  IDeleteFeatureAction,
  IDeleteFeaturesAction,
  IFetchAllFeaturesAction,
  IFetchLineStringFeaturesAction,
  IFetchPointFeaturesAction,
  IFetchPolygonFeaturesAction,
  INormalizeFeaturesAction,
  ISignaleCreateOnDBFeatures
} from "../actions/interfaces/feature-collection.interfaces";
import { FeatureCollectionActionTypes } from "../actions/types/feature-collection.action-types";
import Api from "../../../../api/graphql";
import { FeatureCollectionActionCreators } from "../actions/creators";
import {
  denormalizeFeatures,
  normalizeFeatures
} from "../utils/feature-normalizer/feature-normalizer";
import { ApplicationStateI } from "../../index";
import { mapObjectValues } from "../../../../utils/objects";
import { GeometryType } from "../../../../api/graphql/graphql-api.interface";
import { featuresMockData } from "../features-mock-data";

const createFeaturesOnDB = ({ getState, dispatch }) => next => (
  action: ISignaleCreateOnDBFeatures
) => {
  if (action.type !== FeatureCollectionActionTypes.signalCreateOnDBFeatures)
    return next(action);
  const state: ApplicationStateI = getState();
  let { features: denormFeatures } = denormalizeFeatures(
    state.featureCollection.created
  );
  if (denormFeatures.length > 0) {
    dispatch(FeatureCollectionActionCreators.creatingOnDBFeatures());
    denormFeatures = mapObjectValues(denormFeatures, (key, value) => {
      if (key === "id") return; // remove id attribute from objects
      return value;
    });
    Api.createFeatures(denormFeatures).then(resp => {
      console.log("Api.createFeatures: ", resp);
      dispatch(FeatureCollectionActionCreators.featuresCreatedOnDB());
    });
  }

  return next(action);
};

const deleteFeatures = () => next => (action: IDeleteFeaturesAction) => {
  if (action.type !== FeatureCollectionActionTypes.deleteFeatures)
    return next(action);
  const {
    payload: { ids }
  } = action;
  Api.deleteFeatures(ids).then(res => {
    console.log("Api.deleteFeatures: ", res);
  });
  return next(action);
};

const deleteFeature = () => next => (action: IDeleteFeatureAction) => {
  if (action.type !== FeatureCollectionActionTypes.deleteFeature)
    return next(action);
  const {
    payload: { id }
  } = action;
  Api.deleteFeatures([id]).then(res => {
    console.log("Api.deleteFeature: ", res);
  });
  return next(action);
};

const fetchAllFeatures = ({ getState, dispatch }) => next => (
  action: IFetchAllFeaturesAction
) => {
  if (action.type !== FeatureCollectionActionTypes.fetchAllFeatures) {
    return next(action);
  }
  Api.featuresByGeometryType([
    GeometryType.Polygon,
    GeometryType.LineString,
    GeometryType.Point
  ]).then(response => {
    const {
      data: { features }
    } = response;
    if (features) {
      console.log("Mock features are applied!");
      dispatch(
        FeatureCollectionActionCreators.normalizeFeatures([
          ...features,
          ...featuresMockData
        ])
      );
    }
  });
  return next(action);
};

const fetchPointFeatures = ({ getState, dispatch }) => next => (
  action: IFetchPointFeaturesAction
) => {
  const { type } = action;
  if (type !== FeatureCollectionActionTypes.fetchPointFeatures)
    return next(action);

  const {
    payload: { type: point }
  } = action;

  Api.featuresByGeometryType(point).then(response => {
    const {
      data: { features }
    } = response;
    if (features) {
      dispatch(FeatureCollectionActionCreators.normalizeFeatures(features));
    }
  });

  return next(action);
};

const fetchLineStringFeatures = ({ getState, dispatch }) => next => (
  action: IFetchLineStringFeaturesAction
) => {
  const { type } = action;
  if (type !== FeatureCollectionActionTypes.fetchLineStringFeatures)
    return next(action);
  const {
    payload: { type: lineString }
  } = action;
  Api.featuresByGeometryType(lineString).then(response => {
    console.log(response);
  });

  return next(action);
};

const fetchPolygonFeatures = ({ getState, dispatch }) => next => (
  action: IFetchPolygonFeaturesAction
) => {
  const { type } = action;
  if (type !== FeatureCollectionActionTypes.fetchPolygonFeatures)
    return next(action);
  const {
    payload: { type: polygon }
  } = action;

  Api.featuresByGeometryType(polygon).then(response => {
    console.log(response);
  });

  return next(action);
};

const normalizeFeaturesMiddleware = ({ getState, dispatch }) => next => (
  action: INormalizeFeaturesAction
) => {
  const { type } = action;
  if (type !== FeatureCollectionActionTypes.normalizeFeatures)
    return next(action);
  const {
    payload: { features }
  } = action;

  const normalizeAsync = async features => {
    return normalizeFeatures(features);
  };

  normalizeAsync(features).then(normalizedFeatures => {
    dispatch(
      FeatureCollectionActionCreators.saveToStateNormalizedFeatures(
        normalizedFeatures
      )
    );
  });

  return next(action);
};

export default [
  fetchAllFeatures,
  fetchPointFeatures,
  fetchLineStringFeatures,
  fetchPolygonFeatures,
  normalizeFeaturesMiddleware,
  createFeaturesOnDB,
  deleteFeature,
  deleteFeatures
];
