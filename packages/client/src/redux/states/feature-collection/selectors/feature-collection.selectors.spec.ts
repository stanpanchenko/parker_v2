// @ts-ignore
import { FeatureCollectionSelectors } from "./index";
import {
  FeatureType,
  GeometryType
} from "../../../../api/graphql/graphql-api.interface";
import {
  denormalizedCreatedFeaturesSelector,
  selectedFeatureSelector
} from "./feature-coolection.selectors";

describe("featureCollectionSelectors", () => {
  it("should denormalize state.featureCollection in to array of features", function() {
    const state: any = {
      featureCollection: {
        features: {
          f1: {
            id: "f1",
            type: FeatureType.Feature,
            geometry: "f1_g1",
            properties: "f1_p1"
          }
        },
        geometry: {
          f1_g1: {
            id: "f1_g1",
            type: GeometryType.Polygon,
            coordinates: [
              [
                [-122.4009572, 37.7811682],
                [-122.404562, 37.7769622],
                [-122.4114285, 37.7795401],
                [-122.4074803, 37.7842208],
                [-122.4011288, 37.781236]
              ]
            ]
          }
        },
        properties: {
          f1_p1: {
            id: "f1_p1",
            color: [210, 0, 255]
          }
        }
      }
    };
    const features = FeatureCollectionSelectors.featuresSelector(state);
    expect(features).toEqual([
      {
        id: "f1",
        type: "Feature",
        geometry: {
          id: "f1_g1",
          type: GeometryType.Polygon,
          coordinates: [
            [
              [-122.4009572, 37.7811682],
              [-122.404562, 37.7769622],
              [-122.4114285, 37.7795401],
              [-122.4074803, 37.7842208],
              [-122.4011288, 37.781236]
            ]
          ]
        },
        properties: {
          id: "f1_p1",
          color: [210, 0, 255]
        }
      }
    ]);
  });

  describe("denormalizedCreatedFeaturesSelector", () => {
    it("should return denormalized created features", function() {
      const normalizedFeature = {
        features: {
          f1: {
            id: "f1",
            type: FeatureType.Feature,
            geometry: "f1_g1",
            properties: "f1_p1"
          }
        },
        geometry: {
          f1_g1: {
            id: "f1_g1",
            type: GeometryType.Polygon,
            coordinates: [
              [
                [-122.4009572, 37.7811682],
                [-122.404562, 37.7769622],
                [-122.4114285, 37.7795401],
                [-122.4074803, 37.7842208],
                [-122.4011288, 37.781236]
              ]
            ]
          }
        },
        properties: {
          f1_p1: {
            id: "f1_p1",
            color: [210, 0, 255]
          }
        }
      };
      const state: any = {
        featureCollection: {
          created: normalizedFeature
        }
      };
      expect(denormalizedCreatedFeaturesSelector(state)).toEqual([
        {
          id: "f1",
          type: "Feature",
          geometry: {
            id: "f1_g1",
            type: GeometryType.Polygon,
            coordinates: [
              [
                [-122.4009572, 37.7811682],
                [-122.404562, 37.7769622],
                [-122.4114285, 37.7795401],
                [-122.4074803, 37.7842208],
                [-122.4011288, 37.781236]
              ]
            ]
          },
          properties: {
            id: "f1_p1",
            color: [210, 0, 255]
          }
        }
      ]);
    });
  });

  describe("selectedFeatureSelector", () => {
    it("should return updated feature if it was selected", () => {
      const state: any = {
        featureCollection: {
          selectedFeatureId: "f1",
          updated: {
            features: {
              f1: {
                id: "f1",
                type: FeatureType.Feature,
                geometry: "f1_g1",
                properties: "f1_p1"
              }
            },
            geometry: {
              f1_g1: {
                id: "f1_g1",
                type: GeometryType.Polygon,
                coordinates: [
                  [
                    [-122.4009572, 37.7811682],
                    [-122.404562, 37.7769622],
                    [-122.4114285, 37.7795401],
                    [-122.4074803, 37.7842208],
                    [-122.4011288, 37.781236]
                  ]
                ]
              }
            },
            properties: {
              f1_p1: {
                id: "f1_p1",
                color: [210, 0, 255]
              }
            }
          },
          features: {},
          created: {
            features: {}
          }
        }
      };
      expect(selectedFeatureSelector(state)).toEqual({
        id: "f1",
        type: "Feature",
        geometry: {
          id: "f1_g1",
          type: GeometryType.Polygon,
          coordinates: [
            [
              [-122.4009572, 37.7811682],
              [-122.404562, 37.7769622],
              [-122.4114285, 37.7795401],
              [-122.4074803, 37.7842208],
              [-122.4011288, 37.781236]
            ]
          ]
        },
        properties: {
          id: "f1_p1",
          color: [210, 0, 255]
        }
      });
    });
    it("should return created feature if it was selected", () => {
      const state: any = {
        featureCollection: {
          selectedFeatureId: "f1",
          features: {},
          created: {
            features: {
              f1: {
                id: "f1",
                type: FeatureType.Feature,
                geometry: "f1_g1",
                properties: "f1_p1"
              }
            },
            geometry: {
              f1_g1: {
                id: "f1_g1",
                type: GeometryType.Polygon,
                coordinates: [
                  [
                    [-122.4009572, 37.7811682],
                    [-122.404562, 37.7769622],
                    [-122.4114285, 37.7795401],
                    [-122.4074803, 37.7842208],
                    [-122.4011288, 37.781236]
                  ]
                ]
              }
            },
            properties: {
              f1_p1: {
                id: "f1_p1",
                color: [210, 0, 255]
              }
            }
          }
        }
      };
      expect(selectedFeatureSelector(state)).toEqual({
        id: "f1",
        type: "Feature",
        geometry: {
          id: "f1_g1",
          type: GeometryType.Polygon,
          coordinates: [
            [
              [-122.4009572, 37.7811682],
              [-122.404562, 37.7769622],
              [-122.4114285, 37.7795401],
              [-122.4074803, 37.7842208],
              [-122.4011288, 37.781236]
            ]
          ]
        },
        properties: {
          id: "f1_p1",
          color: [210, 0, 255]
        }
      });
    });
    it("should return undefined when no feature selected", function() {
      const state: any = {
        featureCollection: {
          selectedFeatureId: ""
        }
      };
      expect(selectedFeatureSelector(state)).toBeUndefined();
    });
    it("should return feature if feature selected", async () => {
      const state: any = {
        featureCollection: {
          selectedFeatureId: "f1",
          features: {
            f1: {
              id: "f1",
              type: FeatureType.Feature,
              geometry: "f1_g1",
              properties: "f1_p1"
            }
          },
          geometry: {
            f1_g1: {
              id: "f1_g1",
              type: GeometryType.Polygon,
              coordinates: [
                [
                  [-122.4009572, 37.7811682],
                  [-122.404562, 37.7769622],
                  [-122.4114285, 37.7795401],
                  [-122.4074803, 37.7842208],
                  [-122.4011288, 37.781236]
                ]
              ]
            }
          },
          properties: {
            f1_p1: {
              id: "f1_p1",
              color: [210, 0, 255]
            }
          }
        }
      };
      expect(selectedFeatureSelector(state)).toEqual({
        id: "f1",
        type: "Feature",
        geometry: {
          id: "f1_g1",
          type: GeometryType.Polygon,
          coordinates: [
            [
              [-122.4009572, 37.7811682],
              [-122.404562, 37.7769622],
              [-122.4114285, 37.7795401],
              [-122.4074803, 37.7842208],
              [-122.4011288, 37.781236]
            ]
          ]
        },
        properties: {
          id: "f1_p1",
          color: [210, 0, 255]
        }
      });
    });
  });
});
