import { createSelector } from "reselect";
import { ApplicationStateI } from "../../index";
import {
  INormalizedFeature,
  INormalizedGeometry,
  INormalizedProperties
} from "../features-collection.reducer";

import { denormalizeFeatures } from "../utils/feature-normalizer/feature-normalizer";
import {
  Feature,
  Geometry,
  GeometryType
} from "../../../../api/graphql/graphql-api.interface";

const selectedFeatureId = (state: ApplicationStateI) =>
  state.featureCollection.selectedFeatureId;
const featureByID = (state: ApplicationStateI) =>
  state.featureCollection.features;
const geometryByID = (state: ApplicationStateI) =>
  state.featureCollection.geometry;
const propertyByID = (state: ApplicationStateI) =>
  state.featureCollection.properties;
const deletedNormalizedFeatures = (state: ApplicationStateI) =>
  state.featureCollection.deleted;
const createdNormalizedFeatures = (state: ApplicationStateI) =>
  state.featureCollection.created;
const updatedNormalizedFeatures = (state: ApplicationStateI) =>
  state.featureCollection.updated;

const featuresByIdSelector = createSelector(
  [featureByID],
  features => {
    return features;
  }
);

const geometryByIdSelector = createSelector(
  [geometryByID],
  geometry => {
    return geometry;
  }
);

const propertiesByIdSelector = createSelector(
  [propertyByID],
  properties => {
    return properties;
  }
);
const normalizedCreatedFeaturesSelector = createSelector(
  [createdNormalizedFeatures],
  created => {
    return created;
  }
);
const normalizedUpdatedFeaturesSelector = createSelector(
  [updatedNormalizedFeatures],
  updated => {
    return updated;
  }
);
const normalizedDeletedFeaturesSelector = createSelector(
  [deletedNormalizedFeatures],
  deleted => {
    return deleted;
  }
);

/**
 * @desc Builds denormalized created Features
 */
export const denormalizedCreatedFeaturesSelector = createSelector(
  [normalizedCreatedFeaturesSelector],
  createdFeatures => {
    const { features } = denormalizeFeatures(createdFeatures);
    return features;
  }
);

/**
 * @desc Builds denormalized updated Features
 */
export const denormalizedUpdatedFeaturesSelector = createSelector(
  [normalizedUpdatedFeaturesSelector],
  updatedFeatures => {
    const { features } = denormalizeFeatures(updatedFeatures);
    return features;
  }
);

/**
 * @desc Builds denormalized deleted Features
 */
export const denormalizedDeletedFeaturesSelector = createSelector(
  [normalizedDeletedFeaturesSelector],
  deletedFeatures => {
    const { features } = denormalizeFeatures(deletedFeatures);
    return features;
  }
);

/**
 * @desc Builds denormalized Features from feature,geometry and property. Does not contain created updated and deleted features
 */
export const featuresSelector = createSelector(
  [featuresByIdSelector, geometryByIdSelector, propertiesByIdSelector],
  (
    features: INormalizedFeature,
    geometry: INormalizedGeometry,
    properties: INormalizedProperties
  ) => {
    const { features: normalized } = denormalizeFeatures({
      features,
      geometry,
      properties
    });
    return normalized;
  }
);

/**
 * @desc Builds denormalized features, updated and created features
 */
export const denormalizedCUFeatures = createSelector(
  [
    featuresSelector,
    denormalizedCreatedFeaturesSelector,
    denormalizedUpdatedFeaturesSelector
  ],
  (features, denCreaFeatures, denUpdFeatures) => {
    return [...features, ...denCreaFeatures, ...denUpdFeatures];
  }
);

export const denormalizedLineStringFeatures = createSelector(
  [denormalizedCUFeatures],
  features => {
    return features.filter(f => f.geometry.type === GeometryType.LineString);
  }
);
export const denormalizedPointFeatures = createSelector(
  [denormalizedCUFeatures],
  features => {
    return features.filter(f => f.geometry.type === GeometryType.Point);
  }
);
export const denormalizedPolygonFeatures = createSelector(
  [denormalizedCUFeatures],
  features => {
    return features.filter(f => f.geometry.type === GeometryType.Polygon);
  }
);

export const selectedFeatureSelector = createSelector(
  [
    createdNormalizedFeatures,
    updatedNormalizedFeatures,
    featuresByIdSelector,
    geometryByIdSelector,
    propertiesByIdSelector,
    selectedFeatureId
  ],
  (
    createdFeatures,
    updatedFeatures,
    featuresById,
    geometryById,
    propertiesById,
    id
  ): Feature | undefined => {
    if (id === "") return;
    if (featuresById[id]) {
      const geo = geometryById[featuresById[id].geometry];
      const prop = propertiesById[featuresById[id].properties];
      return {
        ...featuresById[id],
        geometry: {
          ...geo
        },
        properties: {
          ...prop
        }
      };
    }
    if (createdFeatures.features[id]) {
      const geo: any =
        createdFeatures.geometry[createdFeatures.features[id].geometry];
      const prop: any =
        createdFeatures.properties[createdFeatures.features[id].properties];
      return {
        ...createdFeatures.features[id],
        geometry: {
          ...geo
        },
        properties: {
          ...prop
        }
      };
    }
    if (updatedFeatures.features[id]) {
      const geo: any =
        updatedFeatures.geometry[updatedFeatures.features[id].geometry];
      const prop: any =
        updatedFeatures.properties[updatedFeatures.features[id].properties];
      return {
        ...updatedFeatures.features[id],
        geometry: {
          ...geo
        },
        properties: {
          ...prop
        }
      };
    }
    return;
  }
);

export const featureCollectionSelector = createSelector(
  [denormalizedCUFeatures],
  features => {
    return {
      type: "FeatureCollection",
      features: features
    };
  }
);
