import {
  FeatureType,
  GeometryType
} from "../../../api/graphql/graphql-api.interface";

export const featuresMockData = [
  {
    id: "f1",
    type: FeatureType.Feature,
    geometry: {
      id: "f1_g1",
      type: GeometryType.Polygon,
      coordinates: [
        [
          [-122.4009572, 37.7811682],
          [-122.404562, 37.7769622],
          [-122.4114285, 37.7795401],
          [-122.4074803, 37.7842208],
          [-122.4011288, 37.781236]
        ]
      ]
    },
    properties: {
      id: "f1_p1",
      color: [210, 0, 255],
      tags: [
        {
          key: "parking",
          value: "street"
        },
        {
          key: "parking",
          value: "yes"
        },
        {
          key: "parking",
          value: "secure"
        },
        {
          key: "parking",
          value: "private"
        },
        {
          key: "parking",
          value: "space"
        }
      ]
    }
  },
  {
    id: "f2",
    type: FeatureType.Feature,
    geometry: {
      id: "f2_g1",
      type: GeometryType.Polygon,
      coordinates: [
        [
          [-122.4532626, 37.7792743],
          [-122.4572966, 37.7701834],
          [-122.4354098, 37.7635341],
          [-122.4259684, 37.7761536],
          [-122.4532626, 37.7792743]
        ],
        [
          [-122.447301, 37.7759105],
          [-122.4359285, 37.7744858],
          [-122.4391471, 37.7698724],
          [-122.4492751, 37.7714328],
          [-122.447301, 37.7759105]
        ]
      ]
    },
    properties: {
      id: "f2_p1",
      color: [103, 103, 103]
    }
  },
  {
    id: "f4",
    type: FeatureType.Feature,
    geometry: {
      id: "f4_g1",
      type: GeometryType.Point,
      coordinates: [-122.41669, 37.7953]
    },
    properties: {
      id: "f4_g1",
      radius: 50
    }
  },
  {
    id: "f5",
    type: FeatureType.Feature,
    geometry: {
      id: "f5_g1",
      type: GeometryType.LineString,
      coordinates: [
        [-122.40310747834417, 37.79129939280775],
        [-122.39132506725193, 37.78669276639316]
      ]
    },
    properties: {
      id: "f5_p1",
      color: [210, 0, 255]
    }
  }
];
