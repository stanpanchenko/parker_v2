import { AnyAction } from "redux";
import { FeatureCollectionActionTypes } from "./actions/types/feature-collection.action-types";
import {
  FeatureType,
  GeometryType
} from "../../../api/graphql/graphql-api.interface";
import { FeatureCollectionActionReducers } from "./action-reducers";
import { FeatureCollectionActions } from "./actions/interfaces";
import { featuresMockData } from './features-mock-data';

export interface INormalizedFeature {
  [id: string]: {
    id: string;
    type: FeatureType;
    geometry: string;
    properties: string;
  };
}

export interface INormalizedGeometry {
  [id: string]: {
    id: string;
    type: GeometryType;
    coordinates: any[];
  };
}

export type INormalizedProperties = any;

export interface IFeatureCollectionState {
  selectedFeatureId: string;
  updated: {
    [id: string]: INormalizedFeature;
  };
  created: {
    [id: string]: INormalizedFeature;
  };
  deleted: {
    [id: string]: INormalizedFeature;
  };
  features: INormalizedFeature;
  geometry: INormalizedGeometry;
  properties: INormalizedProperties;
}

const initState: IFeatureCollectionState = {
  selectedFeatureId: "",
  updated: {
    features: {},
    geometry: {},
    properties: {}
  },
  created: {
    features: {},
    geometry: {},
    properties: {}
  },
  deleted: {
    features: {},
    geometry: {},
    properties: {}
  },
  features: {
  },
  geometry: {
  },
  properties: {
  
  }
};

const reduce = (
  state,
  action: FeatureCollectionActions
): IFeatureCollectionState => {
  switch (action.type) {
    case FeatureCollectionActionTypes.deleteFeatures:
      return FeatureCollectionActionReducers.deleteFeatures(state, action);
    case FeatureCollectionActionTypes.saveNormalizedFeaturesToState: {
      return FeatureCollectionActionReducers.saveNormalizedFeatures(
        state,
        action
      );
    }
    case FeatureCollectionActionTypes.selectFeature: {
      return FeatureCollectionActionReducers.selectFeature(state, action);
    }
    case FeatureCollectionActionTypes.saveNewFeatureToState: {
      return FeatureCollectionActionReducers.saveNewFeature(state, action);
    }
    default:
      return state;
  }
};

export const FeatureCollectionReducer = (
  state = initState,
  action: FeatureCollectionActions
): IFeatureCollectionState => {
  return reduce(state, action);
};
