// @ts-ignore
import {
  FeatureType,
  GeometryType
} from "../../../../../api/graphql/graphql-api.interface";
import { normalizeFeatures } from "./feature-normalizer";

describe("featureNormalizer", () => {
  it("should normalize geoJSON feature", async () => {
    const input = [
      {
        id: "f1",
        type: FeatureType.Feature,
        geometry: {
          id: "f1_g1",
          type: GeometryType.Point,
          coordinates: [0, 0]
        },
        properties: {
          id: "f1_p1"
        }
      }
    ];

    expect(await normalizeFeatures(input)).toEqual({
      features: {
        f1: {
          id: "f1",
          type: FeatureType.Feature,
          geometry: "f1_g1",
          properties: "f1_p1"
        }
      },
      geometry: {
        f1_g1: {
          id: "f1_g1",
          type: GeometryType.Point,
          coordinates: [0, 0]
        }
      },
      properties: {
        f1_p1: {
          id: "f1_p1"
        }
      }
    });
  });
});
