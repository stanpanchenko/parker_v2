import { denormalize, normalize } from "normalizr";
import { schema } from "normalizr";

const tagsSchema = new schema.Entity("tags");

const geometry = new schema.Entity("geometry");
const properties = new schema.Entity("properties", {
  tags: tagsSchema
});

const feature = new schema.Entity("features", {
  geometry,
  properties
});
export const featuresSchema = { features: [feature] };

export const normalizeFeatures = (features: any[]) => {
  const { entities } = normalize({ features: features }, featuresSchema);
  return entities;
};
export const denormalizeFeatures = (normalizedFeatures: any) => {
  if (!normalizedFeatures.features) return [];

  return denormalize(
    { features: Object.keys(normalizedFeatures.features) },
    featuresSchema,
    normalizedFeatures
  );
};
