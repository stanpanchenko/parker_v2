import {
  IDeleteFeaturesAction,
  ISaveNewFeatureAction,
  ISaveToStateNormalizedFeaturesAction,
  ISelectFeatureAction
} from "../actions/interfaces/feature-collection.interfaces";
import { IFeatureCollectionState } from "../features-collection.reducer";
import uuid from "uuid";
import { normalizeFeatures } from "../utils/feature-normalizer/feature-normalizer";
import { removeProperties } from "../../../../../../api/src/utils/utils";
import { ApplicationStateI } from "../../index";
import { removeProperty } from "../../../../utils/objects";

const getId = (prefix: string = "new") => {
  const uui = uuid();
  const splited = uui.split("-");
  return `${prefix}-${splited[splited.length - 1]}`;
};

export const deleteUpdatedFeature = (
  state: IFeatureCollectionState,
  action: any
) => {
  const {
    payload: { id }
  } = action;
  if (!state.updated.features[id]) return state;
  const feature = state.updated.features[id];
  let newCrProp = removeProperty(state.updated.properties, feature.properties);
  let newCrGeo = removeProperty(state.updated.geometry, feature.geometry);
  let newCrFeat = removeProperty(state.updated.features, id);
  return {
    ...state,
    updated: {
      ...state.updated,
      features: {
        ...newCrFeat
      },
      geometry: {
        ...newCrGeo
      },
      properties: {
        ...newCrProp
      }
    }
  };
};

export const deleteCreatedFeature = (
  state: IFeatureCollectionState,
  action: any
) => {
  const {
    payload: { id }
  } = action;
  if (!state.created.features[id]) return state;
  const feature = state.created.features[id];
  let newCrProp = removeProperty(state.created.properties, feature.properties);
  let newCrGeo = removeProperty(state.created.geometry, feature.geometry);
  let newCrFeat = removeProperty(state.created.features, id);
  return {
    ...state,
    created: {
      ...state.created,
      features: {
        ...newCrFeat
      },
      geometry: {
        ...newCrGeo
      },
      properties: {
        ...newCrProp
      }
    }
  };
};

export const deleteFeature = (
  state: IFeatureCollectionState,
  action: any
): IFeatureCollectionState => {
  const {
    payload: { id }
  } = action;
  let newState = { ...state };
  newState = deleteCreatedFeature(newState, { payload: { id } });

  if (!newState.features[id]) return newState;
  const feature = newState.features[id];
  const propertiesID = feature.properties;
  const geometryID = feature.geometry;

  const newFeatures = removeProperty(newState.features, feature.id);
  const newGeo = removeProperty(newState.geometry, geometryID);
  const newProp = removeProperty(newState.properties, propertiesID);

  return {
    ...newState,
    features: {
      ...newFeatures
    },
    geometry: {
      ...newGeo
    },
    properties: {
      ...newProp
    }
  };
};

export const deleteFeatures = (
  state: IFeatureCollectionState,
  action: IDeleteFeaturesAction
) => {
  const {
    payload: { ids }
  } = action;
  let newState = { ...state };
  ids.forEach(id => {
    newState = deleteFeature(newState, {
      payload: {
        id: id
      }
    });
  });
  return newState;
};

export const saveNormalizedFeatures = (
  state: IFeatureCollectionState,
  action: ISaveToStateNormalizedFeaturesAction
): IFeatureCollectionState => {
  const {
    payload: { normalizedFeatures }
  } = action;
  return {
    ...state,
    features: {
      ...state.features,
      ...normalizedFeatures.features
    },
    geometry: {
      ...state.geometry,
      ...normalizedFeatures.geometry
    },
    properties: {
      ...state.properties,
      ...normalizedFeatures.properties
    }
  };
};

export const saveNewFeature = (
  state: IFeatureCollectionState,
  action: ISaveNewFeatureAction
): IFeatureCollectionState => {
  const {
    payload: { feature }
  } = action;
  console.log("saveNewFeatureToState: ", feature);
  const newFeature = {
    id: getId("new"),
    ...feature,
    geometry: {
      id: getId("new"),
      ...feature.geometry
    },
    properties: {
      id: getId("new"),
      ...feature.properties
    }
  };
  const normalizedFeature = normalizeFeatures([newFeature]);
  return {
    ...state,
    created: {
      ...state.created,
      features: {
        ...state.created.features,
        ...normalizedFeature.features
      },
      geometry: {
        ...state.created.geometry,
        ...normalizedFeature.geometry
      },
      properties: {
        ...state.created.properties,
        ...normalizedFeature.properties
      }
    }
  };
};

export const selectFeature = (
  state: IFeatureCollectionState,
  action: ISelectFeatureAction
) => {
  if (state.selectedFeatureId === action.payload.id) return state;
  return {
    ...state,
    selectedFeatureId: action.payload.id
  };
};
