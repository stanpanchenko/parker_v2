// @ts-ignore
import { FeatureCollectionActionReducers } from "./index";
import { ISaveToStateNormalizedFeaturesAction } from "../actions/interfaces/feature-collection.interfaces";
import { FeatureCollectionActionTypes } from "../actions/types/feature-collection.action-types";
import {
  FeatureType,
  GeometryType
} from "../../../../api/graphql/graphql-api.interface";

describe("featureActionReducers", () => {
  describe("deleteCreatedFeature", () => {
    it("should delete created feature from state", function() {
      const state: any = {
        features: {},
        geometry: {},
        properties: {},
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        created: {
          features: {
            f1: {
              id: "f1",
              type: FeatureType.Feature,
              geometry: "f1_g1",
              properties: "f1_p1"
            }
          },
          geometry: {
            f1_g1: {
              id: "f1_g1",
              type: GeometryType.Point,
              coordinates: [0, 0]
            }
          },
          properties: {
            f1_p1: {
              id: "f1_p1"
            }
          }
        }
      };
      expect(
        FeatureCollectionActionReducers.deleteCreatedFeature(state, {
          payload: { id: "f1" }
        })
      ).toEqual({
        features: {},
        geometry: {},
        properties: {},
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        created: {
          features: {},
          geometry: {},
          properties: {}
        }
      });
    });
  });

  describe("deleteFeatures", () => {
    it("should delete multiple features at once", function() {
      const state: any = {
        created: {
          features: {},
          geometry: {},
          properties: {}
        },
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        features: {
          f1: {
            id: "f1",
            type: FeatureType.Feature,
            geometry: "f1_g1",
            properties: "f1_p1"
          }
        },
        geometry: {
          f1_g1: {
            id: "f1_g1",
            type: GeometryType.Point,
            coordinates: [0, 0]
          }
        },
        properties: {
          f1_p1: {
            id: "f1_p1"
          }
        }
      };

      expect(
        FeatureCollectionActionReducers.deleteFeatures(state, {
          type: FeatureCollectionActionTypes.deleteFeatures,
          payload: { ids: ["f1"] }
        })
      ).toEqual({
        features: {},
        geometry: {},
        properties: {},
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        created: {
          features: {},
          geometry: {},
          properties: {}
        }
      });
    });
  });
  describe("deleteFeature", () => {
    it("should deleteFeature from state", function() {
      const state: any = {
        created: {
          features: {},
          geometry: {},
          properties: {}
        },
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        features: {
          f1: {
            id: "f1",
            type: FeatureType.Feature,
            geometry: "f1_g1",
            properties: "f1_p1"
          }
        },
        geometry: {
          f1_g1: {
            id: "f1_g1",
            type: GeometryType.Point,
            coordinates: [0, 0]
          }
        },
        properties: {
          f1_p1: {
            id: "f1_p1"
          }
        }
      };

      expect(
        FeatureCollectionActionReducers.deleteFeature(state, {
          payload: { id: "f1" }
        })
      ).toEqual({
        created: {
          features: {},
          geometry: {},
          properties: {}
        },
        updated: {
          features: {},
          geometry: {},
          properties: {}
        },
        features: {},
        geometry: {},
        properties: {}
      });
    });
  });
  describe("saveNormalizedFeatures", () => {
    it("should write to state correctly", function() {
      const state: any = {
        features: {},
        geometry: {},
        properties: {}
      };
      const action: ISaveToStateNormalizedFeaturesAction = {
        type: FeatureCollectionActionTypes.saveNormalizedFeaturesToState,
        payload: {
          normalizedFeatures: {
            features: {
              f1: {
                id: "f1",
                type: FeatureType.Feature,
                geometry: "f1_g1",
                properties: "f1_p1"
              }
            },
            geometry: {
              f1_g1: {
                id: "f1_g1",
                type: GeometryType.Point,
                coordinates: [0, 0]
              }
            },
            properties: {
              f1_p1: {
                id: "f1_p1"
              }
            }
          }
        }
      };
      expect(
        FeatureCollectionActionReducers.saveNormalizedFeatures(state, action)
      ).toEqual({
        features: {
          f1: {
            id: "f1",
            type: FeatureType.Feature,
            geometry: "f1_g1",
            properties: "f1_p1"
          }
        },
        geometry: {
          f1_g1: {
            id: "f1_g1",
            type: GeometryType.Point,
            coordinates: [0, 0]
          }
        },
        properties: {
          f1_p1: {
            id: "f1_p1"
          }
        }
      });
    });
  });
});
