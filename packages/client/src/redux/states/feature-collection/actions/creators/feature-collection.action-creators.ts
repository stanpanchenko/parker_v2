import {
  IDeleteFeatureAction, IDeleteFeaturesAction,
  IFetchAllFeaturesAction,
  IFetchLineStringFeaturesAction,
  IFetchPointFeaturesAction,
  IFetchPolygonFeaturesAction,
  INormalizeFeaturesAction,
  ISaveNewFeatureAction,
  ISaveToStateNormalizedFeaturesAction,
  ISelectFeatureAction
} from "../interfaces/feature-collection.interfaces";
import { FeatureCollectionActionTypes } from "../types/feature-collection.action-types";
import {
  Feature,
  GeometryType
} from "../../../../../api/graphql/graphql-api.interface";

export const upsertFeatures = (features: Feature[]) => ({
  type: FeatureCollectionActionTypes.upsertFeatures,
  payload: {
    features
  }
});

export const deleteFeature = (id: string): IDeleteFeatureAction => ({
  type: FeatureCollectionActionTypes.deleteFeature,
  payload: {
    id
  }
});

export const deleteFeatures = (ids: string[]): IDeleteFeaturesAction => ({
  type: FeatureCollectionActionTypes.deleteFeatures,
  payload: {
    ids
  }
});

export const signalCraeteOnDBFeaturesAction = () => ({
  type: FeatureCollectionActionTypes.signalCreateOnDBFeatures
});

export const creatingOnDBFeatures = () => ({
  type: FeatureCollectionActionTypes.creatingOnDBFeatures
});
export const featuresCreatedOnDB = () => ({
  type: FeatureCollectionActionTypes.featuresCreatedOnDB
});

export const createOnDBFeatures = (features: Feature[]) => ({
  type: FeatureCollectionActionTypes.createOnDBFeatures,
  payload: {
    features
  }
});

export const upsertingFeatures = () => ({
  type: FeatureCollectionActionTypes.upsertingFeatures
});

export const upsertedFeatures = () => ({
  type: FeatureCollectionActionTypes.upsertedFeatures
});

export const fetchPointFeatures = (): IFetchPointFeaturesAction => ({
  type: FeatureCollectionActionTypes.fetchPointFeatures,
  payload: {
    type: GeometryType.Point
  }
});

export const fetchLineStringFeatures = (): IFetchLineStringFeaturesAction => ({
  type: FeatureCollectionActionTypes.fetchLineStringFeatures,
  payload: {
    type: GeometryType.LineString
  }
});

export const fetchPolygonFeatures = (): IFetchPolygonFeaturesAction => ({
  type: FeatureCollectionActionTypes.fetchPolygonFeatures,
  payload: {
    type: GeometryType.Polygon
  }
});

export const fetchAllFeatures = (): IFetchAllFeaturesAction => ({
  type: FeatureCollectionActionTypes.fetchAllFeatures
});

export const normalizeFeatures = (
  features: any[]
): INormalizeFeaturesAction => ({
  type: FeatureCollectionActionTypes.normalizeFeatures,
  payload: {
    features
  }
});

export const saveToStateNormalizedFeatures = (
  normalizedFeatures: any
): ISaveToStateNormalizedFeaturesAction => ({
  type: FeatureCollectionActionTypes.saveNormalizedFeaturesToState,
  payload: {
    normalizedFeatures
  }
});

export const saveNewFeature = (feature: any): ISaveNewFeatureAction => ({
  type: FeatureCollectionActionTypes.saveNewFeatureToState,
  payload: {
    feature
  }
});

export const selectFeature = (id: string): ISelectFeatureAction => ({
  type: FeatureCollectionActionTypes.selectFeature,
  payload: {
    id
  }
});
