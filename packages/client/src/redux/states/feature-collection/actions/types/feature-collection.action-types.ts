export enum FeatureCollectionActionTypes {
  fetchAllFeatures = "[FeatureCollectionActionTypes]FETCH_ALL_FEATURES",
  fetchPointFeatures = "[FeatureCollectionActionTypes]FETCH_POINT_FEATURES",
  fetchLineStringFeatures = "[FeatureCollectionActionTypes]FETCH_LINESTRING_FEATURES",
  fetchPolygonFeatures = "[FeatureCollectionActionTypes]FETCH_POLYGON_FEATURES",
  // -------------------------
  normalizeFeatures = "[FeatureCollectionActionTypes]NORMALIZE_FEATURES",
  // -------------------------
  saveNormalizedFeaturesToState = "[FeatureCollectionActionTypes]SAVE_TO_STATE_NORMALIZED_FEATURES",
  saveNewFeatureToState = "[FeatureCollectionActionTypes]SAVE_NEW_FEATURE_IN_STATE",
  // -------------------------
  selectFeature = "[FeatureCollectionActionTypes]SELECT_FEATURE",
  // -------------------------
  upsertFeatures = "[FeatureCollectionActionTypes]UPSERT_FEATURE",
  upsertingFeatures = "[FeatureCollectionActionTypes]UPSERTING_FEATURE",
  upsertedFeatures = "[FeatureCollectionActionTypes]UPSERTED_FEATURE",
  // -------------------------
  createOnDBFeatures = "[FeatureCollectionActionTypes]CREATE_ON_DB_FEATURE",
  creatingOnDBFeatures = "[FeatureCollectionActionTypes]CREATING_ON_DB_FEATURE",
  featuresCreatedOnDB = "[FeatureCollectionActionTypes]FEATURES_CREATED_ON_DB",
  // signaling that the created features needs to be created in to the DB
  signalCreateOnDBFeatures = "[FeatureCollectionActionTypes]SIGNAL_CREATED_ON_DB_FEATURE",
  //--------------------------
  deleteFeature = '[FeatureCollectionActionTypes]DELETE_FEATURE',
  deleteFeatures = '[FeatureCollectionActionTypes]DELETE_FEATURES'
}
