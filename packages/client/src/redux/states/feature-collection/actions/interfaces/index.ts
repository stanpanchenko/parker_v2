import {
  IDeleteFeatureAction, IDeleteFeaturesAction,
  IFetchAllFeaturesAction,
  IFetchLineStringFeaturesAction,
  IFetchPointFeaturesAction,
  IFetchPolygonFeaturesAction,
  INormalizeFeaturesAction,
  ISaveNewFeatureAction,
  ISaveToStateNormalizedFeaturesAction,
  ISelectFeatureAction,
  IUpsertFeaturesAction,
} from './feature-collection.interfaces';

export type FeatureCollectionActions =
  | IDeleteFeaturesAction
  | IDeleteFeatureAction
  | IFetchAllFeaturesAction
  | IFetchPointFeaturesAction
  | IFetchLineStringFeaturesAction
  | IFetchPolygonFeaturesAction
  | INormalizeFeaturesAction
  | ISaveToStateNormalizedFeaturesAction
  | ISaveNewFeatureAction
  | ISelectFeatureAction
  | IUpsertFeaturesAction;
