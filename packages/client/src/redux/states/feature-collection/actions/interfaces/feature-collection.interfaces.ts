import {FeatureCollectionActionTypes} from "../types/feature-collection.action-types";
import {Feature, GeometryType} from "../../../../../api/graphql/graphql-api.interface";


export interface IUpsertFeaturesAction {
	type: FeatureCollectionActionTypes.upsertFeatures,
	payload: {
		features: Feature[]
	}
}

export interface ICreateOnDBFeatures {
	type: FeatureCollectionActionTypes.createOnDBFeatures,
	payload: {
		features: Feature[]
	}
}

export interface ISignaleCreateOnDBFeatures {
	type: FeatureCollectionActionTypes.signalCreateOnDBFeatures,
}

export interface IFetchPointFeaturesAction {
	type: FeatureCollectionActionTypes.fetchPointFeatures
	payload:{
		type: GeometryType.Point
	}
}

export interface IFetchLineStringFeaturesAction {
	type: FeatureCollectionActionTypes.fetchLineStringFeatures
	payload:{
		type: GeometryType.LineString
	}
}

export interface IFetchPolygonFeaturesAction {
	type: FeatureCollectionActionTypes.fetchPolygonFeatures
	payload:{
		type: GeometryType.Polygon
	}
}
export interface IFetchAllFeaturesAction {
	type: FeatureCollectionActionTypes.fetchAllFeatures
}

export interface INormalizeFeaturesAction {
	type: FeatureCollectionActionTypes.normalizeFeatures,
	payload:{
		features: any
	}
}

export interface ISaveToStateNormalizedFeaturesAction {
	type: FeatureCollectionActionTypes.saveNormalizedFeaturesToState,
	payload:{
		normalizedFeatures: any
	}
}

export interface ISaveNewFeatureAction {
	type: FeatureCollectionActionTypes.saveNewFeatureToState,
	payload:{
		feature: Feature
	}
}
export interface ISelectFeatureAction {
	type: FeatureCollectionActionTypes.selectFeature,
	payload:{
		id:string
	}
}

export interface IDeleteFeaturesAction {
	type: FeatureCollectionActionTypes.deleteFeatures,
	payload: {
		ids:string[]
	}
}

export interface IDeleteFeatureAction {
	type: FeatureCollectionActionTypes.deleteFeature,
	payload: {
		id: string
	}
}
