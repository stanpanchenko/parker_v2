import { AnyAction } from 'redux';

export interface ParkerAction extends AnyAction {
  payload: any;
  meta?: any;
}

export interface Action extends MetaAction, PayloadAction{

}

export interface MetaAction extends AnyAction {
  meta: {
    [extras: string]: any
  };
}

export interface PayloadAction extends AnyAction {
  payload: any
}

export interface ConfirmAction extends PayloadAction, MetaAction {
  meta: {
    confirm: boolean,
    [extra: string]: any
  }
}
