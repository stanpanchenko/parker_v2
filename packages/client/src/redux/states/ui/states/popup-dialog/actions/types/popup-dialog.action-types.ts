export enum PopupDialogActionTypes {
  showConfirmation = '[POPUP_DIALOG_ACTION]SHOW_CONFIRMATION',
  removeConfirmation = '[POPUP_DIALOG_ACTION]REMOVE_CONFIRMATION',
  confirmAction = '[POPUP_DIALOG_ACTION]CONFIRM_ACTION',
  denyAction = '[POPUP_DIALOG_ACTION]DENY_ACTION'
}
