import { createSelector, Selector } from "reselect";
import { ApplicationStateI } from "../../../../index";
import { PopupDialogConfirmationI } from '../popup-dialog.reducer';

const confirmation = (state: ApplicationStateI): PopupDialogConfirmationI  =>
  state.ui.popupDialog.confirmation;

export const confirmationDialog: Selector<
  ApplicationStateI,
  PopupDialogConfirmationI
  > = createSelector(
  [confirmation],
  (confirmation): PopupDialogConfirmationI => confirmation
);
