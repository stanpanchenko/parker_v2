import { ConfirmAction } from "../../../../custom-interfaces";
import { PopupDialogActionCreators } from "../actions/creators";
import {
  ConfirmActionActionI,
  DenyActionActionI
} from "../actions/interfaces/popup-dialog.action-interfaces";
import { PopupDialogActionTypes } from "../actions/types/popup-dialog.action-types";

const confirmActionInterceptorMiddleware = ({ getState, dispatch }) => next => (
  action: ConfirmAction
) => {
  if (!action.meta || !action.meta.confirm) return next(action);
  // action to confirm will be added to the dispatch queue and not processed yet

  const onDenyAction = !!action.meta.onDeny
    ? action.meta.onDeny
    : PopupDialogActionCreators.removeConfirmation();
  const onConfirmAction = { type: action.type, payload: action.payload };
  dispatch(
    PopupDialogActionCreators.showConfirmation(onConfirmAction, onDenyAction)
  );
  return;
};

const confirmActionMiddleware = ({ getState, dispatch }) => next => (
  action: ConfirmActionActionI
) => {
  if (action.type !== PopupDialogActionTypes.confirmAction) return next(action);

  dispatch(PopupDialogActionCreators.removeConfirmation());
  dispatch(action.payload.action);
  return;
};

const denyActionMiddleware = ({ getState, dispatch }) => next => (
  action: DenyActionActionI
) => {
  if (action.type !== PopupDialogActionTypes.denyAction) return next(action);

  dispatch(action.payload.action);
  return;
};

export default [
  confirmActionInterceptorMiddleware,
  confirmActionMiddleware,
  denyActionMiddleware
];
