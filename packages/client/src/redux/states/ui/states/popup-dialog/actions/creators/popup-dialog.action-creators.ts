import {
  ConfirmActionActionI,
  DenyActionActionI,
  RemoveConfirmationPopupDialogActionI,
  ShowConfirmationPopupDialogActionI,
} from '../interfaces/popup-dialog.action-interfaces';
import { AnyAction } from 'redux';
import { PopupDialogActionTypes } from '../types/popup-dialog.action-types';

export function showConfirmation(onConfirmAction: AnyAction, onDenyAction: AnyAction):ShowConfirmationPopupDialogActionI {
  return {
    type: PopupDialogActionTypes.showConfirmation,
    payload:{
      onConfirmAction: {...onConfirmAction},
      onDenyAction: {...onDenyAction}
    }
  }
}

export function removeConfirmation(): RemoveConfirmationPopupDialogActionI {
  return {
    type: PopupDialogActionTypes.removeConfirmation
  }
}

export function confirmAction(action: AnyAction): ConfirmActionActionI {
  return {
    type: PopupDialogActionTypes.confirmAction,
    payload: {
      action: {...action}
    }
  }
}

export function denyAction(action: AnyAction): DenyActionActionI{
  return {
    type: PopupDialogActionTypes.denyAction,
    payload: {
      action
    }
  }
}


