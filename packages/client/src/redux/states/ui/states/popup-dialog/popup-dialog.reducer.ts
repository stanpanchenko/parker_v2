import { AnyAction } from "redux";
import { PopupDialogActionsI } from "./actions/interfaces";
import { PopupDialogActionTypes } from "./actions/types/popup-dialog.action-types";
import { PopupDialogActionHandlers } from "./actions/handlers";

export interface DialogActions {
  onConfirmAction: AnyAction;
  onDenyAction: AnyAction;
}
export interface PopupDialogConfirmationI extends DialogActions {}
export interface PopupDialogWarningI extends DialogActions {}

export interface PopupDialogStateI {
  confirmation: PopupDialogConfirmationI | null;
  warning: DialogActions | null;
}
const initState: PopupDialogStateI = {
  confirmation: null,
  warning: null
};

const reduce = (state: PopupDialogStateI, action: PopupDialogActionsI) => {
  switch (action.type) {
    case PopupDialogActionTypes.showConfirmation:
      return PopupDialogActionHandlers.showConfirmation(state, action);
    case PopupDialogActionTypes.removeConfirmation:
      return PopupDialogActionHandlers.removeConfirmation(state, action);
    default:
      return state;
  }
};
export const PopupDialogReducer = (
  state = initState,
  action
): PopupDialogStateI => reduce(state, action);
