import { PopupDialogStateI } from "../../popup-dialog.reducer";
import {
  RemoveConfirmationPopupDialogActionI,
  ShowConfirmationPopupDialogActionI
} from "../interfaces/popup-dialog.action-interfaces";

export function showConfirmation(
  state: PopupDialogStateI,
  action: ShowConfirmationPopupDialogActionI
): PopupDialogStateI {
  return {
    ...state,
    confirmation: {
      onConfirmAction: { ...action.payload.onConfirmAction },
      onDenyAction: { ...action.payload.onDenyAction }
    }
  };
}

export function removeConfirmation(
  state: PopupDialogStateI,
  action: RemoveConfirmationPopupDialogActionI
): PopupDialogStateI {
  return {
    ...state,
    confirmation: null
  };
}
