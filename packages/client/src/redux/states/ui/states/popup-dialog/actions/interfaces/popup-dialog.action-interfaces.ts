import { PopupDialogActionTypes } from '../types/popup-dialog.action-types';
import { PayloadAction } from '../../../../../custom-interfaces';
import { AnyAction } from 'redux';

export interface ShowConfirmationPopupDialogActionI extends PayloadAction{
  type: PopupDialogActionTypes.showConfirmation;
  payload: {
    onConfirmAction: AnyAction,
    onDenyAction: AnyAction,
  }
}
export interface RemoveConfirmationPopupDialogActionI extends AnyAction {
  type: PopupDialogActionTypes.removeConfirmation
}

export interface ConfirmActionActionI {
  type: PopupDialogActionTypes.confirmAction;
  payload: {
    action: AnyAction
  }
}
export interface DenyActionActionI {
  type: PopupDialogActionTypes.denyAction;
  payload: {
    action: AnyAction
  }
}
