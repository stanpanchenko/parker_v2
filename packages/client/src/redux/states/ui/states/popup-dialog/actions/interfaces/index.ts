import {
  RemoveConfirmationPopupDialogActionI,
  ShowConfirmationPopupDialogActionI,
} from './popup-dialog.action-interfaces';

export type PopupDialogActionsI = ShowConfirmationPopupDialogActionI| RemoveConfirmationPopupDialogActionI
