import { ApplicationStateI } from '../../../../index';
import { createSelector } from 'reselect';

const visible = (state: ApplicationStateI): boolean => state.ui.query.visible;

export const queryVisible = createSelector([visible],(visible): boolean => visible);
