import { QueryUIActionTypes } from '../types/query-ui.action-types';


export interface ShowQueryUIActionI {
  type: QueryUIActionTypes.showQuery;
}

export interface HideQueryUIActionI {
  type: QueryUIActionTypes.hideQuery;
}
