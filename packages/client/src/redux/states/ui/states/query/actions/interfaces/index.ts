import {
  HideQueryUIActionI,
  ShowQueryUIActionI
} from "./query-ui.actions-interface";

export type QueryUIActionsInterface = ShowQueryUIActionI | HideQueryUIActionI;
