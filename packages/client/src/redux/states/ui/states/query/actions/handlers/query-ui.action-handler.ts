import { HideQueryUIActionI, ShowQueryUIActionI } from '../interfaces/query-ui.actions-interface';
import { QueryUIStateI } from '../../query-ui.reducer';


export function showQuery(oldState: QueryUIStateI, action: ShowQueryUIActionI) {
  return {
    ...oldState,
    visible: true
  }
}
export function hideQuery(oldState: QueryUIStateI, action: HideQueryUIActionI) {

  return {
    ...oldState,
    visible: false
  };
}
