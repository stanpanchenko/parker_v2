import { QueryUIActionHandlers } from "./actions/handlers";
import { QueryUIActionTypes } from "./actions/types/query-ui.action-types";
import { QueryUIActionsInterface } from "./actions/interfaces";

export interface QueryUIStateI {
  visible: boolean;
}

const initState: QueryUIStateI = {
  visible: false
};

const reduce = (state: QueryUIStateI, action: QueryUIActionsInterface) => {
  switch (action.type) {
    case QueryUIActionTypes.hideQuery:
      return QueryUIActionHandlers.hideQuery(state, action);
    case QueryUIActionTypes.showQuery:
      return QueryUIActionHandlers.showQuery(state, action);
    default:
      return state;
  }
};

export const QueryUIReducer = (
  state = initState,
  action: QueryUIActionsInterface
) => reduce(state, action);
