import {
  HideQueryUIActionI,
  ShowQueryUIActionI
} from "../interfaces/query-ui.actions-interface";
import { QueryUIActionTypes } from '../types/query-ui.action-types';

export const showQuery = (): ShowQueryUIActionI => ({
  type: QueryUIActionTypes.showQuery
});

export const hideQuery = (): HideQueryUIActionI => ({
  type: QueryUIActionTypes.hideQuery
});
