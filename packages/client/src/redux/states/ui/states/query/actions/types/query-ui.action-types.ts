export enum QueryUIActionTypes {
  showQuery = "[QUERY_UI_ACTION]SHOW_QUERY",
  hideQuery = "[QUERY_UI_ACTION]HIDE_QUERY",
}
