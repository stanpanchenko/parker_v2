import { ApplicationSidebarUIActionsI } from "./actions/interfaces";
import { ApplicationSidebarUIActionTypes } from "./actions/types/application-sidebar.action-types";
import { ApplicationSidebarUIActionHandlers } from "./actions/handlers";

export interface ApplicationSidebarUIStateI {
  visible: boolean;
}

const initState: ApplicationSidebarUIStateI = {
  visible: false
};

const reduce = (
  state: ApplicationSidebarUIStateI,
  action: ApplicationSidebarUIActionsI
): ApplicationSidebarUIStateI => {
  switch (action.type) {
    case ApplicationSidebarUIActionTypes.hideAplicationSidebar:
      return ApplicationSidebarUIActionHandlers.hideSidebar(state, action);
    case ApplicationSidebarUIActionTypes.showAplicationSidebar:
      return ApplicationSidebarUIActionHandlers.showSidebar(state, action);
    default:
      return state;
  }
};

export const ApplicationSidebarUiReducer = (
  state = initState,
  action: ApplicationSidebarUIActionsI
) => reduce(state, action);
