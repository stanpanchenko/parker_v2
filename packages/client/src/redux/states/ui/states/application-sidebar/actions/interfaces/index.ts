import {
  HideApplicationSidebarUIActionI,
  ShowApplicationSidebarUIActionI
} from "./application-sidebar.action-interfaces";

export type ApplicationSidebarUIActionsI =
  | ShowApplicationSidebarUIActionI
  | HideApplicationSidebarUIActionI;
