import { ApplicationSidebarUIActionTypes } from '../types/application-sidebar.action-types';

export interface ShowApplicationSidebarUIActionI {
  type: ApplicationSidebarUIActionTypes.showAplicationSidebar
}

export interface HideApplicationSidebarUIActionI {
  type: ApplicationSidebarUIActionTypes.hideAplicationSidebar
}

