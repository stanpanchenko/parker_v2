export enum ApplicationSidebarUIActionTypes {
  showAplicationSidebar = '[APPLICATION_SIDEBAR_UI_ACTION]SHOW_SIDEBAR',
  hideAplicationSidebar = '[APPLICATION_SIDEBAR_UI_ACTION]HIDE_SIDEBAR'
}
