import { Theme, Themes } from '../../../../../shared/providers/theme/Theme';
import { ThemeUIActionsI } from "./actions/interfaces";
import { ThemeUIActionHandlers } from "./actions/handler";
import { ThemeUiActionTypes } from "./actions/type/theme-ui.action-types";

export interface ThemeUIStateI {
  theme: Theme;
}

const initState: ThemeUIStateI = {
  theme: Themes.default
};

const reduce = (
  state: ThemeUIStateI,
  action: ThemeUIActionsI
): ThemeUIStateI => {
  switch (action.type) {
    case ThemeUiActionTypes.changeTheme:
      return ThemeUIActionHandlers.changeTheme(state, action);
    default:
      return state;
  }
};
export const ThemeUIReducer = (state = initState, action: ThemeUIActionsI) =>
  reduce(state, action);
