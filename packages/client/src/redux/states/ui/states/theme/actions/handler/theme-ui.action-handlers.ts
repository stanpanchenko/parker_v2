import { ThemeUIStateI } from "../../theme-ui.reducer";
import { ChangeThemeUIActionI } from "../interfaces/theme-ui.action-interfaces";

export function changeTheme(
  oldState: ThemeUIStateI,
  action: ChangeThemeUIActionI
) {
  return {
    ...oldState,
    theme: action.theme
  };
}
