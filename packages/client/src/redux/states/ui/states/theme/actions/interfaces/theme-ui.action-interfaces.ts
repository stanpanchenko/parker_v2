import { Theme } from '../../../../../../../shared/providers/theme/Theme';
import { ThemeUiActionTypes } from '../type/theme-ui.action-types';

export interface ChangeThemeUIActionI {
  type: ThemeUiActionTypes.changeTheme;
  theme: Theme
}
