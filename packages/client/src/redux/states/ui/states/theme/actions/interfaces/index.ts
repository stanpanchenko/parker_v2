import { ChangeThemeUIActionI } from './theme-ui.action-interfaces';

export type ThemeUIActionsI = ChangeThemeUIActionI;
