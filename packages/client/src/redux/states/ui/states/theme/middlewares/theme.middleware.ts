import { ThemeUiActionTypes } from '../actions/type/theme-ui.action-types';
import { ChangeThemeUIActionI } from '../actions/interfaces/theme-ui.action-interfaces';
import { LocalStorageKeys } from '../../../../../../shared/constants/localstorage-keys';

const saveThemeToLocalStorage = () => next => (action: ChangeThemeUIActionI) => {
  if (action.type === ThemeUiActionTypes.changeTheme){
    localStorage.setItem(LocalStorageKeys.themeKey, action.theme);
  }
  return next(action);
};

export default [saveThemeToLocalStorage]
