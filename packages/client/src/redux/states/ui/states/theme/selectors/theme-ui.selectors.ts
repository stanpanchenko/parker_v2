import { ApplicationStateI } from '../../../../index';
import { createSelector } from 'reselect';

const theme = (state: ApplicationStateI) => state.ui.theme.theme;

export const currentTheme = createSelector([theme], (theme)=> theme);
