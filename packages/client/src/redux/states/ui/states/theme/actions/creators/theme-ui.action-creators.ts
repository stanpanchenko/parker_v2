import { Theme } from '../../../../../../../shared/providers/theme/Theme';
import { ChangeThemeUIActionI } from '../interfaces/theme-ui.action-interfaces';
import { ThemeUiActionTypes } from '../type/theme-ui.action-types';

export const changeTheme = (theme: Theme): ChangeThemeUIActionI => ({
  type: ThemeUiActionTypes.changeTheme,
  theme: theme
});
