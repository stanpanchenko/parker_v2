import { ToastUIStateI } from '../../toast.state-reducer';
import { HideToastActionI, ShowToastActionI } from '../interfaces/toast.action-interfaces';

export function showToast(state: ToastUIStateI, action: ShowToastActionI):ToastUIStateI {
  return {
    ...state,
    visible: true,
    message: action.payload.message
  }
}

export function hideToast(state: ToastUIStateI, action: HideToastActionI):ToastUIStateI {
  return {
    ...state,
    visible: false,
    message: ''
  }
}
