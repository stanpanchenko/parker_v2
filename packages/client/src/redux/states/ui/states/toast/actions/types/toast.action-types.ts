export enum ToastActionTypes {
  showToast = '[ToastAction]SHOW_TOAST',
  hideToast = '[ToastAction]HIDE_TOAST',
}
