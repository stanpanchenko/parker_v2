import { HideToastActionI, ShowToastActionI } from './toast.action-interfaces';

export type ToastActionsI = ShowToastActionI | HideToastActionI
