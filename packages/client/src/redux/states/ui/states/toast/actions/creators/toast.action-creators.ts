import { ToastActionTypes } from '../types/toast.action-types';
import { ToastMessage } from '../../toast.state-reducer';
import { HideToastActionI, ShowToastActionI } from '../interfaces/toast.action-interfaces';

export function showToast(message: ToastMessage): ShowToastActionI {
  return {
    type: ToastActionTypes.showToast,
    payload: {
      message: message
    }
  }
}

export function hideToast(): HideToastActionI {
  return {
    type: ToastActionTypes.hideToast,
  }
}
