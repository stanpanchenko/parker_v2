import { ToastActionsI } from "./actions/interfaces";
import { ToastActionTypes } from "./actions/types/toast.action-types";
import { ToastActionReducers } from "./actions/reducers";

export type ToastMessage = string;
export interface ToastUIStateI {
  visible: boolean;
  message: ToastMessage;
}

const initState: ToastUIStateI = {
  visible: false,
  message: "NO Message!"
};

const reduce = (state: ToastUIStateI, action: ToastActionsI): ToastUIStateI => {
  switch (action.type) {
    case ToastActionTypes.hideToast:
      return ToastActionReducers.hideToast(state, action);
    case ToastActionTypes.showToast:
      return ToastActionReducers.showToast(state, action);
    default:
      return state;
  }
};
export const ToastUIReducer = (state = initState, action: ToastActionsI) =>
  reduce(state, action);
