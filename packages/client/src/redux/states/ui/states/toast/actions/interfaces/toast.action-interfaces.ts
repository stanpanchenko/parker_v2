import { ToastActionTypes } from '../types/toast.action-types';
import { ToastMessage } from '../../toast.state-reducer';

export interface ShowToastActionI {
  type: ToastActionTypes.showToast;
  payload: {
    message: ToastMessage
  }
}
export interface HideToastActionI {
  type: ToastActionTypes.hideToast;
}
