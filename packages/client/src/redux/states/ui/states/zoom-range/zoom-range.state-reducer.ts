export type MinZoom = number;
export type MaxZoom = number;
export type ZoomRange = [MinZoom, MaxZoom];

export interface ZoomRangeStateI {
  streetSpaceTooltipZoomRange: ZoomRange;
  streetSpaceZoomRange: ZoomRange;
  singleSpaceZoomRange: ZoomRange;
}
const initState: ZoomRangeStateI = {
  streetSpaceTooltipZoomRange: [16, 30],
  singleSpaceZoomRange: [18, 30],
  streetSpaceZoomRange: [14, 17]
};
function reduce(state: ZoomRangeStateI, action): ZoomRangeStateI {
  return state;
}
export const ZoomRangeStateReducer = (
  state = initState,
  action
): ZoomRangeStateI => {
  return reduce(state, action);
};
