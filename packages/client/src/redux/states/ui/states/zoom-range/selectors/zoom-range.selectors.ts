import { createSelector } from "reselect";
import { ApplicationStateI } from "../../../../index";

export const streetSpaceTooltipZoomRange = createSelector(
  [(state: ApplicationStateI) => state.ui.zoomRange.streetSpaceTooltipZoomRange],
  range => range
);
export const streetSpaceZoomRange = createSelector(
  [(state: ApplicationStateI) => state.ui.zoomRange.streetSpaceZoomRange],
  range => range
);
export const singleSpaceZoomRange = createSelector(
  [(state: ApplicationStateI) => state.ui.zoomRange.singleSpaceZoomRange],
  range => range
);
