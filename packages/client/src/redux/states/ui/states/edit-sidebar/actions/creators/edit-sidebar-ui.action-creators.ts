import { EditSidebarUIActionTypes } from '../types/edit-sidebar-ui.action-types';
import { HideEditSidebarUIActionI, ShowEditSidebarUIActionI } from '../interfaces/edit-sidebar.action-interfaces';

export const showEditSidebar = (): ShowEditSidebarUIActionI => ({
  type: EditSidebarUIActionTypes.showEditSidebarUI
});

export const hideEditSidebar = (): HideEditSidebarUIActionI => ({
  type: EditSidebarUIActionTypes.hideEditSidebarUI
});
