import { EditSidebarUIActionsI } from "./actions/interfaces";

export interface EditSidebarUIStateI {
  visible: boolean;
}
const initState: EditSidebarUIStateI = {
  visible: false
};

const reduce = (
  state: EditSidebarUIStateI,
  action: EditSidebarUIActionsI
): EditSidebarUIStateI => {

  return state;
};

export const EditSidebarUIReducer = (
  state = initState,
  action: EditSidebarUIActionsI
) => reduce(state, action);
