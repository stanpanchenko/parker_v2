import { EditSidebarUIActionTypes } from '../types/edit-sidebar-ui.action-types';


export interface ShowEditSidebarUIActionI {
  type: EditSidebarUIActionTypes.showEditSidebarUI;
}

export interface HideEditSidebarUIActionI {
  type: EditSidebarUIActionTypes.hideEditSidebarUI;
}
