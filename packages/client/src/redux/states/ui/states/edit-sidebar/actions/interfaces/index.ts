import {
  HideEditSidebarUIActionI,
  ShowEditSidebarUIActionI
} from "./edit-sidebar.action-interfaces";

export type EditSidebarUIActionsI =
  | ShowEditSidebarUIActionI
  | HideEditSidebarUIActionI;
