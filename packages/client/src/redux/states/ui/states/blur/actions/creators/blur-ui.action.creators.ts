import { BlurMapUIActionI, UnblurMapUIActionI } from '../interfaces/blur-ui.action-interfaces';
import { BlurUIActionTypes } from '../types/blur-ui.action-types';


export function blurMap(): BlurMapUIActionI {
  return {
    type: BlurUIActionTypes.blurMap
  }
}

export function unBlurMap(): UnblurMapUIActionI {
  return {
    type: BlurUIActionTypes.unBlurMap
  }
}
