import { BlurMapUIActionI, UnblurMapUIActionI } from './blur-ui.action-interfaces';

export type BlurUIActionsI = BlurMapUIActionI | UnblurMapUIActionI;
