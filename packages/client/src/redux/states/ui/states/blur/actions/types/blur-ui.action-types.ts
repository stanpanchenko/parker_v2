export enum BlurUIActionTypes {
  blurMap = '[BLUR_UI_ACTION]BLUR_MAP',
  unBlurMap = '[BLUR_UI_ACTION]UN_BLUR_MAP'
}
