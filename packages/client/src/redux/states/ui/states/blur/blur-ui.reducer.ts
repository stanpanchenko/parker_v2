import { BlurUIActionsI } from "./actions/interfaces";

export interface BlurUIStateI {
  map: boolean;
}

const initState: BlurUIStateI = {
  map: false
};

const reduce = (state: BlurUIStateI, action: BlurUIActionsI): BlurUIStateI => {
  return state;
};

export const BlurUIReducer = (
  state = initState,
  action: BlurUIActionsI
): BlurUIStateI => reduce(state, action);
