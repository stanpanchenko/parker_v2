import { BlurUIStateI } from '../../blur-ui.reducer';
import { BlurMapUIActionI, UnblurMapUIActionI } from '../interfaces/blur-ui.action-interfaces';

export function blurMap(state: BlurUIStateI,action: BlurMapUIActionI): BlurUIStateI {
  return {
    ...state,
    map: true
  }
}

export function unBlurMap(state: BlurUIStateI, action: UnblurMapUIActionI): BlurUIStateI {
  return {
    ...state,
    map: false
  }
}
