import { Action } from 'redux';
import { BlurUIActionTypes } from '../types/blur-ui.action-types';


export interface BlurMapUIActionI extends Action {
  type: BlurUIActionTypes.blurMap
}
export interface UnblurMapUIActionI {
  type: BlurUIActionTypes.unBlurMap
}
