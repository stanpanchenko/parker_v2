import { combineReducers } from "redux";
import { ThemeUIReducer, ThemeUIStateI } from "./states/theme/theme-ui.reducer";
import { QueryUIReducer, QueryUIStateI } from "./states/query/query-ui.reducer";
import { BlurUIReducer, BlurUIStateI } from "./states/blur/blur-ui.reducer";
import {
  EditSidebarUIReducer,
  EditSidebarUIStateI
} from "./states/edit-sidebar/edit-sidebar.reducer";
import { ApplicationSidebarUIStateI } from "./states/application-sidebar/application-sidebar-ui.reducer";
import { ApplicationSidebarReducer } from "../../../components/application-sidebar/redux/application-sidebar-reducer/application-sidebar.reducer";
import {
  PopupDialogReducer,
  PopupDialogStateI
} from "./states/popup-dialog/popup-dialog.reducer";
import { ToastUIReducer, ToastUIStateI } from './states/toast/toast.state-reducer';
import { ZoomRangeStateI, ZoomRangeStateReducer } from './states/zoom-range/zoom-range.state-reducer';



export interface UIStateI {
  popupDialog: PopupDialogStateI;
  theme: ThemeUIStateI;
  toast: ToastUIStateI;
  query: QueryUIStateI;
  blur: BlurUIStateI;
  zoomRange: ZoomRangeStateI,
  editSidebar: EditSidebarUIStateI;
  applicationSidebar: ApplicationSidebarUIStateI;
}

export const UIReducer = combineReducers({
  popupDialog: PopupDialogReducer,
  theme: ThemeUIReducer,
  toast: ToastUIReducer,
  query: QueryUIReducer,
  editSidebar: EditSidebarUIReducer,
  zoomRange: ZoomRangeStateReducer,
  blur: BlurUIReducer,
  applicationSidebar: ApplicationSidebarReducer
});
