import { MapActionTypes } from "../actions/types/map.action-types";
import { NominatimActionCreators } from "../../../middlewares/vendors/nominatim/actions/creators";
import { ApplicationStateI } from "../../index";
import { MapClickEvent } from "../../../../pages/public/map/components/parker-map/components/deck-map/deck-gl";
import { FeatureCollectionActionCreators } from "../../feature-collection/actions/creators";

const mapClickMiddlewares = ({ getState, dispatch }) => next => action => {
  if (action.type !== MapActionTypes.mapClick) return next(action);
  const payload: MapClickEvent = action.payload;
  const state: ApplicationStateI = getState();

  // hook click actions here
  if (state.applicationMode.inAddingMode) {
    if (payload.layer && payload.object) {
      const id = payload.object.id;
      dispatch(FeatureCollectionActionCreators.selectFeature(id));
    }
    if (state.applicationMode.fetchAddressOnNextMapClick) {
      const lat = payload.lngLat[1];
      const lng = payload.lngLat[0];
      dispatch(
        NominatimActionCreators.fetchAddressByCoordinatesActionCreator({
          lng,
          lat
        })
      );
    }
  }

  return next(action);
};

export const MapMiddlewares = [mapClickMiddlewares];
