import { createSelector, Selector } from "reselect";
import { ApplicationStateI } from "../../index";
import { ILngLat } from "../../../../shared/Interfaces/LatLng.interface";

const getCenterCoords = state => state.map.center;
export const mapCenterSelector = createSelector(
  [getCenterCoords],
  coords => {
    return { ...coords };
  }
);

const getSpaceAreas = (state: ApplicationStateI): ILngLat[] => {
  return [];
};
const spaceAreas: Selector<ApplicationStateI, ILngLat[]> = createSelector(
  [getSpaceAreas],
  spaceAreas => {
    return spaceAreas;
  }
);

export const MapSelectors = {
  spaceAreas: (state: ApplicationStateI) => spaceAreas(state)
};
