import { MapMovedActionI } from './map.action-interfaces';

export type MapActionsI = MapMovedActionI;
