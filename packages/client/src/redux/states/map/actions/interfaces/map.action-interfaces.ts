import { MapActionTypes } from '../types/map.action-types';
import { MapBoundsRectI } from '../../../../../shared/Interfaces/MapBoundsRect.interface';
import { PayloadAction } from '../../../custom-interfaces';

export interface MapMovedActionI extends PayloadAction{
  type: MapActionTypes.mapMoved;
  payload: {
    mapBoundsRect: MapBoundsRectI
  }
}
