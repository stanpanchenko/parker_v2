export enum MapActionTypes {
  mapMoved = '[MAP_ACTION]MAP_MOVED',
  mapClick = '[MAP_ACTION]MAP_CLICK'
}
