import { MapBoundsRectI } from "../../../../../shared/Interfaces/MapBoundsRect.interface";
import { MapActionTypes } from "../types/map.action-types";
import { MapMovedActionI } from "../interfaces/map.action-interfaces";
import { ILngLat } from "../../../../../shared/Interfaces/LatLng.interface";
import {MapClickEvent} from "../../../../../pages/public/map/components/parker-map/components/deck-map/deck-gl";

export function mapMoved(mapBoundsRect: MapBoundsRectI): MapMovedActionI {
  return {
    type: MapActionTypes.mapMoved,
    payload: {
      mapBoundsRect: { ...mapBoundsRect }
    }
  };
}
export function mapClick(e: MapClickEvent) {
  return {
    type: MapActionTypes.mapClick,
    payload: e
  };
}
