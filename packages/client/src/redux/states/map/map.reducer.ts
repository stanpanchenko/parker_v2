import { combineReducers } from "redux";
import {
  IViewport,
  ViewportStateReducer
} from "./states/viewport/viewport.state-reducer";
import {
  PositionMarkerReducer,
  PositionMarkerStateI
} from "./states/position-marker/position-marker.reducer";
import { ITilesState, TilesReducer } from "./states/tiles/tiles-reducer";
import {
  IPointerCoordinatesState,
  PointerCoordinatesReducer
} from "./states/pointer-coordinates/pointer-coordinates.reducer";

export interface MapStateI {
  viewport: IViewport;
  positionMarker: PositionMarkerStateI;
  tiles: ITilesState;
  pointerCoordinates: IPointerCoordinatesState;
}

const stateReducers = {
  viewport: ViewportStateReducer,
  positionMarker: PositionMarkerReducer,
  tiles: TilesReducer,
  pointerCoordinates: PointerCoordinatesReducer
};

export const MapReducer = combineReducers(stateReducers);
