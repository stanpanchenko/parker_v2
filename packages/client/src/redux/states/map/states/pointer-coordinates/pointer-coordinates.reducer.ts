import { PointerActionsInterfaces } from "./actions/interfaces/pointer-actions.interfaces";
import { PointerCoordinatesActionTypes } from "./actions/pointer-coordinates.action-types";

export interface IPointerCoordinatesState {
  coordinate: number[];
  pixel: number[];
}
const initState: IPointerCoordinatesState = {
  pixel: [],
  coordinate: []
};

export const PointerCoordinatesReducer = (
  state = initState,
  action: PointerActionsInterfaces
): IPointerCoordinatesState => {
  switch (action.type) {
    case PointerCoordinatesActionTypes.setPointerCoordinates:
      return {
        ...state,
        coordinate: [...action.payload.coordinate],
        pixel: [...action.payload.pixel]
      };
    default:
      return state;
  }
};
