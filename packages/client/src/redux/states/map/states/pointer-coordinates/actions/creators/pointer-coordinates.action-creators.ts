import {PointerCoordinatesActionTypes} from "../pointer-coordinates.action-types";
import {ISetPointerCoordinatesAction} from "../interfaces/pointer-actions.interfaces";



export const setPointerCoordinates = ({
  coordinate,
  pixel
}): ISetPointerCoordinatesAction => ({
  type: PointerCoordinatesActionTypes.setPointerCoordinates,
  payload: {
    coordinate,
    pixel
  },
});
