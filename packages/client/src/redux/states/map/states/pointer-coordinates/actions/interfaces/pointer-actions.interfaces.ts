import {PointerCoordinatesActionTypes} from "../pointer-coordinates.action-types";

export interface ISetPointerCoordinatesAction {
  type: PointerCoordinatesActionTypes.setPointerCoordinates,
  payload:{
    coordinate:number[],
    pixel: number[]
  }
}

export type PointerActionsInterfaces = ISetPointerCoordinatesAction;
