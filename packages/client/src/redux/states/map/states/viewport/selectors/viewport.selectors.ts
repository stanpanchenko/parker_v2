import { ApplicationStateI } from "../../../../index";
import { createSelector, Selector } from "reselect";
import { IViewport } from "../viewport.state-reducer";

const zoom = (state: ApplicationStateI) => state.map.viewport.zoom;
const viewport = (state: ApplicationStateI) => state.map.viewport;

export const viewportZoom: Selector<ApplicationStateI, number> = createSelector(
  [zoom],
  (zoom): number => zoom
);
export const viewportSelector: Selector<
  ApplicationStateI,
  IViewport
> = createSelector(
  [viewport],
  viewport => viewport
);
