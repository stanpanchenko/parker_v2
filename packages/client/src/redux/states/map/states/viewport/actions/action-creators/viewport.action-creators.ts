import { ViewportActionType } from "../viewport.action-type";
import {
  ISetMapViewAction,
  SetViewportZoomActionI
} from "../interfaces/viewport.action-interfaces";
import { IViewport } from "../../viewport.state-reducer";

export function setZoom(zoom: number): SetViewportZoomActionI {
  return {
    type: ViewportActionType.setZoom,
    payload: {
      zoom
    }
  };
}
export const setView = (view: IViewport): ISetMapViewAction => {
  return {
    type: ViewportActionType.setMapView,
    payload: view
  };
};
