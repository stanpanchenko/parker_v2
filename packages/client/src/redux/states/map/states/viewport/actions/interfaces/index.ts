import {
  ISetMapViewAction,
  SetViewportZoomActionI,
  ViewportCenterActionI
} from "./viewport.action-interfaces";

export type ViewportActionsI =
  | ViewportCenterActionI
  | SetViewportZoomActionI
  | ISetMapViewAction;
