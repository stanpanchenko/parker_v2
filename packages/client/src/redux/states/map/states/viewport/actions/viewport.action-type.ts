export enum ViewportActionType {
  centerViewport = '[ViewportActionType]CENTER_VIEWPORT',
  setZoom = '[ViewportActionType]SET_ZOOM',
  setMapView= '[ViewportActionType]SET_MAP_VIEW'
}
