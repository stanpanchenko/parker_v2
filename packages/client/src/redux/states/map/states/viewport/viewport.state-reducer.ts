import { Reducer } from "redux";
import { ViewportActionHandlers } from "./actions/handlers";
import { ViewportActionType } from "./actions/viewport.action-type";
import { ViewportActionsI } from "./actions/interfaces";
import { SpecialPlaces } from "./special-places";

export type ViewportCenter = [number, number];

export const INITIAL_VIEW_STATE = {
  longitude: SpecialPlaces.AACHEN_MARKT[0],
  latitude: SpecialPlaces.AACHEN_MARKT[1],
  zoom: 13,
  pitch: 2,
  bearing: 0
};

export interface IViewport {
  longitude: number;
  latitude: number;
  zoom: number;
  pitch: number;
  bearing: number;
}

export interface ViewportStateI extends IViewport {}

const initState: ViewportStateI = {
  longitude: SpecialPlaces.AACHEN_MARKT[0],
  latitude: SpecialPlaces.AACHEN_MARKT[1],
  zoom: 13,
  pitch: 2,
  bearing: 0
};

function reduce(
  state: ViewportStateI,
  action: ViewportActionsI
): ViewportStateI {
  switch (action.type) {
    case ViewportActionType.setZoom:
      return ViewportActionHandlers.setViewportZoom(state, action);
    case ViewportActionType.setMapView:
      return ViewportActionHandlers.setMapView(state, action);
    default:
      return state;
  }
}

export const ViewportStateReducer: Reducer<ViewportStateI, ViewportActionsI> = (
  state = initState,
  action: ViewportActionsI
) => reduce(state, action);
