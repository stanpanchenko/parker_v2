import { IViewport, ViewportStateI } from "../../viewport.state-reducer";
import {
  ISetMapViewAction,
  SetViewportZoomActionI
} from "../interfaces/viewport.action-interfaces";

export const setMapView = (
  state: ViewportStateI,
  action: ISetMapViewAction
): ViewportStateI => {
  return {
    ...state,
    ...action.payload
  };
};

export function setViewportZoom(
  state: ViewportStateI,
  action: SetViewportZoomActionI
): ViewportStateI {
  return {
    ...state,
    zoom: action.payload.zoom
  };
}
