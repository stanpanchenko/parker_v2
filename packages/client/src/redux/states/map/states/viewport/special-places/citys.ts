import { ViewportCenter } from "../viewport.state-reducer";

export const AACHEN: ViewportCenter = [50.776351, 6.083862];
export const AACHEN_MARKT: ViewportCenter = [
  6.0839428938925275,
  50.776549109463815
];
export const AACHEN_ADALBERTSTEINWEG: ViewportCenter = [
  6.109866328770297,
  50.77345038485719
];
export const UNI_KLINIK: ViewportCenter = [
  6.0408696345984945,
  50.77532375128836
];
