import { ViewportActionType } from '../viewport.action-type';
import {IViewport} from "../../viewport.state-reducer";

export interface ViewportCenterActionI {
  type: ViewportActionType.centerViewport;
  center: [number, number];
  zoom: number;
}
export interface SetViewportZoomActionI {
  type: ViewportActionType.setZoom;
  payload: {
    zoom: number
  }

}
export interface ISetMapViewAction {
  type: ViewportActionType.setMapView;
  payload: IViewport
}
