import { ApplicationStateI } from "../../../../index";
import { createSelector, Selector } from "reselect";
import { TileName, TilesContainer } from "../tiles-reducer";

const currTileName = (state: ApplicationStateI) => state.map.tiles.currTile;
const availableTiles = (state: ApplicationStateI) => state.map.tiles.tiles;

export const selectedTileSelector: Selector<
  ApplicationStateI,
  string | object
> = createSelector(
  [currTileName, availableTiles],
  (selectedTileName: TileName, tiles: TilesContainer) => {
    if (tiles[selectedTileName]) return tiles[selectedTileName];

    // return first tile url/object available in tiles
    return tiles[Object.keys(tiles)[0]];
  }
);
