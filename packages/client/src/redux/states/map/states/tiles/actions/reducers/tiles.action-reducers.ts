import { ITilesState } from "../../tiles-reducer";
import { ISelectTilesAction } from "../interface/tiles.action-interfaces";

export const selectTile = (
  state: ITilesState,
  action: ISelectTilesAction
): ITilesState => {
  const { name } = action.payload;
  const { currTile } = state;
  if (name === currTile) return state;
  return {
    ...state,
    currTile: name
  };
};
