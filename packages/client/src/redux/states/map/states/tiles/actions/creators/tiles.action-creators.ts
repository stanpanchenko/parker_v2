import {TileName} from "../../tiles-reducer";
import {TilesActionTypes} from "../tiles.action-types";
import {ISelectTilesAction} from "../interface/tiles.action-interfaces";

export const selectTileName = (name: TileName): ISelectTilesAction => ({
	type: TilesActionTypes.selectTile,
	payload:{
		name
	}
});
