import {TilesActionTypes} from "../tiles.action-types";
import {TileName} from "../../tiles-reducer";

export interface ISelectTilesAction {
	type: TilesActionTypes.selectTile,
	payload: {
		name: TileName
	}
}

export type TilesActionsInterface = ISelectTilesAction
