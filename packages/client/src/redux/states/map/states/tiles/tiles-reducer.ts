import { TilesActionsInterface } from "./actions/interface/tiles.action-interfaces";
import { TilesActionTypes } from "./actions/tiles.action-types";
import { TilesActionReducers } from "./actions/reducers";

export enum TileName {
  satellite = "satellite",
  grey = "grey",
  dark = "dark",
  human = "human",
  humanV2 = "humanV2"
}
export interface TilesContainer {
  grey: string;
  dark: string;
  human: string;
  humanV2: string;
  satellite: object;
}
export interface ITilesState {
  currTile: TileName;
  tiles: TilesContainer;
}

const initState: ITilesState = {
  currTile: TileName.grey,
  // TODO: consider fetching styles depending on user privileges
  tiles: {
    grey: "mapbox://styles/stanis1992/ck0l708861x8m1cpqhi1l0p4p",
    dark: "mapbox://styles/stanis1992/ck10z08y407qf1cmlwfh9h2sz",
    human: "mapbox://styles/stanis1992/ck1tm9i810cdy1cqkkt0x0iwg",
    humanV2: "mapbox://styles/stanis1992/ck2bncm0y0xgo1cnvcjkaariv",
    satellite: {
      version: 8,
      sources: {
        "raster-tiles": {
          type: "raster",
          tiles: [
            "https://clarity.maptiles.arcgis.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}}?blankTile=false"
          ],
          tileSize: 256,
          attribution:
            'Map tiles by <a target="_top" rel="noopener" href="https://clarity.maptiles.arcgis.com">OpenStreetMap</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a target="_top" rel="noopener" href="http://openstreetmap.org">OpenStreetMap</a>, under <a target="_top" rel="noopener" href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>'
        }
      },
      layers: [
        {
          id: "simple-tiles",
          type: "raster",
          source: "raster-tiles",
          minzoom: 0,
          maxzoom: 22
        }
      ]
    }
  }
};

const reduce = (
  state: ITilesState,
  action: TilesActionsInterface
): ITilesState => {
  switch (action.type) {
    case TilesActionTypes.selectTile:
      return TilesActionReducers.selectTile(state, action);
    default:
      return state;
  }
};

export const TilesReducer = (
  state = initState,
  action: TilesActionsInterface
): ITilesState => {
  return reduce(state, action);
};
