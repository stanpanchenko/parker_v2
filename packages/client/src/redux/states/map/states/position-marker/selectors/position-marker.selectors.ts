import { createSelector } from "reselect";
import { ApplicationStateI } from "../../../../index";
import {ILngLat} from "../../../../../../shared/Interfaces/LatLng.interface";


export const position = createSelector(
  [(state: ApplicationStateI) => state.map.positionMarker.position],
  (pos): ILngLat => pos
);
export const radius = createSelector([(state: ApplicationStateI)=>state.map.positionMarker.radius], (rad): number =>rad);
