import {
  SetPositionMarkerPositionActionI,
  SetPositionMarkerRadiusActionI,
  TooglePositionMarkerRadius
} from "./position-marker.action-interfaces";

export type PositionMarkerActionsI =
  | SetPositionMarkerPositionActionI
  | SetPositionMarkerRadiusActionI
  | TooglePositionMarkerRadius;
