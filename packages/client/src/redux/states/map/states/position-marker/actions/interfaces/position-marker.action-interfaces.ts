import { PositionMarkerActionTypes } from '../types/position-marker.action-types';
import { PayloadAction } from '../../../../../custom-interfaces';
import {ILngLat} from "../../../../../../../shared/Interfaces/LatLng.interface";

export interface SetPositionMarkerPositionActionI extends PayloadAction{
  type: PositionMarkerActionTypes.setPosition;
  payload: {
    position: ILngLat
  }
}

export interface SetPositionMarkerRadiusActionI extends PayloadAction{
  type: PositionMarkerActionTypes.setRadius,
  payload: {
    radius: number
  }
}
export interface TooglePositionMarkerRadius extends PayloadAction{
  type: PositionMarkerActionTypes.toggleRadius;
  payload: {
    value: boolean
  }
}
