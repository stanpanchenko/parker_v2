import {
  SetPositionMarkerPositionActionI,
  SetPositionMarkerRadiusActionI,
  TooglePositionMarkerRadius
} from "../interfaces/position-marker.action-interfaces";
import { PositionMarkerActionTypes } from "../types/position-marker.action-types";
import {ILngLat} from "../../../../../../../shared/Interfaces/LatLng.interface";

export function setPosition(
  position: ILngLat
): SetPositionMarkerPositionActionI {
  return {
    type: PositionMarkerActionTypes.setPosition,
    payload: {
      position
    }
  };
}

export function setRadius(radius: number): SetPositionMarkerRadiusActionI {
  return {
    type: PositionMarkerActionTypes.setRadius,
    payload: {
      radius
    }
  };
}
export function toggleRadius(value: boolean): TooglePositionMarkerRadius {
  return {
    type: PositionMarkerActionTypes.toggleRadius,
    payload: {
      value
    }
  };
}
