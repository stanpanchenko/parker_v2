import { PositionMarkerStateI } from "../../position-marker.reducer";
import {
  SetPositionMarkerPositionActionI,
  SetPositionMarkerRadiusActionI,
  TooglePositionMarkerRadius
} from "../interfaces/position-marker.action-interfaces";

export function setPosition(
  state: PositionMarkerStateI,
  action: SetPositionMarkerPositionActionI
): PositionMarkerStateI {
  return {
    ...state,
    position: action.payload.position
  };
}
export function setRadius(
  state: PositionMarkerStateI,
  action: SetPositionMarkerRadiusActionI
): PositionMarkerStateI {
  return {
    ...state,
    radius: action.payload.radius
  };
}

export function toggleRadius(
  state: PositionMarkerStateI,
  action: TooglePositionMarkerRadius
): PositionMarkerStateI {
  return {
    ...state,
    visible: action.payload.value
  };
}
