export enum PositionMarkerActionTypes {
  setPosition = '[POSITION_MARKER_ACTION]SET_POSITION',
  setRadius = '[POSITION_MARKER_ACTION]SET_RADIUS',
  toggleRadius ='[POSITION_MARKER_ACTION]TOGGLE_RADIUS'
}
