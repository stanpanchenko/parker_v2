import { Reducer } from "redux";
import { PositionMarkerActionsI } from "./actions/interfaces";
import { PositionMarkerActionHandlers } from "./actions/handlers";
import { PositionMarkerActionTypes } from "./actions/types/position-marker.action-types";
import { SpecialPlaces } from "../viewport/special-places";
import {ILngLat} from "../../../../../shared/Interfaces/LatLng.interface";

export const defaultPositionMarkerRange = 50;

export interface PositionMarkerStateI {
  position: ILngLat;
  radius: number;
  visible: boolean;
}
const initState: PositionMarkerStateI = {
  position: {
    id: "some",
    lat: SpecialPlaces.AACHEN_MARKT[0],
    lng: SpecialPlaces.AACHEN_MARKT[1]
  },
  visible: false,
  radius: defaultPositionMarkerRange
};

const reduce = (
  state: PositionMarkerStateI,
  action: PositionMarkerActionsI
) => {
  switch (action.type) {
    case PositionMarkerActionTypes.toggleRadius:
      return PositionMarkerActionHandlers.toggleRadius(state, action);
    case PositionMarkerActionTypes.setRadius:
      return PositionMarkerActionHandlers.setRadius(state, action);
    case PositionMarkerActionTypes.setPosition:
      return PositionMarkerActionHandlers.setPosition(state, action);
    default:
      return state;
  }
};

export const PositionMarkerReducer: Reducer<
  PositionMarkerStateI,
  PositionMarkerActionsI
> = (state = initState, action: PositionMarkerActionsI): PositionMarkerStateI =>
  reduce(state, action);
