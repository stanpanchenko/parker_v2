import { ErrorI } from "./errors.reducer";
import { ErrorsActionTypes } from "./errors.action-types";

export interface SetErrorsActionI {
  type: ErrorsActionTypes.setError;
  error: ErrorI;
}

function setError(error: ErrorI): SetErrorsActionI {
  return {
    type: ErrorsActionTypes.setError,
    error
  };
}

export const ErrorsActionCreators = {
  setError
};
