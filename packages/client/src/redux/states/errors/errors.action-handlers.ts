import { ErrorsStateI } from "./errors.reducer";
import { SetErrorsActionI } from "./errors.action-creators";

function setError(
  oldState: ErrorsStateI,
  action: SetErrorsActionI
): ErrorsStateI {
  const { error } = action;

  const newState = {
    ...oldState,
    error: error
  };
  return newState;
}

export const ErrorsActionHandlers = {
  setError
};
