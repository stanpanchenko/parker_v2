import { Reducer } from "redux";
import { ErrorsActionTypes } from './errors.action-types';
import { ErrorsActionHandlers } from './errors.action-handlers';


export interface ErrorI {
  statusCode: number;
  message: string;
}

export interface ErrorsStateI {
  occurred: boolean;
  message: string;
  error: ErrorI | null;
}

const initialState: ErrorsStateI = {
  occurred: false,
  message: "",
  error: null
}

export type ErrorActionsI = any;

function reduce(state: ErrorsStateI, action: ErrorActionsI): ErrorsStateI {
  switch (action.type) {
    case ErrorsActionTypes.setError:
      return ErrorsActionHandlers.setError(state,action);
    default:
      return state;
  }
}
export const ErrorsReducer: Reducer<ErrorsStateI, ErrorActionsI> = (
  state = initialState,
  actions: ErrorActionsI
): ErrorsStateI => {
  return reduce(state, actions);
};
