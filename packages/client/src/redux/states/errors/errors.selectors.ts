import { ApplicationStateI } from '../index';
import { ErrorI } from './errors.reducer';
import { createSelector } from 'reselect';

const error = (state: ApplicationStateI):ErrorI => state.errors.error;

const errorSelector = createSelector([error], (error) => error);

export const ErrorsSelectors = {
  errorSelector
};
