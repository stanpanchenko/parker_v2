import Api from "../../../../api/graphql";
import { SearchActionI } from "../actions/interface/query.action-interfaces";
import { QueryActionTypes } from "../actions/types/query.action-types";
import { QueryActionCreators } from "../actions/creators";
import { QueryResultI } from "../query.state-reducer";
import { newID } from "../../../../pages/public/map/components/parker-map/components/defaultSpaceArea";

const search = ({ getState, dispatch }) => next => (action: SearchActionI) => {
  if (action.type !== QueryActionTypes.search) return next(action);
  const search = action.payload.searchString;
  dispatch(QueryActionCreators.setFetching(true));
  // TODO: create debounce function
  Api.search(search).then(res => {
    console.log(res);
    const results: QueryResultI[] = res.map(
      (result): QueryResultI => {
        return {
          id: newID(),
          address: result.address,
          class: result.class,
          lat: result.lat,
          lng: result.lon,
          display_name: result.display_name,
          boundingbox: result.boundingbox
        };
      }
    );
    dispatch(QueryActionCreators.setQueryResults(results));
  });

  return next(action);
};

export default [search];
