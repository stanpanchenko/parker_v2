import { QueryStateI } from "../../query.state-reducer";
import { SetFetchingActionI, SetQueryResultsActionI } from '../interface/query.action-interfaces';

export function setQueryResults(
  state: QueryStateI,
  action: SetQueryResultsActionI
): QueryStateI {
  return {
    ...state,
    results: [...action.payload.results],
    fetching: false,
  };
}

export function setFetching(state: QueryStateI, action: SetFetchingActionI): QueryStateI {
  return {
    ...state,
    fetching: action.payload.isFetching,
  }
}
