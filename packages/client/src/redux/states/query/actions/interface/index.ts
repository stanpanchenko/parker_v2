import { SetFetchingActionI, SetQueryResultsActionI } from './query.action-interfaces';

export type QueryActionsI = SetQueryResultsActionI | SetFetchingActionI;
