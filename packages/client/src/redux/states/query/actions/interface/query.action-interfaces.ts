import { QueryActionTypes } from '../types/query.action-types';
import { QueryResultI } from '../../query.state-reducer';
import { PayloadAction } from '../../../custom-interfaces';

export interface SetQueryResultsActionI {
  type: QueryActionTypes.setQueryResultsAction;
  payload: {
    results: QueryResultI[]
  }
}

export interface SearchActionI {
  type: QueryActionTypes.search;
  payload: {
    searchString: string
  }
}

export interface SetFetchingActionI extends PayloadAction{
  type: QueryActionTypes.setFetching;
  payload:{
    isFetching: boolean
  }
}
