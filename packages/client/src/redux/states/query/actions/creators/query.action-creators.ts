import { QueryResultI } from '../../query.state-reducer';
import { QueryActionTypes } from '../types/query.action-types';
import { SearchActionI, SetFetchingActionI, SetQueryResultsActionI } from '../interface/query.action-interfaces';

export function setQueryResults(results: QueryResultI[]): SetQueryResultsActionI {
  return {
    type: QueryActionTypes.setQueryResultsAction,
    payload: {
      results: [...results]
    }
  }
}

export function search(searchString: string): SearchActionI {
  return {
    type: QueryActionTypes.search,
    payload:{
      searchString
    }
  }
}

export function setFetching(isFetching: boolean): SetFetchingActionI {
  return {
    type: QueryActionTypes.setFetching,
    payload: {
      isFetching
    }
  }
}
