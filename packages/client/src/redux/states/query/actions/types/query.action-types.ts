export enum QueryActionTypes {
  search = "[QUERY_ACTIONS]SEARCH",
  setFetching = '[QUERY_ACTIONS]SET_FETCHING',
  setQueryResultsAction = "[QUERY_ACTIONS]SET_QUERY_RESULTS"
}
