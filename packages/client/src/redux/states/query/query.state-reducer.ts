import { Address, SearchResult } from "../../../api/graphql/graphql-api.interface";
import { Reducer } from "redux";
import { QueryActionsI } from "./actions/interface";
import { QueryActionTypes } from "./actions/types/query.action-types";
import { QueryActionReducers } from "./actions/reducers";

export interface QueryResultI {
  id: string; // will be a temporaly assigned id, is used for reasult-list animation
  address: Address;
  lat: number;
  lng: number;
  boundingbox: number[];
  class: string;
  display_name: string
}

export interface QueryStateI {
  fetching: boolean;
  results: QueryResultI[];
}
const initState: QueryStateI = {
  fetching: false,
  results: []
};

function reduce(state: QueryStateI, action: QueryActionsI): QueryStateI {
  switch (action.type) {
    case QueryActionTypes.setFetching:
      return QueryActionReducers.setFetching(state,action);
    case QueryActionTypes.setQueryResultsAction:
      return QueryActionReducers.setQueryResults(state, action);
    default:
      return state;
  }
}

export const QueryStateReducer: Reducer<QueryStateI, QueryActionsI> = (
  state = initState,
  action
): QueryStateI => reduce(state, action);
