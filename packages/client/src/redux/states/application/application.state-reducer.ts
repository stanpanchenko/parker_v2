import { Reducer } from "redux";
import { ApplicationActionsI } from "./actions/interfaces";
import { ApplicationModeActionTypes } from "./actions/types/application.action-types";
import { ApplicationActionReducers } from "./action-reducers";

export enum EditModeType {
  duplicate = "duplicate",
  drawPoint = "drawPoint",
  drawLineString = "drawLineString",
  drawPolygon = "drawPolygon",
  drawRectangle = "drawRectangle",
  draw90DegreePolygon = "draw90DegreePolygon",
  drawRectangleUsing3Points = "drawRectangleUsing3Points",
  translate = "translate",
  rotate = "rotate",
  scale = "scale",
  extrude = "extrude",
  modify = "modify",
  view = "view"
}

export interface ApplicationModeStateI {
  inAddingMode: boolean;
  editModeType: EditModeType;
  fetchAddressOnNextMapClick: boolean;
  spaceCoordinateStep: number;
}

const initState: ApplicationModeStateI = {
  inAddingMode: false,
  editModeType: EditModeType.view,
  fetchAddressOnNextMapClick: false,
  spaceCoordinateStep: 0.000008
};

export const ApplicationModeReducer: Reducer<
  ApplicationModeStateI,
  ApplicationActionsI
> = (state = initState, action) => {
  switch (action.type) {
    case ApplicationModeActionTypes.fetchAddressOnNextMapClick:
      return ApplicationActionReducers.setFetchAddressOnNextClick(
        state,
        action
      );

    case ApplicationModeActionTypes.enableEditMode:
      return ApplicationActionReducers.enableAddingMode(state);

    case ApplicationModeActionTypes.disableEditMode:
      return ApplicationActionReducers.disableAddingMode(state);

    case ApplicationModeActionTypes.confirmAddingMode:
      return ApplicationActionReducers.disableAddingMode(state);

    case ApplicationModeActionTypes.enableDrawPointType:
      return ApplicationActionReducers.enableDrawPoint(state, action);

    case ApplicationModeActionTypes.enableDrawPolygonType:
      return ApplicationActionReducers.enableDrawPolygon(state, action);

    case ApplicationModeActionTypes.enableDrawLineStringType:
      return ApplicationActionReducers.enableDrawLineString(state, action);

    case ApplicationModeActionTypes.enableViewType:
      return ApplicationActionReducers.enableViewMode(state, action);

    default:
      return state;
  }
};
