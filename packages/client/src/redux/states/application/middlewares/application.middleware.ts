import { IKeyDownAction } from "../actions/interfaces/application.action-interfaces";
import { ApplicationModeActionTypes } from "../actions/types/application.action-types";
import {ApplicationActionCreators} from "../actions/creators";

enum Keys {
  espace = 'Escape'
}

const keyDownActionMiddleware = ({ getState, dispatch }) => next => (
  action: IKeyDownAction
) => {
  const { type } = action;
  if (type !== ApplicationModeActionTypes.keyDownAction) return next(action);
  const {
    payload: { event }
  } = action;
  const {code:key} = event;
  
  if (key === Keys.espace){
    // dispatch action
    dispatch(ApplicationActionCreators.enableViewModeAction());
  }
  
  return next(action);
};

export default [keyDownActionMiddleware];
