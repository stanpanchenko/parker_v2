import {
  ApplicationModeStateI,
  EditModeType
} from "../application.state-reducer";
import {
  IEnableModeAction,
  IEnableViewModeAction,
  ISetFetchAddressOnNextClickFlag
} from "../actions/interfaces/application.action-interfaces";

export const setFetchAddressOnNextClick = (state: ApplicationModeStateI, action: ISetFetchAddressOnNextClickFlag) => {
  return {
    ...state,
    fetchAddressOnNextMapClick: action.payload.toValue
  }
};

export const disableAddingMode = (state: ApplicationModeStateI) => {
  return {
    ...state,
    inAddingMode: false,
    editModeType: EditModeType.view
  };
};

export const toggleAddingMode = (state: ApplicationModeStateI) => {
  return {
    ...state,
    inAddingMode: !state.inAddingMode
  };
};
export const enableAddingMode = (state: ApplicationModeStateI) => {
  return {
    ...state,
    inAddingMode: true
  };
};

export const enableEditorMode = (state: ApplicationModeStateI, action: IEnableModeAction) => {
  const {payload:{mode}} = action;
  return {
    ...state,
    editModeType: mode
  }
}

export const enableDrawPoint = (
  state: ApplicationModeStateI,
  action
): ApplicationModeStateI => {
  return {
    ...state,
    editModeType: EditModeType.drawPoint
  };
};

export const enableViewMode = (
  state: ApplicationModeStateI,
  action: IEnableViewModeAction
): ApplicationModeStateI => {
  return {
    ...state,
    editModeType: EditModeType.view
  };
};


export const enableDrawLineString = (
  state: ApplicationModeStateI,
  action
): ApplicationModeStateI => {
  return {
    ...state,
    editModeType: EditModeType.drawLineString
  };
};
export const enableDrawPolygon = (
  state: ApplicationModeStateI,
  action
): ApplicationModeStateI => {
  return {
    ...state,
    editModeType: EditModeType.drawPolygon
  };
};
