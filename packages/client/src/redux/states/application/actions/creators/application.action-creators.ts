import {
  ConfirmAddingModeActionI,
  IDisableEditModeAction,
  IEnableEditModeAction,
  IEnableModeAction,
  IEnableViewModeAction,
  IKeyDownAction, ISetFetchAddressOnNextClickFlag
} from "../interfaces/application.action-interfaces";
import {ApplicationModeActionTypes} from "../types/application.action-types";
import {EditModeType} from "../../application.state-reducer";

export const setFetchAddressOnClickFlag = (toValue: boolean): ISetFetchAddressOnNextClickFlag => {
  return {
    type: ApplicationModeActionTypes.fetchAddressOnNextMapClick,
    payload:{
      toValue
    }
  }
}

export const enableEditMode = (): IEnableEditModeAction => ({
  type: ApplicationModeActionTypes.enableEditMode
});

export const disableEditMode = (): IDisableEditModeAction => {
  return {
    type: ApplicationModeActionTypes.disableEditMode
  };
};

export const confirmAddingMode = (): ConfirmAddingModeActionI => ({
  type: ApplicationModeActionTypes.confirmAddingMode
});

export const enableDrawPointType = () => ({
  type: ApplicationModeActionTypes.enableDrawPointType
});

export const enableViewModeAction = (): IEnableViewModeAction => ({
  type: ApplicationModeActionTypes.enableViewType
});

export const enableEditorMode = (mode: EditModeType): IEnableModeAction => ({
  type: ApplicationModeActionTypes.enableEditMode,
  payload:{
    mode
  }
});

export const enableDrawLineStringType = () => ({
  type: ApplicationModeActionTypes.enableDrawLineStringType
});

export const enablePolygonType = () => ({
  type: ApplicationModeActionTypes.enableDrawPolygonType
});

export const keyDownAction = (event: any): IKeyDownAction => {
  return {
    type: ApplicationModeActionTypes.keyDownAction,
    payload: {
      event
    }
  }
};
