import {
  IDisableEditModeAction,
  ConfirmAddingModeActionI,
  IEnableEditModeAction,
  IEnableDrawLineStringAction,
  IEnableDrawPointAction,
  IEnableDrawPolygonAction,
  IEnableViewModeAction,
  ISetFetchAddressOnNextClickFlag
} from "./application.action-interfaces";

export type ApplicationActionsI =
  | ISetFetchAddressOnNextClickFlag
  | IDisableEditModeAction
  | ConfirmAddingModeActionI
  | IEnableEditModeAction
  | IEnableDrawPolygonAction
  | IEnableDrawLineStringAction
  | IEnableDrawPointAction | IEnableViewModeAction;
