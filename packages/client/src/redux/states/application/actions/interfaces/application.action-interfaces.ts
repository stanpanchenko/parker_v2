import { ApplicationModeActionTypes } from "../types/application.action-types";
import {EditModeType} from "../../application.state-reducer";

export interface IDisableEditModeAction {
  type: ApplicationModeActionTypes.disableEditMode;
}
export interface ConfirmAddingModeActionI {
  type: ApplicationModeActionTypes.confirmAddingMode;
}
export interface IEnableEditModeAction {
  type: ApplicationModeActionTypes.enableEditMode;
}

export interface IEnableDrawPolygonAction {
  type: ApplicationModeActionTypes.enableDrawPolygonType
}
export interface IEnableDrawLineStringAction {
  type: ApplicationModeActionTypes.enableDrawLineStringType
}
export interface IEnableDrawPointAction {
  type: ApplicationModeActionTypes.enableDrawPointType
}

export interface IEnableViewModeAction {
  type: ApplicationModeActionTypes.enableViewType
}

export interface IEnableModeAction {
  type: ApplicationModeActionTypes.enableEditMode,
  payload: {
    mode: EditModeType
  }
}

export interface ISetFetchAddressOnNextClickFlag {
  type: ApplicationModeActionTypes.fetchAddressOnNextMapClick;
  payload: {
    toValue: boolean
  }
}

export interface IKeyDownAction {
  type: ApplicationModeActionTypes.keyDownAction,
  payload: {
    event:any
  }
}
