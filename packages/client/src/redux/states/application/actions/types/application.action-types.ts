export enum ApplicationModeActionTypes {
  enableEditMode = "[ApplicationModeActionTypes]ENABLE_EDIT_MODE",
  disableEditMode = "[ApplicationModeActionTypes]DISABLE_EDIT_MODE",
  confirmAddingMode = "[ApplicationModeActionTypes]CONFIRM_ADDING",
  keyDownAction = '[APPLICATION_MODE_ACTION]KEY_DOWN_ACTION',

  fetchAddressOnNextMapClick = '[ApplicationModeActionTypes]fetchAddressOnNextMapClick',
  
  // enableEditMode = "[APPLICATION_MODE_ACTION]ENABLE_EDIT_MODE",
  
  enableDrawPointType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DRAW_POINT",
  enableModifyType = "[APPLICATION_MODE_ACTION]EDIT_MODE_MODIFY",
  enableViewType = "[APPLICATION_MODE_ACTION]EDIT_MODE_VIEW",
  enableExtrudeType = "[APPLICATION_MODE_ACTION]EDIT_MODE_EXTRUDE",
  enableScaleType = "[APPLICATION_MODE_ACTION]EDIT_MODE_SCALE",
  enableRotateType = "[APPLICATION_MODE_ACTION]EDIT_MODE_ROTATE",
  enableTranslateType = "[APPLICATION_MODE_ACTION]EDIT_MODE_TRANSLATE",
  enableDuplicateType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DUPLICATE",
  enableDrawLineStringType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DRAW_LINE_STRING",
  enableDrawPolygonType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DRAW_POLYGON",
  enableDraw90PolygonType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DRAW_90_POLYGON",
  enableDrawRectangleType = "[APPLICATION_MODE_ACTION]EDIT_MODE_DRAW_RECTANGLE"
}
