import { ApplicationStateI } from "../../index";
import { createSelector, Selector } from "reselect";
import { EditModeType } from "../application.state-reducer";

const inAdding = (state: ApplicationStateI) =>
  state.applicationMode.inAddingMode;
const fetchAddressOnClickFlag = (state: ApplicationStateI): boolean => state.applicationMode.fetchAddressOnNextMapClick;

export const inAddingMode: Selector<
  ApplicationStateI,
  boolean
> = createSelector(
  [inAdding],
  (inAdding): boolean => inAdding
);

export const fetchAddressOnClickFlagSelector: Selector<ApplicationStateI, boolean> = createSelector([fetchAddressOnClickFlag], (value)=> value);

export const editModeTypeSelector: Selector<
  ApplicationStateI,
  EditModeType
> = createSelector(
  [(state: ApplicationStateI) => state.applicationMode.editModeType],
  (mode: EditModeType): EditModeType => mode
);
