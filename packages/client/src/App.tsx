import React, { useEffect } from "react";
import { AnyAction, Dispatch } from "redux";
import { connect } from "react-redux";
import { ApplicationStateI } from "./redux/states";
import { withTranslation, WithTranslation } from "react-i18next";
import { RouteComponentProps, withRouter } from "react-router";
import { ApplicationRoutes } from "./components/router/application-router";
import { AuthActionsCreators } from "./redux/states/user/substates/authentication/actions";
import { Theme, Themes } from "./shared/providers/theme/Theme";
import { ThemeUIActionCreators } from "./redux/states/ui/states/theme/actions/creators";
import { LocalStorageKeys } from "./shared/constants/localstorage-keys";
import { ApplicationActionCreators } from "./redux/states/application/actions/creators";
import "./App.scss";

interface AppPropsI extends RouteComponentProps, WithTranslation {
  setAuthentication: (value: boolean) => AnyAction;
  loginWithToken: (token: string) => AnyAction;
  error: any;

  changeTheme: (theme: Theme) => AnyAction;
  keyDownAction: (e: any) => AnyAction;
}

function App({ keyDownAction, loginWithToken, changeTheme }: AppPropsI) {
  useEffect(() => {
    const token = localStorage.getItem(LocalStorageKeys.tokenKey);
    const theme = localStorage.getItem(LocalStorageKeys.themeKey);
    if (Themes[theme]) {
      changeTheme(Themes[theme] as Theme);
    }
    if (token) {
      loginWithToken(token);
    }
    window.addEventListener("keydown", keyDownAction);
    return () => {
      window.removeEventListener("keydown", keyDownAction);
    };
  }, []);

  return (
    <>
      <ApplicationRoutes />
    </>
  );
}

const props = (state: ApplicationStateI) => ({
  error: state.errors.error
});

const actions = (dispatch: Dispatch) => ({
  keyDownAction: (e: any) =>
    dispatch(ApplicationActionCreators.keyDownAction(e)),
  setAuthentication: (isAuth: boolean) =>
    dispatch(AuthActionsCreators.setAuth(isAuth)),
  loginWithToken: (token: string) =>
    dispatch(AuthActionsCreators.loginWithToken(token)),
  changeTheme: (theme: Theme) =>
    dispatch(ThemeUIActionCreators.changeTheme(theme))
});

export default withTranslation()(
  withRouter(
    connect(
      props,
      actions
    )(App)
  )
);
