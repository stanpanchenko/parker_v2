let middlWares = [];

export function combinedMiddlewares() {
  return middlWares;
}

function combineMiddlewaresFactory() {
  return function(
    middlewares: ((obj: { getState: Function; dispatch: Function }) => any)[]
  ) {
    middlWares = [...middlWares, ...middlewares];
    console.log("hier");
    return middlWares;
  };
}

export const combineMiddlewares = combineMiddlewaresFactory();

