
import { ILngLat } from "../../shared/Interfaces/LatLng.interface";

export function center(number1, number2) {
  let center: number;
  if (number1 > number2) {
    center = betrag(number1 - number2) / 2;
    return number1 - center;
  } else if (number2 > number1) {
    center = betrag(number2 - number1) / 2;
    return number2 - center;
  } else {
    return number1;
  }
}
/**
 * @name getCenter
 * @desc center between p1 and p2
 * @param {ILngLat} p1
 * @param {ILngLat} p2
 * @return {ILngLat}
 */
export function getCenter(
  p1: { lat: number; lng: number },
  p2: { lat: number; lng: number }
): { lat: number; lng: number } {
  const lngCenter = center(p1.lng, p2.lng);
  const latCenter = center(p1.lat, p2.lat);

  return { lat: latCenter, lng: lngCenter };
}

function betrag(number) {
  return number < 0 ? number * -1 : number;
}

// return distance in meters between two ILngLat
// copy paste from: https://stackoverflow.com/a/43208163/4932576
export function getDistance(
  origin: ILngLat,
  destination: ILngLat
): number {
  const lon1 = toRadian(origin.lng),
    lat1 = toRadian(origin.lat),
    lon2 = toRadian(destination.lng),
    lat2 = toRadian(destination.lat);

  const deltaLat = lat2 - lat1;
  const deltaLon = lon2 - lon1;

  const a =
    Math.pow(Math.sin(deltaLat / 2), 2) +
    Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
  const c = 2 * Math.asin(Math.sqrt(a));
  const EARTH_RADIUS = 6371;
  return c * EARTH_RADIUS * 1000;
}
export const toRadian = degree => {
  return (degree * Math.PI) / 180;
};

/**
 * @desc Rotate LatLang[] around map center(SW-Africa)
 * @param {ILngLat[]} coords
 * @param {number} deg
 * @return {ILngLat[]}
 */
export function rotate(coords: ILngLat[], deg: number): ILngLat[] {
  // evety point will be multiplied by this matrix which rotates the coords
  // cos(x) -sin(x) 0
  // sin(x)  con(x) 0
  //   0      0     1
  const cos = Math.cos;
  const sin = Math.sin;

  return coords.map(c => {
    const x = c.lat;
    const y = c.lng;
    return {
      ...c,
      lat: x * cos(deg) - y * sin(deg),
      lng: x * sin(deg) + y * cos(deg)
    };
  });
}

/**
 * @desc Rotates a figure around given origin
 * @param {ILngLat} origin
 * @param {ILngLat[]} coords
 * @param {number} deg
 */
function rotateFrom(origin: ILngLat, coords: ILngLat[], deg: number) {
  // translate to map origin
  const centered = translate(coords, origin.lat * -1, origin.lng * -1);
  // rotate
  const rotated = rotate(centered, deg);
  // translate back to old position
  const newCoords = translate(rotated, origin.lat, origin.lng);

  return newCoords;
}

/**
 * @desc Adds x and y to each coordinate
 * @param coords
 * @param x
 * @param y
 * @return {ILngLat[]}
 */
export function translate(coords: ILngLat[], x: number, y: number): ILngLat[] {
  const res = coords.map(c => ({
    ...c,
    lat: c.lat + x,
    lng: c.lng + y
  }));

  return res;
}

export function translateTo(latlng: ILngLat, coords: ILngLat[]): ILngLat[] {
  if (coords.length < 1) {
    return [...coords];
  }
  // move to map origin
  const x1 = coords[0].lat * -1;
  const y1 = coords[0].lng * -1;
  const centered = translate([...coords], x1, y1);

  // move to point
  const translated = translate(centered, latlng.lat, latlng.lng);
  return [...translated];
}

/**
 * @desc Returns bounding box of ILngLat[]
 * @param {ILngLat[]} from
 * @return {ILngLat[]} p1, p2, p3, p4
 */
export function boundingBox(
  from: ILngLat[]
): { lat: number; lng: number }[] {
  // find most left(smales lng), lowest (smalles lat), heighest and most right
  if (from.length === 0) return [];

  const smallestLng = from.reduce((next, cur) =>
    cur.lng < next.lng ? cur : next
  ).lng;
  const smallestLat = from.reduce((next, cur) =>
    cur.lat < next.lat ? cur : next
  ).lat;

  const biggestLat = from.reduce((next, cur) =>
    cur.lat > next.lat ? cur : next
  ).lat;
  const biggestLng = from.reduce((next, cur) =>
    cur.lng > next.lng ? cur : next
  ).lng;

  const p1 = {
    lat: biggestLat,
    lng: smallestLng
  };
  const p2 = {
    lat: biggestLat,
    lng: biggestLng
  };
  const p3 = {
    lat: smallestLat,
    lng: biggestLng
  };
  const p4 = {
    lat: smallestLat,
    lng: smallestLng
  };
  return [{ ...p1 }, { ...p2 }, { ...p3 }, { ...p4 }];
}

/**
 * @desc One single ILngLat in the center of a bounding rect
 * @param {ILngLat[]} coordinates
 */
export function centroid(coordinates: ILngLat[]): any {
  const box = boundingBox([...coordinates]);
  const center: { lat: number; lng: number } = getCenter(box[0], box[2]);
  return { ...center };
}

/**
 * @desc Returns area size value for every given coords array
 * @see https://www.mathopenref.com/coordpolygonarea.html
 */
export function areaSize(coords: ILngLat[]): number {
  if (!coords) return 0;
  if (coords.length < 3) return 0;
  const res = coords.reduce((acc, curr, idx, array) => {
    let nIdx = idx + 1;
    if (nIdx > array.length - 1) nIdx = 0;
    const next = array[nIdx];
    return curr.lng * next.lat - curr.lat * next.lng + acc;
  }, 0);
  return betrag(res / 2);
}
