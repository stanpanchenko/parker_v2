export function removeFromArray<T>(array: T[], index: number): T[] {
  if (index < 0 || index > array.length - 1) return array;
  const newArray = [...array];
  return [...newArray.slice(0, index), ...newArray.slice(index + 1)];
}

export function removeFromArrayMany<T>(array: T[], indices: number[]): T[] {
  let res: T[] = [...array];
  indices.forEach(index => {
    res = [...removeFromArray<T>(res, index)];
  });
  return res;
}
