export function removeProperty(obj, key: string) {
  if (!obj[key]) return obj;
  const { [key]: oldProperty, ...rest } = obj;
  return { ...rest };
}
/**
 * @param obj1
 * @param obj2
 * @desc Returns new object containing {[key]:value} which are not in obj2
 */
export function objectDifference(obj1, obj2) {
  return Object.entries(obj1).reduce((acc, [key, value]) => {
    if (obj2[key]) return acc;
    return { ...acc, [key]: value };
  }, {});
}

export function normalize(array: object[], attribute: string): any {
  if (!Array.isArray(array)) return {};
  return array.reduce((acc, r: any, idx) => {
    if (!r[attribute]) return acc;

    if (!acc[r[attribute]])
      return { ...acc, [r[attribute]]: { [r.id]: { ...r } } };
    else
      return {
        ...acc,
        [r[attribute]]: { ...acc[r[attribute]], [r.id]: { ...r } }
      };
  }, {});
}

export const mapObjectValues = (
  targetObject: any,
  callbackFn: (key: string, value: any) => any
): any => {
  if (!targetObject) return targetObject;
  if (typeof targetObject === "number" || typeof targetObject === "string")
    return targetObject;
  if (Array.isArray(targetObject)) {
    return targetObject.map(v => mapObjectValues(v, callbackFn));
  }
  return Object.entries(targetObject).reduce((acc, [key, value]) => {
    const res = callbackFn(key, value);
    if (!Array.isArray(res) && typeof res === "object") {
      return { ...acc, [key]: mapObjectValues(res, callbackFn) };
    }
    if (Array.isArray(res)) {
      return { ...acc, [key]: res.map(v => mapObjectValues(v, callbackFn)) };
    }
    return { ...acc, [key]: res };
  }, {});
};
