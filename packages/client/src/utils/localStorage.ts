const parkerAccessTokenName = "parkerAccessToken";

export function getAccessToken(): string | null {
  return localStorage.getItem(parkerAccessTokenName);
}
export function hasAccessToken() {
  return getAccessToken() !== null;
}
export function removeAccessToken(): void {
  const hastToken = hasAccessToken();
  if (!hastToken) return;

  localStorage.removeItem(parkerAccessTokenName);
}
export function setAccessToken(token: string): void {
  const hasToken = hasAccessToken();
  if (hasToken) removeAccessToken();
  localStorage.setItem(parkerAccessTokenName, token);
}
