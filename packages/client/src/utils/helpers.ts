export function removeFromObj(obj, key: string) {
  if (!obj) return obj;
  if (!obj[key]) return obj;
  const { [key]: toRemove, ...rest } = obj;
  return { ...rest };
}

export function removeFromObjMany(obj, keys: string[]) {
  let o = { ...obj };
  keys.forEach(k => {
    o = { ...removeFromObj(o, k) };
  });
  return o;
}

export function getFromObj(obj, key: string) {
  if (!obj[key]) return obj;
  return { [key]: typeof obj[key] === "object" ? { ...obj[key] } : obj[key] };
}

export function getFromObjMany(obj, keys: string[]) {
  let newObj = {};
  keys.forEach(key => {
    if (obj[key])
      newObj = {
        ...newObj,
        [key]: typeof obj[key] === "object" ? { ...obj[key] } : obj[key]
      };
  });
  return newObj;
}

export function inRange(range: [number, number], value) {
  return value >= range[0] && value <= range[1];
}

export const throttle = (func, limit) => {
  let lastFunc;
  let lastRan;
  return function() {
    const context = this as any;
    const args = arguments;
    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function() {
        if (Date.now() - lastRan >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
};
