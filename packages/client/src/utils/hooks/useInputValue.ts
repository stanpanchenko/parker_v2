import { useCallback, useState } from 'react';

export default function useInputValue(initialState) {
  const [value, setValue] = useState(initialState || "");

  let onChange = useCallback(function(event) {
    setValue(event.currentTarget.value);
  }, []);

  return [
    value,
    {
      onChange
    }
  ];
}
