import { useCallback, useState } from 'react';

export default function useMouseHover() {
  const [hovered, setHovered] = useState(false);
  const [hoverEvent, setEvent] = useState({});
  function handleMouseEnter(e) {
    setHovered(true);
    setEvent(e);
  }
  function handleMouseLeave(e) {
    setHovered(false);
    setEvent(e);
  }
  return [
    hovered,
    {
      onMouseEnter: useCallback(handleMouseEnter, []),
      onMouseLeave: useCallback(handleMouseLeave, [])
    }
  ];
}
