import applicationStore from "../redux/store";

const inDevelopment = process.env.NODE_ENV === "development";

export function handleError(error: any) {
  const { dispatch } = applicationStore;
  console.error(error);
  // if (inDevelopment) {
  // }
}
