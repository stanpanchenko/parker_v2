import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import { StoreProvider } from "./redux/components/store-provider";
import { ApolloClientProvider } from "./api/graphql/apollo-client-provider";
import "./i18n";
import SwitchTheme from "./shared/providers/theme/Theme";

function onUpdate(registration: any) {
  console.log("onUpdateFunction", registration);
}
function onSuccess(registration: any) {
  console.log("onSuccessFunction", registration);
}

const config = {
  onUpdate: onUpdate,
  onSuccess: onSuccess
};

const Application = (
  <Suspense fallback={<div />}>
    <ApolloClientProvider>
      <StoreProvider>
        <BrowserRouter>
          <SwitchTheme>
            <App />
          </SwitchTheme>
        </BrowserRouter>
      </StoreProvider>
    </ApolloClientProvider>
  </Suspense>
);

ReactDOM.render(Application, document.getElementById("root"));

// serviceWorker.register(config);
