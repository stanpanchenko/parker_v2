import styled from "styled-components";
import { ThemePropsI } from "../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { BackDrop } from "../../../../../../shared/components/blur/backdrop";

export const StyledInnerWrapper = styled.div`
  width: 100%;
  height: 100%;
  border: 1px solid ${(props: ThemePropsI) => props.theme.borderColorSecond};
  background-color: ${(props: ThemePropsI) => props.theme.backgroundColor};
  border-radius: 0.7rem;
`;

export const StyledWrapper = styled(BackDrop)`
  width: 20vw;
  padding: 8px 8px;
  border-radius: 0.8rem;
  box-shadow: ${(props: ThemePropsI) => props.theme.shadowPrime};
  min-width: 300px;
  height: 90vh;
`;
