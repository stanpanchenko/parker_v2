import styled from "styled-components";
import Label from "../../../../../../../shared/components/Label/Label";
import React from "react";
import { useTranslation } from "react-i18next";

const StyledListHeadWrapper = styled.div`
  display: flex;
  align-items: center;
`;
const Count = styled.span`
  margin-left: 1rem;
`;

const ListHead = ({ label, elementCount }) => {
  const { t } = useTranslation();
  return (
    <StyledListHeadWrapper>
      <Label text={t(label)} />
      <Count>{elementCount}</Count>
    </StyledListHeadWrapper>
  );
};
export default ListHead;
