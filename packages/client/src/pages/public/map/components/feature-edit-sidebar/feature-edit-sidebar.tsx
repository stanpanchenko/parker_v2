import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../redux/states";
import { FeatureCollectionSelectors } from "../../../../../redux/states/feature-collection/selectors";
import { Feature, Tag } from "../../../../../api/graphql/graphql-api.interface";
import AddressEdit from "./address-edit/address-edit";
import TagsEdit from "./tags-edit/tags-edit";
import UpdatedFeatureList from "./updated-feature-list/updated-feature-list";
import CreatedFeaturesList from "./created-features-list/created-features-list";
import DeleteFeaturesActionButton from "../shared/delete-feature-action-button/delete-feature-action-button";
import FeatureType from "./feature-type/feature-type";
import GeometryType from "./geometry-type/geometry-type";
import { EditSidebarWrapper } from "./edit-sidebar-wrapper/EditSidebarWrapper";
import { EditSidebarContextProvider } from "./context/edit-sidebar-context/EditSidebar.context";
import CreateOnDBFeaturesActionButton from "../shared/create-on-db-features-action-button/create-on-db-features-action-button";

interface IFeatureMetaSidebarProps {
  feature: Feature | undefined;
}
const tags = [
  {
    key: "parking",
    value: "roof"
  },
  {
    key: "parking",
    value: "street"
  },
  {
    key: "parking",
    value: "underground"
  },
  {
    key: "parking",
    value: "no"
  },
  {
    key: "parking",
    value: "yes"
  },
  {
    key: "parking",
    value: "secure"
  },
  {
    key: "parking",
    value: "private"
  },
  {
    key: "parking",
    value: "public"
  },
  {
    key: "parking",
    value: "yes"
  },
  {
    key: "parking",
    value: "yes"
  },
  {
    key: "parking",
    value: "space"
  }
];
const getFeatureType = (fromTags: Tag[]) => {
  let type = "Feature";
  for (let i = 0; i < fromTags.length; ++i) {
    const { key, value } = tags[i];
    if (key === "parking" && value === "space") {
      type = "Parking Space";
      return type;
    }
  }
  return type;
};
const _FeatureMetaSidebar = ({ feature }: IFeatureMetaSidebarProps) => {
  const [address, setAddress] = useState();

  const type = useMemo(() => {
    return getFeatureType(tags);
  }, [tags]);
  const render = () => {
    return (
      <EditSidebarContextProvider value={{}}>
        <EditSidebarWrapper>
          <FeatureType type={type} />
          <GeometryType type={feature.geometry.type} />
          <AddressEdit address={address} onChange={setAddress} />
          <DeleteFeaturesActionButton ids={[feature && feature.id]}>
            Delete
          </DeleteFeaturesActionButton>
          <TagsEdit tags={tags} />
          <UpdatedFeatureList />
          <CreatedFeaturesList />
          <CreateOnDBFeaturesActionButton>Save</CreateOnDBFeaturesActionButton>
        </EditSidebarWrapper>
      </EditSidebarContextProvider>
    );
  };

  return feature ? render() : null;
};

const props = (state: ApplicationStateI) => ({
  feature: FeatureCollectionSelectors.selectedFeatureSelector(state)
});
const FeatureMetaSidebar = connect(props, null)(_FeatureMetaSidebar);
export default FeatureMetaSidebar;
