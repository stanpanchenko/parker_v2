import React from "react";
import { Tag as ITag } from "../../../../../../api/graphql/graphql-api.interface";
import styled from "styled-components";
import { Icon } from "../../../../../../shared/components/icon";
import Button from "../../../../../../shared/components/button/button";
import Label from "../../../../../../shared/components/Label/Label";
import { useTranslation } from "react-i18next";

const StyledTag = styled.span`
  padding: 0.3rem 0.2rem 0.4rem 0.8rem;
  border-radius: 0.25rem;
  color: white;
  letter-spacing: 0.014rem;
  font-size: 0.7rem;
  font-weight: 500;
  background-color: #197fe3;
  margin: 0.14rem;
  display: inline-block;
  box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);

  button {
    background: none;
    color: white;
    border: none;
  }
`;

const Tag = ({ tagKey, tagValue, onClick }) => {
  return (
    <StyledTag>
      <span
        style={{
          display: "flex",
          alignItems: "center"
        }}
      >
        <span>
          {tagKey}:{tagValue}
        </span>
        <Button active={false} onClick={onClick}>
          <Icon name={"times"} />
        </Button>
      </span>
    </StyledTag>
  );
};

interface ITagsEditProps {
  tags?: ITag[];
}
const TagsEdit = ({ tags }: ITagsEditProps) => {
  const { t } = useTranslation();
  return (
    <div>
      <Label text={t("Tags")} />
      {tags.map(({ key, value, id }, idx) => {
        return (
          <Tag
            key={`${id}-${idx}`}
            tagKey={key}
            tagValue={value}
            onClick={() => {}}
          />
        );
      })}
    </div>
  );
};

export default TagsEdit;
