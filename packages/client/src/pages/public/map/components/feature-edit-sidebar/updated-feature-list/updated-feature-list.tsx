import React from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../redux/states";
import { FeatureCollectionSelectors } from "../../../../../../redux/states/feature-collection/selectors";
import FeatureList from "../feature-list/feature-list";
import ListHead from "../feature-list/list-head/list-head";

const _UpdatedFeatureList = ({ features }) => {
  return (
    <div>
      <ListHead label={"Updated"} elementCount={features.length} />
      <FeatureList features={features} />
    </div>
  );
};

const props = (state: ApplicationStateI) => ({
  features: FeatureCollectionSelectors.denormalizedUpdatedFeaturesSelector(
    state
  )
});

const UpdatedFeatureList = connect(
  props,
  null
)(_UpdatedFeatureList);
export default UpdatedFeatureList;
