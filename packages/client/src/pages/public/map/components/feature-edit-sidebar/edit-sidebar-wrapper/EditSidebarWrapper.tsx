import React from 'react';
import {StyledInnerWrapper, StyledWrapper} from "./styled";

export const EditSidebarWrapper = ({ children }) => {
  return (
    <StyledWrapper>
      <StyledInnerWrapper>{children}</StyledInnerWrapper>
    </StyledWrapper>
  );
};