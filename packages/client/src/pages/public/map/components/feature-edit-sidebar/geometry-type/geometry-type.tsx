import React from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../../shared/providers/theme/interfaces/theme-props.interface";

const StyledDiv = styled.div`
  font-size: 0.8rem;
  color: ${(props: ThemePropsI) => props.theme.colorThird};
`;

const GeometryType = ({ type }) => {
  return <StyledDiv>{type}</StyledDiv>;
};

export default GeometryType;
