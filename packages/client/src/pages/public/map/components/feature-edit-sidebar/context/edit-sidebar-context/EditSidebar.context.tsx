import {createContext, useContext} from "react";

export const EditSidebarContext = createContext({});
export const EditSidebarContextProvider = EditSidebarContext.Provider;
export const useEditSidebarContext = () => useContext(EditSidebarContext);