import React from "react";
import FeatureList from "../feature-list/feature-list";
import { ApplicationStateI } from "../../../../../../redux/states";
import { FeatureCollectionSelectors } from "../../../../../../redux/states/feature-collection/selectors";
import { connect } from "react-redux";
import { Feature } from "../../../../../../api/graphql/graphql-api.interface";
import ListHead from "../feature-list/list-head/list-head";
import CreateOnDBFeaturesActionButton from "../../shared/create-on-db-features-action-button/create-on-db-features-action-button";

const _CreatedFeaturesList = ({ features }: { features: Feature[] }) => {
  return (
    <div>
      <ListHead label={"Created"} elementCount={features.length} />
      <FeatureList features={features} />
    </div>
  );
};

const props = (state: ApplicationStateI) => ({
  features: FeatureCollectionSelectors.denormalizedCreatedFeaturesSelector(
    state
  )
});

const CreatedFeaturesList = connect(
  props,
  null
)(_CreatedFeaturesList);

export default CreatedFeaturesList;
