import React, { useEffect, memo } from "react";
import styled from "styled-components";

const StyledDiv = styled.div`
  font-size: 1.3rem;
  font-weight: 600;
`;

interface IProps {
  type: string;
}
const FeatureType = memo(({ type }: IProps) => {
  useEffect(() => {
    console.log("FeatureType render");
  });
  return <StyledDiv>{type}</StyledDiv>;
});

export default FeatureType;
