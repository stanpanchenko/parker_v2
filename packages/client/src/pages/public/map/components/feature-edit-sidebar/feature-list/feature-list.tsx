import React from "react";
import { Feature } from "../../../../../../api/graphql/graphql-api.interface";
import FeatureListElem from "./list-elem/list-elem";
import styled, { css } from "styled-components";
import { scrollbarStyle } from "../../../../../../styles/components/scrollbar";

const StyledUl = styled.ul`
  max-height: 120px;
  overflow-y: scroll;
  ${scrollbarStyle}
`;

interface FeatureListProps {
  features: Feature[];
}

const FeatureList = ({ features }: FeatureListProps) => {
  return (
    <div>
      <StyledUl>
        {features.map((feature: Feature, idx) => {
          return (
            <FeatureListElem key={`${feature.id}-${idx}`} feature={feature} />
          );
        })}
      </StyledUl>
    </div>
  );
};

export default FeatureList;
