import { Feature } from "../../../../../../../api/graphql/graphql-api.interface";
import React from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../../../shared/providers/theme/interfaces/theme-props.interface";

const StyledLi = styled.li`
  font-size: 0.9rem;
  padding: 0.5rem;
  border-bottom: 1px solid
    ${(props: ThemePropsI) => props.theme.borderColorSecond};
`;

const FeatureListElem = ({ feature }: { feature: Feature }) => {
  return <StyledLi>{feature.geometry.type}</StyledLi>;
};

export default FeatureListElem;
