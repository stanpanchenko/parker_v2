import React from "react";
import Input from "../../../../../../shared/components/input/input";
import Label from "../../../../../../shared/components/Label/Label";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import ToggleFetchAddressOnClickFlag from "../../shared/toogle-fetch-address-on-click-flag";
import {useEditSidebarContext} from "../context/edit-sidebar-context/EditSidebar.context";

interface Address {
  street: string;
  houseNr: string;
  code: string;
  city: string;
  country: string;
}

const nullAddress: Address = {
  street: "",
  houseNr: "",
  code: "",
  city: "",
  country: ""
};

interface IAddressEditProps {
  onChange: (address: Address) => void;
  address?: Address;
}
const AddressEdit = ({ onChange, address }: IAddressEditProps) => {
  const { t } = useTranslation();

  const handleChange = (value: string, key: string) => {
    onChange({ ...nullAddress, ...address, [key]: value });
  };

  return (
    <div>
      <Label text={t("Adresse")} />
      <Input
        type={"text"}
        placeholder={"Strasse"}
        onChange={value => handleChange(value, "street")}
      />
      <Input
        type={"text"}
        placeholder={"Nr."}
        style={{
          width: "3.1rem"
        }}
        onChange={value => handleChange(value, "houseNr")}
      />
      <br />
      <Input
        type={"text"}
        placeholder={"PLZ"}
        style={{
          width: "3.1rem"
        }}
        onChange={value => handleChange(value, "code")}
      />
      <Input
        type={"text"}
        placeholder={"Stadt"}
        onChange={value => handleChange(value, "city")}
      />
      <ToggleFetchAddressOnClickFlag />
    </div>
  );
};

export default AddressEdit;
