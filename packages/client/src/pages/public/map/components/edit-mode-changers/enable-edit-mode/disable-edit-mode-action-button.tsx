import React, { ReactNode } from "react";
import Button from "../../../../../../shared/components/button/button";
import { ApplicationActionCreators } from "../../../../../../redux/states/application/actions/creators";
import { IDisableEditModeAction } from "../../../../../../redux/states/application/actions/interfaces/application.action-interfaces";

interface IProps {
  onClick: (action: IDisableEditModeAction) => void;
  children: ReactNode;
}

const DisableEditModeActionButton = ({ onClick, children }: IProps) => {
  return (
    <Button
      active={true}
      onClick={() => onClick(ApplicationActionCreators.disableEditMode())}
    >
      {children}
    </Button>
  );
};

export default DisableEditModeActionButton;
