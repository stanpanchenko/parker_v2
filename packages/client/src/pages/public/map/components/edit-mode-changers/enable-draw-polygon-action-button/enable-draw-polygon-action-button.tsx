import { ApplicationStateI } from "../../../../../../redux/states";
import { Dispatch } from "redux";
import { ApplicationActionCreators } from "../../../../../../redux/states/application/actions/creators";
import { connect } from "react-redux";
import React from "react";
import { ApplicationSelectors } from "../../../../../../redux/states/application/selectors";
import Button from "../../../../../../shared/components/button/button";
import { EditModeType } from "../../../../../../redux/states/application/application.state-reducer";

const _EnableDrawPolygonActionButton = ({
  children,
  enableDrawPolygon,
  editModeType
}) => {
  return (
    <Button
      active={editModeType === EditModeType.drawPolygon}
      onClick={enableDrawPolygon}
    >
      {children}
    </Button>
  );
};

const props = (state: ApplicationStateI) => ({
  editModeType: ApplicationSelectors.editModeTypeSelector(state)
});
const actions = (dispatch: Dispatch) => ({
  enableDrawPolygon: () =>
    dispatch(ApplicationActionCreators.enablePolygonType())
});

const EnableDrawPolygonActionButton = connect(
  props,
  actions
)(_EnableDrawPolygonActionButton);
export default EnableDrawPolygonActionButton;
