import React from "react";

import EnableDrawPolygonActionButton from "./enable-draw-polygon-action-button/enable-draw-polygon-action-button";
import EnableDrawPointActionButton from "./enable-draw-point-action-button/enable-draw-point-action-button";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { Icon } from "../../../../../shared/components/icon";
import { connect } from "react-redux";
import { ApplicationSelectors } from "../../../../../redux/states/application/selectors";
import { BackDrop } from "../../../../../shared/components/blur/backdrop";
import EnableDrawLineStringActionButton from "./enable-draw-line-string-action-button/enable-draw-line-string-action-button";
import SelectTileButton from "../shared/select-tile-button/select-tile-button";
import { TileName } from "../../../../../redux/states/map/states/tiles/tiles-reducer";

const ButtonGroupStyled = styled(BackDrop)`
  justify-content: space-around;
  overflow: hidden;
  display: flex;
  border-radius: 2rem;
  box-shadow: ${(props: ThemePropsI) => props.theme.shadowPrime};
  & > button {
    &:hover {
      svg {
        color: ${(props: ThemePropsI) => props.theme.colorPrime};
      }
    }
    &.button-active {
      svg {
        color: ${(props: ThemePropsI) => props.theme.colorPrime};
      }
    }
    outline: none;
    padding: 1rem 1.5rem;
    background: none;
    border: none;
    font-size: 0.8rem;
    border-left: 1px solid
      ${(props: ThemePropsI) => props.theme.borderColorSecond};
    border-right: 1px solid
      ${(props: ThemePropsI) => props.theme.borderColorSecond};
    svg {
      color: ${(props: ThemePropsI) => props.theme.colors.icon.color};
    }
    &:first-child {
      border-right: none;
      border-left: none;
    }
    &:last-child {
      border-left: none;
      border-right: none;
    }
  }
`;

const _EditModeChangers = ({ editModeType }) => {
  return (
    <ButtonGroupStyled>
      <EnableDrawPointActionButton>
        <Icon name={"mapMarker"} />
      </EnableDrawPointActionButton>
      <EnableDrawLineStringActionButton>
        <Icon name={"mapLine"} />
      </EnableDrawLineStringActionButton>
      <EnableDrawPolygonActionButton>
        <Icon name={"polygon"} />
      </EnableDrawPolygonActionButton>
    </ButtonGroupStyled>
  );
};
const props = state => ({
  editModeType: ApplicationSelectors.editModeTypeSelector(state)
});
const EditModeChangers = connect(
  props,
  null
)(_EditModeChangers);

export default EditModeChangers;
