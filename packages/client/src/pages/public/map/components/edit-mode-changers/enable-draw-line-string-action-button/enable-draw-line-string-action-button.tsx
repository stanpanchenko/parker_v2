import { ApplicationStateI } from "../../../../../../redux/states";
import { Dispatch } from "redux";
import { ApplicationActionCreators } from "../../../../../../redux/states/application/actions/creators";
import { connect } from "react-redux";
import React from "react";
import Button from "../../../../../../shared/components/button/button";
import { ApplicationSelectors } from "../../../../../../redux/states/application/selectors";
import { EditModeType } from "../../../../../../redux/states/application/application.state-reducer";

const _EnableDrawLineStringActionButton = ({
  editModeType,
  children,
  enableDrawlineString
}) => {
  return (
    <Button
      active={editModeType === EditModeType.drawLineString}
      onClick={enableDrawlineString}
    >
      {children}
    </Button>
  );
};

const props = (state: ApplicationStateI) => ({
  editModeType: ApplicationSelectors.editModeTypeSelector(state)
});
const actions = (dispatch: Dispatch) => ({
  enableDrawlineString: () =>
    dispatch(ApplicationActionCreators.enableDrawLineStringType())
});

const EnableDrawLineStringActionButton = connect(
  props,
  actions
)(_EnableDrawLineStringActionButton);
export default EnableDrawLineStringActionButton;
