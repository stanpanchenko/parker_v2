import React, {ReactNode} from 'react';
import Button from "../../../../../../shared/components/button/button";
import {ApplicationActionCreators} from "../../../../../../redux/states/application/actions/creators";
import {IEnableEditModeAction} from "../../../../../../redux/states/application/actions/interfaces/application.action-interfaces";

interface IProps {
	onClick: (action: IEnableEditModeAction) => any;
	children: ReactNode;
}

const EnableEditModeActionButton = ({onClick, children}:IProps) => {
	return (
		<Button active={true} onClick={()=> onClick(ApplicationActionCreators.enableEditMode())}>{children}</Button>
	);
};

export default EnableEditModeActionButton;
