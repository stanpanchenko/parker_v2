import React from "react";
import { ApplicationStateI } from "../../../../../../redux/states";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { ApplicationActionCreators } from "../../../../../../redux/states/application/actions/creators";
import { ApplicationSelectors } from "../../../../../../redux/states/application/selectors";
import { EditModeType } from "../../../../../../redux/states/application/application.state-reducer";
import Button from "../../../../../../shared/components/button/button";

const _EnableDrawPointActionButton = ({
  enableDrawPoint,
  editModeType,
  children
}) => {
  return (
    <Button
      active={editModeType === EditModeType.drawPoint}
      onClick={enableDrawPoint}
    >
      {children}
    </Button>
  );
};

const props = (state: ApplicationStateI) => ({
  editModeType: ApplicationSelectors.editModeTypeSelector(state)
});
const actions = (dispatch: Dispatch) => ({
  enableDrawPoint: () =>
    dispatch(ApplicationActionCreators.enableDrawPointType())
});

const EnableDrawPointActionButton = connect(
  props,
  actions
)(_EnableDrawPointActionButton);
export default EnableDrawPointActionButton;
