import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ApplicationSelectors } from "../../../../../../redux/states/application/selectors";
import DisableEditModeActionButton from "./disable-edit-mode-action-button";
import EnableEditModeActionButton from "./enable-edit-mode-action-button";
import {
  IDisableEditModeAction,
  IEnableEditModeAction
} from "../../../../../../redux/states/application/actions/interfaces/application.action-interfaces";
import { TilesActionCreators } from "../../../../../../redux/states/map/states/tiles/actions/creators";
import { TileName } from "../../../../../../redux/states/map/states/tiles/tiles-reducer";
import { useActionCreator } from "../../../../../../redux/components/useActionCreatorHook";

interface IEnableEditModeProps {}
const ToggleEditModeButton = ({  }: IEnableEditModeProps) => {
  const dispatch = useDispatch();
  const inEditMode = useSelector(ApplicationSelectors.inAddingMode);
  const [selectTile] = useActionCreator(TilesActionCreators.selectTileName);

  const enable = (action: IEnableEditModeAction) => {
    dispatch(action);
    selectTile(TileName.satellite);
  };

  const disable = (action: IDisableEditModeAction) => {
    dispatch(action);
    selectTile(TileName.grey);
  };

  return inEditMode ? (
    <DisableEditModeActionButton onClick={disable}>
      Disable
    </DisableEditModeActionButton>
  ) : (
    <EnableEditModeActionButton onClick={enable}>
      Enable
    </EnableEditModeActionButton>
  );
};

export default ToggleEditModeButton;
