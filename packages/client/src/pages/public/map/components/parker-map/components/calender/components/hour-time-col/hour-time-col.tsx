import React from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { hourHeight } from "../../calender";
import { HourHeight } from "../../styled";

const HourColumnWrapper = styled.div`
  display: inline-block;
  position: relative;
  background-color: ${({ theme }: ThemePropsI) => theme.backgroundColorSecond};
  padding: 0 0px 0 0;
  width: 25px;
`;
const Border = styled.div`
  border-bottom: 1px solid transparent;
  box-sizing: border-box;
`;
const HourLabel = styled.div`
  font-size: 10px;
  text-align: center;
  transform: translateY(-10px);
  color: ${({ theme }: ThemePropsI) => theme.textColorSecond};
  pointer-events: none;
`;
const hours = Array.from(Array(24));
export function HourTimeCol() {
  return (
    <HourColumnWrapper>
      {hours.map((_, idxAsHour) => {
        return (
          <Border key={`hour-${idxAsHour}`}>
            <div
              style={{
                height: `${hourHeight - 1}px`
              }}
            >
              <HourLabel>{`${idxAsHour}h`}</HourLabel>
            </div>
          </Border>
        );
      })}
    </HourColumnWrapper>
  );
}
