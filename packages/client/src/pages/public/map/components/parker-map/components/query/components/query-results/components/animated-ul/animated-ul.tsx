import React, { useEffect, useState } from "react";
import posed, { PoseGroup } from "react-pose";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { QueryResultI } from "../../../../../../../../../../../redux/states/query/query.state-reducer";
import { normalize } from "../../../../../../../../../../../utils/objects";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../../../../../../redux/states";

const LIStyled = styled.li`
  padding: 0;
  position: relative;
  margin-top: 25px;
`;
const GrouptULStyled = styled.ul`
  overflow: hidden;
  position: relative;
  color: ${({ theme }: ThemePropsI) => theme.textColorPrime};
  padding: 0;
`;

const GroupTitle = styled.div`
  padding-left: 15px;
  font-size: 0.7rem;
  color: ${({ theme }: ThemePropsI) => theme.textColorSecond};
  padding-bottom: 5px;
`;

const AnimatedLI = posed(LIStyled)({
  enter: {
    opacity: 1,
    delay: ({ idx }) => 40 * idx
  },
  exit: { opacity: 0 }
});

const UlStyled = styled.ul`
  // hide scroll bars
  margin: 0 -30px -30px 0;

  padding: 0 30px 30px 0;

  max-height: calc(100vh - 150px);
`;

const ExpandULAnimation = posed(UlStyled)({
  init: {
    height: 0
  },
  open: {
    height: ({ height }) => height
  },
  closed: {
    height: 0
  }
});

const Li = styled.li`
  padding: 0 0 0 15px;
  text-rendering: geometricPrecision;
  cursor: pointer;
  display: inline-block;
  width: calc(100% + 15px);
  &:hover {
    background-color: ${({ theme }: ThemePropsI) => theme.hoverPrime};
  }
`;
const LiContent = styled.div`
  padding: 13px 0 12px 0;
  z-index: 5;
  font-size: 0.9rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  border-bottom: 1px solid
    ${({ theme }: ThemePropsI) => theme.borderColorSecond};
`;

interface OwnPropsI {
  listElements: QueryResultI[];
}

interface ReduxProps {}

function displayName(name: string) {
  const a = name.split(",");
  return a.slice(0, 3).join(",");
}

function AnimatedUL({ listElements }: OwnPropsI & ReduxProps) {
  const [queryResults, setQueryResults] = useState<{
    [groupName: string]: {
      [resultID: string]: QueryResultI;
    };
  }>({});
  const [listHeight, setListHeight] = useState(0);

  function calcHeight(list: any[]): number {
    return 30 + list.length * 55;
  }

  useEffect(() => {
    // groups results by class attribute
    const list = normalize(listElements, "class");
    const height = calcHeight(listElements);
    setListHeight(height);
    setQueryResults(list);
  }, [listElements]);

  function handleResultClick(result) {
    console.log(result);
    const lat = result.lat;
    const lng = result.lng;
    // centerMap({ center: [lat, lng], zoom: 18 });
  }
  return (
    <div
      style={{
        overflow: "hidden"
      }}
    >
      <ExpandULAnimation
        height={listHeight} // one li height
        pose={Object.keys(queryResults).length > 0 ? "open" : "closed"}
      >
        <PoseGroup animateOnMount>
          {Object.entries(queryResults).map(([groupName, group], idx) => (
            <AnimatedLI key={`li-value-${groupName}`} idx={idx}>
              <GroupTitle>{groupName.toUpperCase()}</GroupTitle>
              <GrouptULStyled>
                {Object.entries(group).map(([resultID, result], idx) => (
                  <Li
                    key={`res-${idx}`}
                    onClick={() => handleResultClick(result)}
                  >
                    <LiContent>{displayName(result.display_name)}</LiContent>
                  </Li>
                ))}
              </GrouptULStyled>
            </AnimatedLI>
          ))}
        </PoseGroup>
      </ExpandULAnimation>
    </div>
  );
}
const props = (state: ApplicationStateI) => ({});
const actions = dispatch => ({});
export default connect(
  props,
  actions
)(AnimatedUL);
