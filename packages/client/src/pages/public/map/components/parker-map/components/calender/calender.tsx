import React, { useEffect, useRef, useState } from "react";
import { useCurrTimeInMin } from "./hooks/useCurrTimeInMin";
import { HourTimeCol } from "./components/hour-time-col/hour-time-col";
import { Sticky } from "../../../../../../../shared/components/sticky";
import Day from "./components/day/day/day";
import CalenderHead from "./components/head/calender-head";
import { CurTimeLine } from "./components/cur-time-line";
import { ContentWrapper, DaysView, ScrollView, SizingWrapper } from "./styled";
import { PricePeriod } from "../../../../../../../api/graphql/graphql-api.interface";
import { Weekday, weekDays } from "./utils/dates.utils";
import { CalenderUtils } from "./utils";

export const hourHeight = 35;
export const dayWidth = 85;

interface CalenderPropsI {
  pricePeriods: PricePeriod[];
}

function getPricePeriods(
  pricePeriods: PricePeriod[],
  forDate: Date
): PricePeriod[] {
  return pricePeriods;
}

function Calender({ pricePeriods }: CalenderPropsI) {
  const [curWeekDates, setCurWeekDates] = useState<Date[]>([]);
  const visibleHours = 24;
  const curTimeInMin = useCurrTimeInMin();
  const [curTimeLinePos, setCurTimelinePos] = useState(0);
  const scrollViewRef = useRef<HTMLDivElement>();
  const daysViewWrapperRef = useRef<HTMLDivElement>();

  function scrollToTime(timeInMin: number) {
    if (scrollViewRef && scrollViewRef.current) {
      if (timeInMin >= 50) {
        scrollViewRef.current.scrollTop = timeInMin - 50;
      }
    }
  }

  function scrollToWeekDay(weekday: Weekday) {
    if (scrollViewRef && scrollViewRef.current) {
      const dayWrapperWidth =
        (daysViewWrapperRef.current &&
          daysViewWrapperRef.current.offsetWidth) ||
        1;
      const dayWidth = dayWrapperWidth / curWeekDates.length;
      const scrollPos =
        curWeekDates
          .map(date => CalenderUtils.weekDayName(date))
          .findIndex(day => day === weekday) * dayWidth;
      scrollViewRef.current.scrollLeft = scrollPos;
    }
  }
  function getTimePos(): number {
    const timeLineHeight =
      (daysViewWrapperRef.current && daysViewWrapperRef.current.offsetHeight) ||
      1;
    const currTimePosition =
      (timeLineHeight / visibleHours / 60) * curTimeInMin;
    return currTimePosition;
  }

  useEffect(() => {
    const currTimePos = getTimePos();
    scrollToTime(getTimePos());
    scrollToWeekDay(weekDays[new Date().getDay() - 1]);
    setCurTimelinePos(currTimePos);
  }, [curTimeInMin]);

  useEffect(() => {
    const currTimePos = getTimePos();
    scrollToTime(getTimePos());
    scrollToWeekDay(weekDays[new Date().getDay() - 1]);
    setCurTimelinePos(currTimePos);
  }, [curWeekDates.length]);

  useEffect(() => {
    const week = CalenderUtils.weekDates(new Date());
    setCurWeekDates(week);
  }, []);

  console.log(pricePeriods);

  return (
    <SizingWrapper>
      <ScrollView ref={scrollViewRef}>
        <ContentWrapper className={"content-wrapper"}>
          <Sticky.Top
            distance={"0px"}
            style={{
              zIndex: 200
            }}
          >
            <CalenderHead visibleDates={curWeekDates} />
          </Sticky.Top>
          <div>
            <Sticky.Left
              distance={"0px"}
              style={{
                zIndex: 150,
                verticalAlign: "top"
              }}
            >
              <HourTimeCol />
            </Sticky.Left>
            <DaysView className={"day-voew"} ref={daysViewWrapperRef}>
              <CurTimeLine top={curTimeLinePos} />
              {curWeekDates.map((date, idx) => (
                <Day
                  key={`day-${idx}`}
                  visibleHours={visibleHours}
                  date={date}
                  pricePeriods={getPricePeriods(pricePeriods, date)}
                />
              ))}
            </DaysView>
          </div>
        </ContentWrapper>
      </ScrollView>
    </SizingWrapper>
  );
}
export default Calender;
