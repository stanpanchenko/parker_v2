import React from "react";
import { CSSTransition } from "react-transition-group";
import "./query-disclaimer-component.style.scss";

interface PropsI {
  visible: boolean;
}

export const QueryDisclaimerComponent = (props: PropsI) => {
  const { visible } = props;
  return (
    <CSSTransition
      classNames={"query-disclaimer-component-appear-animation"}
      timeout={400}
      in={visible}
    >
      <div className={"query-disclaimer-component"}>
        <div className={"query-component__content"}>
          Finden Sie einen Parkplaz neben einem Ort oder einer Straße.
        </div>
      </div>
    </CSSTransition>
  );
};
