import { SpecialPlaces } from "../../../../../../../redux/states/map/states/viewport/special-places";
import React from "react";
import {Popup} from "./components/popup/popup";

export const PopupsManager = () => {
  return (
    <div>
      <Popup
        longitude={SpecialPlaces.AACHEN_MARKT[0]}
        latitude={SpecialPlaces.AACHEN_MARKT[1]}
      >
        AACHEN
      </Popup>
    </div>
  );
};
