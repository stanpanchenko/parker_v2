import React, { ChangeEvent, KeyboardEvent, FocusEvent, useState } from "react";
import styled from "styled-components";
import { ThemePropsI } from "../../../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { GoogleIconComponent } from "../../../../../../../../../../../shared/components/google-icon-component";
import posed from "react-pose";

const InputStyled = styled.input`
    text-rendering: geometricPrecision;
  padding-left: 20px;
  background: transparent;
  border: none;
  outline: none;
  padding-top: 9px;
  color: ${({ theme }: ThemePropsI) => theme.textColorPrime}
  padding-bottom: 10px;
  font-size: 0.9rem;
  width: 100%;
  ::placeholder {
    color: white;
    opacity: 0.7;
  }
`;
const InputWrapper = styled.div`
  align-items: center;
  display: flex;
  width: 100%;
  background-color: ${({ theme }: ThemePropsI) => theme.inputBackgroundPrime};
  border-radius: ${({ theme }: ThemePropsI) => theme.radius.prime}px;
  border: 1px solid ${(props: ThemePropsI) => props.theme.borderColorSecond};
`;

const Wrapper = styled.div`
  padding: 5px;
  display: flex;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  align-items: center;
`;

const IconStyledWrapper = styled.span`
  pointer-events: none;
  color: ${({ theme }: ThemePropsI) => theme.textColorSecond};
  position: absolute;
`;

const RotateAnimation = posed(IconStyledWrapper)({
  init: {
    scaleX: 1,
    x: 8
  },
  rotate: {
    scaleX: 0
  },
  normal: {
    scaleX: 1
  }
});

const Placeholder = styled.span`
  position: absolute;
  left: 45px;
  pointer-events: none;
  color: ${({ theme }: ThemePropsI) => theme.inputPlaceholderColor};
`;

interface OwnProps {
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  onKeyDown: (e: KeyboardEvent<HTMLInputElement>) => void;
  value: string;
}

export default function AnimatedInput({
  value,
  onChange,
  onKeyDown
}: OwnProps) {
  const [animate, setAnimate] = useState<boolean>(false);
  function handleKeyDown(e: KeyboardEvent<HTMLInputElement>) {
    onKeyDown(e);
  }
  function handleInputChange(e: ChangeEvent<HTMLInputElement>) {
    onChange(e);
  }

  function handleInputFocus(e: FocusEvent<HTMLInputElement>) {
    setAnimate(true);
  }

  function handleInputBlur(e: FocusEvent<HTMLInputElement>) {
    if (value === "") setAnimate(false);
  }

  return (
    <Wrapper>
      <InputWrapper>
        <RotateAnimation pose={animate ? "rotate" : "normal"}>
          <GoogleIconComponent classNames={[""]} name={"search"} />
        </RotateAnimation>
        {!animate ? <Placeholder>Search</Placeholder> : null}

        <InputStyled
          onKeyDown={handleKeyDown}
          value={value}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          type={"text"}
          onChange={handleInputChange}
        />
      </InputWrapper>
    </Wrapper>
  );
}
