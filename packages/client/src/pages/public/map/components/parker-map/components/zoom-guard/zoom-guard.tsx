import React, { ReactNode } from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../../redux/states";
import { ViewportSelectors } from "../../../../../../../redux/states/map/states/viewport/selectors";
import { ZoomRange } from "../../../../../../../redux/states/ui/states/zoom-range/zoom-range.state-reducer";

interface ZoomGuardReduxPropsI {
  zoom: number;
}

interface ZoomGuardOwnPropsI extends ZoomGuardReduxPropsI {
  zoomRange: ZoomRange;
  children: ReactNode;
}

function ZoomGuard({ zoom, zoomRange, children }: ZoomGuardOwnPropsI) {
  return <>{zoom >= zoomRange[0] && zoom <= zoomRange[1] ? children : null}</>;
}
export default connect(
  (state: ApplicationStateI) => ({
    zoom: ViewportSelectors.viewportZoom(state)
  }),
  null
)(ZoomGuard);
