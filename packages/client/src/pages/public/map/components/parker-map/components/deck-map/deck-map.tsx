import React, { ReactNode } from "react";
import DeckGL from "@deck.gl/react";
import InteractiveMap, { Popup } from "react-map-gl";
import { ApplicationStateI } from "../../../../../../../redux/states";
import { Dispatch } from "redux";
import { FeatureCollectionActionCreators } from "../../../../../../../redux/states/feature-collection/actions/creators";
import { connect } from "react-redux";
import { MapboxLayer } from "@deck.gl/mapbox";
import { TilesSelectors } from "../../../../../../../redux/states/map/states/tiles/selectors";
import { throttle } from "../../../../../../../utils/helpers";
import env from "../../../../../../../env";
import { PopupsManager } from "../popup-manager/popup-manager";

interface IMapView {
  longitude: number;
  latitude: number;
  zoom: number;
  pitch: number;
  bearing: number;
}
interface IDeckMapReduxProps {
  tiles: string | object;
}
interface IDeckMapProps extends IDeckMapReduxProps {
  initialView: IMapView;
  getCursor: () => string;
  styleUrl: string;
  onViewChange: (view: any) => any;
  onClick?: (e: any) => void;
  onHover?: (e: any) => void;
  children: any;
}

const MAPBOX_TOKEN = env.MAP_BOX_TOKEN;

export class DeckMap extends React.Component<IDeckMapProps, any> {
  // DeckGL and mapbox will both draw into this WebGL context

  state: any = {};
  _map: any;
  _deck: any;

  // DeckGL and mapbox will both draw into this WebGL context
  _onWebGLInitialized = gl => {
    this.setState({ gl });
  };

  _onMapLoad = () => {
    const map = this._map;
    const deck = this._deck;

    map.addLayer(new MapboxLayer({ id: "my-scatterplot", deck }));
    document
      .getElementById("deckgl-wrapper")
      .addEventListener("contextmenu", this._allowRightClick);
  };

  _allowRightClick = e => {
    e.preventDefault();
  };

  _controller = {
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    keyboard: true,
    scrollZoom: true,
    dragPan: true,
    dragRotate: true,
    doubleClickZoom: true
  };

  render() {
    const { gl } = this.state;
    const {
      children,
      styleUrl,
      initialView,
      onViewChange,
      onClick,
      getCursor,
      onHover,
      tiles
    } = this.props;
    return (
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      <DeckGL
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        getCursor={getCursor}
        // @ts-ignore
        controller={this._controller}
        onClick={onClick ? onClick : null}
        onHover={onHover}
        onViewStateChanged={console.log}
        onViewStateChange={throttle(onViewChange, 50)}
        initialViewState={initialView}
        ref={ref => {
          // save a reference to the Deck instance
          // @ts-ignore
          this._deck = ref && ref.deck;
        }}
        onWebGLInitialized={this._onWebGLInitialized}
      >
        {gl && (
          <InteractiveMap
            height={"100vh"}
            width={"100vw"}
            ref={ref => {
              // save a reference to the mapboxgl.Map instance
              this._map = ref && ref.getMap();
            }}
            mapboxApiAccessToken={MAPBOX_TOKEN}
            onLoad={this._onMapLoad}
            gl={gl}
            mapStyle={tiles}
          >
            <PopupsManager />
          </InteractiveMap>
        )}
        {children}
      </DeckGL>
    );
  }
}
const props = (state: ApplicationStateI) => ({
  tiles: TilesSelectors.selectedTileSelector(state)
});

const actions = (dispatch: Dispatch) => ({
  fetchPointFeatures: () =>
    dispatch(FeatureCollectionActionCreators.fetchPointFeatures())
});

export default connect(
  props,
  actions
)(DeckMap);
