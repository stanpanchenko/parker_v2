import { ISODateString } from "../../../../../../../../api/graphql/api.interfaces";

export type Weekday =
  | "Monday"
  | "Tuesday"
  | "Wednesday"
  | "Thursday"
  | "Friday"
  | "Saturday"
  | "Sunday";
export const weekDays: Weekday[] = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
];

function startOfWeek(date: Date) {
  const diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
  const newDate = new Date(date.setDate(diff));
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  return newDate;
}

function endOfWeek(date: Date) {
  const lastday = date.getDate() - (date.getDay() - 1) + 6;
  const newDate = new Date(date.setDate(lastday));
  date.setHours(23);
  date.setMinutes(59);
  date.setSeconds(59);
  return newDate;
}

export function weekDates(date: Date): Date[] {
  const startWeekDate = startOfWeek(date);
  const week = [startWeekDate];
  for (let i = 1; i < 7; ++i) {
    const newDate = new Date();
    newDate.setDate(startWeekDate.getDate() + i);
    week.push(newDate);
  }
  return week;
}

function curWeekStartDate() {
  return startOfWeek(new Date());
}

function curWeekEndDate(): Date {
  return startOfWeek(new Date());
}

export function endOfCurrentWeekISODateString(): ISODateString {
  const date = curWeekEndDate();
  return date.toISOString();
}

export function startOfCurrentWeekISODateString(): ISODateString {
  const date = curWeekStartDate();
  return date.toISOString();
}

export function weekDayName(date: Date) {
  const normalizedDay = date.getDay() - 1;
  const dayIdx = normalizedDay === -1 ? 6 : normalizedDay;

  return weekDays[dayIdx];
}
