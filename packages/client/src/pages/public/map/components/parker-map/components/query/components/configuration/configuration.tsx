import React, { useState } from "react";
import styled from "styled-components";
import ToggleIcon from "../../../../../../../../../shared/components/toggle-icon/toggle-icon";
import { ThemePropsI } from "../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";

const IconWrapper = styled.span`
  display: flex;
  transition: background-color
    ${({ theme }: ThemePropsI) => theme.animation.times.medium};
  background-color: ${({ theme }: ThemePropsI) => theme.backgroundColorThird};
  &:hover {
    background-color: ${({ theme }: ThemePropsI) => theme.hoverPrime};
  }
  border-radius: 30px;
  height: 35px;
  width: 35px;
  align-items: center;
  justify-content: center;
  i {
    font-size: 15px;
  }
`;

const Wrapper = styled.div<any>``;

const IconsWrapper = styled.div`
  display: flex;
  font-size: 0.9rem;
  align-content: center;
  justify-content: center;
`;
export default function Configuration(props) {
  const [options, setOptions] = useState([
    {
      name: "directions_car",
      enabled: false
    },
    {
      name: "directions_bus",
      enabled: false
    },
    {
      name: "local_shipping",
      enabled: false
    },
    {
      name: "local_taxi",
      enabled: false
    },
    {
      name: "motorcycle",
      enabled: false
    }
  ]);
  function handleOptionClick(option) {}
  return (
    <Wrapper>
      <IconsWrapper>
        {options.map((option, idx) => {
          return (
            <div
              key={`option-${option.name}-${idx}`}
              style={{
                display: "inline-block",
                margin: "5px 5px 5px 5px",
                cursor: "pointer"
              }}
            >
              <IconWrapper>
                <ToggleIcon
                  name={option.name}
                  onStateChange={state =>
                    handleOptionClick({ name: option.name, state })
                  }
                />
              </IconWrapper>
            </div>
          );
        })}
      </IconsWrapper>
    </Wrapper>
  );
}
