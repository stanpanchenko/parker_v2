import React from "react";
import { Popup as MapboxPopup } from "react-map-gl";
import styled from "styled-components";

const StyledPopup = styled(MapboxPopup)`
  will-change: auto;
  z-index: 1;
  .mapboxgl-popup-content {
    position: relative;
    background: #fff;
    border-radius: 5px;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
    padding: 10px 10px 15px;
    pointer-events: auto;
  }
`;

export const Popup = props => {
  return <StyledPopup {...props} />;
};
