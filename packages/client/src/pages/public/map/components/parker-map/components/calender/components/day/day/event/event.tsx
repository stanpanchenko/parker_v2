import React from "react";
import styled from "styled-components";
import { Sticky } from "../../../../../../../../../../../shared/components/sticky";
import { DateTime } from "../../../../../../../../../../../api/graphql/graphql-api.interface";
import { ISODateString } from "../../../../../../../../../../../api/graphql/api.interfaces";

const EventStyle = styled.div`
  position: relative;
  width: 94%;
  height: 98.5%;
  left: 3%;
  background-color: #e3ebf6;
  border-radius: 8px;
  border: 1px solid #b1bfd3;
`;

const PriceStyle = styled.div`
  text-align: center;
  font-size: 12px;
  font-weight: 600;
  color: #223dcf;
`;
const TimeStyle = styled.div`
  font-size: 0.6rem;
  text-align: center;
  font-weight: 500;
  color: #7c98bf;
`;
const Event = ({
  price,
  startTime,
  endTime
}: {
  startTime: ISODateString;
  endTime: ISODateString;
  price: string;
}) => {
  const startTimeDate = new Date(startTime);
  const endTimeDate = new Date(endTime);

  const startHours = startTimeDate.getHours();
  const startMinutes = startTimeDate.getMinutes();

  const endHours = endTimeDate.getHours();
  const endMinutes = endTimeDate.getMinutes();

  // const starTimeString = `${startHours < 10 ? `0${startHours}` : startHours}:${
  //   startMinutes < 10 ? `0${startMinutes}` : startMinutes
  // }`;
  // const endTimeString = `${endHours < 10 ? `0${endHours}` : endHours}:${
  //   endMinutes < 10 ? `0${endMinutes}` : endMinutes
  // }`;

  const starTimeString = `${startHours < 10 ? `0${startHours}` : startHours}`;
  const endTimeString = `${endHours < 10 ? `0${endHours}` : endHours}`;

  return (
    <EventStyle>
      <Sticky.Top
        distance={"30px"}
        style={{
          width: "100%"
        }}
      >
        <div>
          <TimeStyle>{`${starTimeString}-${endTimeString}`}</TimeStyle>
          <PriceStyle>{price}</PriceStyle>
        </div>
      </Sticky.Top>
    </EventStyle>
  );
};

export default Event;
