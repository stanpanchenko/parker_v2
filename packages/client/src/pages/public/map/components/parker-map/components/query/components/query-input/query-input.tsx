import React, { KeyboardEvent, ChangeEvent, useState } from "react";
import { AnimatedInput } from "./components/animated-input";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../../../../redux/states";
import { SearchActionI } from "../../../../../../../../../redux/states/query/actions/interface/query.action-interfaces";
import { QueryActionCreators } from "../../../../../../../../../redux/states/query/actions/creators";

interface ReduxProps {
  search: (value: string) => SearchActionI;
}
interface OwnPropsI {}

function QueryInput({ search }: OwnPropsI & ReduxProps) {
  const [inputValue, setInputValue] = useState<string>("");

  function handleInputChange(e: ChangeEvent<HTMLInputElement>) {
    const value = e.target.value;
    setInputValue(value);
  }
  function handleKEyDown(e: KeyboardEvent<HTMLInputElement>) {
    if (e.key === "Enter" && inputValue !== "") {
      search(inputValue);
    }
  }
  return (
    <AnimatedInput
      onChange={handleInputChange}
      value={inputValue}
      onKeyDown={handleKEyDown}
    />
  );
}

const props = (state: ApplicationStateI) => ({});
const actions = dispatch => ({
  search: (value: string) => dispatch(QueryActionCreators.search(value))
});
export default connect(
  props,
  actions
)(QueryInput);
