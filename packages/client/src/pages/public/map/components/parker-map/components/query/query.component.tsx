import React from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../../redux/states";
import { QueryAnimated } from "./styled/QueryWrapper.styled";
import Configuration from "./components/configuration/configuration";
import { UserSelectors } from "../../../../../../../redux/states/user/selectors/user.selectors";
import { AnyAction, Dispatch } from "redux";
import { UserMapPosition } from "../../../../../../../redux/states/user/substates/position/reducer";
import { QueryUISelectors } from "../../../../../../../redux/states/ui/states/query/selectors";
import { QueryResultList } from "./components/query-results";
import styled from "styled-components";
import { QueryInput } from "./components/query-input";
import { QueryActionCreators } from "../../../../../../../redux/states/query/actions/creators";
import Backdrop from "../../../../../../../shared/components/blur/backdrop";
import IconButton from "../../../../../../../shared/components/icon-button/icon-button.component";
import { QueryUIActionCreators } from "../../../../../../../redux/states/ui/states/query/actions/creators";

const Wrapper = styled.div``;

const S = styled.div``;
interface OwnPropsI {}

interface ReduxPropsI {
  search: (value: string) => AnyAction;
  queryVisible: boolean;
  showQuery: () => AnyAction;
  userLocation: UserMapPosition;
  fetchSpacesWithAddresses: (value: string) => any;
  setSearchResults: (results: any[]) => AnyAction;
}

function QueryComponent({ showQuery, queryVisible }: ReduxPropsI & OwnPropsI) {
  function clearResultList() {}

  function handleInputChange(value: string) {}

  return (
    <>
      {!queryVisible ? (
        <IconButton
          round={true}
          onClick={showQuery}
          name={"search"}
          shadow={true}
        />
      ) : null}
      <QueryAnimated pose={queryVisible ? "visible" : "hidden"}>
        <Backdrop blur={"25px"} borderRadius={8} withShadow>
          <S>
            <Wrapper>
              <QueryInput />
            </Wrapper>
            <Configuration />
            <QueryResultList />
          </S>
        </Backdrop>
      </QueryAnimated>
    </>
  );
}

const props = (state: ApplicationStateI) => ({
  userLocation: UserSelectors.currentPosition(state),
  queryVisible: QueryUISelectors.queryVisible(state)
});
const actions = (dispatch: Dispatch) => ({
  showQuery: () => dispatch(QueryUIActionCreators.showQuery()),
  fetchSpacesWithAddresses: (address: string) =>
    dispatch({ type: "not implemented" }),
  setSearchResults: (results: any[]) => dispatch({ type: "not implemented" }),
  search: (query: string) => dispatch(QueryActionCreators.search(query))
});

export const Query = connect(
  props,
  actions
)(QueryComponent);
