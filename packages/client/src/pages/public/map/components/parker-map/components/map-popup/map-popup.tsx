import React from "react";
import styled from "styled-components";
import { CSSTransition } from "react-transition-group";
import "./map-pop-component.style.scss";
import { AnyAction } from "redux";
import { connect } from "react-redux";
import ThemeI from "../../../../../../../shared/providers/theme/theme.interface";
import { MessagesStateI } from "../../../../../../../redux/states/messages/messages.reducer";
import { Theme } from "../../../../../../../shared/providers/theme/Theme";
import { MessageActionCreators } from "../../../../../../../redux/states/messages/action-creators";
import { ApplicationStateI } from "../../../../../../../redux/states";
import { ActionType } from "../../../../../../../redux/states/dispatch-queue/dispatch-queue.reducer";
import { DispatchQueueActionCreators } from "../../../../../../../redux/states/dispatch-queue/action-creators";
import { RedispatchActionActionI } from "../../../../../../../redux/states/dispatch-queue/action-interfaces/dispatch-queue.actions-interface";
import { PopupDialogSelectors } from "../../../../../../../redux/states/ui/states/popup-dialog/selectors";
import { PopupDialogConfirmationI } from "../../../../../../../redux/states/ui/states/popup-dialog/popup-dialog.reducer";
import { PopupDialogActionCreators } from "../../../../../../../redux/states/ui/states/popup-dialog/actions/creators";

const MapBackground = styled("div")(
  {
    position: "fixed",
    backgroundColor: "#00000094",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
    zIndex: 200
  },
  props => ({})
);

const PopupAlertArea = styled("div")(
  {
    position: "absolute",
    width: "33vw",
    height: "33vh",
    minHeight: "300px",
    minWidth: "300px",
    left: "50%",
    top: "50%",
    zIndex: 300
    // transform: 'translate(-50%, -50%)',
  },
  props => ({})
);
interface StyledAlertWindowPropsI {
  theme: ThemeI;
}
const StyledAlertWindow = styled("div")(
  {
    borderRadius: "10px",
    position: "relative",
    padding: "10px",
    width: "100%",
    height: "80%",
    boxShadow:
      "0px 10px 33px 0px rgba(0,0,0,0.17), 0px 3px 5px 0px rgba(0,0,0,0.17)"
  },
  (props: StyledAlertWindowPropsI) => ({
    backgroundColor: props.theme.backgroundColor
  })
);
interface StyledButtonPropsI {
  theme: ThemeI;
}
const StyledButton = styled("button")(
  {
    width: "33%",
    height: "45px",
    margin: "1rem",
    fontSize: "1rem",
    letterSpacing: "1px",
    borderRadius: "30px",
    boxShadow:
      "0px 10px 33px 0px rgba(0,0,0,0.17), 0px 3px 5px 0px rgba(0,0,0,0.17)"
  },
  (props: StyledButtonPropsI) => ({
    backgroundColor: props.theme.backgroundColorSecond,
    color: props.theme.textColorPrime,
    border: `1px solid ${props.theme.borderColorPrime}`
  })
);

const ControllsArea = styled("div")(
  {
    display: "flex",
    justifyContent: "space-around"
  },
  props => ({})
);

interface PropsI {
  message: MessagesStateI;
  hideMessage: () => AnyAction;
  theme: Theme;
  actionToRedispatch: any;
  actionToConfirm: ActionType;
  redispatch: (actionType: ActionType) => RedispatchActionActionI;
  confirmation: PopupDialogConfirmationI | null;
  onConfirm: (action: AnyAction) => AnyAction;
  onDeny: (action: AnyAction) => AnyAction;
}

function MapPopup({ confirmation, onConfirm, onDeny }: PropsI) {
  function handleConfirmation() {
    onConfirm(confirmation.onConfirmAction);
  }
  function handleDeny() {
    onDeny(confirmation.onDenyAction);
  }

  // TODO: handle different action(types)
  return (
    <>
      <CSSTransition
        classNames={"fade-in-animation"}
        timeout={700}
        mountOnEnter={true}
        unmountOnExit={true}
        in={!!confirmation}
      >
        <MapBackground className={"popup-background"} />
      </CSSTransition>

      <CSSTransition
        classNames={"alert-section-animation"}
        timeout={700}
        in={!!confirmation}
        unmountOnExit={true}
        mountOnEnter={true}
      >
        <PopupAlertArea className={"alert-section"}>
          <>
            <StyledAlertWindow>window</StyledAlertWindow>

            <ControllsArea>
              <StyledButton onClick={() => handleConfirmation()}>
                OK
              </StyledButton>
              <StyledButton onClick={() => handleDeny()}>NOPE</StyledButton>
            </ControllsArea>
          </>
        </PopupAlertArea>
      </CSSTransition>
    </>
  );
}

const props = (state: ApplicationStateI) => ({
  message: state.message,
  theme: state.ui.theme.theme,
  actionToRedispatch: state.message.redispatchAction,
  actionToConfirm: "",
  confirmation: PopupDialogSelectors.confirmationDialog(state)
});

const actions = dispatch => ({
  hideMessage: () => dispatch(MessageActionCreators.hideMessage()),
  redispatch: (actionType: ActionType) =>
    dispatch(DispatchQueueActionCreators.reDispatch(actionType)),
  onConfirm: (action: AnyAction) =>
    dispatch(PopupDialogActionCreators.confirmAction(action)),
  onDeny: (action: AnyAction) =>
    dispatch(PopupDialogActionCreators.denyAction(action))
});

export default connect(
  props,
  actions
)(MapPopup);
