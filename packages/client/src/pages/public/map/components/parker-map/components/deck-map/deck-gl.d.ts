// @ts-ignore
declare module "@deck.gl/mapbox";
// @ts-ignore
declare module "@deck.gl/core";
// @ts-ignore
declare module "@deck.gl/layers";
// @ts-ignore
declare module "@deck.gl/react";

export interface MapClickEvent {
  color: Uint8Array | null;
  coordinate: [number, number];
  devicePixel: [number, number];
  index: number;
  layer: any;
  lngLat: [number, number];
  object: any;
  picked: boolean;
  pixel: [number, number];
  pixelRatio: number;
  x: number;
  y: number;
}
