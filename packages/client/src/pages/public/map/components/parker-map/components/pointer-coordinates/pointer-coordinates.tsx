import React from "react";

const PointerCoordinates = ({ pointerCoordinate }) => {
  return (
    <div
      style={{
        position: "absolute",
        top: `${pointerCoordinate.pixel[1] + 10}px`,
        left: `${pointerCoordinate.pixel[0] + 10}px`,
        fontSize: ".5rem",
        fontWeight: 600,
        backgroundColor: "white",
        padding: "4px",
        zIndex: 100,
        borderRadius: "5px",
        opacity: 0.5,
        letterSpacing: 0.1245,
        pointerEvents: "none"
      }}
    >
      <div>Lng: {pointerCoordinate.coordinate[0]}</div>
      <div>Lat: {pointerCoordinate.coordinate[1]}</div>
    </div>
  );
};
export default PointerCoordinates;
