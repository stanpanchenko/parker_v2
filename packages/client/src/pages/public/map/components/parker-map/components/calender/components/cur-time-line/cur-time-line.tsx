import React from "react";
import styled from "styled-components";
import { Sticky } from "../../../../../../../../../shared/components/sticky";

const CurrTimeLinePos = styled.div`
  height: 2px;
  position: absolute;
  left: 0;
  z-index: 100;
  right: 0;
`;

const LineStyle = styled.div`
  width: 100%;
  position: relative;
  background-color: #e42374;
  height: 1px;
  display: inline-block;
`;

const Dot = styled.div`
  position: relative;
  display: inline-block;
  width: 5px;
  z-index: 500;
  top: 2px;
  left: 0;
  height: 5px;
  background-color: #e42374;
  border-radius: 4px;
`;
const CurTimeLine = ({ top }) => {
  return (
    <CurrTimeLinePos
      style={{
        top: `${top}px`
      }}
    >
      <Sticky.Left
        distance={"25px"}
        style={{
          display: "inline-block"
        }}
      >
        <Dot />
      </Sticky.Left>
      <LineStyle />
    </CurrTimeLinePos>
  );
};

export default CurTimeLine;
