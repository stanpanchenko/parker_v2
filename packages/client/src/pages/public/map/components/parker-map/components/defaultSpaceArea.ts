import uuid from "uuid";

export const newIDPrefix = "new-";
export function newID(additionallTag?: string): string {
  const randomId = uuid();
  const randomIdParts = randomId.split("-");
  const partialId = randomIdParts[randomIdParts.length - 1];
  return `${newIDPrefix}${additionallTag ? additionallTag : ""}${partialId}`;
}
