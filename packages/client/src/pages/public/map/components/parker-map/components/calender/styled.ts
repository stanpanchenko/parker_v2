import styled from "styled-components";
import { ThemePropsI } from "../../../../../../../shared/providers/theme/interfaces/theme-props.interface";

export const HourHeight = styled.div<{ height: number }>`
  height: ${({ height }) => height}px;
`;

export const SizingWrapper = styled.div`
  height: 190px;
  overflow: hidden;
  white-space: nowrap;
  width: 250px;
  margin: 0 -20px 0 -15px;
  position: relative;
  border-bottom: 1px solid
    ${({ theme }: ThemePropsI) => theme.borderColorSecond};
`;

export const ContentWrapper = styled.div`
  position: relative;
  display: inline-block;
`;

export const DaysView = styled.div`
  display: inline-block;
  vertical-align: top;
  white-space: nowrap;
  position: relative;
`;

export const ScrollView = styled.div`
  overflow: scroll;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  ::-webkit-scrollbar {
    width: 0;
    height: 4px;
  }

  ::-webkit-scrollbar-track {
    background: #e2e2e2;
  }

  ::-webkit-scrollbar-thumb {
    background: #b0b0b0;
    border-radius: 8px;
  }

  ::-webkit-scrollbar-thumb:hover {
  }
`;
