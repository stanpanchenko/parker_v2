import { useEffect, useState } from "react";

export function useCurrTimeInMin() {
  const [curTimeInMin, setCurTimeInMin] = useState(undefined);
  function getTime() {
    const date = new Date();
    const time =
        date.getHours() * 60 /*curr time hour in min*/ +
        date.getMinutes() /*curr time in min*/;
    return time;
  }

  useEffect(() => {
    const time = getTime();
    setCurTimeInMin(time);
  }, []);

  useEffect(() => {
    let timeIntervalID: any = -1;
    timeIntervalID = setInterval(getTime, 1000 * 60 /*every min*/);
    return () => {
      clearInterval(timeIntervalID);
    };
  }, []);
  return curTimeInMin || getTime();
}
