import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../../../../redux/states";
import { AnyAction, Dispatch } from "redux";
import { IViewport } from "../../../../../../../../../redux/states/map/states/viewport/viewport.state-reducer";
import { ViewportActionCreators } from "../../../../../../../../../redux/states/map/states/viewport/actions/action-creators";
import styled from "styled-components";
import LoadingDotsComponent from "../../../../../../../../../shared/components/loading-dots/loading-dots.component";
import { QueryResultI } from "../../../../../../../../../redux/states/query/query.state-reducer";
import AnimatedUL from "./components/animated-ul/animated-ul";

const LoadingWrapper = styled.div``;

interface ReduxPropsI {
  fetching: boolean;
  results: any[];
}
function SearchResultList({ results, fetching }: ReduxPropsI) {
  // const [fetched, setFetching] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<QueryResultI[]>([]);

  useEffect(() => {
    // results array needs to be [] for a moment to re-init UL animation and its height
    setSearchResult([]);
    setTimeout(() => {
      setSearchResult(results);
    }, 70);
  }, [results]);

  return (
    <>
      {fetching ? (
        <LoadingWrapper>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              padding: "15px"
            }}
          >
            <LoadingDotsComponent />
          </div>
        </LoadingWrapper>
      ) : (
        <AnimatedUL listElements={searchResult} />
      )}
    </>
  );
}

const props = (state: ApplicationStateI) => ({
  spaces: [],
  results: [],
  fetching: false
});

const actions = (dispatch: Dispatch) => ({});

export default connect(
  props,
  actions
)(SearchResultList);
