import ThemeI from "../../../../../../../../shared/providers/theme/theme.interface";
import styled from "styled-components";
import media from "../../../../../../../../shared/media-queries";
import posed from "react-pose";
import { ThemePropsI } from "../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";

interface PropsI {
  theme: ThemeI;
}
const QueryWrapperStyled = styled.div`
  position: absolute;
  left: 10px;
  top: 10px;
  z-index: 500;

  width: 30vw;
  will-change: opacity, transform, top;
  @media ${media.mobile} {
    width: 95vw;
  }
  @media ${media.iPhone6_7_8} {
    width: 50vw;
  }

  @media ${media.tablet} {
    width: 35vw;
  }
  @media ${media.desktop} {
    width: 25vw;
  }
`;

export const QueryBG = styled.div`
  border-radius: 8px;
  background-color: ${({ theme }: ThemePropsI) =>
    theme.backgroundColorSecondTransparent};
  box-shadow: ${({ theme }: ThemePropsI) => theme.shadowPrime};
`;

export const QueryAnimated = posed(QueryWrapperStyled)({
  hidden: {
    opacity: 0,
    y: "-100%",
    scale: 0.7
  },
  visible: {
    opacity: 1,
    y: "0%",
    scale: 1
  }
});
