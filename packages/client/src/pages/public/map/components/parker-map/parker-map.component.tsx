import React, { useCallback, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { CSSTransition } from "react-transition-group";
import { ApplicationStateI } from "../../../../../redux/states";
import { MapPopup } from "./components/map-popup";
import { MapBoundsRectI } from "../../../../../shared/Interfaces/MapBoundsRect.interface";
import DeckMap from "./components/deck-map/deck-map";
import { AnyAction, Dispatch } from "redux";
import { FeatureCollectionActionCreators } from "../../../../../redux/states/feature-collection/actions/creators";
import { FeatureCollectionSelectors } from "../../../../../redux/states/feature-collection/selectors";
import { EditableGeoJsonLayer } from "@nebula.gl/layers";
import { PathLayer, PolygonLayer, ScatterplotLayer } from "@deck.gl/layers";
import { EditModeType } from "../../../../../redux/states/application/application.state-reducer";
import { ApplicationSelectors } from "../../../../../redux/states/application/selectors";
import {
  Feature,
  GeometryType
} from "../../../../../api/graphql/graphql-api.interface";

import {
  INITIAL_VIEW_STATE,
  IViewport
} from "../../../../../redux/states/map/states/viewport/viewport.state-reducer";
import { ViewportActionCreators } from "../../../../../redux/states/map/states/viewport/actions/action-creators";
import { MapActionCreators } from "../../../../../redux/states/map/actions/creators";
import { MapClickEvent } from "./components/deck-map/deck-gl";
import "./map.style.scss";

const mapStyle = [
  "mapbox://styles/stanis1992/ck0l708861x8m1cpqhi1l0p4p",
  "mapbox://styles/stanis1992/ck10z08y407qf1cmlwfh9h2sz"
];

function getLocation(): Promise<{ lat: number; lng: number }> {
  return new Promise((res, rej) => {
    const geoLocator = navigator.geolocation;
    geoLocator &&
      geoLocator.getCurrentPosition(
        pos => {
          res({
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          });
        },
        err => {
          rej(err);
        },
        {
          timeout: 15000,
          enableHighAccuracy: true,
          maximumAge: 1000 * 60
        }
      );
  });
}

function getBoundsRect(e): MapBoundsRectI {
  const target = e.target;
  const bounds = target.getBounds();
  const center = bounds.getCenter();
  const southEast = bounds.getSouthEast();
  const southWest = bounds.getSouthWest();

  const northEast = bounds.getNorthEast();
  const northWest = bounds.getNorthWest();

  const mapBoundsRect: MapBoundsRectI = {
    center: center,
    southEast: southEast,
    southWest: southWest,

    northEast: northEast,
    northWest: northWest
  };
  return mapBoundsRect;
}

interface IParkerMapProps {}
interface IParkerMapReduxProps {
  onViewChange: (view: IViewport) => AnyAction;
  fetchAllFeatures: () => AnyAction;
  featureCollection: any;
  inEditMode: boolean;
  editModeType: EditModeType;
  saveNewFeature: (feature: any) => AnyAction;
  polygonFeatures: any[];
  pointFeatures: any[];
  lineStringFeatures: any[];
  selectedFeature: Feature | undefined;
}

const spaceSizeFactor = 0.000009; // 1 meter
enum EditTypes {
  addFeature = "addFeature",
  movePosition = "movePosition",
  addPosition = "addPosition",
  removePosition = "removePosition",
  finishMovePosition = "finishMovePosition",
  scaling = "scaling",
  scaled = "scaled",
  rotating = "rotating",
  rotated = "rotated",
  translating = "translating",
  translated = "translated",
  startExtruding = "startExtruding",
  extruding = "extruding",
  extruded = "extruded",
  split = "split"
}

function ParkerMap({
  fetchAllFeatures,
  pointFeatures,
  polygonFeatures,
  lineStringFeatures,
  saveNewFeature,
  featureCollection,
  inEditMode,
  editModeType,
  onViewChange
}: IParkerMapProps & IParkerMapReduxProps) {
  const dispatch = useDispatch();

  const dispatchMapClickPosition = (e: MapClickEvent) =>
    dispatch(MapActionCreators.mapClick(e));

  useEffect(() => {
    fetchAllFeatures();
    getLocation()
      .then(location => {
        console.log("user location found: ", location);
      })
      .catch(err => {
        console.error("[Map]: Location: ", err);
      });
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleResize = (e: any) => {};

  const handleOnEdit = ({
    updatedData: featureCollection,
    editType,
    editContext
  }) => {
    if (editType === EditTypes.addFeature) {
      saveNewFeature(featureCollection.features[editContext.featureIndexes[0]]);
    }
  };

  const handleMapClick = (e: MapClickEvent) => {
    dispatchMapClickPosition(e);
  };

  const getCursor = () => {
    return editModeType === EditModeType.view ? "grab" : "crosshair";
  };

  const handlePolygoOnHoverEvent = e => {};
  const handleLineStringOnHoverEvent = e => {};
  const handlePointOnHoverEvent = e => {};

  const handleHoverOnFeature = e => {
    const { type: geometryType } = e.object.geometry;
    switch (geometryType) {
      case GeometryType.Polygon: {
        handlePolygoOnHoverEvent(e);
        break;
      }
      case GeometryType.Point: {
        handlePointOnHoverEvent(e);
        break;
      }
      case GeometryType.LineString: {
        handleLineStringOnHoverEvent(e);
        break;
      }
      default:
        break;
    }
  };
  const handleMouseHoverOverMap = e => {
    if (e.coordinate && e.pixel) {
      // throttle(
      //   setPointerCoordinates({
      //     coordinate: e.coordinate,
      //     pixel: e.pixel
      //   }),
      //   15
      // );
    }
    if (e.object) {
      handleHoverOnFeature(e);
    }
  };

  const handleViewChange = useCallback((view: any) => {
    const { longitude, latitude, zoom, pitch, bearing } = view.viewState;
    onViewChange({
      longitude,
      latitude,
      zoom,
      pitch,
      bearing
    });
  }, []);

  return (
    <CSSTransition
      classNames={"map-animation"}
      appear={true} // on mount
      in
      timeout={70}
    >
      <div className={"map"} id={"map-container"}>
        {/*{inEditMode && (*/}
        {/*  <PointerCoordinates pointerCoordinate={pointerCoordinate} />*/}
        {/*)}*/}
        <DeckMap
          onHover={handleMouseHoverOverMap}
          getCursor={getCursor}
          onClick={handleMapClick}
          onViewChange={(view: any) => handleViewChange(view)}
          styleUrl={mapStyle[0]}
          initialView={INITIAL_VIEW_STATE}
        >
          <PolygonLayer
            id={'polygon-layer'}
            getFillColor={(feature: any) =>
              feature.properties.color || [103, 103, 103]
            }
            getPolygon={(feature: any) => feature.geometry.coordinates}
            pickable={true}
            onClick={(e: any) => {
              console.log("poly click: ", e);
            }}
            stroked={true}
            filled={true}
            wireframe={true}
            data={polygonFeatures}
          />

          <PathLayer
            id={"line-layer"}
            getColor={feature => [210, 0, 210]}
            getWidth={2}
            pickable={true}
            widthScale={2}
            widthMinPixels={2}
            getPath={feature => feature.geometry.coordinates}
            data={lineStringFeatures}
          />
          {inEditMode ? (
            <EditableGeoJsonLayer
              id={"geojson-layer"}
              data={featureCollection}
              mode={editModeType || EditModeType.view}
              selectedFeatureIndexes={[]}
              onEdit={handleOnEdit}
            />
          ) : null}

          <ScatterplotLayer
            getColor={(data: any) => {
              return [255, 0, 0];
            }}
            pickable={true}
            onClick={(e: any) => {
              console.log("click");
            }}
            getRadius={(feature: any) => feature.properties.radius || 1}
            getPosition={feature => feature.geometry.coordinates}
            data={pointFeatures}
          />
        </DeckMap>
        <MapPopup />
      </div>
    </CSSTransition>
  );
}

const props = (state: ApplicationStateI) => ({
  selectedFeature: FeatureCollectionSelectors.selectedFeatureSelector(state),
  inEditMode: ApplicationSelectors.inAddingMode(state),
  editModeType: ApplicationSelectors.editModeTypeSelector(state),
  featureCollection: FeatureCollectionSelectors.featureCollectionSelector(
    state
  ),
  polygonFeatures: FeatureCollectionSelectors.denormalizedPolygonFeatures(
    state
  ),
  lineStringFeatures: FeatureCollectionSelectors.denormalizedLineStringFeatures(
    state
  ),
  pointFeatures: FeatureCollectionSelectors.denormalizedPointFeatures(state)
});
const actions = (dispatch: Dispatch) => ({
  onViewChange: (view: IViewport) =>
    dispatch(ViewportActionCreators.setView(view)),
  fetchAllFeatures: () =>
    dispatch(FeatureCollectionActionCreators.fetchAllFeatures()),
  saveNewFeature: (feature: any) =>
    dispatch(FeatureCollectionActionCreators.saveNewFeature(feature))
});

export default connect(
  props,
  actions
)(ParkerMap);
