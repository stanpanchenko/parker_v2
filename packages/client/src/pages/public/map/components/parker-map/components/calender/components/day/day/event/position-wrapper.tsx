import React, { ReactNode } from "react";

const EventPositionWrapper = ({
  top,
  height,
  children
}: {
  children: ReactNode;
  top: number;
  height: number;
}) => {
  return (
    <div
      style={{
        position: "absolute",
        top: `${top}px`,
        height: `${height}px`,
        width: "100%"
      }}
    >
      {children}
    </div>
  );
};

export default EventPositionWrapper;
