import React from "react";
import styled from "styled-components";
import { hourHeight } from "../../calender";
import { ThemePropsI } from "../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { HourHeight } from "../../styled";

const HourLabel = styled.span`
  opacity: 0;
`;

const Border = styled.div`
  border-bottom: 1px solid
    ${({ theme }: ThemePropsI) => theme.borderColorSecond};
    box-sizing: border-box;,
`;

interface DayPropsI {
  hoursCount: number;
}

function HoursColumn({ hoursCount }: DayPropsI) {
  const hours = Array.from(Array(hoursCount)).map((_, idx) => idx);
  return (
    <div className={"hours-container"}>
      {hours.map((h, idx) => (
        <Border
          key={`hour-${idx}`}
          style={{
            height: `${hourHeight}px`
          }}
        >
          <HourLabel>_</HourLabel>
        </Border>
      ))}
    </div>
  );
}
{
  /*<Border key={`hour-${idx}`}>*/
}
{
  /*</Border>*/
}
export default HoursColumn;
