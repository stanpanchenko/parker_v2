import React, { useEffect, useRef } from "react";
import HoursColumn from "../hours-column";
import styled from "styled-components";
import { dayWidth, hourHeight } from "../../../calender";
import { Event } from "./event";
import { CalenderUtils } from "../../../utils";
import { PricePeriod } from "../../../../../../../../../../api/graphql/graphql-api.interface";
import EventPositionWrapper from "./event/position-wrapper";
import { ISODateString } from "../../../../../../../../../../api/graphql/api.interfaces";

const DayWrapper = styled.div`
  position: relative;
  display: inline-block;
`;

type Minutes = number;
interface PositionDimension {
  top: number;
  height: number;
}
function positionPricePeriod(
  containerHeight: number,
  hoursCount: number,
  startTime: ISODateString,
  endTime: ISODateString
): PositionDimension {
  const startTimeDate = new Date(startTime);
  const endTimeDate = new Date(endTime);
  const start =
    (containerHeight / hoursCount / 60) * startTimeDate.getHours() * 60 +
    startTimeDate.getMinutes();
  const end =
    (containerHeight / hoursCount / 60) * endTimeDate.getHours() * 60 +
    endTimeDate.getMinutes();
  const height = end - start;
  return {
    top: start,
    height: height
  };
}

function getEventsDim(
  dayWrapperHeight: number,
  pricePeriods: PricePeriod[]
): PositionDimension[] {
  const eventsDim = [];
  pricePeriods.forEach(period => {
    const wrapperHeight = dayWrapperHeight;

    const eventDim = positionPricePeriod(
      wrapperHeight,
      24,
      period.startTime,
      period.endTime
    );
    eventsDim.push(eventDim);
  });
  return eventsDim;
}

interface DayPopsI {
  pricePeriods: PricePeriod[];
  visibleHours: number;
  date: Date;
}
const Day = ({ date, pricePeriods, visibleHours }: DayPopsI) => {
  const dayWrapperRef = useRef<HTMLDivElement>();
  useEffect(() => {}, [pricePeriods]);

  return (
    <DayWrapper
      ref={dayWrapperRef}
      style={{
        width: `${dayWidth}px`
      }}
    >
      {pricePeriods.map(({ startTime, endTime, price, priceUnit }, idx) => {
        const position = positionPricePeriod(
          visibleHours * hourHeight,
          visibleHours,
          startTime,
          endTime
        );

        return (
          <EventPositionWrapper
            key={`event-${CalenderUtils.weekDayName(date)}-${idx}`}
            {...position}
          >
            <Event
              price={`$${price / 100}/${priceUnit}`}
              startTime={startTime}
              endTime={endTime}
            />
          </EventPositionWrapper>
        );
      })}
      <HoursColumn hoursCount={visibleHours} />
    </DayWrapper>
  );
};

export default Day;
