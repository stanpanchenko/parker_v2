import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { dayWidth } from "../../calender";
import { ThemePropsI } from "../../../../../../../../../shared/providers/theme/interfaces/theme-props.interface";
import { weekDays } from "../../utils/dates.utils";
import { CalenderUtils } from "../../utils";
import { useTranslation } from "react-i18next";

const HeadWrapper = styled.div`
  padding: 4px 0 4px 0;
  display: flex;
  border-bottom: 1px solid
    ${({ theme }: ThemePropsI) => theme.borderColorSecond};
  border-top: 1px solid ${({ theme }: ThemePropsI) => theme.borderColorSecond};
  margin-bottom: 10px;
  background-color: ${({ theme }: ThemePropsI) => theme.backgroundColorSecond};
`;
const DayNameWrapper = styled.div`
  display: inline-block;
  text-align: center;
`;
const DaysWrapper = styled.div`
  display: inline-block;
`;

const TimeLineWrapper = styled.div`
  position: sticky;
  // background-color: ${({ theme }: ThemePropsI) =>
    theme.backgroundColorSecond};
  left: 0px;
  display: inline-block;
  width: 25px;
  text-align: center;
`;
const HeadValueFont = styled.div`
  font-size: 11px;
  font-weight: 500;
  color: ${({ theme }: ThemePropsI) => theme.textColorSecond};
`;

const DayDateStyle = styled.div`
  font-size: 9px;
  color: #969696;
`;

interface HeadPropsI {
  visibleDates: Date[];
}
const CalenderHead = ({ visibleDates }: HeadPropsI) => {
  const { t } = useTranslation();
  return (
    <HeadWrapper>
      <TimeLineWrapper>
        {/*<HeadValueFont>Time</HeadValueFont>*/}
      </TimeLineWrapper>
      <DaysWrapper>
        {visibleDates.map((date, idx) => {
          const dayName = CalenderUtils.weekDayName(date);
          const monthdate = date.getDate();
          return (
            <DayNameWrapper
              style={{
                width: `${dayWidth}px`
              }}
              key={`day-${dayName}-${idx}`}
            >
              <HeadValueFont>
                <div
                  style={{
                    color:
                      CalenderUtils.weekDayName(new Date()) === dayName
                        ? "blue"
                        : null
                  }}
                >
                  {t(dayName).slice(0, 3)}
                </div>
              </HeadValueFont>
            </DayNameWrapper>
          );
        })}
      </DaysWrapper>
    </HeadWrapper>
  );
};

export default CalenderHead;
