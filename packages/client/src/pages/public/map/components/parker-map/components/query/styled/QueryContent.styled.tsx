import styled from "styled-components";

interface PropsI {
  inputFocused: boolean;
}

const QueryContentStyled = styled("div")`
  transition: padding 0.4s cubic-bezier(0.175, 0.885, 0.32, 1.275);
  padding: ${(props: PropsI) => (props.inputFocused ? "10px" : 0)};
`;
export default QueryContentStyled;
