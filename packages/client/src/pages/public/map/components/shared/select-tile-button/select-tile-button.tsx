import React, { ReactNode } from "react";
import { TileName } from "../../../../../../redux/states/map/states/tiles/tiles-reducer";
import { useDispatch } from "react-redux";
import { TilesActionCreators } from "../../../../../../redux/states/map/states/tiles/actions/creators";
import Button from "../../../../../../shared/components/button/button";

interface IProps {
  tileName: TileName;
  children: ReactNode;
}

const SelectTileButton = ({ tileName, children }: IProps) => {
  const dispatch = useDispatch();
  return (
    <Button
      active={true}
      onClick={() => dispatch(TilesActionCreators.selectTileName(tileName))}
    >
      {children}
    </Button>
  );
};

export default SelectTileButton;
