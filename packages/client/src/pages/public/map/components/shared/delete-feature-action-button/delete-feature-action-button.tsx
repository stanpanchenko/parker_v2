import React, { ReactNode } from "react";
import Button from "../../../../../../shared/components/button/button";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../redux/states";
import { FeatureCollectionActionCreators } from "../../../../../../redux/states/feature-collection/actions/creators";
import { AnyAction } from "redux";

interface IProps {
  ids: string[];
  onDelete: (ids: string[]) => AnyAction;
  children: ReactNode;
}

const _DeleteFeaturesActionButton = ({ ids, onDelete, children }: IProps) => {
  const handleDelete = () => {
    onDelete(ids);
  };

  return (
    <Button active={true} onClick={handleDelete}>
      {children}
    </Button>
  );
};

const props = (state: ApplicationStateI) => ({});
const action = dispatch => ({
  onDelete: (ids: string[]) =>
    dispatch(FeatureCollectionActionCreators.deleteFeatures(ids))
});

const DeleteFeaturesActionButton = connect(
  props,
  action
)(_DeleteFeaturesActionButton);

export default DeleteFeaturesActionButton;
