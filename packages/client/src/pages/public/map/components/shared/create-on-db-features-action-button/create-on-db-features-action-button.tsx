import React from "react";
import { connect } from "react-redux";
import { ApplicationStateI } from "../../../../../../redux/states";
import { FeatureCollectionActionCreators } from "../../../../../../redux/states/feature-collection/actions/creators";
import Button from "../../../../../../shared/components/button/button";

const _CreateOnDBFeaturesActionButton = ({
  createOnDBFeaturesAction,
  children
}) => {
  return (
    <Button active={false} onClick={createOnDBFeaturesAction}>
      {children}
    </Button>
  );
};
const props = (state: ApplicationStateI) => ({});

const actions = dispatch => ({
  createOnDBFeaturesAction: () =>
    dispatch(FeatureCollectionActionCreators.signalCraeteOnDBFeaturesAction())
});
const CreateOnDBFeaturesActionButton = connect(
  props,
  actions
)(_CreateOnDBFeaturesActionButton);
export default CreateOnDBFeaturesActionButton;
