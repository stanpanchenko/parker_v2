import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import { ApplicationActionCreators } from "../../../../../redux/states/application/actions/creators";
import {ApplicationSelectors} from "../../../../../redux/states/application/selectors";

const ToggleFetchAddressOnClickFlag = () => {
  const enabled = useSelector(ApplicationSelectors.fetchAddressOnClickFlagSelector);
  const dispatch = useDispatch();
  
  const toggleFlag = () => {
    dispatch(ApplicationActionCreators.setFetchAddressOnClickFlag(!enabled));
  };
  return <button style={{
    color: enabled ? 'blue' : 'black'
  }} onClick={toggleFlag}>Guess Address</button>;
};

export default ToggleFetchAddressOnClickFlag;
