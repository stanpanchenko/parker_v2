import React from "react";
import { RightControlls } from "../../../../../components/right-controlls/right-controlls.component";
import ToggleEditModeButton from "../edit-mode-changers/enable-edit-mode/toggle-edit-mode";
import { ApplicationStateI } from "../../../../../redux/states";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { ApplicationSelectors } from "../../../../../redux/states/application/selectors";
import { Toast } from "../../../../../components/toast";
import { Query } from "../parker-map/components/query/query.component";
import EditModeChangers from "../edit-mode-changers/edit-mode-changers";
import FeatureEditSidebar from "../feature-edit-sidebar/feature-edit-sidebar";
import { FeatureCollectionSelectors } from "../../../../../redux/states/feature-collection/selectors";
import { Feature } from "../../../../../api/graphql/graphql-api.interface";

interface IMapControlsReduxProps {
  selectedFeature: Feature | undefined;
}

interface IMapControlsProps extends IMapControlsReduxProps {
  inEditingMode: boolean;
}

const _MapControls = ({
  inEditingMode,
  selectedFeature
}: IMapControlsProps) => {
  // TODO: add PosedGroup to animate elements on mount and unmount
  // TODO: mount and unmount UI elements instead of moving them out of screen
  console.log("selectedFeature: ", selectedFeature);
  return (
    <>
      <Toast />
      <div
        style={{
          position: "absolute",
          top: "35px",
          left: "35px",
          zIndex: 100,
          transition: "transform 400ms cubic-bezier(0.075, 0.82, 0.165, 1)",
          transform: `translateX(${
            selectedFeature && inEditingMode ? 0 : "-160%"
          })`
        }}
      >
        <FeatureEditSidebar />
      </div>
      <div
        style={{
          position: "absolute",
          top: "35px",
          left: "35px",
          zIndex: 100,
          transition: "transform 400ms cubic-bezier(0.075, 0.82, 0.165, 1)",
          transform: `translateY(${inEditingMode ? "-160%" : 0})`
        }}
      >
        <Query />
      </div>
      <div
        style={{
          position: "absolute",
          top: "-100px",
          left: "50%",
          transition: "transform 400ms cubic-bezier(0.075, 0.82, 0.165, 1)",
          transform: `translateX(-50%) translateY(${
            inEditingMode ? "120px" : 0
          })`
        }}
      >
        <EditModeChangers />
      </div>
      <div
        style={{
          position: "absolute",
          top: "5px",
          transition: "transform 400ms cubic-bezier(0.075, 0.82, 0.165, 1)",
          transform: `translateX(${inEditingMode ? "120%" : 0})`,
          right: "20px",
          zIndex: 100
        }}
      >
        <RightControlls />
      </div>
      <div
        style={{
          position: "absolute",
          transition: "right 400ms",
          right: "20px",
          bottom: "20px"
        }}
      >
        <ToggleEditModeButton />
      </div>
    </>
  );
};

const props = (state: ApplicationStateI) => ({
  inEditingMode: ApplicationSelectors.inAddingMode(state),
  selectedFeature: FeatureCollectionSelectors.selectedFeatureSelector(state)
});
const actions = (dispatch: Dispatch) => ({
  // enableEditMode: ()=> dispatch(ApplicationActionCreators.enableEditMode())
});

const MapControls = connect(
  props,
  actions
)(_MapControls);
export default MapControls;
