import React from "react";
import ParkerMap from "./components/parker-map/parker-map.component";
import { ApplicationStateI } from "../../../redux/states";
import { AnyAction, Dispatch } from "redux";
import { MessageActionCreators } from "../../../redux/states/messages/action-creators";
import { AuthActionsCreators } from "../../../redux/states/user/substates/authentication/actions";
import { RouteComponentProps, withRouter } from "react-router";
import { connect } from "react-redux";
import { MessagesStateI } from "../../../redux/states/messages/messages.reducer";
import { ApplicationSidebar } from "../../../components/application-sidebar/application-sidebar.component";
import { ToastMessage } from "../../../redux/states/ui/states/toast/toast.state-reducer";
import { ToastActionCreators } from "../../../redux/states/ui/states/toast/actions/creators";
import { ShowToastActionI } from "../../../redux/states/ui/states/toast/actions/interfaces/toast.action-interfaces";
import MapControls from "./components/map-controls/map-controls";

interface PropsI extends RouteComponentProps {
  message: MessagesStateI;
  showQuery: () => AnyAction;
  hideQuery: () => AnyAction;
  showMessage: (message: string) => AnyAction;
  hideMessage: () => AnyAction;
  setAuthentication: (value: boolean) => AnyAction;
  showToast: (message: ToastMessage) => ShowToastActionI;
}

function MapPage({ showToast, hideMessage, showMessage, message }: PropsI) {
  return (
    <>
      <ParkerMap />
      <ApplicationSidebar />
      <MapControls />
      <button
        style={{
          position: "absolute",
          bottom: "20px",
          left: "20px",
          zIndex: 301
        }}
        onClick={() => {
          if (message.hasMessage) {
            hideMessage();
          } else {
            showMessage("test message from App.tsx");
          }
        }}
      >
        Toggle Popup
      </button>
    </>
  );
}
const props = (state: ApplicationStateI) => ({
  message: state.message
});

const actions = (dispatch: Dispatch) => ({
  hideQuery: () => dispatch({ type: "not implemented" }),
  showQuery: () => dispatch({ type: "not implemented" }),

  showMessage: (message: string) =>
    dispatch(MessageActionCreators.showMessage(message)),
  hideMessage: () => dispatch(MessageActionCreators.hideMessage()),
  setAuthentication: (isAuth: boolean) =>
    dispatch(AuthActionsCreators.setAuth(isAuth)),
  showToast: (message: ToastMessage) =>
    dispatch(ToastActionCreators.showToast(message))
});
export default withRouter(
  connect(
    props,
    actions
  )(MapPage)
);
