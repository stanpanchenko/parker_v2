import React from "react";
import styled from "styled-components";

type Left = "left";
type Right = "right";
type Center = "center";

interface StyledComponentPropsI {
  color?: string;
  align: Left | Right | Center | null;
}

const StyledParagraph = styled("p")(
  {
    fontSize: "1.3rem",
    lineHeight: 1.33349,
    fontWeight: 400,
    letterSpacing: ".009em"
  },
  (props: StyledComponentPropsI) => ({
    color: props.color,
    textAlign: props.align ? props.align : null
  })
);

interface PropsI {
  children: any;
  classNames?: string[];
}
const Paragraph = (props: PropsI) => {
  return (
    <p className={props.classNames ? props.classNames.join(" ") : null}>
      {props.children}
    </p>
  );
};

export default Paragraph;
