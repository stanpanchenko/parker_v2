import React from "react";
import styled from "styled-components";

const DivStyled = styled("div")(
  {
    opacity: 0.1,
    height: "400px",
    position: "relative",
    backgroundSize: "cover",
    backgroundColor: "white"
  },
  (props: { url: string }) => ({
    backgroundImage: props.url ? `url(${props.url})` : null
  })
);

interface PropsI {
  children: any;
  url: string;
}
const ImageIntro = (props: PropsI) => {
  return (
    <section style={{ position: "relative" }}>
      <DivStyled url={props.url} />
      {props.children}
    </section>
  );
};
export default ImageIntro;
