import React from "react";
import "./about-page-component.style.scss";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router";
import ImageIntro from "./components/image-intro-component/image-intro.component";
import styled from "styled-components";
import Footer from "./components/footer/footer";
import PageFadeinAnimation from "../../../animations/Fadein/page-fadein-animation";
import PageNavigation from "../../shared/components/page-navigation-component/page-navigation.component";
import SwitchColor from "../../../shared/components/SwitchColor/SwitchColor";
import P from "../../../shared/components/p/P";
import Link from "../../shared/components/link/Link";

const LiStyled = styled("li")({
  maxWidth: "400px"
});

const UlStyled = styled("ul")({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  paddingLeft: "4vw",
  paddingRight: "4vw"
});

const IntroStyled = styled("div")({
  position: "absolute",
  top: "55px",
  left: "50%",
  transform: "translate(-50%)"
});

interface PropsI {}

interface StateI {}

class AboutPageComponent_ extends React.Component<
  PropsI & RouteComponentProps,
  StateI
> {
  state = {};

  componentDidMount() {}

  writeToState(obj) {
    this.setState(s => ({
      ...s,
      ...obj
    }));
  }

  render() {
    return (
      <PageFadeinAnimation>
        <div className={"about-page-component"}>
          <PageNavigation
            navigation={[
              {
                url: `${this.props.match.url}/parker`,
                text: "Parker"
              },
              {
                url: `${this.props.match.url}/organisation`,
                text: "Organisation"
              },
              {
                url: `${this.props.match.url}/menschen`,
                text: "Menschen"
              }
            ]}
          />
          <ImageIntro
            url={
              "https://www.viraluniversal.com/wp-content/uploads/2018/12/rent.jpg"
            }
          >
            <IntroStyled className={"intro"}>
              <SwitchColor>
                <h1 className={"center text-600"}>Parker</h1>
              </SwitchColor>
              <SwitchColor>
                <h2 className={"title-500 center"}>
                  Echtzeit <br /> Parkmanagment
                </h2>
              </SwitchColor>
              <P center={true}>Wir bringen die Zukunft ins heute!</P>

              <div className={"center"}>
                <Link to={""} classNames={["center"]}>
                  Sehen Sie es selbst
                </Link>
              </div>
            </IntroStyled>
          </ImageIntro>

          <section>
            <div>
              <UlStyled>
                <LiStyled>
                  <h3 className={"h3 list-item__title"}>Parken</h3>
                  <div>
                    <P>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Accusamus aperiam aut cupiditate deserunt doloremque
                      eligendi esse illum, ipsa laborum magnam necessitatibus
                      optio quaerat recusandae repudiandae rerum saepe suscipit
                      vel veniam!
                    </P>
                    <P>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Assumenda corporis, iusto nisi officia officiis quod
                      reiciendis sequi unde. Adipisci blanditiis dolor ipsam
                      mollitia nisi omnis optio sed temporibus ullam veniam?
                    </P>
                  </div>
                </LiStyled>
                <LiStyled>
                  <h3 className={"h3 list-item__title"}>Parken</h3>
                  <div>
                    <P>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Accusamus aperiam aut cupiditate deserunt doloremque
                      eligendi esse illum, ipsa laborum magnam necessitatibus
                      optio quaerat recusandae repudiandae rerum saepe suscipit
                      vel veniam!
                    </P>
                    <P>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Assumenda corporis, iusto nisi officia officiis quod
                      reiciendis sequi unde. Adipisci blanditiis dolor ipsam
                      mollitia nisi omnis optio sed temporibus ullam veniam?
                    </P>
                  </div>
                </LiStyled>
              </UlStyled>
            </div>
          </section>
          <Footer />
        </div>
      </PageFadeinAnimation>
    );
  }
}

const props = state => ({});

const actions = dispatch => ({});

const AboutPage = withRouter(
  connect(
    props,
    actions
  )(AboutPageComponent_)
);
export default AboutPage;
