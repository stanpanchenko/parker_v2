import React from 'react';
import './h3-component.style.scss';


const H3 = (props) => {
  return (
    <h3 className={['h3-component', props.classNames ? props.classNames.join(' ') : ''].join(' ')}
        style={{...props.style}}>
      
      {props.children}
      
    </h3>
  );
};

export default H3;