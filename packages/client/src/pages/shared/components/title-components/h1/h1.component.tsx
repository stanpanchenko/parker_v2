import React from 'react';
import './h1-component.style.scss';


const H1 = (props) => {
  return (
    <h1 className={'h1-component'}>
      {props.children}
    </h1>
  );
};

export default H1;