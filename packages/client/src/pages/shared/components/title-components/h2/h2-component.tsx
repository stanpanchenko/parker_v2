import React from 'react';
import './h2-component.style.scss';


const H2 = (props) => {
  return (
    <h2 className={'h2-component'}>
      {props.children}
    </h2>
  );
};

export default H2;