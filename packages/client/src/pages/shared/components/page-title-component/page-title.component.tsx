import React from "react";
import "./page-title-component.style.scss";

import H2 from "../title-components/h2/h2-component";
import H3 from "../title-components/h3/h3-component";

interface PropsI {
  title: string;
  titleTop: string;
  classNames?: string[];
}

const PageTitleComponent = (props: PropsI) => {
  return (
    <div
      className={[
        "page-title-component",
        props.classNames ? props.classNames.join(" ") : ""
      ].join(" ")}
    >
      <H2>{props.titleTop}</H2>
      <H3
        style={{
          maxWidth: "500px",
          margin: "auto"
        }}
      >
        {props.title}
      </H3>
    </div>
  );
};

export default PageTitleComponent;
