import React from "react";

interface PropsI {
  text: string;
}

const TitleTopComponent = (props: PropsI) => {
  return <h2>{props.text}</h2>;
};

export default TitleTopComponent;
