import React from "react";
import "./layout-component.style.scss";

interface PropsI {
  children: any;
}

const LayoutComponent = (props: PropsI) => {
  return <div className={"layout-component"}>{props.children}</div>;
};

export default LayoutComponent;
