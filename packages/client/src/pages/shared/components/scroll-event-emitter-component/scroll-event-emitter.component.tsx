import React, { SyntheticEvent } from "react";

interface PropsI {
  onScroll: (event: SyntheticEvent<HTMLDivElement>) => any;
  children: any;
  style: any;
}

const ScrollEventEmitter = (props: PropsI) => {
  return (
    <div
      className={"scroll-event-emitter-component"}
      onScroll={e => props.onScroll(e)}
      style={{
        overflowY: "scroll",
        marginRight: "-40px",
        paddingRight: "20px",
        ...props.style
      }}
    >
      {props.children}
    </div>
  );
};

export default ScrollEventEmitter;
