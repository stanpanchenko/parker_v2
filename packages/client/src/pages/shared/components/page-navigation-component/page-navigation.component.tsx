import React from "react";
import "./page-navigation-component.style.scss";
import { Link } from "react-router-dom";
import * as uuid from "uuid";

interface PropsI {
  navigation: { url: string; text: string }[];
}

const PageNavigation = (props: PropsI) => {
  return (
    <div className={"page-navigation"}>
      {props.navigation.map(({ url, text }, idx) => {
        return (
          <span
            key={`page-nav-link-${uuid()}`}
            style={{
              marginLeft: "2vw",
              marginRight: "2vw"
            }}
          >
            <Link to={url}>{text}</Link>
          </span>
        );
      })}
    </div>
  );
};

export default PageNavigation;
