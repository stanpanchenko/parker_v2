import React from "react";

interface PropsI {
  top: string;
  children: any;
}

const StickyElement = (props: PropsI) => {
  return (
    <div
      className={"sticky-element"}
      style={{
        position: "sticky",
        top: props.top
      }}
    >
      {props.children}
    </div>
  );
};

export default StickyElement;
