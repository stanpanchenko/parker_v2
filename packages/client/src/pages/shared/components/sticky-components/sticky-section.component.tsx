import React from "react";

interface PropsI {
  height: string;
  stickyAt: string;
  children: any;
}

const StickySection = (props: PropsI) => {
  return (
    <section>
      <div className={"sticky-container"} style={{ height: props.height }}>
        <div
          className={"sticky-element"}
          style={{
            position: "sticky",
            top: props.stickyAt
          }}
        >
          {props.children}
        </div>
      </div>
    </section>
  );
};

export default StickySection;
