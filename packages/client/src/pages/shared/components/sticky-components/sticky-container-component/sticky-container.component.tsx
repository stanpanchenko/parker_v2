import React from "react";
import "./sticky-container-component.style.scss";

interface PropsI {
  height: string;
  children: any;
}

const StickyContainer = (props: PropsI) => {
  return (
    <div className={"sticky-container"} style={{ height: props.height }}>
      {props.children}
    </div>
  );
};

export default StickyContainer;
