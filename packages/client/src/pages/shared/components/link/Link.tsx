import React from "react";
import "./link-component.style.scss";
import { GoogleIconComponent } from "../../../../shared/components/google-icon-component";

interface PropsI {
  to: string;
  children: any;
  classNames?: string[];
}

const Link = (props: PropsI) => {
  return (
    <span className={"link-wrapper"} style={{ display: "inline-block" }}>
      <a
        className={["link-normal", ...props.classNames].join(" ")}
        href={props.to}
      >
        {props.children}
      </a>
      <span className={"link-icon"}>
        <GoogleIconComponent
          classNames={["accent-color"]}
          name={"keyboard_arrow_right"}
        />
      </span>
    </span>
  );
};

export default Link;
