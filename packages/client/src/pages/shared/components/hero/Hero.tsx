import React from "react";
import styled from "styled-components";

const HeroStyled = styled("div")({
  height: "400px"
});

interface PropsI {}
const Hero = (props: PropsI) => {
  return <HeroStyled>hero</HeroStyled>;
};

export default Hero;
