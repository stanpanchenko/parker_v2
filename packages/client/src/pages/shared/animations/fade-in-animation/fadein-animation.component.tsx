import React from "react";
import { CSSTransition } from "react-transition-group";
import "./fadein-animation-component.style.scss";

interface PropsI {
  children: any;
  in_: boolean;
}

const FadeoutAnimation = (props: PropsI) => {
  return (
    <CSSTransition
      classNames={"fadein-animation"}
      timeout={1}
      in={true}
      appear={true}
    >
      <div className={"fadein"}>{props.children}</div>
    </CSSTransition>
  );
};

export default FadeoutAnimation;
