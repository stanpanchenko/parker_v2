import React from "react";
import { CSSTransition } from "react-transition-group";
import "./fadeout-animation-component.style.scss";

interface PropsI {
  children: any;
  in_: boolean;
}

const FadeoutAnimation = (props: PropsI) => {
  return (
    <CSSTransition classNames={"fadeout-animation"} timeout={1} in={props.in_}>
      <div className={"fadeout"}>{props.children}</div>
    </CSSTransition>
  );
};

export default FadeoutAnimation;
