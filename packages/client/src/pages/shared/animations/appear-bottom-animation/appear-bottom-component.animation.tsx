import React from "react";
import { CSSTransition } from "react-transition-group";
import "./appear-component-animation.style.scss";

interface PropsI {
  timeout?: number;
  children: any;
}

const AppearBottomAnimation = (props: PropsI) => {
  return (
    <CSSTransition
      classNames={"appear-bottom-component-animation"}
      timeout={props.timeout ? props.timeout : 5}
      in={true}
      appear={true}
    >
      <div className={"appear-bottom-component"}>{props.children}</div>
    </CSSTransition>
  );
};

export default AppearBottomAnimation;
