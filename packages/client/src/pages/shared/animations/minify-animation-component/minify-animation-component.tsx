import React from "react";
import "./minify-animation-component.style.scss";
import { CSSTransition } from "react-transition-group";

interface PropsI {
  children: any;
  in_: boolean;
}

const MinifyAnimationComponent = (props: PropsI) => {
  return (
    <CSSTransition
      classNames={"minify-animation-component-animation"}
      in={props.in_}
      unmountOnExit={true}
      timeout={10}
    >
      <div className={"minify-animation-component"}>{props.children}</div>
    </CSSTransition>
  );
};

export default MinifyAnimationComponent;
