import React from "react";
import { ApolloProvider } from "react-apollo";
import { Apollo } from "../apollo-client";

export default ({ children }) => {
  return <ApolloProvider client={Apollo}>{children}</ApolloProvider>;
};
