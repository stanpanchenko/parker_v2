import {
  Address,
  AddressOrderByInput,
  AddressUpdateInput,
  AddressWhereInput,
  AddressWhereUniqueInput,
  IMutation,
  IQuery,
  LoginInput,
  SignUpInput,
  User,
  UserCreateInput,
  UserWhereInput,
  UserWhereUniqueInput,
  SearchResult,
  LoginOutput,
  PricePeriod,
  PricePeriodWhereInput,
  PricePeriodOrderByInput,
  Tag,
  TagWhereUniqueInput,
  TagWhereInput,
  TagOrderByInput,
  Feature,
  FeatureInput,
  TagKey,
  TagKeyWhereUniqueInput,
  TagKeyWhereInput,
  TagKeyOrderByInput,
  TagValueWhereInput,
  TagValueOrderByInput,
  TagValue,
  TagValueWhereUniqueInput,
  FeatureWhereUniqueInput,
  FeatureWhereInput,
  GeometryWhereInput,
  Geometry,
  GeometryWhereUniqueInput,
  TagInput
} from "./graphql-api.interface";
import { Queries, Mutations } from "./queries";
import { QueryOptions, MutationOptions } from "apollo-client";

export class GraphQLApi implements IQuery, IMutation {
  private readonly _client;
  constructor(client) {
    this._client = client;
  }

  tag(where: TagWhereUniqueInput): Promise</*Tag*/ any> {
    return;
  }

  createTag(data: TagInput): Promise<Tag> {
    return Promise.resolve({} as Tag);
  }
  createTags(data: TagInput[]): Promise<Tag[]> {
    return Promise.resolve([]);
  }

  tags(
    where?: TagWhereInput,
    orderBy?: TagOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number
  ): Promise</*Tag[]*/ any> {
    return;
  }

  async createFeature(data: FeatureInput): Promise<Feature> {
    const option: MutationOptions = {
      mutation: Mutations.createFeature,
      variables: {
        data
      }
    };
    return this._client.mutate(option);
  }

  async deleteFeature(where: FeatureWhereUniqueInput): Promise<Feature> {
    const options: MutationOptions = {
      mutation: Mutations.deleteFeature,
      variables: {
        where
      }
    };
    return this._client
      .mutate(options)
      .then(({ data: { deleteFeature } }) => deleteFeature);
  }

  async deleteFeatures(where: FeatureWhereUniqueInput[]): Promise<Feature[]> {
    const options: MutationOptions = {
      mutation: Mutations.deleteFeatures,
      variables: {
        where
      }
    };
    return this._client
      .mutate(options)
      .then(({ data: { deleteFeatures } }) => deleteFeatures);
  }

  async createFeatures(data: FeatureInput[]): Promise<Feature[]> {
    const options: MutationOptions = {
      mutation: Mutations.createFeatures,
      variables: {
        data
      }
    };

    return this._client
      .mutate(options)
      .then(({ data: { createFeatures } }) => createFeatures);
  }

  tagKey(where: TagKeyWhereUniqueInput): Promise</*TagKey*/ any> {
    return Promise.resolve({});
  }

  tagKeys(
    where?: TagKeyWhereInput,
    orderBy?: TagKeyOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number
  ): Promise<TagKey[]> {
    return Promise.resolve([]);
  }

  tagValues(
    where?: TagValueWhereInput,
    orderBy?: TagValueOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number
  ): Promise<TagValue[]> {
    return Promise.resolve([]);
  }
  tagValue(where: TagValueWhereUniqueInput): Promise</*TagValue*/ any> {
    return Promise.resolve({});
  }
  feature(where: FeatureWhereUniqueInput): Promise</*Feature*/ any> {
    return Promise.resolve({});
  }

  /**
   * @desc Returns GeoJSON Features array
   * @param {FeatureWhereInput} where
   */
  features(where: FeatureWhereInput): Promise<Feature[]> {
    const options: QueryOptions = {
      query: Queries.features,
      variables: {
        where
      }
    };
    return this._client.query(options);
  }

  geometries(where: GeometryWhereInput): Promise<Geometry[]> {
    return Promise.resolve([]);
  }
  geometry(where: GeometryWhereUniqueInput): Promise</*Geometry*/ any> {
    return Promise.resolve({});
  }

  async search(query: string): Promise<[SearchResult]> {
    const option: QueryOptions = {
      query: Queries.search,
      variables: { query }
    };
    return this._client.query(option).then(({ data: { search } }) => search);
  }

  async user(where: UserWhereUniqueInput): Promise<User> {
    const options: QueryOptions = {
      query: Queries.user,
      variables: {
        where
      }
    };

    return await this._client.query(options);
  }
  async users(where: UserWhereInput): Promise<User[]> {
    const options: QueryOptions = {
      query: Queries.users,
      variables: {
        where
      }
    };

    return await this._client.query(options);
  }

  async address(where: AddressWhereUniqueInput): Promise<Address> {
    return Promise.resolve({} as Address);
  }

  addresses(
    where?: AddressWhereInput,
    orderBy?: AddressOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number
  ): Promise<Address[]> {
    return new Promise<Address[]>((res, rej) => {});
  }

  public async login(loginInput: LoginInput): Promise<LoginOutput> {
    const options: MutationOptions = {
      mutation: Mutations.login,
      variables: {
        loginInput
      }
    };
    return this._client.mutate(options).then(({ data: { login } }) => login);
  }

  public async signup(signUpInput: SignUpInput): Promise<User> {
    const options: MutationOptions = {
      mutation: Mutations.signUp,
      variables: {
        signUpInput
      }
    };

    return this._client.mutate(options).then(({ data }) => data);
  }

  public async createUser(data: UserCreateInput): Promise<User> {
    const options: MutationOptions = {
      mutation: Mutations.createUser,
      variables: {
        data
      }
    };
    return this._client.mutate(options).then(({ data }) => data);
  }

  public async validateToken(token: string) {
    const options: MutationOptions = {
      mutation: Mutations.validateToken,
      variables: {
        tokenInput: token
      }
    };

    return this._client.mutate(options).then(({ data }) => data);
  }

  public async loginWithToken(tokenInput?: string): Promise<LoginOutput> {
    const options: MutationOptions = {
      mutation: Mutations.loginWithToken,
      variables: {
        tokenInput: tokenInput
      }
    };

    return this._client
      .mutate(options)
      .then(({ data }) => ({ ...data.loginOutput }));
  }

  public async updateAddress(
    data: AddressUpdateInput,
    where: AddressWhereUniqueInput
  ): Promise<Address> {
    const options: MutationOptions = {
      mutation: Mutations.updateAddress,
      variables: {
        where,
        data
      }
    };
    return this._client
      .mutate(options)
      .then(({ data: { updateAddress } }) => updateAddress);
  }

  async pricePeriods(
    where?: PricePeriodWhereInput,
    orderBy?: PricePeriodOrderByInput,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number
  ): Promise<PricePeriod[]> {
    const options: QueryOptions = {
      query: Queries.pricePeriods,
      variables: {
        where: where,
        orderedBy: orderBy,
        skip: skip,
        after: after,
        before: before,
        first: first,
        last: last
      }
    };
    return this._client
      .query(options)
      .then(({ data: { pricePeriods } }) => pricePeriods);
  }
}
