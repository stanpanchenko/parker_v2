import { Apollo } from "./apollo-client";
import {
  Address,
  Feature,
  FeatureInput,
  FeatureType,
  FeatureWhereInput,
  GeometryType,
  LoginInput,
  LoginOutput,
  Roles,
  SearchResult,
  SignUpInput,
  User,
  UserCreateInput
} from "./graphql-api.interface";
import { MapBoundsRectI } from "../../shared/Interfaces/MapBoundsRect.interface";
import { GraphQLApi } from "./graphql-api";

export default class ParkerApi {
  private graphqlApi;

  constructor() {
    this.graphqlApi = new GraphQLApi(Apollo);
  }

  async createFeature(data: FeatureInput) {
    return this.graphqlApi.createFeature(data);
  }

  async createFeatures(features: FeatureInput[]): Promise<Feature[]> {
    return this.graphqlApi.createFeatures(features);
  }

  async deleteFeatures(ids: string[]) {
    const where = [];
    ids.forEach(id => {
      where.push({ id });
    });

    return this.graphqlApi.deleteFeatures(where);
  }

  async deleteFeature(id: string) {
    return this.graphqlApi.deleteFeature({ id });
  }

  async createPointFeature(coordinates: any[], properties: any) {
    const data: FeatureInput = {
      type: FeatureType.Feature,
      geometry: {
        type: GeometryType.Point,
        coordinates
      },
      properties
    };
    return this.graphqlApi.createFeature(data);
  }

  async createLineStringFeature(coordinates: any[], properties: any) {
    const data: FeatureInput = {
      type: FeatureType.Feature,
      geometry: {
        type: GeometryType.LineString,
        coordinates
      },
      properties
    };
    return this.graphqlApi.createFeature(data);
  }

  async createPolygonFeature(coordinates: any[], properties: any) {
    const data: FeatureInput = {
      type: FeatureType.Feature,
      geometry: {
        type: GeometryType.Polygon,
        coordinates
      },
      properties
    };
    return this.graphqlApi.createFeature(data);
  }

  async featuresByGeometryType(type: GeometryType | GeometryType[]) {
    let where: FeatureWhereInput = {};
    if (typeof type === "string") {
      where = {
        geometry: {
          type
        }
      };
    } else {
      // is array of types
      if (type.length === 1) {
        where = {
          geometry: {
            type: type[0]
          }
        };
      } else {
        where = {
          ...where,
          OR: []
        };

        type.forEach(t => {
          where.OR.push({
            geometry: {
              type: t
            }
          });
        });
      }
    }

    // pass "all"
    return this.graphqlApi.features(where);
  }

  /**
   * @param mapBoundsRect
   * @desc Returns ParkingSpaces inside of given MapBoundsRectI. Only NorthEast and SouthWest are respected.
   */
  async getParkingSpacesInMapBoundsRect(mapBoundsRect: MapBoundsRectI) {
    const { southWest } = mapBoundsRect;
    const { northEast } = mapBoundsRect;

    const where: any = {
      geometry: {
        coordinates_some: {
          // parking spaces in a rect on a map
          AND: {
            //  south west (bottom left on the map)
            lat_gte: southWest.lat,
            lng_gte: southWest.lng,
            // north east (top right on the map)
            lat_lte: northEast.lat,
            lng_lte: northEast.lng
          }
        }
      }
    };
    return this.graphqlApi
      .parkingSpaces(where)
      .then(parkingSpaces => parkingSpaces);
  }

  async login(loginInput: LoginInput): Promise<LoginOutput> {
    return this.graphqlApi.login(loginInput);
  }

  async signup(signUpInput: SignUpInput): Promise<User> {
    return this.graphqlApi.signup(signUpInput);
  }

  async createUser(user: SignUpInput) {
    const userData: UserCreateInput = {
      email: user.email,
      isActive: true,
      password: user.password,
      name: user.name,
      roles: {
        set: [Roles.USER]
      }
    };
    return this.graphqlApi.createUser(userData);
  }
  async validateToken(token: string) {
    return this.graphqlApi.validateToken(token);
  }

  async loginWithToken(token: string): Promise<LoginOutput> {
    return this.graphqlApi.loginWithToken(token);
  }

  async updateAddress(address: Address) {
    const { id, ...rest } = address;
    const data: any = {
      ...rest
    };
    const where = {
      id: address.id
    };

    return this.graphqlApi.updateAddress(data, where);
  }

  async search(queryString: string): Promise<SearchResult[]> {
    return this.graphqlApi.search(queryString).then(res => res);
  }
}
