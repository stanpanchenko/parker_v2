import { gql } from "apollo-boost";

const updateAddress = gql`
  mutation($data: AddressUpdateInput!, $where: AddressWhereUniqueInput!) {
    updateAddress(where: $where, data: $data) {
      id
      city
      country
      postcode
      house_number
      parkingSpace {
        id
      }
    }
  }
`;

const user = gql`
  query User($where: UserWhereUniqueInput!) {
    user(where: $where) {
      roles
      name
      email
      id
      isActive
      registrationToken
      password
    }
  }
`;
const users = gql`
  query User($where: UserWhereUniqueInput!) {
    user(where: $where) {
      roles
      name
      email
      id
      isActive
      registrationToken
      password
    }
  }
`;

const login = gql`
  mutation($loginInput: LoginInput!) {
    login(loginInput: $loginInput) {
      expiresIn
      token
      roles
      user {
        email
        id
      }
    }
  }
`;

const signUp = gql`
  mutation($signUpInput: SignUpInput!) {
    signup(signUpInput: $signUpInput) {
      id
    }
  }
`;

const createUser = gql`
  mutation($data: UserCreateInput!) {
    createUser(data: $data) {
      id
    }
  }
`;

const loginWithToken = gql`
  mutation($tokenInput: String!) {
    loginOutput: loginWithToken(tokenInput: $tokenInput) {
      expiresIn
      token
      roles
      user {
        id
        email
      }
    }
  }
`;

const validateToken = gql`
  mutation($tokenInput: String!) {
    valid: validateToken(tokenInput: $tokenInput)
  }
`;

const search = gql`
  query($query: String!) {
    search(query: $query) {
      class
      boundingbox
      lat
      lon
      display_name
      address {
        road
        house_number
        city
        county
        postcode
        country
        town
        state_district
      }
    }
  }
`;

const pricePeriods = gql`
  query PricePeriod(
    $where: PricePeriodWhereInput!
    $orderBy: PricePeriodOrderByInput
    $skip: Int
    $after: String
    $before: String
    $first: Int
    $last: Int
  ) {
    pricePeriods(
      where: $where
      orderBy: $orderBy
      skip: $skip
      after: $after
      before: $before
      first: $first
      last: $last
    ) {
      id
      endDate
      startDate
      price
      priceUnit
      repeat
      startTime
      endTime
      space {
        id
      }
    }
  }
`;

const features = gql`
  query($where: FeatureWhereInput!) {
    features(where: $where) {
      id
      type
      geometry {
        id
        type
        coordinates
      }
      properties {
        id
        tags {
          key
          value
        }
      }
    }
  }
`;

const createFeature = gql`
  mutation($data: FeatureInput!) {
    createFeature(data: $data) {
      id
      type
      geometry {
        type
        coordinates
      }
      properties {
        tags {
          key
          value
        }
      }
    }
  }
`;

const createFeatures = gql`
  mutation($data: [FeatureInput!]!) {
    createFeatures(data: $data) {
      id
      type
      geometry {
        type
        coordinates
      }
      properties {
        tags {
          key
          value
        }
      }
    }
  }
`;

const deleteFeature = gql`
  mutation($where: FeatureWhereUniqueInput!) {
    deleteFeature(where: $where) {
      id
    }
  }
`;

const deleteFeatures = gql`
  mutation($where: [FeatureWhereUniqueInput!]!) {
    deleteFeatures(where: $where) {
      id
    }
  }
`;

export const Queries = {
  search,
  user,
  users,
  pricePeriods,
  features
};

export const Mutations = {
  login,
  signUp,
  validateToken,
  createUser,
  loginWithToken,
  updateAddress,
  createFeature,
  createFeatures,
  deleteFeature,
  deleteFeatures
};
