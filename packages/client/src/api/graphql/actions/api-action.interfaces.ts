import { ApiActionTypes } from "./api.action-types";

export interface ApiFetchSpacesActionI {
  type: ApiActionTypes.fetchParkingSpace;
}
export interface ApiFetchParkingSpaceAddressActionI {
  type: ApiActionTypes.fetchParkingSpaceAddress;
  payload: {
    id: string;
  };
}
