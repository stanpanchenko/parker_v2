export enum ApiActionTypes {
  fetchParkingSpace = "[API_ACTIONS]:FETCH_PARKING_SPACES",
  fetchParkingSpaceAddress = '[API_ACTIONS]:FETCH_PARKINGSPACE_ADDRESS',
  saveNewParkingSpace = '[API_ACTIONS]:SAVE_NEW_PARKINGSPACE'
}
