import { ApiActionTypes } from "./api.action-types";
import {
  ApiFetchParkingSpaceAddressActionI,
  ApiFetchSpacesActionI
} from "./api-action.interfaces";

export function fetchParkingSpaces(): ApiFetchSpacesActionI {
  return {
    type: ApiActionTypes.fetchParkingSpace
  };
}

export function fetchParkingSpacesAddress(
  id: string
): ApiFetchParkingSpaceAddressActionI {
  return {
    type: ApiActionTypes.fetchParkingSpaceAddress,
    payload: {
      id
    }
  };
}
