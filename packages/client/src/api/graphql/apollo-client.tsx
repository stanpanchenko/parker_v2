import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import { onError } from "apollo-link-error";
import env from "../../env";
import { setContext } from "apollo-link-context";
import { LocalStorageKeys } from "../../shared/constants/localstorage-keys";
import { ApolloLink } from "apollo-link";

const GRAPHQL_END_POINT = env.API_GRAPHQL_ENDPOINT;

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem(LocalStorageKeys.tokenKey);

  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  };
});

export const Apollo = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false
  }),
  connectToDevTools: true,
  link: authLink.concat(
    ApolloLink.from([
      onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
          console.error(graphQLErrors);
        }
        if (networkError) {
          console.error(networkError);
          console.error(`[Network error]: ${networkError}`);
        }
      }),
      new HttpLink({
        uri: `${env.API_ADDRESS}:${env.API_PORT}${GRAPHQL_END_POINT}`
      })
    ])
  )
});
