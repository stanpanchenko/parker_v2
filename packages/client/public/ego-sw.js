// importScripts('https://unpkg.com/workbox-sw@0.0.2/build/importScripts/workbox-sw.dev.v0.0.2.js');
// importScripts('https://unpkg.com/workbox-runtime-caching@1.3.0/build/importScripts/workbox-runtime-caching.prod.v1.3.0.js');
// importScripts('https://unpkg.com/workbox-routing@1.3.0/build/importScripts/workbox-routing.prod.v1.3.0.js');

// precaches static files in dev mode
// const assetRoute = new workbox.routing.RegExpRoute({
//     regExp: new RegExp('^http://localhost:3000/static/*'),
//     handler: new workbox.runtimeCaching.CacheFirst()
// });
//
// const router = new workbox.routing.Router();
// // router.addFetchListener();
// router.registerRoutes({routes: [assetRoute]});

// adds downloaded request data to cache dynamically
// router.setDefaultHandler({
//     handler: new workbox.runtimeCaching.CacheFirst()
// });


self.addEventListener('install', function (event) {
   console.log('SW installed: ', event);
   event.waitUntil(
       caches.open('workbox-runtime-caching-http://localhost:3000/')
           .then(cache => {
               cache.add('/favicon.ico');
           }).catch(err => console.error(err))
   )
});

self.addEventListener('activate', function (event) {
    console.log('SW activate: ', event);
    return self.clients.claim();
});

self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request) // if a requested url(path) is available in cache, loads it from cache
            .then(res => {
                if(res) {
                    console.log('found stuff in cache: ', res);
                    return res;
                } else{
                    console.log('calls to internet!');
                    if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') return;
                    return fetch(event.request);
                }
            }).catch(err => console.error(err))
    );
});