#Parker

##Depandancies
Docker
NPM (Node)

##Stack Description
The Database (Mongo) is running in the Docker 
and maps its files to ``/mongo/data`` folder


## Running
0. start docker
1. cd to ``/api`` and run ``npm run webpack``
2. run ``npm run start:db``
3. run ``npm run server``
4. cd ``/client`` run ``npm run start``

